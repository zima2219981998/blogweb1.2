<?php

/* FOSUserBundle:Security:login_content.html.twig */
class __TwigTemplate_9f32286c85f863ed10fc1cddbb63386fa9dd66b70791621a53e2b4320ba7289c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        echo "
<div class=\"row\" style=\"text-decoration: none; text-align: center;  margin-top: 120px;\">

    <div class=\"col-md-8 col-sm-6 col-xs-6\">
        <center>
            <div class=\"jumbotron\">
                <img src=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("logo.png"), "html", null, true);
        echo "\" class=\"img img-responsive\">
                <h2>Create your own blog</h2>
                <a class=\"btn btn-primary btn-lg\" href=\"";
        // line 10
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_registration_register");
        echo "\" role=\"button\">Sign up</a>
            </div>
        </center>
    </div>

    <div class=\"col-md-4 col-sm-6 col-xs-6\">
        <div class=\"jumbotron\">
            <h3>Sign In</h3>
            <form class=\"form-group\" action=\"";
        // line 18
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_security_check");
        echo "\" method=\"post\">
                ";
        // line 19
        if (($context["csrf_token"] ?? null)) {
            // line 20
            echo "                    <input type=\"hidden\" name=\"_csrf_token\" value=\"";
            echo twig_escape_filter($this->env, ($context["csrf_token"] ?? null), "html", null, true);
            echo "\" />
                ";
        }
        // line 22
        echo "
                <input class=\"form-control\" type=\"text\" id=\"username\" name=\"_username\" placeholder=\"Email\" value=\"";
        // line 23
        echo twig_escape_filter($this->env, ($context["last_username"] ?? null), "html", null, true);
        echo "\" required=\"required\" />

                <input class=\"form-control\" type=\"password\" id=\"password\" name=\"_password\" placeholder=\"Password\" required=\"required\" />

                <input type=\"checkbox\" id=\"remember_me\" name=\"_remember_me\" value=\"on\" />
                <label class=\"control-label\" for=\"remember_me\">Remember me</label>

                <input class=\"btn btn-default\" type=\"submit\" id=\"_submit\" name=\"_submit\" value=\"Login\" />
            </form>
        </div>
    </div>
</div>

";
        // line 36
        if (($context["error"] ?? null)) {
            // line 37
            echo "    <div class=\"alert alert-warning\" role=\"alert\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans($this->getAttribute(($context["error"] ?? null), "messageKey", array()), $this->getAttribute(($context["error"] ?? null), "messageData", array()), "security"), "html", null, true);
            echo "</div>
";
        }
        // line 39
        echo "
<div class=\"container\" style=\"position:fixed; bottom:0;\">
    <hr>
    <p class=\"text-center\">BlogWEB © 2018</p>
</div>";
    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Security:login_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  82 => 39,  76 => 37,  74 => 36,  58 => 23,  55 => 22,  49 => 20,  47 => 19,  43 => 18,  32 => 10,  27 => 8,  19 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "FOSUserBundle:Security:login_content.html.twig", "/Users/zima/projekty/blogweb/app/Resources/FOSUserBundle/views/Security/login_content.html.twig");
    }
}
