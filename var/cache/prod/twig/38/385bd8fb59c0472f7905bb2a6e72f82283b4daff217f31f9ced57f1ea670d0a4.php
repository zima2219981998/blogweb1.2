<?php

/* ZimaBlogwebBundle:Blog:settings.html.twig */
class __TwigTemplate_55a65d6671804e08df17cb4131e3d428c91297a16d67402e48d996b42ed862e3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@ZimaBlogweb/base.html.twig", "ZimaBlogwebBundle:Blog:settings.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@ZimaBlogweb/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "BlockWEB|Settings";
    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        // line 6
        echo "    ";
        // line 7
        echo "    <div class=\"col-lg-6 col-md-6 col-xs-12\">
        <h2>Profile</h2> <hr>
        ";
        // line 9
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? null), 'form_start', array("attr" => array("novalidate" => "novalidate")));
        echo "
        <div class=\"form-group\">
            <label>Username</label> ";
        // line 11
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? null), "username", array()), 'widget');
        echo "
        </div>
        <div class=\"form-group\">
            <label>Fullname</label> ";
        // line 14
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? null), "fullname", array()), 'widget');
        echo "
        </div>
        <div class=\"form-group\">
            <label>Interests</label> ";
        // line 17
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? null), "interests", array()), 'widget');
        echo "
        </div>
        <div class=\"form-group\">
            <label>About Me</label> ";
        // line 20
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? null), "aboutme", array()), 'widget');
        echo "
        </div>
        <div class=\"form-group\">
            <label>Birthday</label> ";
        // line 23
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? null), "birthday", array()), 'widget');
        echo "
        </div>
        <div class=\"form-group\">
            ";
        // line 26
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? null), "submit", array()), 'widget', array("attr" => array("class" => "btn btn-primary")));
        echo "
        </div>
    </div>
    <div class=\"col-lg-6 col-md-6 col-xs-12\">
        <h2>Account</h2> <hr>
        <div class=\"form-group\">
            <label>Password</label> <a href=\"";
        // line 32
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_change_password", array("id" => $this->getAttribute($this->getAttribute(($context["app"] ?? null), "user", array()), "id", array()))), "html", null, true);
        echo "\"><i>Change password</i></a>
        </div>
        <div class=\"form-group\">
            <a href=\"";
        // line 35
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("blog_delete_account", array("id" => $this->getAttribute($this->getAttribute(($context["app"] ?? null), "user", array()), "id", array()))), "html", null, true);
        echo "\"><i>Delete an account</i></a>
        </div>
    </div>
    ";
        // line 38
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? null), 'form_end');
        echo " <br>




";
    }

    public function getTemplateName()
    {
        return "ZimaBlogwebBundle:Blog:settings.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  100 => 38,  94 => 35,  88 => 32,  79 => 26,  73 => 23,  67 => 20,  61 => 17,  55 => 14,  49 => 11,  44 => 9,  40 => 7,  38 => 6,  35 => 5,  29 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "ZimaBlogwebBundle:Blog:settings.html.twig", "/Users/zima/projekty/blogweb/src/Zima/BlogwebBundle/Resources/views/Blog/settings.html.twig");
    }
}
