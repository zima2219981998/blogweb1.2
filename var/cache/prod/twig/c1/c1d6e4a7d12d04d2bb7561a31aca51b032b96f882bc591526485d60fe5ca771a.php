<?php

/* ZimaBlogwebBundle:Blog:contentsUser.html.twig */
class __TwigTemplate_bab9b4ba9461b3076a0c586002e3bedc82fa455220ca255470b4680e3efd9df3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@ZimaBlogweb/base.html.twig", "ZimaBlogwebBundle:Blog:contentsUser.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@ZimaBlogweb/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        // line 4
        echo "
    <nav class=\"navbar navbar-default \">
        <div class=\"container-fluid\">
            <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-1\">
                <ul class=\"nav navbar-nav\">
                    <li class=\"active\"><a href=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("blog_edit", array("id" => $this->getAttribute(($context["post"] ?? null), "id", array()))), "html", null, true);
        echo "\">Edit</a></li>
                    <li><a href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("blog_delete", array("id" => $this->getAttribute(($context["post"] ?? null), "id", array()))), "html", null, true);
        echo "\">Delete</a></li>
                </ul>
            </div>
        </div>
    </nav>

    <div class=\"well\">
        <p style=\"font-size: 40px;\">";
        // line 17
        echo twig_escape_filter($this->env, $this->getAttribute(($context["post"] ?? null), "title", array()), "html", null, true);
        echo "</p>
        <p>wrote: <i>";
        // line 18
        echo twig_escape_filter($this->env, $this->getAttribute(($context["post"] ?? null), "owner", array()), "html", null, true);
        echo "</i> ";
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute(($context["post"] ?? null), "createdAt", array()), "Y-m-d H:i"), "html", null, true);
        echo " </p> <hr>
        ";
        // line 19
        echo $this->getAttribute(($context["post"] ?? null), "contents", array());
        echo "
    </div>

    <hr>
    <h3>Comments:</h3>

    ";
        // line 25
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["commentForm"] ?? null), 'form_start', array("attr" => array("novalidate" => "novalidate")));
        echo "
    <div class=\"form-group\">
        ";
        // line 27
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["commentForm"] ?? null), "comment", array()), 'widget', array("attr" => array("placeholder" => "Write a comment")));
        echo "
    </div>
    ";
        // line 29
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["commentForm"] ?? null), 'form_end');
        echo " <br>



    ";
        // line 34
        echo "    ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["selectcomments"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["comment"]) {
            // line 35
            echo "        <div class=\"well well-sm\">
            <p><b>";
            // line 36
            echo twig_escape_filter($this->env, $this->getAttribute($context["comment"], "owner", array()), "html", null, true);
            echo "</b> (";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["comment"], "createdAt", array()), "Y-m-d H:i"), "html", null, true);
            echo ")</p>
            <p>";
            // line 37
            echo twig_escape_filter($this->env, $this->getAttribute($context["comment"], "comment", array()), "html", null, true);
            echo "</p>
            ";
            // line 39
            echo "        </div>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['comment'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 41
        echo "

";
    }

    public function getTemplateName()
    {
        return "ZimaBlogwebBundle:Blog:contentsUser.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  113 => 41,  106 => 39,  102 => 37,  96 => 36,  93 => 35,  88 => 34,  81 => 29,  76 => 27,  71 => 25,  62 => 19,  56 => 18,  52 => 17,  42 => 10,  38 => 9,  31 => 4,  28 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "ZimaBlogwebBundle:Blog:contentsUser.html.twig", "/Users/zima/projekty/blogweb/src/Zima/BlogwebBundle/Resources/views/Blog/contentsUser.html.twig");
    }
}
