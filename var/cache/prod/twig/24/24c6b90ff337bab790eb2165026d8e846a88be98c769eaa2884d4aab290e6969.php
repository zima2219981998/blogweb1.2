<?php

/* ZimaBlogwebBundle:Blog:userBlog.html.twig */
class __TwigTemplate_852116bc58d0379244e7f18825860d0fa866c140adf1f0710456f2bbcc393414 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@ZimaBlogweb/base.html.twig", "ZimaBlogwebBundle:Blog:userBlog.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@ZimaBlogweb/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["infoAboutUser"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["info"]) {
            // line 5
            echo "        BlogWEB|";
            echo twig_escape_filter($this->env, $this->getAttribute($context["info"], "username", array()), "html", null, true);
            echo "
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['info'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
    }

    // line 9
    public function block_body($context, array $blocks = array())
    {
        // line 10
        echo "
    <div class=\"col-lg-8 col-md-8 col-xs-12\">
        ";
        // line 12
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["findContents"] ?? null));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["_key"] => $context["content"]) {
            // line 13
            echo "                <div class=\"well\">
                    <center><p style=\"font-size: 25px;\">";
            // line 14
            echo twig_escape_filter($this->env, $this->getAttribute($context["content"], "title", array()), "html", null, true);
            echo "</p></center>
                    <p>";
            // line 15
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["content"], "createdAt", array()), "Y-m-d H:i"), "html", null, true);
            echo "</p> <hr>
                    <p><b>";
            // line 16
            echo twig_escape_filter($this->env, $this->getAttribute($context["content"], "shortdescription", array()), "html", null, true);
            echo "</b></p>
                    <a href=\"";
            // line 17
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("blog_content", array("id" => $this->getAttribute($context["content"], "id", array()))), "html", null, true);
            echo "\">More…</a>
                </div>
        ";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 20
            echo "            <div class=\"col-xs-12 col-sm-7 col-md-9\">
                <div class=\"well\">
                    <center> <h3>You don't have any contents</h3> </center>
                </div>
            </div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['content'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 26
        echo "        <div class=\"navigation text-center\">
            ";
        // line 27
        echo $this->env->getExtension('Knp\Bundle\PaginatorBundle\Twig\Extension\PaginationExtension')->render($this->env, ($context["findContents"] ?? null));
        echo "
        </div>
    </div>

    <div class=\"col-lg-4 col-md-4 col-xs-12\">
        <div class=\"panel panel-default\">
            <div class=\"panel-body\">
                ";
        // line 34
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["infoAboutUser"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["info"]) {
            // line 35
            echo "                    <h3>";
            echo twig_escape_filter($this->env, $this->getAttribute($context["info"], "username", array()), "html", null, true);
            echo "</h3>
                    <p>";
            // line 36
            echo twig_escape_filter($this->env, $this->getAttribute($context["info"], "fullname", array()), "html", null, true);
            echo "</p>
                    <p>";
            // line 37
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["info"], "birthday", array()), "Y-m-d"), "html", null, true);
            echo "</p>
                    <p>";
            // line 38
            echo twig_escape_filter($this->env, $this->getAttribute($context["info"], "interests", array()), "html", null, true);
            echo "</p>
                    <p>";
            // line 39
            echo twig_escape_filter($this->env, $this->getAttribute($context["info"], "aboutme", array()), "html", null, true);
            echo "</p>
                    <a href=\"";
            // line 40
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("blog_add_friend", array("id" => $this->getAttribute($context["info"], "id", array()))), "html", null, true);
            echo "\">Follow</a>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['info'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 42
        echo "            </div>
        </div>
    </div>


";
    }

    public function getTemplateName()
    {
        return "ZimaBlogwebBundle:Blog:userBlog.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  140 => 42,  132 => 40,  128 => 39,  124 => 38,  120 => 37,  116 => 36,  111 => 35,  107 => 34,  97 => 27,  94 => 26,  83 => 20,  75 => 17,  71 => 16,  67 => 15,  63 => 14,  60 => 13,  55 => 12,  51 => 10,  48 => 9,  37 => 5,  32 => 4,  29 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "ZimaBlogwebBundle:Blog:userBlog.html.twig", "/Users/zima/projekty/blogweb/src/Zima/BlogwebBundle/Resources/views/Blog/userBlog.html.twig");
    }
}
