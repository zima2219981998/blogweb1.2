<?php

/* ZimaBlogwebBundle:Blog:contents.html.twig */
class __TwigTemplate_304625a1d716b40966440eaa6616713df50e07870bc2cdeb150af3130d7d1e8d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@ZimaBlogweb/base.html.twig", "ZimaBlogwebBundle:Blog:contents.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@ZimaBlogweb/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        // line 4
        echo "
    ";
        // line 6
        echo "        ";
        // line 7
        echo "            ";
        // line 8
        echo "                ";
        // line 9
        echo "                    ";
        // line 10
        echo "                        ";
        // line 11
        echo "                        ";
        // line 12
        echo "                    ";
        // line 13
        echo "                ";
        // line 14
        echo "            ";
        // line 15
        echo "        ";
        // line 16
        echo "    ";
        // line 17
        echo "
    <div class=\"well\">
        <p style=\"font-size: 25px;\">";
        // line 19
        echo twig_escape_filter($this->env, $this->getAttribute(($context["post"] ?? null), "title", array()), "html", null, true);
        echo "</p>
        <p>wrote: <i>";
        // line 20
        echo twig_escape_filter($this->env, $this->getAttribute(($context["post"] ?? null), "owner", array()), "html", null, true);
        echo "</i> ";
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute(($context["post"] ?? null), "createdAt", array()), "Y-m-d H:i"), "html", null, true);
        echo " </p> <hr>
        ";
        // line 21
        echo $this->getAttribute(($context["post"] ?? null), "contents", array());
        echo "
    </div>

    <hr>
    <h3>Comments:</h3>


    ";
        // line 28
        if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("ROLE_USER")) {
            // line 29
            echo "        ";
            echo             $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["commentForm"] ?? null), 'form_start', array("attr" => array("novalidate" => "novalidate")));
            echo "
        <div class=\"form-group\">
            ";
            // line 31
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["commentForm"] ?? null), "comment", array()), 'widget', array("attr" => array("placeholder" => "Write a comment")));
            echo "
        </div>
        ";
            // line 33
            echo             $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["commentForm"] ?? null), 'form_end');
            echo " <br>
    ";
        }
        // line 35
        echo "
    ";
        // line 37
        echo "    ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["selectcomments"] ?? null));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["_key"] => $context["comment"]) {
            // line 38
            echo "        <div class=\"well well-sm\">
            <p><b>";
            // line 39
            echo twig_escape_filter($this->env, $this->getAttribute($context["comment"], "owner", array()), "html", null, true);
            echo "</b> (";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["comment"], "createdAt", array()), "Y-m-d H:i"), "html", null, true);
            echo ")</p>
            <p>";
            // line 40
            echo twig_escape_filter($this->env, $this->getAttribute($context["comment"], "comment", array()), "html", null, true);
            echo "</p>
            <a class=\"btn btn-danger\" href=\"";
            // line 41
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("blog_del_comment", array("id" => $this->getAttribute($context["comment"], "id", array()))), "html", null, true);
            echo "\">X</a>
        </div>
    ";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 44
            echo "        <div class=\"well\">
            <p>Brak komentarzy</p>
        </div>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['comment'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 48
        echo "

";
    }

    public function getTemplateName()
    {
        return "ZimaBlogwebBundle:Blog:contents.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  137 => 48,  128 => 44,  120 => 41,  116 => 40,  110 => 39,  107 => 38,  101 => 37,  98 => 35,  93 => 33,  88 => 31,  82 => 29,  80 => 28,  70 => 21,  64 => 20,  60 => 19,  56 => 17,  54 => 16,  52 => 15,  50 => 14,  48 => 13,  46 => 12,  44 => 11,  42 => 10,  40 => 9,  38 => 8,  36 => 7,  34 => 6,  31 => 4,  28 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "ZimaBlogwebBundle:Blog:contents.html.twig", "/Users/zima/projekty/blogweb/src/Zima/BlogwebBundle/Resources/views/Blog/contents.html.twig");
    }
}
