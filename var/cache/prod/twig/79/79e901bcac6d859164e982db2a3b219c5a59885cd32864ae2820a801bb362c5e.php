<?php

/* ZimaBlogwebBundle:Blog:other.html.twig */
class __TwigTemplate_0e609c6176c35620df5bd5395b65596d3555653f218f3dfd41dfd6f560cd0c4e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@ZimaBlogweb/base.html.twig", "ZimaBlogwebBundle:Blog:other.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@ZimaBlogweb/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        // line 4
        echo "
    <div class=\"col-xs-12 col-sm-12 col-md-12\">
        ";
        // line 6
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["selectUndeletedPost"] ?? null));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["_key"] => $context["content"]) {
            // line 7
            echo "                <div class=\"well\">
                    <center><p style=\"font-size: 25px;\">";
            // line 8
            echo twig_escape_filter($this->env, $this->getAttribute($context["content"], "title", array()), "html", null, true);
            echo "</p></center>
                    <p>wrote: <i><a href=\"";
            // line 9
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("blog_user", array("username" => $this->getAttribute($context["content"], "owner", array()))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["content"], "owner", array()), "html", null, true);
            echo "</a></i> ";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["content"], "createdAt", array()), "Y-m-d H:i"), "html", null, true);
            echo " </p> <hr>
                    <p><b>";
            // line 10
            echo twig_escape_filter($this->env, $this->getAttribute($context["content"], "shortdescription", array()), "html", null, true);
            echo "</b></p>
                    <a href=\"";
            // line 11
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("blog_content", array("id" => $this->getAttribute($context["content"], "id", array()))), "html", null, true);
            echo "\">More…</a>
                </div>
        ";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 14
            echo "                <div class=\"well\">
                    <center> <h3>You haven't got friends yet</h3> </center>
                </div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['content'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 18
        echo "

        <div class=\"navigation text-center\">
            ";
        // line 21
        echo $this->env->getExtension('Knp\Bundle\PaginatorBundle\Twig\Extension\PaginationExtension')->render($this->env, ($context["selectUndeletedPost"] ?? null));
        echo "
        </div>
    </div>

";
    }

    public function getTemplateName()
    {
        return "ZimaBlogwebBundle:Blog:other.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  81 => 21,  76 => 18,  67 => 14,  59 => 11,  55 => 10,  47 => 9,  43 => 8,  40 => 7,  35 => 6,  31 => 4,  28 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "ZimaBlogwebBundle:Blog:other.html.twig", "/Users/zima/projekty/blogweb/src/Zima/BlogwebBundle/Resources/views/Blog/other.html.twig");
    }
}
