<?php

/* @Framework/Form/reset_widget.html.php */
class __TwigTemplate_436b7be45f198ece1208128e31e21edac9f4bb0c7e7505b981f56024d24b69ec extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_318c89711add1e0b1150823a228b6455148209f5923aa4323b3be01ba901b566 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_318c89711add1e0b1150823a228b6455148209f5923aa4323b3be01ba901b566->enter($__internal_318c89711add1e0b1150823a228b6455148209f5923aa4323b3be01ba901b566_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/reset_widget.html.php"));

        $__internal_c8457bb2667a6abec0227d6862db1feefff554d952bcf59d34f6c6b993b67575 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c8457bb2667a6abec0227d6862db1feefff554d952bcf59d34f6c6b993b67575->enter($__internal_c8457bb2667a6abec0227d6862db1feefff554d952bcf59d34f6c6b993b67575_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/reset_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'button_widget', array('type' => isset(\$type) ? \$type : 'reset')) ?>
";
        
        $__internal_318c89711add1e0b1150823a228b6455148209f5923aa4323b3be01ba901b566->leave($__internal_318c89711add1e0b1150823a228b6455148209f5923aa4323b3be01ba901b566_prof);

        
        $__internal_c8457bb2667a6abec0227d6862db1feefff554d952bcf59d34f6c6b993b67575->leave($__internal_c8457bb2667a6abec0227d6862db1feefff554d952bcf59d34f6c6b993b67575_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/reset_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'button_widget', array('type' => isset(\$type) ? \$type : 'reset')) ?>
", "@Framework/Form/reset_widget.html.php", "/Users/zima/projekty/blogweb/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/reset_widget.html.php");
    }
}
