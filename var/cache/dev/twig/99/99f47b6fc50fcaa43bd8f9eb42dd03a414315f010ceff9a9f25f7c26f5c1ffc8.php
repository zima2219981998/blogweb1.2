<?php

/* @Framework/Form/repeated_row.html.php */
class __TwigTemplate_1e0bb6bc8a29da09f8f29f28ec6c1a9007b7710791daf2b0eebe975f38e53029 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_07ee7c6a35263e58d6251d84c236742733081fd86c874bfa900834a307836b38 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_07ee7c6a35263e58d6251d84c236742733081fd86c874bfa900834a307836b38->enter($__internal_07ee7c6a35263e58d6251d84c236742733081fd86c874bfa900834a307836b38_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/repeated_row.html.php"));

        $__internal_22e41e4038889da98c210907a1e0b47a1588f4a0fdecf9c807de504e450a6bc3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_22e41e4038889da98c210907a1e0b47a1588f4a0fdecf9c807de504e450a6bc3->enter($__internal_22e41e4038889da98c210907a1e0b47a1588f4a0fdecf9c807de504e450a6bc3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/repeated_row.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_rows') ?>
";
        
        $__internal_07ee7c6a35263e58d6251d84c236742733081fd86c874bfa900834a307836b38->leave($__internal_07ee7c6a35263e58d6251d84c236742733081fd86c874bfa900834a307836b38_prof);

        
        $__internal_22e41e4038889da98c210907a1e0b47a1588f4a0fdecf9c807de504e450a6bc3->leave($__internal_22e41e4038889da98c210907a1e0b47a1588f4a0fdecf9c807de504e450a6bc3_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/repeated_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_rows') ?>
", "@Framework/Form/repeated_row.html.php", "/Users/zima/projekty/blogweb/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/repeated_row.html.php");
    }
}
