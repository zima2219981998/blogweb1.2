<?php

/* FOSUserBundle:Profile:edit.html.twig */
class __TwigTemplate_4297db37906469a38a61c9f5e828353bfcaad370170b3761723c7cee906aa9cb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Profile:edit.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_35fb64c3e1addba4e2db57687705a98652f7996f224dd93b4bae2fb17ecafe0b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_35fb64c3e1addba4e2db57687705a98652f7996f224dd93b4bae2fb17ecafe0b->enter($__internal_35fb64c3e1addba4e2db57687705a98652f7996f224dd93b4bae2fb17ecafe0b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Profile:edit.html.twig"));

        $__internal_c2f22256bfa13ab13f35c42beaf65b6144d860fe965f0979e71e28f18083fa8d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c2f22256bfa13ab13f35c42beaf65b6144d860fe965f0979e71e28f18083fa8d->enter($__internal_c2f22256bfa13ab13f35c42beaf65b6144d860fe965f0979e71e28f18083fa8d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Profile:edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_35fb64c3e1addba4e2db57687705a98652f7996f224dd93b4bae2fb17ecafe0b->leave($__internal_35fb64c3e1addba4e2db57687705a98652f7996f224dd93b4bae2fb17ecafe0b_prof);

        
        $__internal_c2f22256bfa13ab13f35c42beaf65b6144d860fe965f0979e71e28f18083fa8d->leave($__internal_c2f22256bfa13ab13f35c42beaf65b6144d860fe965f0979e71e28f18083fa8d_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_3b38927798d9072ad927644db2868a5bd602990c92c2e371727c71cf4cdabeef = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3b38927798d9072ad927644db2868a5bd602990c92c2e371727c71cf4cdabeef->enter($__internal_3b38927798d9072ad927644db2868a5bd602990c92c2e371727c71cf4cdabeef_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_fd8e37e564c8c98decefa5f9f72173768544879134cfe04481a47872e909dd25 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fd8e37e564c8c98decefa5f9f72173768544879134cfe04481a47872e909dd25->enter($__internal_fd8e37e564c8c98decefa5f9f72173768544879134cfe04481a47872e909dd25_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Profile/edit_content.html.twig", "FOSUserBundle:Profile:edit.html.twig", 4)->display($context);
        
        $__internal_fd8e37e564c8c98decefa5f9f72173768544879134cfe04481a47872e909dd25->leave($__internal_fd8e37e564c8c98decefa5f9f72173768544879134cfe04481a47872e909dd25_prof);

        
        $__internal_3b38927798d9072ad927644db2868a5bd602990c92c2e371727c71cf4cdabeef->leave($__internal_3b38927798d9072ad927644db2868a5bd602990c92c2e371727c71cf4cdabeef_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Profile:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Profile/edit_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Profile:edit.html.twig", "/Users/zima/projekty/blogweb/app/Resources/FOSUserBundle/views/Profile/edit.html.twig");
    }
}
