<?php

/* FOSUserBundle:Registration:email.txt.twig */
class __TwigTemplate_b1c42109df2707530660e93ef1ba70d6a708ed48456bf258254a4644ef92626c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'subject' => array($this, 'block_subject'),
            'body_text' => array($this, 'block_body_text'),
            'body_html' => array($this, 'block_body_html'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b47b3d61be6a414bbe36cc9a306da99905c6fd6a8490543b574ef6a860ddb67e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b47b3d61be6a414bbe36cc9a306da99905c6fd6a8490543b574ef6a860ddb67e->enter($__internal_b47b3d61be6a414bbe36cc9a306da99905c6fd6a8490543b574ef6a860ddb67e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:email.txt.twig"));

        $__internal_b71dbb619819ee30114626db24e1be67117031f5bb1ca9defb08fbbe0e92f520 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b71dbb619819ee30114626db24e1be67117031f5bb1ca9defb08fbbe0e92f520->enter($__internal_b71dbb619819ee30114626db24e1be67117031f5bb1ca9defb08fbbe0e92f520_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:email.txt.twig"));

        // line 2
        $this->displayBlock('subject', $context, $blocks);
        // line 7
        echo "
";
        // line 8
        $this->displayBlock('body_text', $context, $blocks);
        // line 13
        $this->displayBlock('body_html', $context, $blocks);
        
        $__internal_b47b3d61be6a414bbe36cc9a306da99905c6fd6a8490543b574ef6a860ddb67e->leave($__internal_b47b3d61be6a414bbe36cc9a306da99905c6fd6a8490543b574ef6a860ddb67e_prof);

        
        $__internal_b71dbb619819ee30114626db24e1be67117031f5bb1ca9defb08fbbe0e92f520->leave($__internal_b71dbb619819ee30114626db24e1be67117031f5bb1ca9defb08fbbe0e92f520_prof);

    }

    // line 2
    public function block_subject($context, array $blocks = array())
    {
        $__internal_a6a53690a4afc0cf264fd73b567608a011a515ca37ca5a0843db29bf96ad3b4a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a6a53690a4afc0cf264fd73b567608a011a515ca37ca5a0843db29bf96ad3b4a->enter($__internal_a6a53690a4afc0cf264fd73b567608a011a515ca37ca5a0843db29bf96ad3b4a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "subject"));

        $__internal_7a0b344ad6a69a127545105306003b7ffedf7bf8ed29c203cf2e995e5a2c3199 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7a0b344ad6a69a127545105306003b7ffedf7bf8ed29c203cf2e995e5a2c3199->enter($__internal_7a0b344ad6a69a127545105306003b7ffedf7bf8ed29c203cf2e995e5a2c3199_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "subject"));

        // line 4
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.email.subject", array("%username%" => $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "username", array()), "%confirmationUrl%" => ($context["confirmationUrl"] ?? $this->getContext($context, "confirmationUrl"))), "FOSUserBundle");
        
        $__internal_7a0b344ad6a69a127545105306003b7ffedf7bf8ed29c203cf2e995e5a2c3199->leave($__internal_7a0b344ad6a69a127545105306003b7ffedf7bf8ed29c203cf2e995e5a2c3199_prof);

        
        $__internal_a6a53690a4afc0cf264fd73b567608a011a515ca37ca5a0843db29bf96ad3b4a->leave($__internal_a6a53690a4afc0cf264fd73b567608a011a515ca37ca5a0843db29bf96ad3b4a_prof);

    }

    // line 8
    public function block_body_text($context, array $blocks = array())
    {
        $__internal_dfde8b90ad6a32d48fc994804779fc53dac8ba5e46fab0f3ee3e946ef865d58c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_dfde8b90ad6a32d48fc994804779fc53dac8ba5e46fab0f3ee3e946ef865d58c->enter($__internal_dfde8b90ad6a32d48fc994804779fc53dac8ba5e46fab0f3ee3e946ef865d58c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_text"));

        $__internal_f02320d224d159ef385a700fedb35900ec6126cebe741d1ba4811773a649e925 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f02320d224d159ef385a700fedb35900ec6126cebe741d1ba4811773a649e925->enter($__internal_f02320d224d159ef385a700fedb35900ec6126cebe741d1ba4811773a649e925_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_text"));

        // line 10
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.email.message", array("%username%" => $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "username", array()), "%confirmationUrl%" => ($context["confirmationUrl"] ?? $this->getContext($context, "confirmationUrl"))), "FOSUserBundle");
        echo "
";
        
        $__internal_f02320d224d159ef385a700fedb35900ec6126cebe741d1ba4811773a649e925->leave($__internal_f02320d224d159ef385a700fedb35900ec6126cebe741d1ba4811773a649e925_prof);

        
        $__internal_dfde8b90ad6a32d48fc994804779fc53dac8ba5e46fab0f3ee3e946ef865d58c->leave($__internal_dfde8b90ad6a32d48fc994804779fc53dac8ba5e46fab0f3ee3e946ef865d58c_prof);

    }

    // line 13
    public function block_body_html($context, array $blocks = array())
    {
        $__internal_b994b9a3057534562d9e129550e071817d7667ad2ac543e2f5aa7d3f10667fcd = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b994b9a3057534562d9e129550e071817d7667ad2ac543e2f5aa7d3f10667fcd->enter($__internal_b994b9a3057534562d9e129550e071817d7667ad2ac543e2f5aa7d3f10667fcd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_html"));

        $__internal_9dc12450ab4238e9dea4e737f2a0e29e2a43aec1fd0c9cdd052ddefe3ab1d2d6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9dc12450ab4238e9dea4e737f2a0e29e2a43aec1fd0c9cdd052ddefe3ab1d2d6->enter($__internal_9dc12450ab4238e9dea4e737f2a0e29e2a43aec1fd0c9cdd052ddefe3ab1d2d6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_html"));

        
        $__internal_9dc12450ab4238e9dea4e737f2a0e29e2a43aec1fd0c9cdd052ddefe3ab1d2d6->leave($__internal_9dc12450ab4238e9dea4e737f2a0e29e2a43aec1fd0c9cdd052ddefe3ab1d2d6_prof);

        
        $__internal_b994b9a3057534562d9e129550e071817d7667ad2ac543e2f5aa7d3f10667fcd->leave($__internal_b994b9a3057534562d9e129550e071817d7667ad2ac543e2f5aa7d3f10667fcd_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:email.txt.twig";
    }

    public function getDebugInfo()
    {
        return array (  85 => 13,  73 => 10,  64 => 8,  54 => 4,  45 => 2,  35 => 13,  33 => 8,  30 => 7,  28 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% trans_default_domain 'FOSUserBundle' %}
{% block subject %}
{%- autoescape false -%}
{{ 'registration.email.subject'|trans({'%username%': user.username, '%confirmationUrl%': confirmationUrl}) }}
{%- endautoescape -%}
{% endblock %}

{% block body_text %}
{% autoescape false %}
{{ 'registration.email.message'|trans({'%username%': user.username, '%confirmationUrl%': confirmationUrl}) }}
{% endautoescape %}
{% endblock %}
{% block body_html %}{% endblock %}
", "FOSUserBundle:Registration:email.txt.twig", "/Users/zima/projekty/blogweb/app/Resources/FOSUserBundle/views/Registration/email.txt.twig");
    }
}
