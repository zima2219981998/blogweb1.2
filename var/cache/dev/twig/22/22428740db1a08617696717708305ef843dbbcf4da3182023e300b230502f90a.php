<?php

/* FOSUserBundle:Resetting:request.html.twig */
class __TwigTemplate_ddf32f4b26e0bc515ef4e36a4fef30aa39f3dfa6f5efd7543b11e85f09bfd442 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Resetting:request.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8534253d5599d18fabef2ecfedb43cc2570c1ecde124945a7e1ed44c7dec3748 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8534253d5599d18fabef2ecfedb43cc2570c1ecde124945a7e1ed44c7dec3748->enter($__internal_8534253d5599d18fabef2ecfedb43cc2570c1ecde124945a7e1ed44c7dec3748_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:request.html.twig"));

        $__internal_1076ec570b97698114e619efd2565df52be847b1d8f88d50083b9a837008fbce = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1076ec570b97698114e619efd2565df52be847b1d8f88d50083b9a837008fbce->enter($__internal_1076ec570b97698114e619efd2565df52be847b1d8f88d50083b9a837008fbce_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:request.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_8534253d5599d18fabef2ecfedb43cc2570c1ecde124945a7e1ed44c7dec3748->leave($__internal_8534253d5599d18fabef2ecfedb43cc2570c1ecde124945a7e1ed44c7dec3748_prof);

        
        $__internal_1076ec570b97698114e619efd2565df52be847b1d8f88d50083b9a837008fbce->leave($__internal_1076ec570b97698114e619efd2565df52be847b1d8f88d50083b9a837008fbce_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_fb37b597312008a024a6671fc3e71783d216233216a18d51bf7e7f296524c513 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fb37b597312008a024a6671fc3e71783d216233216a18d51bf7e7f296524c513->enter($__internal_fb37b597312008a024a6671fc3e71783d216233216a18d51bf7e7f296524c513_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_f5154c5dc7e5134fc561911752582a05a0b7e22b6afe4e52f11d0f83b3b21453 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f5154c5dc7e5134fc561911752582a05a0b7e22b6afe4e52f11d0f83b3b21453->enter($__internal_f5154c5dc7e5134fc561911752582a05a0b7e22b6afe4e52f11d0f83b3b21453_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Resetting/request_content.html.twig", "FOSUserBundle:Resetting:request.html.twig", 4)->display($context);
        
        $__internal_f5154c5dc7e5134fc561911752582a05a0b7e22b6afe4e52f11d0f83b3b21453->leave($__internal_f5154c5dc7e5134fc561911752582a05a0b7e22b6afe4e52f11d0f83b3b21453_prof);

        
        $__internal_fb37b597312008a024a6671fc3e71783d216233216a18d51bf7e7f296524c513->leave($__internal_fb37b597312008a024a6671fc3e71783d216233216a18d51bf7e7f296524c513_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:request.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Resetting/request_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Resetting:request.html.twig", "/Users/zima/projekty/blogweb/app/Resources/FOSUserBundle/views/Resetting/request.html.twig");
    }
}
