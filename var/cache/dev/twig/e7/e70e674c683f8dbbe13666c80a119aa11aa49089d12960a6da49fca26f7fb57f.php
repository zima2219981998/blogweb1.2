<?php

/* @Framework/Form/url_widget.html.php */
class __TwigTemplate_71e9441851beb2c8c69f24995685af19fde9ef6f02d66d4422467faa388edf95 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ba388290f246de0a7f9c117c19d00067aa998e0642afa99e610859286deee1a3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ba388290f246de0a7f9c117c19d00067aa998e0642afa99e610859286deee1a3->enter($__internal_ba388290f246de0a7f9c117c19d00067aa998e0642afa99e610859286deee1a3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/url_widget.html.php"));

        $__internal_787e71c432888efd343d92ca8dec115efe357a8ffce4163ea757da2dfb33a9a9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_787e71c432888efd343d92ca8dec115efe357a8ffce4163ea757da2dfb33a9a9->enter($__internal_787e71c432888efd343d92ca8dec115efe357a8ffce4163ea757da2dfb33a9a9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/url_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'url')) ?>
";
        
        $__internal_ba388290f246de0a7f9c117c19d00067aa998e0642afa99e610859286deee1a3->leave($__internal_ba388290f246de0a7f9c117c19d00067aa998e0642afa99e610859286deee1a3_prof);

        
        $__internal_787e71c432888efd343d92ca8dec115efe357a8ffce4163ea757da2dfb33a9a9->leave($__internal_787e71c432888efd343d92ca8dec115efe357a8ffce4163ea757da2dfb33a9a9_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/url_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'url')) ?>
", "@Framework/Form/url_widget.html.php", "/Users/zima/projekty/blogweb/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/url_widget.html.php");
    }
}
