<?php

/* ZimaBlogwebBundle:Blog:searchContents.html.twig */
class __TwigTemplate_25e7c38b0c4be4c995a8edbf9e442030e2c17df459410e5402f3517c5e97e7cb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@ZimaBlogweb/base.html.twig", "ZimaBlogwebBundle:Blog:searchContents.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@ZimaBlogweb/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_80058b5d859da8b0742c4da244812e4d6a75a855080ecc19e3d8b619b8d0559b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_80058b5d859da8b0742c4da244812e4d6a75a855080ecc19e3d8b619b8d0559b->enter($__internal_80058b5d859da8b0742c4da244812e4d6a75a855080ecc19e3d8b619b8d0559b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "ZimaBlogwebBundle:Blog:searchContents.html.twig"));

        $__internal_8024ec9cad80a5fe41a3e45d3b9522b207ed77b369ebc5d35575676489ebadc6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8024ec9cad80a5fe41a3e45d3b9522b207ed77b369ebc5d35575676489ebadc6->enter($__internal_8024ec9cad80a5fe41a3e45d3b9522b207ed77b369ebc5d35575676489ebadc6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "ZimaBlogwebBundle:Blog:searchContents.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_80058b5d859da8b0742c4da244812e4d6a75a855080ecc19e3d8b619b8d0559b->leave($__internal_80058b5d859da8b0742c4da244812e4d6a75a855080ecc19e3d8b619b8d0559b_prof);

        
        $__internal_8024ec9cad80a5fe41a3e45d3b9522b207ed77b369ebc5d35575676489ebadc6->leave($__internal_8024ec9cad80a5fe41a3e45d3b9522b207ed77b369ebc5d35575676489ebadc6_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_5d91a3cd6659468671bdf41894d0dcf43da4c5e106d8b5ac83468d610c9ebbde = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5d91a3cd6659468671bdf41894d0dcf43da4c5e106d8b5ac83468d610c9ebbde->enter($__internal_5d91a3cd6659468671bdf41894d0dcf43da4c5e106d8b5ac83468d610c9ebbde_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_19c31176b6b2b7b2f4bb527bb71f513cb6305737d1584e926822c1d6265b6aba = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_19c31176b6b2b7b2f4bb527bb71f513cb6305737d1584e926822c1d6265b6aba->enter($__internal_19c31176b6b2b7b2f4bb527bb71f513cb6305737d1584e926822c1d6265b6aba_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "
    <div class=\"col-lg-8 col-md-8 col-xs-12\">
        ";
        // line 6
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["result"] ?? $this->getContext($context, "result")));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["_key"] => $context["content"]) {
            // line 7
            echo "                <div class=\"well\">
                    <center><p style=\"font-size: 25px;\">";
            // line 8
            echo twig_escape_filter($this->env, $this->getAttribute($context["content"], "title", array()), "html", null, true);
            echo "</p></center>
                    <p>wrote: ";
            // line 9
            echo twig_escape_filter($this->env, $this->getAttribute($context["content"], "owner", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["content"], "createdAt", array()), "Y-m-d H:i"), "html", null, true);
            echo " </p> <hr>
                    <p><b>";
            // line 10
            echo twig_escape_filter($this->env, $this->getAttribute($context["content"], "shortdescription", array()), "html", null, true);
            echo "</b></p>
                    <a href=\"";
            // line 11
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("blog_content", array("id" => $this->getAttribute($context["content"], "id", array()))), "html", null, true);
            echo "\">More…</a>
                </div>
        ";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 14
            echo "                <div class=\"well\">
                    <center> <h3>No results found</h3> </center>
                </div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['content'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 18
        echo "        <div class=\"navigation text-center\">
            ";
        // line 19
        echo $this->env->getExtension('Knp\Bundle\PaginatorBundle\Twig\Extension\PaginationExtension')->render($this->env, ($context["result"] ?? $this->getContext($context, "result")));
        echo "
        </div>
    </div>


";
        
        $__internal_19c31176b6b2b7b2f4bb527bb71f513cb6305737d1584e926822c1d6265b6aba->leave($__internal_19c31176b6b2b7b2f4bb527bb71f513cb6305737d1584e926822c1d6265b6aba_prof);

        
        $__internal_5d91a3cd6659468671bdf41894d0dcf43da4c5e106d8b5ac83468d610c9ebbde->leave($__internal_5d91a3cd6659468671bdf41894d0dcf43da4c5e106d8b5ac83468d610c9ebbde_prof);

    }

    public function getTemplateName()
    {
        return "ZimaBlogwebBundle:Blog:searchContents.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  95 => 19,  92 => 18,  83 => 14,  75 => 11,  71 => 10,  65 => 9,  61 => 8,  58 => 7,  53 => 6,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@ZimaBlogweb/base.html.twig' %}

{% block body %}

    <div class=\"col-lg-8 col-md-8 col-xs-12\">
        {% for content in result %}
                <div class=\"well\">
                    <center><p style=\"font-size: 25px;\">{{ content.title }}</p></center>
                    <p>wrote: {{ content.owner }} {{ content.createdAt|date('Y-m-d H:i') }} </p> <hr>
                    <p><b>{{ content.shortdescription }}</b></p>
                    <a href=\"{{ path('blog_content', {\"id\": content.id}) }}\">More…</a>
                </div>
        {% else %}
                <div class=\"well\">
                    <center> <h3>No results found</h3> </center>
                </div>
        {% endfor %}
        <div class=\"navigation text-center\">
            {{ knp_pagination_render(result) }}
        </div>
    </div>


{% endblock %}", "ZimaBlogwebBundle:Blog:searchContents.html.twig", "/Users/zima/projekty/blogweb/src/Zima/BlogwebBundle/Resources/views/Blog/searchContents.html.twig");
    }
}
