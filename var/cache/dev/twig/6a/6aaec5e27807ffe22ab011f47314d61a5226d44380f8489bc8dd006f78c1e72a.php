<?php

/* FOSUserBundle:Resetting:check_email.html.twig */
class __TwigTemplate_c42901647514d92223deb52dd494fbc0183fdb7cf3a2422454c4532b75feea13 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Resetting:check_email.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6d71da38dc35c2e2920203a5973dd5e4e673840df3b9f24211532a2cb01bd084 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6d71da38dc35c2e2920203a5973dd5e4e673840df3b9f24211532a2cb01bd084->enter($__internal_6d71da38dc35c2e2920203a5973dd5e4e673840df3b9f24211532a2cb01bd084_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:check_email.html.twig"));

        $__internal_3a453559b44b08ac0e52ee54b1527b99de687825c5cc5c67f188af71dbdf1b15 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3a453559b44b08ac0e52ee54b1527b99de687825c5cc5c67f188af71dbdf1b15->enter($__internal_3a453559b44b08ac0e52ee54b1527b99de687825c5cc5c67f188af71dbdf1b15_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:check_email.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_6d71da38dc35c2e2920203a5973dd5e4e673840df3b9f24211532a2cb01bd084->leave($__internal_6d71da38dc35c2e2920203a5973dd5e4e673840df3b9f24211532a2cb01bd084_prof);

        
        $__internal_3a453559b44b08ac0e52ee54b1527b99de687825c5cc5c67f188af71dbdf1b15->leave($__internal_3a453559b44b08ac0e52ee54b1527b99de687825c5cc5c67f188af71dbdf1b15_prof);

    }

    // line 5
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_ad344850dc1cdbd556c2b73249628dfaec66385271fd669b5387363d9a26aa1a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ad344850dc1cdbd556c2b73249628dfaec66385271fd669b5387363d9a26aa1a->enter($__internal_ad344850dc1cdbd556c2b73249628dfaec66385271fd669b5387363d9a26aa1a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_7a59193086fc7db949ba5bd1d49cb56cfd272ad7ee9738fc2749dac7564864a3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7a59193086fc7db949ba5bd1d49cb56cfd272ad7ee9738fc2749dac7564864a3->enter($__internal_7a59193086fc7db949ba5bd1d49cb56cfd272ad7ee9738fc2749dac7564864a3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 6
        echo "<p>
";
        // line 7
        echo nl2br(twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("resetting.check_email", array("%tokenLifetime%" => ($context["tokenLifetime"] ?? $this->getContext($context, "tokenLifetime"))), "FOSUserBundle"), "html", null, true));
        echo "
</p>
";
        
        $__internal_7a59193086fc7db949ba5bd1d49cb56cfd272ad7ee9738fc2749dac7564864a3->leave($__internal_7a59193086fc7db949ba5bd1d49cb56cfd272ad7ee9738fc2749dac7564864a3_prof);

        
        $__internal_ad344850dc1cdbd556c2b73249628dfaec66385271fd669b5387363d9a26aa1a->leave($__internal_ad344850dc1cdbd556c2b73249628dfaec66385271fd669b5387363d9a26aa1a_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:check_email.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  52 => 7,  49 => 6,  40 => 5,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% trans_default_domain 'FOSUserBundle' %}

{% block fos_user_content %}
<p>
{{ 'resetting.check_email'|trans({'%tokenLifetime%': tokenLifetime})|nl2br }}
</p>
{% endblock %}
", "FOSUserBundle:Resetting:check_email.html.twig", "/Users/zima/projekty/blogweb/app/Resources/FOSUserBundle/views/Resetting/check_email.html.twig");
    }
}
