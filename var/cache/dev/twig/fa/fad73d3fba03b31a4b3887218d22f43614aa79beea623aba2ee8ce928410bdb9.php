<?php

/* @Framework/Form/form_errors.html.php */
class __TwigTemplate_19cb81b07af5c79f8927eadfe4cfaa7c1559fb3b59df5d2a62500a4746b9b451 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_387f483a26921f218653d047b0d7eb941d2573ff3b7e3697cfc7f09be825c2a5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_387f483a26921f218653d047b0d7eb941d2573ff3b7e3697cfc7f09be825c2a5->enter($__internal_387f483a26921f218653d047b0d7eb941d2573ff3b7e3697cfc7f09be825c2a5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_errors.html.php"));

        $__internal_f430327a98cbb69f5abd2f22bd2c7f79effb3414dd539abf904219bedce46ee8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f430327a98cbb69f5abd2f22bd2c7f79effb3414dd539abf904219bedce46ee8->enter($__internal_f430327a98cbb69f5abd2f22bd2c7f79effb3414dd539abf904219bedce46ee8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_errors.html.php"));

        // line 1
        echo "<?php if (count(\$errors) > 0): ?>
    <ul>
        <?php foreach (\$errors as \$error): ?>
            <li><?php echo \$error->getMessage() ?></li>
        <?php endforeach; ?>
    </ul>
<?php endif ?>
";
        
        $__internal_387f483a26921f218653d047b0d7eb941d2573ff3b7e3697cfc7f09be825c2a5->leave($__internal_387f483a26921f218653d047b0d7eb941d2573ff3b7e3697cfc7f09be825c2a5_prof);

        
        $__internal_f430327a98cbb69f5abd2f22bd2c7f79effb3414dd539abf904219bedce46ee8->leave($__internal_f430327a98cbb69f5abd2f22bd2c7f79effb3414dd539abf904219bedce46ee8_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_errors.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (count(\$errors) > 0): ?>
    <ul>
        <?php foreach (\$errors as \$error): ?>
            <li><?php echo \$error->getMessage() ?></li>
        <?php endforeach; ?>
    </ul>
<?php endif ?>
", "@Framework/Form/form_errors.html.php", "/Users/zima/projekty/blogweb/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_errors.html.php");
    }
}
