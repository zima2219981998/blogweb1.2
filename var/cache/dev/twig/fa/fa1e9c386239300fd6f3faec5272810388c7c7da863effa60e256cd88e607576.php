<?php

/* FOSUserBundle:Group:new_content.html.twig */
class __TwigTemplate_bd74197556c6c96cdf8a192e2a482e02ae81e2d10fa6571d81fc0a250ab5c82a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_91abfd8eb063990d1ef3953dbb050f89febfed306e646939035f1011585da2be = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_91abfd8eb063990d1ef3953dbb050f89febfed306e646939035f1011585da2be->enter($__internal_91abfd8eb063990d1ef3953dbb050f89febfed306e646939035f1011585da2be_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:new_content.html.twig"));

        $__internal_7540c5d3441ea64f85a9cd09feeb7ae023b02af32f2aaa22e4df64bd1b19e36b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7540c5d3441ea64f85a9cd09feeb7ae023b02af32f2aaa22e4df64bd1b19e36b->enter($__internal_7540c5d3441ea64f85a9cd09feeb7ae023b02af32f2aaa22e4df64bd1b19e36b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:new_content.html.twig"));

        // line 2
        echo "
";
        // line 3
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_start', array("action" => $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_group_new"), "attr" => array("class" => "fos_user_group_new")));
        echo "
    ";
        // line 4
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        echo "
    <div>
        <input type=\"submit\" value=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("group.new.submit", array(), "FOSUserBundle"), "html", null, true);
        echo "\" />
    </div>
";
        // line 8
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_end');
        echo "
";
        
        $__internal_91abfd8eb063990d1ef3953dbb050f89febfed306e646939035f1011585da2be->leave($__internal_91abfd8eb063990d1ef3953dbb050f89febfed306e646939035f1011585da2be_prof);

        
        $__internal_7540c5d3441ea64f85a9cd09feeb7ae023b02af32f2aaa22e4df64bd1b19e36b->leave($__internal_7540c5d3441ea64f85a9cd09feeb7ae023b02af32f2aaa22e4df64bd1b19e36b_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:new_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  42 => 8,  37 => 6,  32 => 4,  28 => 3,  25 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% trans_default_domain 'FOSUserBundle' %}

{{ form_start(form, { 'action': path('fos_user_group_new'), 'attr': { 'class': 'fos_user_group_new' } }) }}
    {{ form_widget(form) }}
    <div>
        <input type=\"submit\" value=\"{{ 'group.new.submit'|trans }}\" />
    </div>
{{ form_end(form) }}
", "FOSUserBundle:Group:new_content.html.twig", "/Users/zima/projekty/blogweb/vendor/friendsofsymfony/user-bundle/Resources/views/Group/new_content.html.twig");
    }
}
