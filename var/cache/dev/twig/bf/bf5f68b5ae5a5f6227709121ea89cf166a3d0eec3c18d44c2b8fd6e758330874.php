<?php

/* @Framework/Form/widget_attributes.html.php */
class __TwigTemplate_9092ab781beafc84ba48eddbf45af7dceddfc1e5998f6558b685369fb069fce1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_54b528a99bc1f061b99a245f1f3f89f8c106eb4144cac962a76e5c74c976eee3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_54b528a99bc1f061b99a245f1f3f89f8c106eb4144cac962a76e5c74c976eee3->enter($__internal_54b528a99bc1f061b99a245f1f3f89f8c106eb4144cac962a76e5c74c976eee3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/widget_attributes.html.php"));

        $__internal_2a212fbb7882ef970f81dc75f5f8200446550966168dc319d3a3253e6d1f3e85 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2a212fbb7882ef970f81dc75f5f8200446550966168dc319d3a3253e6d1f3e85->enter($__internal_2a212fbb7882ef970f81dc75f5f8200446550966168dc319d3a3253e6d1f3e85_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/widget_attributes.html.php"));

        // line 1
        echo "id=\"<?php echo \$view->escape(\$id) ?>\" name=\"<?php echo \$view->escape(\$full_name) ?>\"<?php if (\$disabled): ?> disabled=\"disabled\"<?php endif ?>
<?php if (\$required): ?> required=\"required\"<?php endif ?>
<?php echo \$attr ? ' '.\$view['form']->block(\$form, 'attributes') : '' ?>
";
        
        $__internal_54b528a99bc1f061b99a245f1f3f89f8c106eb4144cac962a76e5c74c976eee3->leave($__internal_54b528a99bc1f061b99a245f1f3f89f8c106eb4144cac962a76e5c74c976eee3_prof);

        
        $__internal_2a212fbb7882ef970f81dc75f5f8200446550966168dc319d3a3253e6d1f3e85->leave($__internal_2a212fbb7882ef970f81dc75f5f8200446550966168dc319d3a3253e6d1f3e85_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/widget_attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("id=\"<?php echo \$view->escape(\$id) ?>\" name=\"<?php echo \$view->escape(\$full_name) ?>\"<?php if (\$disabled): ?> disabled=\"disabled\"<?php endif ?>
<?php if (\$required): ?> required=\"required\"<?php endif ?>
<?php echo \$attr ? ' '.\$view['form']->block(\$form, 'attributes') : '' ?>
", "@Framework/Form/widget_attributes.html.php", "/Users/zima/projekty/blogweb/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/widget_attributes.html.php");
    }
}
