<?php

/* TwigBundle:Exception:exception.atom.twig */
class __TwigTemplate_379b68ce20cab05436ec86c695c361f5eda08e59edd8427caf239fda3f89d6da extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_35851c73be8d9d1445d63c5352fb4aa2e7ab365d345cfdb6400921d8db60a248 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_35851c73be8d9d1445d63c5352fb4aa2e7ab365d345cfdb6400921d8db60a248->enter($__internal_35851c73be8d9d1445d63c5352fb4aa2e7ab365d345cfdb6400921d8db60a248_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.atom.twig"));

        $__internal_9c866f1a75b9e54645735c48649096dd29a23c67c1a0ef52378adcd8c9b01ac7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9c866f1a75b9e54645735c48649096dd29a23c67c1a0ef52378adcd8c9b01ac7->enter($__internal_9c866f1a75b9e54645735c48649096dd29a23c67c1a0ef52378adcd8c9b01ac7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.atom.twig"));

        // line 1
        echo twig_include($this->env, $context, "@Twig/Exception/exception.xml.twig", array("exception" => ($context["exception"] ?? $this->getContext($context, "exception"))));
        echo "
";
        
        $__internal_35851c73be8d9d1445d63c5352fb4aa2e7ab365d345cfdb6400921d8db60a248->leave($__internal_35851c73be8d9d1445d63c5352fb4aa2e7ab365d345cfdb6400921d8db60a248_prof);

        
        $__internal_9c866f1a75b9e54645735c48649096dd29a23c67c1a0ef52378adcd8c9b01ac7->leave($__internal_9c866f1a75b9e54645735c48649096dd29a23c67c1a0ef52378adcd8c9b01ac7_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.atom.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ include('@Twig/Exception/exception.xml.twig', { exception: exception }) }}
", "TwigBundle:Exception:exception.atom.twig", "/Users/zima/projekty/blogweb/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception.atom.twig");
    }
}
