<?php

/* TwigBundle:Exception:error.atom.twig */
class __TwigTemplate_55671db51f4d40452d4a464e2c8c493570244941b287ca1c0382c4ed80c1a19a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_466bcc9dbf742e6f99103e903192384fe0053939fae6407b534072caafe4f258 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_466bcc9dbf742e6f99103e903192384fe0053939fae6407b534072caafe4f258->enter($__internal_466bcc9dbf742e6f99103e903192384fe0053939fae6407b534072caafe4f258_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.atom.twig"));

        $__internal_839536ece427780a25474a3264328be4050e24285438b288e70dd9c97f24a568 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_839536ece427780a25474a3264328be4050e24285438b288e70dd9c97f24a568->enter($__internal_839536ece427780a25474a3264328be4050e24285438b288e70dd9c97f24a568_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.atom.twig"));

        // line 1
        echo twig_include($this->env, $context, "@Twig/Exception/error.xml.twig");
        echo "
";
        
        $__internal_466bcc9dbf742e6f99103e903192384fe0053939fae6407b534072caafe4f258->leave($__internal_466bcc9dbf742e6f99103e903192384fe0053939fae6407b534072caafe4f258_prof);

        
        $__internal_839536ece427780a25474a3264328be4050e24285438b288e70dd9c97f24a568->leave($__internal_839536ece427780a25474a3264328be4050e24285438b288e70dd9c97f24a568_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.atom.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ include('@Twig/Exception/error.xml.twig') }}
", "TwigBundle:Exception:error.atom.twig", "/Users/zima/projekty/blogweb/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/error.atom.twig");
    }
}
