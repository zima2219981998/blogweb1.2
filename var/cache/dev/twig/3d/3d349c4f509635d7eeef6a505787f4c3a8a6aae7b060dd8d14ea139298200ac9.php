<?php

/* @Framework/Form/submit_widget.html.php */
class __TwigTemplate_1df537efa12d29c167037c61511d0c728be2a1b7100cb11eaf904ab7680529a2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_173af1c553938fb777cfa9975c175e615aef1c5ed5b5163798cb9b85ff46a3ed = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_173af1c553938fb777cfa9975c175e615aef1c5ed5b5163798cb9b85ff46a3ed->enter($__internal_173af1c553938fb777cfa9975c175e615aef1c5ed5b5163798cb9b85ff46a3ed_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/submit_widget.html.php"));

        $__internal_2f48468902e9b42351d62af19e474c9229d1b936213834e7f45206fbb4e2545c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2f48468902e9b42351d62af19e474c9229d1b936213834e7f45206fbb4e2545c->enter($__internal_2f48468902e9b42351d62af19e474c9229d1b936213834e7f45206fbb4e2545c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/submit_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'button_widget', array('type' => isset(\$type) ? \$type : 'submit')) ?>
";
        
        $__internal_173af1c553938fb777cfa9975c175e615aef1c5ed5b5163798cb9b85ff46a3ed->leave($__internal_173af1c553938fb777cfa9975c175e615aef1c5ed5b5163798cb9b85ff46a3ed_prof);

        
        $__internal_2f48468902e9b42351d62af19e474c9229d1b936213834e7f45206fbb4e2545c->leave($__internal_2f48468902e9b42351d62af19e474c9229d1b936213834e7f45206fbb4e2545c_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/submit_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'button_widget', array('type' => isset(\$type) ? \$type : 'submit')) ?>
", "@Framework/Form/submit_widget.html.php", "/Users/zima/projekty/blogweb/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/submit_widget.html.php");
    }
}
