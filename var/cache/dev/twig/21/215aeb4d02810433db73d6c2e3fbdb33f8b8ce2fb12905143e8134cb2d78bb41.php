<?php

/* ZimaBlogwebBundle:Blog:tabFriends.html.twig */
class __TwigTemplate_77849d80082e6e46b05a5ac0b4b0c57e8381cdeea58f7412c5f6551a042f6999 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@ZimaBlogweb/base.html.twig", "ZimaBlogwebBundle:Blog:tabFriends.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@ZimaBlogweb/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1dcc118ab8f7a6db9d8fa6a41fbd11f5d81edd6795b979f3b9d95d2e3ea0d3a0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1dcc118ab8f7a6db9d8fa6a41fbd11f5d81edd6795b979f3b9d95d2e3ea0d3a0->enter($__internal_1dcc118ab8f7a6db9d8fa6a41fbd11f5d81edd6795b979f3b9d95d2e3ea0d3a0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "ZimaBlogwebBundle:Blog:tabFriends.html.twig"));

        $__internal_2472e47721a2b009978f463ae49f0bb8f6380e04fe0182eadd57a34df2b46838 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2472e47721a2b009978f463ae49f0bb8f6380e04fe0182eadd57a34df2b46838->enter($__internal_2472e47721a2b009978f463ae49f0bb8f6380e04fe0182eadd57a34df2b46838_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "ZimaBlogwebBundle:Blog:tabFriends.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_1dcc118ab8f7a6db9d8fa6a41fbd11f5d81edd6795b979f3b9d95d2e3ea0d3a0->leave($__internal_1dcc118ab8f7a6db9d8fa6a41fbd11f5d81edd6795b979f3b9d95d2e3ea0d3a0_prof);

        
        $__internal_2472e47721a2b009978f463ae49f0bb8f6380e04fe0182eadd57a34df2b46838->leave($__internal_2472e47721a2b009978f463ae49f0bb8f6380e04fe0182eadd57a34df2b46838_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_ec4908cf7e77051cd8b1c1babc5b17bf70888156c72086e870b33ae60d38c8c1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ec4908cf7e77051cd8b1c1babc5b17bf70888156c72086e870b33ae60d38c8c1->enter($__internal_ec4908cf7e77051cd8b1c1babc5b17bf70888156c72086e870b33ae60d38c8c1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_da4b43d24af22315b6326c24e028c471514c90e8c928876c2f0389106ea62ec2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_da4b43d24af22315b6326c24e028c471514c90e8c928876c2f0389106ea62ec2->enter($__internal_da4b43d24af22315b6326c24e028c471514c90e8c928876c2f0389106ea62ec2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo " BlogWEB|Friends";
        
        $__internal_da4b43d24af22315b6326c24e028c471514c90e8c928876c2f0389106ea62ec2->leave($__internal_da4b43d24af22315b6326c24e028c471514c90e8c928876c2f0389106ea62ec2_prof);

        
        $__internal_ec4908cf7e77051cd8b1c1babc5b17bf70888156c72086e870b33ae60d38c8c1->leave($__internal_ec4908cf7e77051cd8b1c1babc5b17bf70888156c72086e870b33ae60d38c8c1_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_f1afcec79c53d8d1d9439989e0a405bfb5b1b9010912bdc36da7ce144be5ffad = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f1afcec79c53d8d1d9439989e0a405bfb5b1b9010912bdc36da7ce144be5ffad->enter($__internal_f1afcec79c53d8d1d9439989e0a405bfb5b1b9010912bdc36da7ce144be5ffad_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_ca3d13fca7d1c034ee81e8e56bb00ceb612a2b8fdc31e273ed36691481dcbc34 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ca3d13fca7d1c034ee81e8e56bb00ceb612a2b8fdc31e273ed36691481dcbc34->enter($__internal_ca3d13fca7d1c034ee81e8e56bb00ceb612a2b8fdc31e273ed36691481dcbc34_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "
    <div class=\"col-xs-12 col-sm-5 col-md-7\">

        <h1>Friends</h1>

        ";
        // line 11
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["selectFriends"] ?? $this->getContext($context, "selectFriends")));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["_key"] => $context["user"]) {
            // line 12
            echo "            <div class=\"list-group\">
                <a href=\"";
            // line 13
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("blog_user", array("username" => $this->getAttribute($context["user"], "username", array()))), "html", null, true);
            echo "\" class=\"list-group-item\">
                    <h4 class=\"list-group-item-heading\">";
            // line 14
            echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "username", array()), "html", null, true);
            echo "</h4>
                    <p class=\"list-group-item-text\"><b>";
            // line 15
            echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "fullname", array()), "html", null, true);
            echo "</b> ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "interests", array()), "html", null, true);
            echo "</p>
                </a>
                <a href=\"";
            // line 17
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("blog_delete_friend", array("id" => $this->getAttribute($context["user"], "id", array()))), "html", null, true);
            echo "\" class=\"btn btn-warning\">unfollow</a>
            </div>
        ";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 20
            echo "            <h2>You don't have friends</h2>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['user'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 22
        echo "
        <div class=\"navigation text-center\">
            ";
        // line 24
        echo $this->env->getExtension('Knp\Bundle\PaginatorBundle\Twig\Extension\PaginationExtension')->render($this->env, ($context["selectFriends"] ?? $this->getContext($context, "selectFriends")));
        echo "
        </div>
    </div>

";
        
        $__internal_ca3d13fca7d1c034ee81e8e56bb00ceb612a2b8fdc31e273ed36691481dcbc34->leave($__internal_ca3d13fca7d1c034ee81e8e56bb00ceb612a2b8fdc31e273ed36691481dcbc34_prof);

        
        $__internal_f1afcec79c53d8d1d9439989e0a405bfb5b1b9010912bdc36da7ce144be5ffad->leave($__internal_f1afcec79c53d8d1d9439989e0a405bfb5b1b9010912bdc36da7ce144be5ffad_prof);

    }

    public function getTemplateName()
    {
        return "ZimaBlogwebBundle:Blog:tabFriends.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  117 => 24,  113 => 22,  106 => 20,  98 => 17,  91 => 15,  87 => 14,  83 => 13,  80 => 12,  75 => 11,  68 => 6,  59 => 5,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@ZimaBlogweb/base.html.twig' %}

{% block title %} BlogWEB|Friends{% endblock %}

{% block body %}

    <div class=\"col-xs-12 col-sm-5 col-md-7\">

        <h1>Friends</h1>

        {% for user  in selectFriends%}
            <div class=\"list-group\">
                <a href=\"{{ path('blog_user', {'username': user.username }) }}\" class=\"list-group-item\">
                    <h4 class=\"list-group-item-heading\">{{ user.username }}</h4>
                    <p class=\"list-group-item-text\"><b>{{ user.fullname }}</b> {{ user.interests }}</p>
                </a>
                <a href=\"{{ path('blog_delete_friend', {'id': user.id}) }}\" class=\"btn btn-warning\">unfollow</a>
            </div>
        {% else %}
            <h2>You don't have friends</h2>
        {% endfor %}

        <div class=\"navigation text-center\">
            {{ knp_pagination_render(selectFriends) }}
        </div>
    </div>

{% endblock %}", "ZimaBlogwebBundle:Blog:tabFriends.html.twig", "/Users/zima/projekty/blogweb/src/Zima/BlogwebBundle/Resources/views/Blog/tabFriends.html.twig");
    }
}
