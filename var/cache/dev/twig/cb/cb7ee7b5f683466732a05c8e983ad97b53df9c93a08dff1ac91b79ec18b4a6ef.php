<?php

/* ZimaBlogwebBundle:Blog:edit.html.twig */
class __TwigTemplate_d2495b0ef00ab5d9b93243914cdbe3506fb829e44bc2286261896748ce41b673 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@ZimaBlogweb/base.html.twig", "ZimaBlogwebBundle:Blog:edit.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@ZimaBlogweb/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1624ed44191aba11518f392239d45d6166beb8a7d3bdeba78996ed53a68ade46 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1624ed44191aba11518f392239d45d6166beb8a7d3bdeba78996ed53a68ade46->enter($__internal_1624ed44191aba11518f392239d45d6166beb8a7d3bdeba78996ed53a68ade46_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "ZimaBlogwebBundle:Blog:edit.html.twig"));

        $__internal_8cd78f5cf9af0162afd83f57e75cb4116b27f8385c0a051363ddb2ce6ba72564 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8cd78f5cf9af0162afd83f57e75cb4116b27f8385c0a051363ddb2ce6ba72564->enter($__internal_8cd78f5cf9af0162afd83f57e75cb4116b27f8385c0a051363ddb2ce6ba72564_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "ZimaBlogwebBundle:Blog:edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_1624ed44191aba11518f392239d45d6166beb8a7d3bdeba78996ed53a68ade46->leave($__internal_1624ed44191aba11518f392239d45d6166beb8a7d3bdeba78996ed53a68ade46_prof);

        
        $__internal_8cd78f5cf9af0162afd83f57e75cb4116b27f8385c0a051363ddb2ce6ba72564->leave($__internal_8cd78f5cf9af0162afd83f57e75cb4116b27f8385c0a051363ddb2ce6ba72564_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_0abf4fa5ac2fe0773c8fb56b00059e3a73902d6b50fdfcfc56dd6bf6d8b9d404 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0abf4fa5ac2fe0773c8fb56b00059e3a73902d6b50fdfcfc56dd6bf6d8b9d404->enter($__internal_0abf4fa5ac2fe0773c8fb56b00059e3a73902d6b50fdfcfc56dd6bf6d8b9d404_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_08c2a80b24a4e6dfdb348b3535f011bd966e3e633db75bc6397ec88f32bdf4aa = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_08c2a80b24a4e6dfdb348b3535f011bd966e3e633db75bc6397ec88f32bdf4aa->enter($__internal_08c2a80b24a4e6dfdb348b3535f011bd966e3e633db75bc6397ec88f32bdf4aa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "BlockWEB|Edit";
        
        $__internal_08c2a80b24a4e6dfdb348b3535f011bd966e3e633db75bc6397ec88f32bdf4aa->leave($__internal_08c2a80b24a4e6dfdb348b3535f011bd966e3e633db75bc6397ec88f32bdf4aa_prof);

        
        $__internal_0abf4fa5ac2fe0773c8fb56b00059e3a73902d6b50fdfcfc56dd6bf6d8b9d404->leave($__internal_0abf4fa5ac2fe0773c8fb56b00059e3a73902d6b50fdfcfc56dd6bf6d8b9d404_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_e6e3cf2fdafa8e28bd461dcd7c6c009823e46af384b17daf4f6bcd7b29aac6c1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e6e3cf2fdafa8e28bd461dcd7c6c009823e46af384b17daf4f6bcd7b29aac6c1->enter($__internal_e6e3cf2fdafa8e28bd461dcd7c6c009823e46af384b17daf4f6bcd7b29aac6c1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_d0a415330fa09881264c26299752347ef11ba45e96647c47452d92877c931381 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d0a415330fa09881264c26299752347ef11ba45e96647c47452d92877c931381->enter($__internal_d0a415330fa09881264c26299752347ef11ba45e96647c47452d92877c931381_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <h1>Edit Contents</h1>
    ";
        // line 7
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["formPost"] ?? $this->getContext($context, "formPost")), 'form', array("attr" => array("novalidate" => "novalidate")));
        echo "


";
        
        $__internal_d0a415330fa09881264c26299752347ef11ba45e96647c47452d92877c931381->leave($__internal_d0a415330fa09881264c26299752347ef11ba45e96647c47452d92877c931381_prof);

        
        $__internal_e6e3cf2fdafa8e28bd461dcd7c6c009823e46af384b17daf4f6bcd7b29aac6c1->leave($__internal_e6e3cf2fdafa8e28bd461dcd7c6c009823e46af384b17daf4f6bcd7b29aac6c1_prof);

    }

    public function getTemplateName()
    {
        return "ZimaBlogwebBundle:Blog:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  71 => 7,  68 => 6,  59 => 5,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@ZimaBlogweb/base.html.twig' %}

{% block title %}BlockWEB|Edit{% endblock %}

{% block body %}
    <h1>Edit Contents</h1>
    {{ form(formPost, {\"attr\": {\"novalidate\": \"novalidate\"}}) }}


{% endblock %}", "ZimaBlogwebBundle:Blog:edit.html.twig", "/Users/zima/projekty/blogweb/src/Zima/BlogwebBundle/Resources/views/Blog/edit.html.twig");
    }
}
