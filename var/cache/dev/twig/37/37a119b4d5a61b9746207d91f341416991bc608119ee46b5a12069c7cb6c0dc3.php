<?php

/* @ZimaBlogweb/base.html.twig */
class __TwigTemplate_f82e938ee058ff2fa634d3abef1218b4c83e14521eee9f52240dd74ccde62cba extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_cbed7f0fa38efdd6f44093293b02dc0d5cb400507f3eac412374c5d6e74698d0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_cbed7f0fa38efdd6f44093293b02dc0d5cb400507f3eac412374c5d6e74698d0->enter($__internal_cbed7f0fa38efdd6f44093293b02dc0d5cb400507f3eac412374c5d6e74698d0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@ZimaBlogweb/base.html.twig"));

        $__internal_c7ef675167525e13e2b31180b58db7cfec2920a3d715de350f6bc30825df14ba = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c7ef675167525e13e2b31180b58db7cfec2920a3d715de350f6bc30825df14ba->enter($__internal_c7ef675167525e13e2b31180b58db7cfec2920a3d715de350f6bc30825df14ba_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@ZimaBlogweb/base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
<head>
    <meta charset=\"UTF-8\" />
    <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
    <link href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\" rel=\"stylesheet\">
    <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
</head>
<body>

    <nav class=\"navbar navbar-default navbar-fixed-top\">
        <div class=\"container-fluid\">
            <div class=\"navbar-header\">
                <a href=\"";
        // line 14
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("blog_other");
        echo "\">
                    <img src=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("logo.png"), "html", null, true);
        echo "\" class=\"img img-responsive\" width=\"200\">
                </a>
            </div>
    ";
        // line 18
        if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("ROLE_USER")) {
            // line 19
            echo "            <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-1\">
                <ul class=\"nav navbar-nav\">
                    <li class=\"active\"><a href=\"";
            // line 21
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("blog_home", array("username" => $this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "user", array()), "username", array()))), "html", null, true);
            echo "\">Home <span class=\"sr-only\"></span></a></li>
                    <li><a href=\"";
            // line 22
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("blog_addcontents");
            echo "\">Add contents</a></li>
                </ul>

                <form class=\"navbar-form navbar-left\" method=\"post\" action=\"";
            // line 25
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("blog_search");
            echo "\">
                    <div class=\"form-group\">
                        <input type=\"text\" name=\"search\" class=\"form-control\" placeholder=\"Search Contents\">
                    </div>
                </form>
                <form class=\"navbar-form navbar-left\" method=\"post\" action=\"";
            // line 30
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("blog_search_user");
            echo "\">
                    <div class=\"form-group\">
                        <input type=\"text\" name=\"searchUsers\" class=\"form-control\" placeholder=\"Search Users\">
                    </div>
                </form>

                <ul class=\"nav navbar-nav navbar-right\">
                    <li class=\"dropdown\">
                        <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\">";
            // line 38
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "user", array()), "username", array()), "html", null, true);
            echo " <span class=\"caret\"></span></a>
                        <ul class=\"dropdown-menu\">
                            <li><a href=\"";
            // line 40
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("blog_user", array("username" => $this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "user", array()), "username", array()))), "html", null, true);
            echo "\">Show Profile</a></li>
                            <li><a href=\"";
            // line 41
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("blog_tab_friends", array("username" => $this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "user", array()), "username", array()))), "html", null, true);
            echo "\">Friends</a></li>
                            <li role=\"separator\" class=\"divider\"></li>
                            <li><a href=\"";
            // line 43
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("blog_settings", array("id" => $this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "user", array()), "id", array()))), "html", null, true);
            echo "\">Settings</a></li>
                            <li role=\"separator\" class=\"divider\"></li>
                            <li><a href=\"";
            // line 45
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_security_logout");
            echo "\">Logout</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    ";
        } else {
            // line 52
            echo "            <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-1\">

                <form class=\"navbar-form navbar-left\" method=\"post\" action=\"";
            // line 54
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("blog_search");
            echo "\">
                    <div class=\"form-group\">
                        <input type=\"text\" name=\"search\" class=\"form-control\" placeholder=\"Search Contents\">
                    </div>
                </form>

                <ul class=\"nav navbar-nav navbar-right\">
                    <li><a href=\"";
            // line 61
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_security_login");
            echo "\"><u>Sign In</u></a></li>
                    <li><a href=\"";
            // line 62
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_registration_register");
            echo "\"><u>Sign Up</u></a></li>
                </ul>

            </div>
        </div>
    ";
        }
        // line 68
        echo "    </nav> <br>

    <div class=\"container\" style=\"margin-top: 50px;\">
        ";
        // line 71
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "flashes", array(0 => "success"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
            // line 72
            echo "            <div class=\"alert alert-success\" role=\"alert\">";
            echo twig_escape_filter($this->env, $context["message"], "html", null, true);
            echo "</div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 74
        echo "        ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "flashes", array(0 => "error"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
            // line 75
            echo "            <div class=\"alert alert-danger\" role=\"alert\">";
            echo twig_escape_filter($this->env, $context["message"], "html", null, true);
            echo "</div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 77
        echo "        ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "flashes", array(0 => "warning"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
            // line 78
            echo "            <div class=\"alert alert-warning\" role=\"alert\">";
            echo twig_escape_filter($this->env, $context["message"], "html", null, true);
            echo "</div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 80
        echo "    </div>

    <div class=\"container\">
        ";
        // line 83
        $this->displayBlock('body', $context, $blocks);
        // line 84
        echo "    </div>


<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js\"></script>

<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\" integrity=\"sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa\" crossorigin=\"anonymous\"></script>
</body>
</html>
";
        
        $__internal_cbed7f0fa38efdd6f44093293b02dc0d5cb400507f3eac412374c5d6e74698d0->leave($__internal_cbed7f0fa38efdd6f44093293b02dc0d5cb400507f3eac412374c5d6e74698d0_prof);

        
        $__internal_c7ef675167525e13e2b31180b58db7cfec2920a3d715de350f6bc30825df14ba->leave($__internal_c7ef675167525e13e2b31180b58db7cfec2920a3d715de350f6bc30825df14ba_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_3a2b6f52644667cc0a7718a1b47cde8d51c9a75ea68ec1d4ab5c4f7b6d758ecd = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3a2b6f52644667cc0a7718a1b47cde8d51c9a75ea68ec1d4ab5c4f7b6d758ecd->enter($__internal_3a2b6f52644667cc0a7718a1b47cde8d51c9a75ea68ec1d4ab5c4f7b6d758ecd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_97c355a4e454793839a14302370fa6125c3d204cf853e695e78db67d0298bfbc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_97c355a4e454793839a14302370fa6125c3d204cf853e695e78db67d0298bfbc->enter($__internal_97c355a4e454793839a14302370fa6125c3d204cf853e695e78db67d0298bfbc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "BlockWEB";
        
        $__internal_97c355a4e454793839a14302370fa6125c3d204cf853e695e78db67d0298bfbc->leave($__internal_97c355a4e454793839a14302370fa6125c3d204cf853e695e78db67d0298bfbc_prof);

        
        $__internal_3a2b6f52644667cc0a7718a1b47cde8d51c9a75ea68ec1d4ab5c4f7b6d758ecd->leave($__internal_3a2b6f52644667cc0a7718a1b47cde8d51c9a75ea68ec1d4ab5c4f7b6d758ecd_prof);

    }

    // line 83
    public function block_body($context, array $blocks = array())
    {
        $__internal_3ae59e48406f3e100018b277a34e48dba24c21d923dcedda54145cb9ad9dbcd7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3ae59e48406f3e100018b277a34e48dba24c21d923dcedda54145cb9ad9dbcd7->enter($__internal_3ae59e48406f3e100018b277a34e48dba24c21d923dcedda54145cb9ad9dbcd7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_af26c71a5addef0b41db33766178b9089bb5e9672c948223c8464183bd833883 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_af26c71a5addef0b41db33766178b9089bb5e9672c948223c8464183bd833883->enter($__internal_af26c71a5addef0b41db33766178b9089bb5e9672c948223c8464183bd833883_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_af26c71a5addef0b41db33766178b9089bb5e9672c948223c8464183bd833883->leave($__internal_af26c71a5addef0b41db33766178b9089bb5e9672c948223c8464183bd833883_prof);

        
        $__internal_3ae59e48406f3e100018b277a34e48dba24c21d923dcedda54145cb9ad9dbcd7->leave($__internal_3ae59e48406f3e100018b277a34e48dba24c21d923dcedda54145cb9ad9dbcd7_prof);

    }

    public function getTemplateName()
    {
        return "@ZimaBlogweb/base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  239 => 83,  221 => 5,  202 => 84,  200 => 83,  195 => 80,  186 => 78,  181 => 77,  172 => 75,  167 => 74,  158 => 72,  154 => 71,  149 => 68,  140 => 62,  136 => 61,  126 => 54,  122 => 52,  112 => 45,  107 => 43,  102 => 41,  98 => 40,  93 => 38,  82 => 30,  74 => 25,  68 => 22,  64 => 21,  60 => 19,  58 => 18,  52 => 15,  48 => 14,  38 => 7,  33 => 5,  27 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
<head>
    <meta charset=\"UTF-8\" />
    <title>{% block title %}BlockWEB{% endblock %}</title>
    <link href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\" rel=\"stylesheet\">
    <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('favicon.ico') }}\" />
</head>
<body>

    <nav class=\"navbar navbar-default navbar-fixed-top\">
        <div class=\"container-fluid\">
            <div class=\"navbar-header\">
                <a href=\"{{ path('blog_other') }}\">
                    <img src=\"{{ asset('logo.png') }}\" class=\"img img-responsive\" width=\"200\">
                </a>
            </div>
    {% if is_granted(\"ROLE_USER\") %}
            <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-1\">
                <ul class=\"nav navbar-nav\">
                    <li class=\"active\"><a href=\"{{ path('blog_home', {\"username\": app.user.username}) }}\">Home <span class=\"sr-only\"></span></a></li>
                    <li><a href=\"{{ path('blog_addcontents') }}\">Add contents</a></li>
                </ul>

                <form class=\"navbar-form navbar-left\" method=\"post\" action=\"{{ path('blog_search') }}\">
                    <div class=\"form-group\">
                        <input type=\"text\" name=\"search\" class=\"form-control\" placeholder=\"Search Contents\">
                    </div>
                </form>
                <form class=\"navbar-form navbar-left\" method=\"post\" action=\"{{ path('blog_search_user') }}\">
                    <div class=\"form-group\">
                        <input type=\"text\" name=\"searchUsers\" class=\"form-control\" placeholder=\"Search Users\">
                    </div>
                </form>

                <ul class=\"nav navbar-nav navbar-right\">
                    <li class=\"dropdown\">
                        <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\">{{ app.user.username }} <span class=\"caret\"></span></a>
                        <ul class=\"dropdown-menu\">
                            <li><a href=\"{{ path('blog_user', {\"username\": app.user.username}) }}\">Show Profile</a></li>
                            <li><a href=\"{{ path('blog_tab_friends', {\"username\": app.user.username}) }}\">Friends</a></li>
                            <li role=\"separator\" class=\"divider\"></li>
                            <li><a href=\"{{ path('blog_settings', {\"id\": app.user.id}) }}\">Settings</a></li>
                            <li role=\"separator\" class=\"divider\"></li>
                            <li><a href=\"{{ path('fos_user_security_logout') }}\">Logout</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    {% else %}
            <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-1\">

                <form class=\"navbar-form navbar-left\" method=\"post\" action=\"{{ path('blog_search') }}\">
                    <div class=\"form-group\">
                        <input type=\"text\" name=\"search\" class=\"form-control\" placeholder=\"Search Contents\">
                    </div>
                </form>

                <ul class=\"nav navbar-nav navbar-right\">
                    <li><a href=\"{{ path('fos_user_security_login') }}\"><u>Sign In</u></a></li>
                    <li><a href=\"{{ path('fos_user_registration_register') }}\"><u>Sign Up</u></a></li>
                </ul>

            </div>
        </div>
    {% endif %}
    </nav> <br>

    <div class=\"container\" style=\"margin-top: 50px;\">
        {% for message in app.flashes(\"success\") %}
            <div class=\"alert alert-success\" role=\"alert\">{{ message }}</div>
        {% endfor %}
        {% for message in app.flashes(\"error\") %}
            <div class=\"alert alert-danger\" role=\"alert\">{{ message }}</div>
        {% endfor %}
        {% for message in app.flashes(\"warning\") %}
            <div class=\"alert alert-warning\" role=\"alert\">{{ message }}</div>
        {% endfor %}
    </div>

    <div class=\"container\">
        {% block body %}{% endblock %}
    </div>


<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js\"></script>

<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\" integrity=\"sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa\" crossorigin=\"anonymous\"></script>
</body>
</html>
", "@ZimaBlogweb/base.html.twig", "/Users/zima/projekty/blogweb/src/Zima/BlogwebBundle/Resources/views/base.html.twig");
    }
}
