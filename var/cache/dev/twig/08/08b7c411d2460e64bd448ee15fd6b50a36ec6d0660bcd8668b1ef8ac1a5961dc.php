<?php

/* FOSUserBundle:Group:new.html.twig */
class __TwigTemplate_52ae23a8908b1f46eae9d530b0c53a9bfbcd3461c0879263ce9c0f7c4f28e41a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Group:new.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4aaeac5b4ed398fb0a37dbb7ea9c10ec0b46228a3eadd7b97bd0cf9342754b62 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4aaeac5b4ed398fb0a37dbb7ea9c10ec0b46228a3eadd7b97bd0cf9342754b62->enter($__internal_4aaeac5b4ed398fb0a37dbb7ea9c10ec0b46228a3eadd7b97bd0cf9342754b62_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:new.html.twig"));

        $__internal_c446f6d9b0b5f567b1731ec9574cbbedba6bf36c0289b1092663880a6b5a3a41 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c446f6d9b0b5f567b1731ec9574cbbedba6bf36c0289b1092663880a6b5a3a41->enter($__internal_c446f6d9b0b5f567b1731ec9574cbbedba6bf36c0289b1092663880a6b5a3a41_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:new.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_4aaeac5b4ed398fb0a37dbb7ea9c10ec0b46228a3eadd7b97bd0cf9342754b62->leave($__internal_4aaeac5b4ed398fb0a37dbb7ea9c10ec0b46228a3eadd7b97bd0cf9342754b62_prof);

        
        $__internal_c446f6d9b0b5f567b1731ec9574cbbedba6bf36c0289b1092663880a6b5a3a41->leave($__internal_c446f6d9b0b5f567b1731ec9574cbbedba6bf36c0289b1092663880a6b5a3a41_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_6422ffabe72482869453349e0e2cb7ccc6bace20be26b1b6f5cc87cab2cc7a0d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6422ffabe72482869453349e0e2cb7ccc6bace20be26b1b6f5cc87cab2cc7a0d->enter($__internal_6422ffabe72482869453349e0e2cb7ccc6bace20be26b1b6f5cc87cab2cc7a0d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_14f3c941e19506734d597b1db1da9d28441b0df598ac524c8a6bf5509b016a7f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_14f3c941e19506734d597b1db1da9d28441b0df598ac524c8a6bf5509b016a7f->enter($__internal_14f3c941e19506734d597b1db1da9d28441b0df598ac524c8a6bf5509b016a7f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Group/new_content.html.twig", "FOSUserBundle:Group:new.html.twig", 4)->display($context);
        
        $__internal_14f3c941e19506734d597b1db1da9d28441b0df598ac524c8a6bf5509b016a7f->leave($__internal_14f3c941e19506734d597b1db1da9d28441b0df598ac524c8a6bf5509b016a7f_prof);

        
        $__internal_6422ffabe72482869453349e0e2cb7ccc6bace20be26b1b6f5cc87cab2cc7a0d->leave($__internal_6422ffabe72482869453349e0e2cb7ccc6bace20be26b1b6f5cc87cab2cc7a0d_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Group/new_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Group:new.html.twig", "/Users/zima/projekty/blogweb/vendor/friendsofsymfony/user-bundle/Resources/views/Group/new.html.twig");
    }
}
