<?php

/* @Framework/Form/form_rest.html.php */
class __TwigTemplate_b5b9c5a58406a713700b212525f75a4fea8fd60f224081591fee320571d41e5a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0bf9ec4fa690926f1c9b1e3d103a8336937353bfbdf519c0232e4018fedf081b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0bf9ec4fa690926f1c9b1e3d103a8336937353bfbdf519c0232e4018fedf081b->enter($__internal_0bf9ec4fa690926f1c9b1e3d103a8336937353bfbdf519c0232e4018fedf081b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rest.html.php"));

        $__internal_2723b19e7e106a45b4dfea69f20551bad7e3bd0cb619f668b624c98f1fedee7c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2723b19e7e106a45b4dfea69f20551bad7e3bd0cb619f668b624c98f1fedee7c->enter($__internal_2723b19e7e106a45b4dfea69f20551bad7e3bd0cb619f668b624c98f1fedee7c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rest.html.php"));

        // line 1
        echo "<?php foreach (\$form as \$child): ?>
    <?php if (!\$child->isRendered()): ?>
        <?php echo \$view['form']->row(\$child) ?>
    <?php endif; ?>
<?php endforeach; ?>
";
        
        $__internal_0bf9ec4fa690926f1c9b1e3d103a8336937353bfbdf519c0232e4018fedf081b->leave($__internal_0bf9ec4fa690926f1c9b1e3d103a8336937353bfbdf519c0232e4018fedf081b_prof);

        
        $__internal_2723b19e7e106a45b4dfea69f20551bad7e3bd0cb619f668b624c98f1fedee7c->leave($__internal_2723b19e7e106a45b4dfea69f20551bad7e3bd0cb619f668b624c98f1fedee7c_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_rest.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php foreach (\$form as \$child): ?>
    <?php if (!\$child->isRendered()): ?>
        <?php echo \$view['form']->row(\$child) ?>
    <?php endif; ?>
<?php endforeach; ?>
", "@Framework/Form/form_rest.html.php", "/Users/zima/projekty/blogweb/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_rest.html.php");
    }
}
