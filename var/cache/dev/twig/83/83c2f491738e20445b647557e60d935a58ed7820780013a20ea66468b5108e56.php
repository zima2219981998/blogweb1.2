<?php

/* @Framework/FormTable/form_row.html.php */
class __TwigTemplate_f32c3fc260cba5dcbb8923f16c4fa6cf777e4ed55ee4c8a38c93a002a99bdfe6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ace2d1c18e77588d023ef3487633800de45aafe331d86b32a7dcb27deaf15225 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ace2d1c18e77588d023ef3487633800de45aafe331d86b32a7dcb27deaf15225->enter($__internal_ace2d1c18e77588d023ef3487633800de45aafe331d86b32a7dcb27deaf15225_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/form_row.html.php"));

        $__internal_eb680dbe3ed2fa00331085d4d6c429e1e68724a1433f8975734e9e5dfc575919 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_eb680dbe3ed2fa00331085d4d6c429e1e68724a1433f8975734e9e5dfc575919->enter($__internal_eb680dbe3ed2fa00331085d4d6c429e1e68724a1433f8975734e9e5dfc575919_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/form_row.html.php"));

        // line 1
        echo "<tr>
    <td>
        <?php echo \$view['form']->label(\$form); ?>
    </td>
    <td>
        <?php echo \$view['form']->errors(\$form); ?>
        <?php echo \$view['form']->widget(\$form); ?>
    </td>
</tr>
";
        
        $__internal_ace2d1c18e77588d023ef3487633800de45aafe331d86b32a7dcb27deaf15225->leave($__internal_ace2d1c18e77588d023ef3487633800de45aafe331d86b32a7dcb27deaf15225_prof);

        
        $__internal_eb680dbe3ed2fa00331085d4d6c429e1e68724a1433f8975734e9e5dfc575919->leave($__internal_eb680dbe3ed2fa00331085d4d6c429e1e68724a1433f8975734e9e5dfc575919_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/form_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<tr>
    <td>
        <?php echo \$view['form']->label(\$form); ?>
    </td>
    <td>
        <?php echo \$view['form']->errors(\$form); ?>
        <?php echo \$view['form']->widget(\$form); ?>
    </td>
</tr>
", "@Framework/FormTable/form_row.html.php", "/Users/zima/projekty/blogweb/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/FormTable/form_row.html.php");
    }
}
