<?php

/* @Framework/Form/choice_options.html.php */
class __TwigTemplate_406f9107dd5226644c6d0a2cc0dade0d57cc16f200ba4a3bcaf119da02807df4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_674168e7b25105b50d1d5b97e4bf8cba7fd357a754052234d219577afbbf9f6e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_674168e7b25105b50d1d5b97e4bf8cba7fd357a754052234d219577afbbf9f6e->enter($__internal_674168e7b25105b50d1d5b97e4bf8cba7fd357a754052234d219577afbbf9f6e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_options.html.php"));

        $__internal_4774ba6102e62bade24e933a253c79f575b5fb67b6b9433f5a3956f5149403e4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4774ba6102e62bade24e933a253c79f575b5fb67b6b9433f5a3956f5149403e4->enter($__internal_4774ba6102e62bade24e933a253c79f575b5fb67b6b9433f5a3956f5149403e4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_options.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'choice_widget_options') ?>
";
        
        $__internal_674168e7b25105b50d1d5b97e4bf8cba7fd357a754052234d219577afbbf9f6e->leave($__internal_674168e7b25105b50d1d5b97e4bf8cba7fd357a754052234d219577afbbf9f6e_prof);

        
        $__internal_4774ba6102e62bade24e933a253c79f575b5fb67b6b9433f5a3956f5149403e4->leave($__internal_4774ba6102e62bade24e933a253c79f575b5fb67b6b9433f5a3956f5149403e4_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choice_options.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'choice_widget_options') ?>
", "@Framework/Form/choice_options.html.php", "/Users/zima/projekty/blogweb/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/choice_options.html.php");
    }
}
