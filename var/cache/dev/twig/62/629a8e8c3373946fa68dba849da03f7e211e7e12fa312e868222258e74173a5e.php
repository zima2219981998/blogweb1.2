<?php

/* FOSUserBundle:Group:show.html.twig */
class __TwigTemplate_5583f78e737ab14e367a91c0e2ee282f80b66958bef20ff0d743aad8a1fcbf54 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Group:show.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_36533f10baec2f062816f117ec1bfd56f8c42338c6e1e1b8be56e3baf71c13ee = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_36533f10baec2f062816f117ec1bfd56f8c42338c6e1e1b8be56e3baf71c13ee->enter($__internal_36533f10baec2f062816f117ec1bfd56f8c42338c6e1e1b8be56e3baf71c13ee_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:show.html.twig"));

        $__internal_c4b0168be7c33ee5790172e47684ebe53a6f9c40b83bf9002a952e573e12fc50 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c4b0168be7c33ee5790172e47684ebe53a6f9c40b83bf9002a952e573e12fc50->enter($__internal_c4b0168be7c33ee5790172e47684ebe53a6f9c40b83bf9002a952e573e12fc50_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_36533f10baec2f062816f117ec1bfd56f8c42338c6e1e1b8be56e3baf71c13ee->leave($__internal_36533f10baec2f062816f117ec1bfd56f8c42338c6e1e1b8be56e3baf71c13ee_prof);

        
        $__internal_c4b0168be7c33ee5790172e47684ebe53a6f9c40b83bf9002a952e573e12fc50->leave($__internal_c4b0168be7c33ee5790172e47684ebe53a6f9c40b83bf9002a952e573e12fc50_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_d0c80dc85690218f5f9bd51ef6532c7003ffcbbc9d32f702dd20847d21060f60 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d0c80dc85690218f5f9bd51ef6532c7003ffcbbc9d32f702dd20847d21060f60->enter($__internal_d0c80dc85690218f5f9bd51ef6532c7003ffcbbc9d32f702dd20847d21060f60_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_01e3abe35dbbfcfd728f560b7ec2be06698958e815fdf3bf26d509c234dceb92 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_01e3abe35dbbfcfd728f560b7ec2be06698958e815fdf3bf26d509c234dceb92->enter($__internal_01e3abe35dbbfcfd728f560b7ec2be06698958e815fdf3bf26d509c234dceb92_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Group/show_content.html.twig", "FOSUserBundle:Group:show.html.twig", 4)->display($context);
        
        $__internal_01e3abe35dbbfcfd728f560b7ec2be06698958e815fdf3bf26d509c234dceb92->leave($__internal_01e3abe35dbbfcfd728f560b7ec2be06698958e815fdf3bf26d509c234dceb92_prof);

        
        $__internal_d0c80dc85690218f5f9bd51ef6532c7003ffcbbc9d32f702dd20847d21060f60->leave($__internal_d0c80dc85690218f5f9bd51ef6532c7003ffcbbc9d32f702dd20847d21060f60_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Group/show_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Group:show.html.twig", "/Users/zima/projekty/blogweb/vendor/friendsofsymfony/user-bundle/Resources/views/Group/show.html.twig");
    }
}
