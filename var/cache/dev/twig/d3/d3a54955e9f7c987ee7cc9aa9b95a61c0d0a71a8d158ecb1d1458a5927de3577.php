<?php

/* @Framework/Form/form_start.html.php */
class __TwigTemplate_f27c4d5e50a5d738c7fc99621ef50fc127d35155dbd810739af58b0a94fce158 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_16ab4a5c7a066e50dc57c9cbb3354ded9835252f1df99e7128ec8baccfcf3f4a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_16ab4a5c7a066e50dc57c9cbb3354ded9835252f1df99e7128ec8baccfcf3f4a->enter($__internal_16ab4a5c7a066e50dc57c9cbb3354ded9835252f1df99e7128ec8baccfcf3f4a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_start.html.php"));

        $__internal_b27900ddbc2b8eb46609e4b5ff783a96d1d350e3f0dee9f44d41f08ca7428979 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b27900ddbc2b8eb46609e4b5ff783a96d1d350e3f0dee9f44d41f08ca7428979->enter($__internal_b27900ddbc2b8eb46609e4b5ff783a96d1d350e3f0dee9f44d41f08ca7428979_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_start.html.php"));

        // line 1
        echo "<?php \$method = strtoupper(\$method) ?>
<?php \$form_method = \$method === 'GET' || \$method === 'POST' ? \$method : 'POST' ?>
<form name=\"<?php echo \$name ?>\" method=\"<?php echo strtolower(\$form_method) ?>\"<?php if (\$action !== ''): ?> action=\"<?php echo \$action ?>\"<?php endif ?><?php foreach (\$attr as \$k => \$v) { printf(' %s=\"%s\"', \$view->escape(\$k), \$view->escape(\$v)); } ?><?php if (\$multipart): ?> enctype=\"multipart/form-data\"<?php endif ?>>
<?php if (\$form_method !== \$method): ?>
    <input type=\"hidden\" name=\"_method\" value=\"<?php echo \$method ?>\" />
<?php endif ?>
";
        
        $__internal_16ab4a5c7a066e50dc57c9cbb3354ded9835252f1df99e7128ec8baccfcf3f4a->leave($__internal_16ab4a5c7a066e50dc57c9cbb3354ded9835252f1df99e7128ec8baccfcf3f4a_prof);

        
        $__internal_b27900ddbc2b8eb46609e4b5ff783a96d1d350e3f0dee9f44d41f08ca7428979->leave($__internal_b27900ddbc2b8eb46609e4b5ff783a96d1d350e3f0dee9f44d41f08ca7428979_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_start.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php \$method = strtoupper(\$method) ?>
<?php \$form_method = \$method === 'GET' || \$method === 'POST' ? \$method : 'POST' ?>
<form name=\"<?php echo \$name ?>\" method=\"<?php echo strtolower(\$form_method) ?>\"<?php if (\$action !== ''): ?> action=\"<?php echo \$action ?>\"<?php endif ?><?php foreach (\$attr as \$k => \$v) { printf(' %s=\"%s\"', \$view->escape(\$k), \$view->escape(\$v)); } ?><?php if (\$multipart): ?> enctype=\"multipart/form-data\"<?php endif ?>>
<?php if (\$form_method !== \$method): ?>
    <input type=\"hidden\" name=\"_method\" value=\"<?php echo \$method ?>\" />
<?php endif ?>
", "@Framework/Form/form_start.html.php", "/Users/zima/projekty/blogweb/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_start.html.php");
    }
}
