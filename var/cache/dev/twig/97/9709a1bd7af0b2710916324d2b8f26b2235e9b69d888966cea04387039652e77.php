<?php

/* @Framework/Form/email_widget.html.php */
class __TwigTemplate_5a22ea3af96700bb9e6c4038489e8b932c82a44fb83fe760125ed92bfbdc0902 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2337c4e7c1908bdac783381f6dab2073ebb822599e5353f143bf6c0d09c03695 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2337c4e7c1908bdac783381f6dab2073ebb822599e5353f143bf6c0d09c03695->enter($__internal_2337c4e7c1908bdac783381f6dab2073ebb822599e5353f143bf6c0d09c03695_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/email_widget.html.php"));

        $__internal_5cb3a59cef86874d479a838f62f724e2778b616cbd71e5082f439699bc52ad5a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5cb3a59cef86874d479a838f62f724e2778b616cbd71e5082f439699bc52ad5a->enter($__internal_5cb3a59cef86874d479a838f62f724e2778b616cbd71e5082f439699bc52ad5a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/email_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'email')) ?>
";
        
        $__internal_2337c4e7c1908bdac783381f6dab2073ebb822599e5353f143bf6c0d09c03695->leave($__internal_2337c4e7c1908bdac783381f6dab2073ebb822599e5353f143bf6c0d09c03695_prof);

        
        $__internal_5cb3a59cef86874d479a838f62f724e2778b616cbd71e5082f439699bc52ad5a->leave($__internal_5cb3a59cef86874d479a838f62f724e2778b616cbd71e5082f439699bc52ad5a_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/email_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'email')) ?>
", "@Framework/Form/email_widget.html.php", "/Users/zima/projekty/blogweb/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/email_widget.html.php");
    }
}
