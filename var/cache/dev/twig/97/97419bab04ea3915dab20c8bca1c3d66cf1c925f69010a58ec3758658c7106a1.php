<?php

/* FOSUserBundle:Group:edit.html.twig */
class __TwigTemplate_4b37da05799defb03d12f97b35990c7d65441e04c97e22c8a5d6adb7f0ccd184 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Group:edit.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ee261dd04f71cc02f4aae3ee044ae23be1690ba3a52b5f67e4bca0495f1dc252 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ee261dd04f71cc02f4aae3ee044ae23be1690ba3a52b5f67e4bca0495f1dc252->enter($__internal_ee261dd04f71cc02f4aae3ee044ae23be1690ba3a52b5f67e4bca0495f1dc252_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:edit.html.twig"));

        $__internal_2f88be77210646d7396404dca3e4814832ce900f630375f075472bcad1b4db33 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2f88be77210646d7396404dca3e4814832ce900f630375f075472bcad1b4db33->enter($__internal_2f88be77210646d7396404dca3e4814832ce900f630375f075472bcad1b4db33_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_ee261dd04f71cc02f4aae3ee044ae23be1690ba3a52b5f67e4bca0495f1dc252->leave($__internal_ee261dd04f71cc02f4aae3ee044ae23be1690ba3a52b5f67e4bca0495f1dc252_prof);

        
        $__internal_2f88be77210646d7396404dca3e4814832ce900f630375f075472bcad1b4db33->leave($__internal_2f88be77210646d7396404dca3e4814832ce900f630375f075472bcad1b4db33_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_a71e1d050c501290f5095767b8f05f7a9ea8721c5d66694400bcad3fb95f45a5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a71e1d050c501290f5095767b8f05f7a9ea8721c5d66694400bcad3fb95f45a5->enter($__internal_a71e1d050c501290f5095767b8f05f7a9ea8721c5d66694400bcad3fb95f45a5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_9c747f04d0a29994b2c3a7677ef679a8349fd7d43d06892de26945497ffe484f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9c747f04d0a29994b2c3a7677ef679a8349fd7d43d06892de26945497ffe484f->enter($__internal_9c747f04d0a29994b2c3a7677ef679a8349fd7d43d06892de26945497ffe484f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Group/edit_content.html.twig", "FOSUserBundle:Group:edit.html.twig", 4)->display($context);
        
        $__internal_9c747f04d0a29994b2c3a7677ef679a8349fd7d43d06892de26945497ffe484f->leave($__internal_9c747f04d0a29994b2c3a7677ef679a8349fd7d43d06892de26945497ffe484f_prof);

        
        $__internal_a71e1d050c501290f5095767b8f05f7a9ea8721c5d66694400bcad3fb95f45a5->leave($__internal_a71e1d050c501290f5095767b8f05f7a9ea8721c5d66694400bcad3fb95f45a5_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Group/edit_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Group:edit.html.twig", "/Users/zima/projekty/blogweb/vendor/friendsofsymfony/user-bundle/Resources/views/Group/edit.html.twig");
    }
}
