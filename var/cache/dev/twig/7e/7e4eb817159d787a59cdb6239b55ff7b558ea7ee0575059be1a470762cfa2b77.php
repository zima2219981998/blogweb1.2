<?php

/* @Framework/Form/button_row.html.php */
class __TwigTemplate_8ff116b1d6afd5b31599fd6ec819d8745238fc9525c15c57014a143c6a57c910 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_056d03726306e0fb6c40f2b960436018e61161cc91bec6487e2917b2e1c579c7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_056d03726306e0fb6c40f2b960436018e61161cc91bec6487e2917b2e1c579c7->enter($__internal_056d03726306e0fb6c40f2b960436018e61161cc91bec6487e2917b2e1c579c7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_row.html.php"));

        $__internal_3fe3b372f3e68f0fe7d186b99ec8fb50a59cab5f5df23479afb27ba636ee64a0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3fe3b372f3e68f0fe7d186b99ec8fb50a59cab5f5df23479afb27ba636ee64a0->enter($__internal_3fe3b372f3e68f0fe7d186b99ec8fb50a59cab5f5df23479afb27ba636ee64a0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_row.html.php"));

        // line 1
        echo "<div>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
";
        
        $__internal_056d03726306e0fb6c40f2b960436018e61161cc91bec6487e2917b2e1c579c7->leave($__internal_056d03726306e0fb6c40f2b960436018e61161cc91bec6487e2917b2e1c579c7_prof);

        
        $__internal_3fe3b372f3e68f0fe7d186b99ec8fb50a59cab5f5df23479afb27ba636ee64a0->leave($__internal_3fe3b372f3e68f0fe7d186b99ec8fb50a59cab5f5df23479afb27ba636ee64a0_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/button_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
", "@Framework/Form/button_row.html.php", "/Users/zima/projekty/blogweb/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/button_row.html.php");
    }
}
