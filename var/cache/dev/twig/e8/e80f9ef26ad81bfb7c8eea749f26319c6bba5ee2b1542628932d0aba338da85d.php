<?php

/* TwigBundle:Exception:error.js.twig */
class __TwigTemplate_5424452fb6cb72fb3aee4b7bf49f7cbec8785aafd9c2a0bf6a4387f14d419a22 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1e8d68a8d6ed07679b1d6e60b0274cf46d1538deea0b783736f1b5cf549f17a2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1e8d68a8d6ed07679b1d6e60b0274cf46d1538deea0b783736f1b5cf549f17a2->enter($__internal_1e8d68a8d6ed07679b1d6e60b0274cf46d1538deea0b783736f1b5cf549f17a2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.js.twig"));

        $__internal_bd3574f69611f2e9fb15ef32ec02955f788856efa7f27432ad6240721668783f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bd3574f69611f2e9fb15ef32ec02955f788856efa7f27432ad6240721668783f->enter($__internal_bd3574f69611f2e9fb15ef32ec02955f788856efa7f27432ad6240721668783f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.js.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twig_escape_filter($this->env, ($context["status_code"] ?? $this->getContext($context, "status_code")), "js", null, true);
        echo " ";
        echo twig_escape_filter($this->env, ($context["status_text"] ?? $this->getContext($context, "status_text")), "js", null, true);
        echo "

*/
";
        
        $__internal_1e8d68a8d6ed07679b1d6e60b0274cf46d1538deea0b783736f1b5cf549f17a2->leave($__internal_1e8d68a8d6ed07679b1d6e60b0274cf46d1538deea0b783736f1b5cf549f17a2_prof);

        
        $__internal_bd3574f69611f2e9fb15ef32ec02955f788856efa7f27432ad6240721668783f->leave($__internal_bd3574f69611f2e9fb15ef32ec02955f788856efa7f27432ad6240721668783f_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.js.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 2,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("/*
{{ status_code }} {{ status_text }}

*/
", "TwigBundle:Exception:error.js.twig", "/Users/zima/projekty/blogweb/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/error.js.twig");
    }
}
