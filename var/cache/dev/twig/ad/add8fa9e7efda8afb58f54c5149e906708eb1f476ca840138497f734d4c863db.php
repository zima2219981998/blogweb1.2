<?php

/* ZimaBlogwebBundle:Blog:searchUsers.html.twig */
class __TwigTemplate_4767d210f11fedb7b9aedcf07a98ea44a5799579920be9bf640dae64eb7ee434 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@ZimaBlogweb/base.html.twig", "ZimaBlogwebBundle:Blog:searchUsers.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@ZimaBlogweb/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_bbb8a7f6b38b9cd1d88fc500cbac3cdf710af11903bb66b34700bd9bc2280990 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bbb8a7f6b38b9cd1d88fc500cbac3cdf710af11903bb66b34700bd9bc2280990->enter($__internal_bbb8a7f6b38b9cd1d88fc500cbac3cdf710af11903bb66b34700bd9bc2280990_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "ZimaBlogwebBundle:Blog:searchUsers.html.twig"));

        $__internal_4c3399ac16d06122a1bfcc9f2fda5ebba559ba1cc6bb7efbebe6f5ba7d5bccbf = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4c3399ac16d06122a1bfcc9f2fda5ebba559ba1cc6bb7efbebe6f5ba7d5bccbf->enter($__internal_4c3399ac16d06122a1bfcc9f2fda5ebba559ba1cc6bb7efbebe6f5ba7d5bccbf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "ZimaBlogwebBundle:Blog:searchUsers.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_bbb8a7f6b38b9cd1d88fc500cbac3cdf710af11903bb66b34700bd9bc2280990->leave($__internal_bbb8a7f6b38b9cd1d88fc500cbac3cdf710af11903bb66b34700bd9bc2280990_prof);

        
        $__internal_4c3399ac16d06122a1bfcc9f2fda5ebba559ba1cc6bb7efbebe6f5ba7d5bccbf->leave($__internal_4c3399ac16d06122a1bfcc9f2fda5ebba559ba1cc6bb7efbebe6f5ba7d5bccbf_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_2bf5b073209f4ed967451d053fb47d6b49ea7321d961f8f0fb24a57ff869e918 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2bf5b073209f4ed967451d053fb47d6b49ea7321d961f8f0fb24a57ff869e918->enter($__internal_2bf5b073209f4ed967451d053fb47d6b49ea7321d961f8f0fb24a57ff869e918_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_c7b73fae2e9058589d087f3a35b9d22121db8591b9b5950ef4e58d450ca1ca8f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c7b73fae2e9058589d087f3a35b9d22121db8591b9b5950ef4e58d450ca1ca8f->enter($__internal_c7b73fae2e9058589d087f3a35b9d22121db8591b9b5950ef4e58d450ca1ca8f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "
    <div class=\"col-lg-8 col-md-8 col-xs-12\">

        <h1>Persons</h1>

        ";
        // line 9
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["result"] ?? $this->getContext($context, "result")));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["_key"] => $context["user"]) {
            // line 10
            echo "            <div class=\"list-group\">
                <a href=\"";
            // line 11
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("blog_user", array("username" => $this->getAttribute($context["user"], "username", array()))), "html", null, true);
            echo "\" class=\"list-group-item\">
                    <h4 class=\"list-group-item-heading\">";
            // line 12
            echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "username", array()), "html", null, true);
            echo "</h4>
                    <p class=\"list-group-item-text\"><b>";
            // line 13
            echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "fullname", array()), "html", null, true);
            echo "</b> ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "interests", array()), "html", null, true);
            echo "</p>
                </a>
            </div>
        ";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 17
            echo "            <div class=\"well\">
                <center> <h3>No results found</h3> </center>
            </div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['user'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 21
        echo "        <div class=\"navigation text-center\">
            ";
        // line 22
        echo $this->env->getExtension('Knp\Bundle\PaginatorBundle\Twig\Extension\PaginationExtension')->render($this->env, ($context["result"] ?? $this->getContext($context, "result")));
        echo "
        </div>
    </div>


";
        
        $__internal_c7b73fae2e9058589d087f3a35b9d22121db8591b9b5950ef4e58d450ca1ca8f->leave($__internal_c7b73fae2e9058589d087f3a35b9d22121db8591b9b5950ef4e58d450ca1ca8f_prof);

        
        $__internal_2bf5b073209f4ed967451d053fb47d6b49ea7321d961f8f0fb24a57ff869e918->leave($__internal_2bf5b073209f4ed967451d053fb47d6b49ea7321d961f8f0fb24a57ff869e918_prof);

    }

    public function getTemplateName()
    {
        return "ZimaBlogwebBundle:Blog:searchUsers.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  95 => 22,  92 => 21,  83 => 17,  72 => 13,  68 => 12,  64 => 11,  61 => 10,  56 => 9,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@ZimaBlogweb/base.html.twig' %}

{% block body %}

    <div class=\"col-lg-8 col-md-8 col-xs-12\">

        <h1>Persons</h1>

        {% for user in result %}
            <div class=\"list-group\">
                <a href=\"{{ path('blog_user', {'username': user.username }) }}\" class=\"list-group-item\">
                    <h4 class=\"list-group-item-heading\">{{ user.username }}</h4>
                    <p class=\"list-group-item-text\"><b>{{ user.fullname }}</b> {{ user.interests }}</p>
                </a>
            </div>
        {% else %}
            <div class=\"well\">
                <center> <h3>No results found</h3> </center>
            </div>
        {% endfor %}
        <div class=\"navigation text-center\">
            {{ knp_pagination_render(result) }}
        </div>
    </div>


{% endblock %}", "ZimaBlogwebBundle:Blog:searchUsers.html.twig", "/Users/zima/projekty/blogweb/src/Zima/BlogwebBundle/Resources/views/Blog/searchUsers.html.twig");
    }
}
