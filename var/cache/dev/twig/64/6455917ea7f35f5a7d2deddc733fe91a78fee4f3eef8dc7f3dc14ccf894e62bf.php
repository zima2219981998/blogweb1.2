<?php

/* ZimaBlogwebBundle:Blog:other.html.twig */
class __TwigTemplate_d25d225e0f49d4dbdccc70fcae4b3076010168dd26129dbd2b5bc3f746cbaa62 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@ZimaBlogweb/base.html.twig", "ZimaBlogwebBundle:Blog:other.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@ZimaBlogweb/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6fc2c5f8eeb16fabfba3161e83540901f7471644cb67b719f645428e5c7c6eba = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6fc2c5f8eeb16fabfba3161e83540901f7471644cb67b719f645428e5c7c6eba->enter($__internal_6fc2c5f8eeb16fabfba3161e83540901f7471644cb67b719f645428e5c7c6eba_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "ZimaBlogwebBundle:Blog:other.html.twig"));

        $__internal_81997875853ac95225d2bd1094fc484996b171c66129d7d48faf5cb9ce55f0e8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_81997875853ac95225d2bd1094fc484996b171c66129d7d48faf5cb9ce55f0e8->enter($__internal_81997875853ac95225d2bd1094fc484996b171c66129d7d48faf5cb9ce55f0e8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "ZimaBlogwebBundle:Blog:other.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_6fc2c5f8eeb16fabfba3161e83540901f7471644cb67b719f645428e5c7c6eba->leave($__internal_6fc2c5f8eeb16fabfba3161e83540901f7471644cb67b719f645428e5c7c6eba_prof);

        
        $__internal_81997875853ac95225d2bd1094fc484996b171c66129d7d48faf5cb9ce55f0e8->leave($__internal_81997875853ac95225d2bd1094fc484996b171c66129d7d48faf5cb9ce55f0e8_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_9d4ae6bf58cb8bdea5a5b5e986eb31b337419b20683a57d7cf71ca4d28d237be = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9d4ae6bf58cb8bdea5a5b5e986eb31b337419b20683a57d7cf71ca4d28d237be->enter($__internal_9d4ae6bf58cb8bdea5a5b5e986eb31b337419b20683a57d7cf71ca4d28d237be_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_42faf74b70d0628b94c5f87b8152028fa875d9e3c27c228e5897d8f5732afa6b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_42faf74b70d0628b94c5f87b8152028fa875d9e3c27c228e5897d8f5732afa6b->enter($__internal_42faf74b70d0628b94c5f87b8152028fa875d9e3c27c228e5897d8f5732afa6b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "
    <div class=\"col-xs-12 col-sm-12 col-md-12\">
        ";
        // line 6
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["selectUndeletedPost"] ?? $this->getContext($context, "selectUndeletedPost")));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["_key"] => $context["content"]) {
            // line 7
            echo "                <div class=\"well\">
                    <center><p style=\"font-size: 25px;\">";
            // line 8
            echo twig_escape_filter($this->env, $this->getAttribute($context["content"], "title", array()), "html", null, true);
            echo "</p></center>
                    <p>wrote: <i><a href=\"";
            // line 9
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("blog_user", array("username" => $this->getAttribute($context["content"], "owner", array()))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["content"], "owner", array()), "html", null, true);
            echo "</a></i> ";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["content"], "createdAt", array()), "Y-m-d H:i"), "html", null, true);
            echo " </p> <hr>
                    <p><b>";
            // line 10
            echo twig_escape_filter($this->env, $this->getAttribute($context["content"], "shortdescription", array()), "html", null, true);
            echo "</b></p>
                    <a href=\"";
            // line 11
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("blog_content", array("id" => $this->getAttribute($context["content"], "id", array()))), "html", null, true);
            echo "\">More…</a>
                </div>
        ";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 14
            echo "                <div class=\"well\">
                    <center> <h3>You haven't got friends yet</h3> </center>
                </div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['content'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 18
        echo "

        <div class=\"navigation text-center\">
            ";
        // line 21
        echo $this->env->getExtension('Knp\Bundle\PaginatorBundle\Twig\Extension\PaginationExtension')->render($this->env, ($context["selectUndeletedPost"] ?? $this->getContext($context, "selectUndeletedPost")));
        echo "
        </div>
    </div>

";
        
        $__internal_42faf74b70d0628b94c5f87b8152028fa875d9e3c27c228e5897d8f5732afa6b->leave($__internal_42faf74b70d0628b94c5f87b8152028fa875d9e3c27c228e5897d8f5732afa6b_prof);

        
        $__internal_9d4ae6bf58cb8bdea5a5b5e986eb31b337419b20683a57d7cf71ca4d28d237be->leave($__internal_9d4ae6bf58cb8bdea5a5b5e986eb31b337419b20683a57d7cf71ca4d28d237be_prof);

    }

    public function getTemplateName()
    {
        return "ZimaBlogwebBundle:Blog:other.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  99 => 21,  94 => 18,  85 => 14,  77 => 11,  73 => 10,  65 => 9,  61 => 8,  58 => 7,  53 => 6,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@ZimaBlogweb/base.html.twig' %}

{% block body %}

    <div class=\"col-xs-12 col-sm-12 col-md-12\">
        {% for content  in selectUndeletedPost%}
                <div class=\"well\">
                    <center><p style=\"font-size: 25px;\">{{ content.title }}</p></center>
                    <p>wrote: <i><a href=\"{{ path('blog_user', {'username': content.owner}) }}\">{{ content.owner }}</a></i> {{ content.createdAt|date('Y-m-d H:i') }} </p> <hr>
                    <p><b>{{ content.shortdescription }}</b></p>
                    <a href=\"{{ path('blog_content', {\"id\": content.id}) }}\">More…</a>
                </div>
        {% else %}
                <div class=\"well\">
                    <center> <h3>You haven't got friends yet</h3> </center>
                </div>
        {% endfor %}


        <div class=\"navigation text-center\">
            {{ knp_pagination_render(selectUndeletedPost) }}
        </div>
    </div>

{% endblock %}", "ZimaBlogwebBundle:Blog:other.html.twig", "/Users/zima/projekty/blogweb/src/Zima/BlogwebBundle/Resources/views/Blog/other.html.twig");
    }
}
