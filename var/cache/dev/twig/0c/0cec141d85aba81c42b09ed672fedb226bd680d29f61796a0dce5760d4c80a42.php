<?php

/* @Framework/Form/password_widget.html.php */
class __TwigTemplate_93f1d15db6f975e983cf8443b287538d83dcf5606cc16ab45a5ed5cc8e29b0b5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8edd0a76a186ddac100bb85a504ccd0d11f0473644cd9c37c158058e160b33de = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8edd0a76a186ddac100bb85a504ccd0d11f0473644cd9c37c158058e160b33de->enter($__internal_8edd0a76a186ddac100bb85a504ccd0d11f0473644cd9c37c158058e160b33de_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/password_widget.html.php"));

        $__internal_cfebfbcb123e2045c5aa233078f5280b465341d851a8b73c8b2eaadb44d71e4f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cfebfbcb123e2045c5aa233078f5280b465341d851a8b73c8b2eaadb44d71e4f->enter($__internal_cfebfbcb123e2045c5aa233078f5280b465341d851a8b73c8b2eaadb44d71e4f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/password_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'password')) ?>
";
        
        $__internal_8edd0a76a186ddac100bb85a504ccd0d11f0473644cd9c37c158058e160b33de->leave($__internal_8edd0a76a186ddac100bb85a504ccd0d11f0473644cd9c37c158058e160b33de_prof);

        
        $__internal_cfebfbcb123e2045c5aa233078f5280b465341d851a8b73c8b2eaadb44d71e4f->leave($__internal_cfebfbcb123e2045c5aa233078f5280b465341d851a8b73c8b2eaadb44d71e4f_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/password_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'password')) ?>
", "@Framework/Form/password_widget.html.php", "/Users/zima/projekty/blogweb/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/password_widget.html.php");
    }
}
