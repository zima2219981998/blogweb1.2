<?php

/* @Framework/Form/form.html.php */
class __TwigTemplate_dd8fff2f9cf40e1990bf782aeb7f4cae9cb45fcf81d4e753338bd8cccf243352 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b5dd69568ff5a6c25618a31db297737e48c2d970370af683993318e0f0964a87 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b5dd69568ff5a6c25618a31db297737e48c2d970370af683993318e0f0964a87->enter($__internal_b5dd69568ff5a6c25618a31db297737e48c2d970370af683993318e0f0964a87_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form.html.php"));

        $__internal_95a966be10276ed22e3479238c01d765d59d039fcbae2bf100be280a0c4fc2f5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_95a966be10276ed22e3479238c01d765d59d039fcbae2bf100be280a0c4fc2f5->enter($__internal_95a966be10276ed22e3479238c01d765d59d039fcbae2bf100be280a0c4fc2f5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form.html.php"));

        // line 1
        echo "<?php echo \$view['form']->start(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
<?php echo \$view['form']->end(\$form) ?>
";
        
        $__internal_b5dd69568ff5a6c25618a31db297737e48c2d970370af683993318e0f0964a87->leave($__internal_b5dd69568ff5a6c25618a31db297737e48c2d970370af683993318e0f0964a87_prof);

        
        $__internal_95a966be10276ed22e3479238c01d765d59d039fcbae2bf100be280a0c4fc2f5->leave($__internal_95a966be10276ed22e3479238c01d765d59d039fcbae2bf100be280a0c4fc2f5_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->start(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
<?php echo \$view['form']->end(\$form) ?>
", "@Framework/Form/form.html.php", "/Users/zima/projekty/blogweb/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form.html.php");
    }
}
