<?php

/* FOSUserBundle:Resetting:email.txt.twig */
class __TwigTemplate_dc5e9ce1a861571c46d6c91bdd5e8296d9a7f021b607446ae3fba5878138a46d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'subject' => array($this, 'block_subject'),
            'body_text' => array($this, 'block_body_text'),
            'body_html' => array($this, 'block_body_html'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_18058bbf462a4edcbc04acc30981f2a6450be148ec525d1f1cfe653a2b1a3211 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_18058bbf462a4edcbc04acc30981f2a6450be148ec525d1f1cfe653a2b1a3211->enter($__internal_18058bbf462a4edcbc04acc30981f2a6450be148ec525d1f1cfe653a2b1a3211_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:email.txt.twig"));

        $__internal_4647acb6e57783d4323afd91d0a01087aa061a9864d833e084c644ee36e94428 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4647acb6e57783d4323afd91d0a01087aa061a9864d833e084c644ee36e94428->enter($__internal_4647acb6e57783d4323afd91d0a01087aa061a9864d833e084c644ee36e94428_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:email.txt.twig"));

        // line 2
        $this->displayBlock('subject', $context, $blocks);
        // line 7
        echo "
";
        // line 8
        $this->displayBlock('body_text', $context, $blocks);
        // line 13
        $this->displayBlock('body_html', $context, $blocks);
        
        $__internal_18058bbf462a4edcbc04acc30981f2a6450be148ec525d1f1cfe653a2b1a3211->leave($__internal_18058bbf462a4edcbc04acc30981f2a6450be148ec525d1f1cfe653a2b1a3211_prof);

        
        $__internal_4647acb6e57783d4323afd91d0a01087aa061a9864d833e084c644ee36e94428->leave($__internal_4647acb6e57783d4323afd91d0a01087aa061a9864d833e084c644ee36e94428_prof);

    }

    // line 2
    public function block_subject($context, array $blocks = array())
    {
        $__internal_3aa3cc6b74b77d63252d3d9557dde8a652f5e79e02d52ff10c7d588cfba76cee = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3aa3cc6b74b77d63252d3d9557dde8a652f5e79e02d52ff10c7d588cfba76cee->enter($__internal_3aa3cc6b74b77d63252d3d9557dde8a652f5e79e02d52ff10c7d588cfba76cee_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "subject"));

        $__internal_7eefa0c521e429adb797bafc06a17c15984b693eb5fe4d6d5599ec7d008debd8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7eefa0c521e429adb797bafc06a17c15984b693eb5fe4d6d5599ec7d008debd8->enter($__internal_7eefa0c521e429adb797bafc06a17c15984b693eb5fe4d6d5599ec7d008debd8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "subject"));

        // line 4
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("resetting.email.subject", array("%username%" => $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "username", array())), "FOSUserBundle");
        
        $__internal_7eefa0c521e429adb797bafc06a17c15984b693eb5fe4d6d5599ec7d008debd8->leave($__internal_7eefa0c521e429adb797bafc06a17c15984b693eb5fe4d6d5599ec7d008debd8_prof);

        
        $__internal_3aa3cc6b74b77d63252d3d9557dde8a652f5e79e02d52ff10c7d588cfba76cee->leave($__internal_3aa3cc6b74b77d63252d3d9557dde8a652f5e79e02d52ff10c7d588cfba76cee_prof);

    }

    // line 8
    public function block_body_text($context, array $blocks = array())
    {
        $__internal_4a1f668f19b2b9ac18b0029afb15dc2327101db2e19f8a76acf8a5a51df717bf = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4a1f668f19b2b9ac18b0029afb15dc2327101db2e19f8a76acf8a5a51df717bf->enter($__internal_4a1f668f19b2b9ac18b0029afb15dc2327101db2e19f8a76acf8a5a51df717bf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_text"));

        $__internal_6eab99b13f6b80477370c6bbe1693746f82cdad492fe71c82031b7c70f7ba537 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6eab99b13f6b80477370c6bbe1693746f82cdad492fe71c82031b7c70f7ba537->enter($__internal_6eab99b13f6b80477370c6bbe1693746f82cdad492fe71c82031b7c70f7ba537_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_text"));

        // line 10
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("resetting.email.message", array("%username%" => $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "username", array()), "%confirmationUrl%" => ($context["confirmationUrl"] ?? $this->getContext($context, "confirmationUrl"))), "FOSUserBundle");
        echo "
";
        
        $__internal_6eab99b13f6b80477370c6bbe1693746f82cdad492fe71c82031b7c70f7ba537->leave($__internal_6eab99b13f6b80477370c6bbe1693746f82cdad492fe71c82031b7c70f7ba537_prof);

        
        $__internal_4a1f668f19b2b9ac18b0029afb15dc2327101db2e19f8a76acf8a5a51df717bf->leave($__internal_4a1f668f19b2b9ac18b0029afb15dc2327101db2e19f8a76acf8a5a51df717bf_prof);

    }

    // line 13
    public function block_body_html($context, array $blocks = array())
    {
        $__internal_b54a4437b7866f5a905d4d1d62ca1bac5d8e63b8283364cc7709eb66b5d01a93 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b54a4437b7866f5a905d4d1d62ca1bac5d8e63b8283364cc7709eb66b5d01a93->enter($__internal_b54a4437b7866f5a905d4d1d62ca1bac5d8e63b8283364cc7709eb66b5d01a93_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_html"));

        $__internal_fd2086a370316e08cb6b28a1c756671066fb4b9326eebb68e29aaf4bc1e53045 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fd2086a370316e08cb6b28a1c756671066fb4b9326eebb68e29aaf4bc1e53045->enter($__internal_fd2086a370316e08cb6b28a1c756671066fb4b9326eebb68e29aaf4bc1e53045_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_html"));

        
        $__internal_fd2086a370316e08cb6b28a1c756671066fb4b9326eebb68e29aaf4bc1e53045->leave($__internal_fd2086a370316e08cb6b28a1c756671066fb4b9326eebb68e29aaf4bc1e53045_prof);

        
        $__internal_b54a4437b7866f5a905d4d1d62ca1bac5d8e63b8283364cc7709eb66b5d01a93->leave($__internal_b54a4437b7866f5a905d4d1d62ca1bac5d8e63b8283364cc7709eb66b5d01a93_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:email.txt.twig";
    }

    public function getDebugInfo()
    {
        return array (  85 => 13,  73 => 10,  64 => 8,  54 => 4,  45 => 2,  35 => 13,  33 => 8,  30 => 7,  28 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% trans_default_domain 'FOSUserBundle' %}
{% block subject %}
{%- autoescape false -%}
{{ 'resetting.email.subject'|trans({'%username%': user.username}) }}
{%- endautoescape -%}
{% endblock %}

{% block body_text %}
{% autoescape false %}
{{ 'resetting.email.message'|trans({'%username%': user.username, '%confirmationUrl%': confirmationUrl}) }}
{% endautoescape %}
{% endblock %}
{% block body_html %}{% endblock %}
", "FOSUserBundle:Resetting:email.txt.twig", "/Users/zima/projekty/blogweb/app/Resources/FOSUserBundle/views/Resetting/email.txt.twig");
    }
}
