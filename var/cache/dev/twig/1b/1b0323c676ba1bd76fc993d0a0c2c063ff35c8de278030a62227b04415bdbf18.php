<?php

/* @Framework/Form/radio_widget.html.php */
class __TwigTemplate_f822448f63c04c9937f8e69dc086729e89e6a015e5e4e46279a80a6a7e60a0a4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_420b1946a184aa1e59a3f35be704e06d29989e7dcb4c0f4deb4e70f24da3c8bc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_420b1946a184aa1e59a3f35be704e06d29989e7dcb4c0f4deb4e70f24da3c8bc->enter($__internal_420b1946a184aa1e59a3f35be704e06d29989e7dcb4c0f4deb4e70f24da3c8bc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/radio_widget.html.php"));

        $__internal_cadbb234ea8382deaaecf200c5c8f994841dbd130169abeb130933bc6980ab92 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cadbb234ea8382deaaecf200c5c8f994841dbd130169abeb130933bc6980ab92->enter($__internal_cadbb234ea8382deaaecf200c5c8f994841dbd130169abeb130933bc6980ab92_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/radio_widget.html.php"));

        // line 1
        echo "<input type=\"radio\"
    <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
    value=\"<?php echo \$view->escape(\$value) ?>\"
    <?php if (\$checked): ?> checked=\"checked\"<?php endif ?>
/>
";
        
        $__internal_420b1946a184aa1e59a3f35be704e06d29989e7dcb4c0f4deb4e70f24da3c8bc->leave($__internal_420b1946a184aa1e59a3f35be704e06d29989e7dcb4c0f4deb4e70f24da3c8bc_prof);

        
        $__internal_cadbb234ea8382deaaecf200c5c8f994841dbd130169abeb130933bc6980ab92->leave($__internal_cadbb234ea8382deaaecf200c5c8f994841dbd130169abeb130933bc6980ab92_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/radio_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<input type=\"radio\"
    <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
    value=\"<?php echo \$view->escape(\$value) ?>\"
    <?php if (\$checked): ?> checked=\"checked\"<?php endif ?>
/>
", "@Framework/Form/radio_widget.html.php", "/Users/zima/projekty/blogweb/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/radio_widget.html.php");
    }
}
