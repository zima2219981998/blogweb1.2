<?php

/* TwigBundle:Exception:error.css.twig */
class __TwigTemplate_7ce177cc8e17c3fc53ce46d0ac16135a2069a82f196deb9350e3971cc466c301 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d8622067e3757cd74a640856770ce24cea61441775f0e7397b2b5938550d3bb1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d8622067e3757cd74a640856770ce24cea61441775f0e7397b2b5938550d3bb1->enter($__internal_d8622067e3757cd74a640856770ce24cea61441775f0e7397b2b5938550d3bb1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.css.twig"));

        $__internal_8b131fad124fc260c5fb9241f9a07d071611ee12ce7e1c1892f9bde2c8ed64f1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8b131fad124fc260c5fb9241f9a07d071611ee12ce7e1c1892f9bde2c8ed64f1->enter($__internal_8b131fad124fc260c5fb9241f9a07d071611ee12ce7e1c1892f9bde2c8ed64f1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.css.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twig_escape_filter($this->env, ($context["status_code"] ?? $this->getContext($context, "status_code")), "css", null, true);
        echo " ";
        echo twig_escape_filter($this->env, ($context["status_text"] ?? $this->getContext($context, "status_text")), "css", null, true);
        echo "

*/
";
        
        $__internal_d8622067e3757cd74a640856770ce24cea61441775f0e7397b2b5938550d3bb1->leave($__internal_d8622067e3757cd74a640856770ce24cea61441775f0e7397b2b5938550d3bb1_prof);

        
        $__internal_8b131fad124fc260c5fb9241f9a07d071611ee12ce7e1c1892f9bde2c8ed64f1->leave($__internal_8b131fad124fc260c5fb9241f9a07d071611ee12ce7e1c1892f9bde2c8ed64f1_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.css.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 2,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("/*
{{ status_code }} {{ status_text }}

*/
", "TwigBundle:Exception:error.css.twig", "/Users/zima/projekty/blogweb/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/error.css.twig");
    }
}
