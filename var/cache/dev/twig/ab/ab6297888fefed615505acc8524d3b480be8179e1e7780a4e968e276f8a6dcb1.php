<?php

/* TwigBundle:Exception:traces.xml.twig */
class __TwigTemplate_54921b3ab5b1f09f5b3298947028e5e775d62cacf20a79efa4ad7a855a6a849b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5b185cf9676f7941978ba309a8464fa400ca43d9aedea7515a58f330c55663f8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5b185cf9676f7941978ba309a8464fa400ca43d9aedea7515a58f330c55663f8->enter($__internal_5b185cf9676f7941978ba309a8464fa400ca43d9aedea7515a58f330c55663f8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:traces.xml.twig"));

        $__internal_060766f2f3942801002a1387009a11c5a3d27475a7d5695feb24aa16e6280abd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_060766f2f3942801002a1387009a11c5a3d27475a7d5695feb24aa16e6280abd->enter($__internal_060766f2f3942801002a1387009a11c5a3d27475a7d5695feb24aa16e6280abd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:traces.xml.twig"));

        // line 1
        echo "        <traces>
";
        // line 2
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["exception"] ?? $this->getContext($context, "exception")), "trace", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["trace"]) {
            // line 3
            echo "            <trace>
";
            // line 4
            echo twig_include($this->env, $context, "@Twig/Exception/trace.txt.twig", array("trace" => $context["trace"]), false);
            echo "

            </trace>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['trace'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 8
        echo "        </traces>
";
        
        $__internal_5b185cf9676f7941978ba309a8464fa400ca43d9aedea7515a58f330c55663f8->leave($__internal_5b185cf9676f7941978ba309a8464fa400ca43d9aedea7515a58f330c55663f8_prof);

        
        $__internal_060766f2f3942801002a1387009a11c5a3d27475a7d5695feb24aa16e6280abd->leave($__internal_060766f2f3942801002a1387009a11c5a3d27475a7d5695feb24aa16e6280abd_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:traces.xml.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  45 => 8,  35 => 4,  32 => 3,  28 => 2,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("        <traces>
{% for trace in exception.trace %}
            <trace>
{{ include('@Twig/Exception/trace.txt.twig', { trace: trace }, with_context = false) }}

            </trace>
{% endfor %}
        </traces>
", "TwigBundle:Exception:traces.xml.twig", "/Users/zima/projekty/blogweb/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/traces.xml.twig");
    }
}
