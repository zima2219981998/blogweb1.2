<?php

/* @Framework/Form/datetime_widget.html.php */
class __TwigTemplate_e9d7611abd08e102a8a0ebe8c842f89ab77a8462e20a12e8ff9957556066047b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_044130ae9115ef6d1a9a14c46295df2c7eebca9e695d5bd619af1c6dff11631e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_044130ae9115ef6d1a9a14c46295df2c7eebca9e695d5bd619af1c6dff11631e->enter($__internal_044130ae9115ef6d1a9a14c46295df2c7eebca9e695d5bd619af1c6dff11631e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/datetime_widget.html.php"));

        $__internal_e31496764178b6f69fe490f07ca841558b11ea238a6e14473de8d42d032f615d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e31496764178b6f69fe490f07ca841558b11ea238a6e14473de8d42d032f615d->enter($__internal_e31496764178b6f69fe490f07ca841558b11ea238a6e14473de8d42d032f615d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/datetime_widget.html.php"));

        // line 1
        echo "<?php if (\$widget == 'single_text'): ?>
    <?php echo \$view['form']->block(\$form, 'form_widget_simple'); ?>
<?php else: ?>
    <div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
        <?php echo \$view['form']->widget(\$form['date']).' '.\$view['form']->widget(\$form['time']) ?>
    </div>
<?php endif ?>
";
        
        $__internal_044130ae9115ef6d1a9a14c46295df2c7eebca9e695d5bd619af1c6dff11631e->leave($__internal_044130ae9115ef6d1a9a14c46295df2c7eebca9e695d5bd619af1c6dff11631e_prof);

        
        $__internal_e31496764178b6f69fe490f07ca841558b11ea238a6e14473de8d42d032f615d->leave($__internal_e31496764178b6f69fe490f07ca841558b11ea238a6e14473de8d42d032f615d_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/datetime_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (\$widget == 'single_text'): ?>
    <?php echo \$view['form']->block(\$form, 'form_widget_simple'); ?>
<?php else: ?>
    <div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
        <?php echo \$view['form']->widget(\$form['date']).' '.\$view['form']->widget(\$form['time']) ?>
    </div>
<?php endif ?>
", "@Framework/Form/datetime_widget.html.php", "/Users/zima/projekty/blogweb/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/datetime_widget.html.php");
    }
}
