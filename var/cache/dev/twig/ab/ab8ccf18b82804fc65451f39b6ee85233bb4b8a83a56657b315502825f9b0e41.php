<?php

/* TwigBundle:Exception:exception.rdf.twig */
class __TwigTemplate_f3a1f31d812866c6c230aae9c383e55e7715bc76a6b8b07ef48e80965d1e4524 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_dfd892903beb6be9315c4257295596778b496861928882b0ad364157b3f88fe8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_dfd892903beb6be9315c4257295596778b496861928882b0ad364157b3f88fe8->enter($__internal_dfd892903beb6be9315c4257295596778b496861928882b0ad364157b3f88fe8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.rdf.twig"));

        $__internal_da9a9c1e0d2fa1508f1cd8ebab4169b773ee5ba70c5dc061db4b5e28a7d0e3a2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_da9a9c1e0d2fa1508f1cd8ebab4169b773ee5ba70c5dc061db4b5e28a7d0e3a2->enter($__internal_da9a9c1e0d2fa1508f1cd8ebab4169b773ee5ba70c5dc061db4b5e28a7d0e3a2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.rdf.twig"));

        // line 1
        echo twig_include($this->env, $context, "@Twig/Exception/exception.xml.twig", array("exception" => ($context["exception"] ?? $this->getContext($context, "exception"))));
        echo "
";
        
        $__internal_dfd892903beb6be9315c4257295596778b496861928882b0ad364157b3f88fe8->leave($__internal_dfd892903beb6be9315c4257295596778b496861928882b0ad364157b3f88fe8_prof);

        
        $__internal_da9a9c1e0d2fa1508f1cd8ebab4169b773ee5ba70c5dc061db4b5e28a7d0e3a2->leave($__internal_da9a9c1e0d2fa1508f1cd8ebab4169b773ee5ba70c5dc061db4b5e28a7d0e3a2_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.rdf.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ include('@Twig/Exception/exception.xml.twig', { exception: exception }) }}
", "TwigBundle:Exception:exception.rdf.twig", "/Users/zima/projekty/blogweb/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception.rdf.twig");
    }
}
