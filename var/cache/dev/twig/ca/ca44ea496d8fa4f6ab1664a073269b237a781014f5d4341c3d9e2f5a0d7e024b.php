<?php

/* @Framework/Form/collection_widget.html.php */
class __TwigTemplate_61fd9ff5c0a7c0b2a10a762179d5d90dd8e83ca4ef0b55af98365d56ebc792f8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_45b7506bca94c33076a1fd450e8c7d36b41c573673eefc69cb5ef51ab1436c19 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_45b7506bca94c33076a1fd450e8c7d36b41c573673eefc69cb5ef51ab1436c19->enter($__internal_45b7506bca94c33076a1fd450e8c7d36b41c573673eefc69cb5ef51ab1436c19_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/collection_widget.html.php"));

        $__internal_992edb8fc36b77139d20aeab639c6e16e6b2d3428130f178e12ecb5cd8c11b26 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_992edb8fc36b77139d20aeab639c6e16e6b2d3428130f178e12ecb5cd8c11b26->enter($__internal_992edb8fc36b77139d20aeab639c6e16e6b2d3428130f178e12ecb5cd8c11b26_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/collection_widget.html.php"));

        // line 1
        echo "<?php if (isset(\$prototype)): ?>
    <?php \$attr['data-prototype'] = \$view->escape(\$view['form']->row(\$prototype)) ?>
<?php endif ?>
<?php echo \$view['form']->widget(\$form, array('attr' => \$attr)) ?>
";
        
        $__internal_45b7506bca94c33076a1fd450e8c7d36b41c573673eefc69cb5ef51ab1436c19->leave($__internal_45b7506bca94c33076a1fd450e8c7d36b41c573673eefc69cb5ef51ab1436c19_prof);

        
        $__internal_992edb8fc36b77139d20aeab639c6e16e6b2d3428130f178e12ecb5cd8c11b26->leave($__internal_992edb8fc36b77139d20aeab639c6e16e6b2d3428130f178e12ecb5cd8c11b26_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/collection_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (isset(\$prototype)): ?>
    <?php \$attr['data-prototype'] = \$view->escape(\$view['form']->row(\$prototype)) ?>
<?php endif ?>
<?php echo \$view['form']->widget(\$form, array('attr' => \$attr)) ?>
", "@Framework/Form/collection_widget.html.php", "/Users/zima/projekty/blogweb/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/collection_widget.html.php");
    }
}
