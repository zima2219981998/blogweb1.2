<?php

/* @FOSUser/Registration/register_content.html.twig */
class __TwigTemplate_3ae5897d7746c18a307207805e7fd75edb8eb6e09708d597e2ccdf95dfb2e8e2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("@ZimaBlogweb/base.html.twig", "@FOSUser/Registration/register_content.html.twig", 2);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@ZimaBlogweb/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4ccd49c339141570ccb395941ca1c35b05429df912cef93961fa1994673f132d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4ccd49c339141570ccb395941ca1c35b05429df912cef93961fa1994673f132d->enter($__internal_4ccd49c339141570ccb395941ca1c35b05429df912cef93961fa1994673f132d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Registration/register_content.html.twig"));

        $__internal_7149edcdd0a461e4afaf878d44bb3d44aaba3865eb1848b6bfb55e60a9363f9a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7149edcdd0a461e4afaf878d44bb3d44aaba3865eb1848b6bfb55e60a9363f9a->enter($__internal_7149edcdd0a461e4afaf878d44bb3d44aaba3865eb1848b6bfb55e60a9363f9a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Registration/register_content.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_4ccd49c339141570ccb395941ca1c35b05429df912cef93961fa1994673f132d->leave($__internal_4ccd49c339141570ccb395941ca1c35b05429df912cef93961fa1994673f132d_prof);

        
        $__internal_7149edcdd0a461e4afaf878d44bb3d44aaba3865eb1848b6bfb55e60a9363f9a->leave($__internal_7149edcdd0a461e4afaf878d44bb3d44aaba3865eb1848b6bfb55e60a9363f9a_prof);

    }

    // line 4
    public function block_body($context, array $blocks = array())
    {
        $__internal_8f19117e03c61fa3b08c605381f15270d33bd74e73251df398f1a8c50127f65b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8f19117e03c61fa3b08c605381f15270d33bd74e73251df398f1a8c50127f65b->enter($__internal_8f19117e03c61fa3b08c605381f15270d33bd74e73251df398f1a8c50127f65b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_568fc372dc1ab3a689a38a2a68efd94da55567a931dfc5fb85f3dbe80c699bf8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_568fc372dc1ab3a689a38a2a68efd94da55567a931dfc5fb85f3dbe80c699bf8->enter($__internal_568fc372dc1ab3a689a38a2a68efd94da55567a931dfc5fb85f3dbe80c699bf8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 5
        echo "
    <h1>Register</h1>

";
        // line 8
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_start', array("method" => "post", "action" => $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_registration_register"), "attr" => array("class" => "fos_user_registration_register")));
        echo "
    <div class=\"form-group\">
        ";
        // line 10
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "email", array()), 'widget', array("attr" => array("placeholder" => "Email")));
        echo "
    </div>
    <div class=\"form-group\">
        ";
        // line 13
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "username", array()), 'widget', array("attr" => array("placeholder" => "Username")));
        echo "
    </div>
    <div class=\"form-group\">
        ";
        // line 16
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "plainPassword", array()), "first", array()), 'widget', array("attr" => array("placeholder" => "Password")));
        echo "
    </div>
    <div class=\"form-group\">
        ";
        // line 19
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "plainPassword", array()), "second", array()), 'widget', array("attr" => array("placeholder" => "Confirm password")));
        echo "
    </div>
    <div>
        <input class=\"btn btn-default\" type=\"submit\" value=\"Registration\" />
    </div>
";
        // line 24
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_end');
        echo "

    <center><img src=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("logo.png"), "html", null, true);
        echo "\" class=\"img img-responsive\"></center>

    <div class=\"container\" style=\"position:fixed; bottom:0;\">
        <hr>
        <p class=\"text-center\">BlogWEB © 2018</p>
    </div>

";
        
        $__internal_568fc372dc1ab3a689a38a2a68efd94da55567a931dfc5fb85f3dbe80c699bf8->leave($__internal_568fc372dc1ab3a689a38a2a68efd94da55567a931dfc5fb85f3dbe80c699bf8_prof);

        
        $__internal_8f19117e03c61fa3b08c605381f15270d33bd74e73251df398f1a8c50127f65b->leave($__internal_8f19117e03c61fa3b08c605381f15270d33bd74e73251df398f1a8c50127f65b_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Registration/register_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  90 => 26,  85 => 24,  77 => 19,  71 => 16,  65 => 13,  59 => 10,  54 => 8,  49 => 5,  40 => 4,  11 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% trans_default_domain 'FOSUserBundle' %}
{% extends \"@ZimaBlogweb/base.html.twig\" %}

{% block body %}

    <h1>Register</h1>

{{ form_start(form, {'method': 'post', 'action': path('fos_user_registration_register'), 'attr': {'class': 'fos_user_registration_register'}}) }}
    <div class=\"form-group\">
        {{ form_widget(form.email, {'attr': {'placeholder': 'Email'}}) }}
    </div>
    <div class=\"form-group\">
        {{ form_widget(form.username, {'attr': {'placeholder': 'Username'}}) }}
    </div>
    <div class=\"form-group\">
        {{ form_widget(form.plainPassword.first, {'attr': {'placeholder': 'Password'}}) }}
    </div>
    <div class=\"form-group\">
        {{ form_widget(form.plainPassword.second, {'attr': {'placeholder': 'Confirm password'}}) }}
    </div>
    <div>
        <input class=\"btn btn-default\" type=\"submit\" value=\"Registration\" />
    </div>
{{ form_end(form) }}

    <center><img src=\"{{ asset('logo.png') }}\" class=\"img img-responsive\"></center>

    <div class=\"container\" style=\"position:fixed; bottom:0;\">
        <hr>
        <p class=\"text-center\">BlogWEB © 2018</p>
    </div>

{% endblock %}", "@FOSUser/Registration/register_content.html.twig", "/Users/zima/projekty/blogweb/app/Resources/FOSUserBundle/views/Registration/register_content.html.twig");
    }
}
