<?php

/* @Framework/Form/money_widget.html.php */
class __TwigTemplate_6adb8bada21d046bac38a389cbe82186373789c9f7ef491c764439822e70bcac extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_60536387ee5d51fa78bba7eb146ee9941ecea143c9904434ad10645cc4f606f9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_60536387ee5d51fa78bba7eb146ee9941ecea143c9904434ad10645cc4f606f9->enter($__internal_60536387ee5d51fa78bba7eb146ee9941ecea143c9904434ad10645cc4f606f9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/money_widget.html.php"));

        $__internal_7e56ec7fdf0a3e6fccb7c3ab79d691857c9ed92f275a397cb6d0cfad3ce2bbe0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7e56ec7fdf0a3e6fccb7c3ab79d691857c9ed92f275a397cb6d0cfad3ce2bbe0->enter($__internal_7e56ec7fdf0a3e6fccb7c3ab79d691857c9ed92f275a397cb6d0cfad3ce2bbe0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/money_widget.html.php"));

        // line 1
        echo "<?php echo str_replace('";
        echo twig_escape_filter($this->env, ($context["widget"] ?? $this->getContext($context, "widget")), "html", null, true);
        echo "', \$view['form']->block(\$form, 'form_widget_simple'), \$money_pattern) ?>
";
        
        $__internal_60536387ee5d51fa78bba7eb146ee9941ecea143c9904434ad10645cc4f606f9->leave($__internal_60536387ee5d51fa78bba7eb146ee9941ecea143c9904434ad10645cc4f606f9_prof);

        
        $__internal_7e56ec7fdf0a3e6fccb7c3ab79d691857c9ed92f275a397cb6d0cfad3ce2bbe0->leave($__internal_7e56ec7fdf0a3e6fccb7c3ab79d691857c9ed92f275a397cb6d0cfad3ce2bbe0_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/money_widget.html.php";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo str_replace('{{ widget }}', \$view['form']->block(\$form, 'form_widget_simple'), \$money_pattern) ?>
", "@Framework/Form/money_widget.html.php", "/Users/zima/projekty/blogweb/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/money_widget.html.php");
    }
}
