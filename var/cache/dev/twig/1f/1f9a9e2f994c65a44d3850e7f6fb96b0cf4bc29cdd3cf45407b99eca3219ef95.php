<?php

/* @Framework/Form/button_widget.html.php */
class __TwigTemplate_a22706028430f02d0443ee167495c46085a8c6b1e212a55ae961a956ae0efda4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b5e51d7e3b333d04c1cf1b1d271a2097a6193af33e50e225dfa7f6d9598822cd = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b5e51d7e3b333d04c1cf1b1d271a2097a6193af33e50e225dfa7f6d9598822cd->enter($__internal_b5e51d7e3b333d04c1cf1b1d271a2097a6193af33e50e225dfa7f6d9598822cd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_widget.html.php"));

        $__internal_2b86c76662a7e97c8a0bf81588f4e2de7025870995be0290c92f4322b631b65a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2b86c76662a7e97c8a0bf81588f4e2de7025870995be0290c92f4322b631b65a->enter($__internal_2b86c76662a7e97c8a0bf81588f4e2de7025870995be0290c92f4322b631b65a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_widget.html.php"));

        // line 1
        echo "<?php if (!\$label) { \$label = isset(\$label_format)
    ? strtr(\$label_format, array('%name%' => \$name, '%id%' => \$id))
    : \$view['form']->humanize(\$name); } ?>
<button type=\"<?php echo isset(\$type) ? \$view->escape(\$type) : 'button' ?>\" <?php echo \$view['form']->block(\$form, 'button_attributes') ?>><?php echo \$view->escape(false !== \$translation_domain ? \$view['translator']->trans(\$label, array(), \$translation_domain) : \$label) ?></button>
";
        
        $__internal_b5e51d7e3b333d04c1cf1b1d271a2097a6193af33e50e225dfa7f6d9598822cd->leave($__internal_b5e51d7e3b333d04c1cf1b1d271a2097a6193af33e50e225dfa7f6d9598822cd_prof);

        
        $__internal_2b86c76662a7e97c8a0bf81588f4e2de7025870995be0290c92f4322b631b65a->leave($__internal_2b86c76662a7e97c8a0bf81588f4e2de7025870995be0290c92f4322b631b65a_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/button_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (!\$label) { \$label = isset(\$label_format)
    ? strtr(\$label_format, array('%name%' => \$name, '%id%' => \$id))
    : \$view['form']->humanize(\$name); } ?>
<button type=\"<?php echo isset(\$type) ? \$view->escape(\$type) : 'button' ?>\" <?php echo \$view['form']->block(\$form, 'button_attributes') ?>><?php echo \$view->escape(false !== \$translation_domain ? \$view['translator']->trans(\$label, array(), \$translation_domain) : \$label) ?></button>
", "@Framework/Form/button_widget.html.php", "/Users/zima/projekty/blogweb/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/button_widget.html.php");
    }
}
