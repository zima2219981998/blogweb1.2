<?php

/* @Framework/Form/integer_widget.html.php */
class __TwigTemplate_45d4af6b027bbd42933a4e7734352029e2d1704a6be5c8106cc3be1ebbe02a47 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_530ab55346e197eea7e5bd718e69712ba1a6de6b9fd91967f78ed28bbc51d5e8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_530ab55346e197eea7e5bd718e69712ba1a6de6b9fd91967f78ed28bbc51d5e8->enter($__internal_530ab55346e197eea7e5bd718e69712ba1a6de6b9fd91967f78ed28bbc51d5e8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/integer_widget.html.php"));

        $__internal_b690d92cccdb34532e463640c2c6e5ae393643d9e807ce97f950821e04ccf921 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b690d92cccdb34532e463640c2c6e5ae393643d9e807ce97f950821e04ccf921->enter($__internal_b690d92cccdb34532e463640c2c6e5ae393643d9e807ce97f950821e04ccf921_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/integer_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'number')) ?>
";
        
        $__internal_530ab55346e197eea7e5bd718e69712ba1a6de6b9fd91967f78ed28bbc51d5e8->leave($__internal_530ab55346e197eea7e5bd718e69712ba1a6de6b9fd91967f78ed28bbc51d5e8_prof);

        
        $__internal_b690d92cccdb34532e463640c2c6e5ae393643d9e807ce97f950821e04ccf921->leave($__internal_b690d92cccdb34532e463640c2c6e5ae393643d9e807ce97f950821e04ccf921_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/integer_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'number')) ?>
", "@Framework/Form/integer_widget.html.php", "/Users/zima/projekty/blogweb/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/integer_widget.html.php");
    }
}
