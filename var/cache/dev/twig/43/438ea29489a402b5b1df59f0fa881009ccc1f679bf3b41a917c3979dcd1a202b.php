<?php

/* @Framework/Form/range_widget.html.php */
class __TwigTemplate_a80a20a86170b34b6e282f4d4cddfc4d38dbaafc25b8ea8b794f0b7448ef4fe3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e2ec79aa2260ec673f58e6d4cdf5cb0217a09f416465644eb45355cfb54f0467 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e2ec79aa2260ec673f58e6d4cdf5cb0217a09f416465644eb45355cfb54f0467->enter($__internal_e2ec79aa2260ec673f58e6d4cdf5cb0217a09f416465644eb45355cfb54f0467_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/range_widget.html.php"));

        $__internal_b5956f08688afa8df08f03fa1b087f8d563331dc646a4eee4630a8564d7e4bdf = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b5956f08688afa8df08f03fa1b087f8d563331dc646a4eee4630a8564d7e4bdf->enter($__internal_b5956f08688afa8df08f03fa1b087f8d563331dc646a4eee4630a8564d7e4bdf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/range_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'range'));
";
        
        $__internal_e2ec79aa2260ec673f58e6d4cdf5cb0217a09f416465644eb45355cfb54f0467->leave($__internal_e2ec79aa2260ec673f58e6d4cdf5cb0217a09f416465644eb45355cfb54f0467_prof);

        
        $__internal_b5956f08688afa8df08f03fa1b087f8d563331dc646a4eee4630a8564d7e4bdf->leave($__internal_b5956f08688afa8df08f03fa1b087f8d563331dc646a4eee4630a8564d7e4bdf_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/range_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'range'));
", "@Framework/Form/range_widget.html.php", "/Users/zima/projekty/blogweb/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/range_widget.html.php");
    }
}
