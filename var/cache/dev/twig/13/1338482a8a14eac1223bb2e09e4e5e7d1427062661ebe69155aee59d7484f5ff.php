<?php

/* @Framework/Form/choice_attributes.html.php */
class __TwigTemplate_09fc762e4b7e7a1c555684e40ab2666d65a68036c187da42e9f35baeb5e5622d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3ad57cd1f162499041b9474b1ac456210c692fc965c22d35849c0e384ad50641 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3ad57cd1f162499041b9474b1ac456210c692fc965c22d35849c0e384ad50641->enter($__internal_3ad57cd1f162499041b9474b1ac456210c692fc965c22d35849c0e384ad50641_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_attributes.html.php"));

        $__internal_20ec9c333206bf05af6c85ec2f7e67ac03b05fb71e4f980d036126111de84618 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_20ec9c333206bf05af6c85ec2f7e67ac03b05fb71e4f980d036126111de84618->enter($__internal_20ec9c333206bf05af6c85ec2f7e67ac03b05fb71e4f980d036126111de84618_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_attributes.html.php"));

        // line 1
        echo "<?php if (\$disabled): ?>disabled=\"disabled\" <?php endif ?>
<?php foreach (\$choice_attr as \$k => \$v): ?>
<?php if (\$v === true): ?>
<?php printf('%s=\"%s\" ', \$view->escape(\$k), \$view->escape(\$k)) ?>
<?php elseif (\$v !== false): ?>
<?php printf('%s=\"%s\" ', \$view->escape(\$k), \$view->escape(\$v)) ?>
<?php endif ?>
<?php endforeach ?>
";
        
        $__internal_3ad57cd1f162499041b9474b1ac456210c692fc965c22d35849c0e384ad50641->leave($__internal_3ad57cd1f162499041b9474b1ac456210c692fc965c22d35849c0e384ad50641_prof);

        
        $__internal_20ec9c333206bf05af6c85ec2f7e67ac03b05fb71e4f980d036126111de84618->leave($__internal_20ec9c333206bf05af6c85ec2f7e67ac03b05fb71e4f980d036126111de84618_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choice_attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (\$disabled): ?>disabled=\"disabled\" <?php endif ?>
<?php foreach (\$choice_attr as \$k => \$v): ?>
<?php if (\$v === true): ?>
<?php printf('%s=\"%s\" ', \$view->escape(\$k), \$view->escape(\$k)) ?>
<?php elseif (\$v !== false): ?>
<?php printf('%s=\"%s\" ', \$view->escape(\$k), \$view->escape(\$v)) ?>
<?php endif ?>
<?php endforeach ?>
", "@Framework/Form/choice_attributes.html.php", "/Users/zima/projekty/blogweb/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/choice_attributes.html.php");
    }
}
