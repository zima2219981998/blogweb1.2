<?php

/* @Framework/Form/choice_widget_expanded.html.php */
class __TwigTemplate_a39186a18fc08cf9e0bd3cee43697a2f1b28e6203cc63143fd866121e030a457 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_946e5a2aa8d96e1be424baed7d1d7b1b178b236850f37976120f5ec0e002adcd = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_946e5a2aa8d96e1be424baed7d1d7b1b178b236850f37976120f5ec0e002adcd->enter($__internal_946e5a2aa8d96e1be424baed7d1d7b1b178b236850f37976120f5ec0e002adcd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_widget_expanded.html.php"));

        $__internal_6e8ca9b762f0482099d8f7c468fbf566b2a094acc13c5428b89cf17ec8636ac4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6e8ca9b762f0482099d8f7c468fbf566b2a094acc13c5428b89cf17ec8636ac4->enter($__internal_6e8ca9b762f0482099d8f7c468fbf566b2a094acc13c5428b89cf17ec8636ac4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_widget_expanded.html.php"));

        // line 1
        echo "<div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
<?php foreach (\$form as \$child): ?>
    <?php echo \$view['form']->widget(\$child) ?>
    <?php echo \$view['form']->label(\$child, null, array('translation_domain' => \$choice_translation_domain)) ?>
<?php endforeach ?>
</div>
";
        
        $__internal_946e5a2aa8d96e1be424baed7d1d7b1b178b236850f37976120f5ec0e002adcd->leave($__internal_946e5a2aa8d96e1be424baed7d1d7b1b178b236850f37976120f5ec0e002adcd_prof);

        
        $__internal_6e8ca9b762f0482099d8f7c468fbf566b2a094acc13c5428b89cf17ec8636ac4->leave($__internal_6e8ca9b762f0482099d8f7c468fbf566b2a094acc13c5428b89cf17ec8636ac4_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choice_widget_expanded.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
<?php foreach (\$form as \$child): ?>
    <?php echo \$view['form']->widget(\$child) ?>
    <?php echo \$view['form']->label(\$child, null, array('translation_domain' => \$choice_translation_domain)) ?>
<?php endforeach ?>
</div>
", "@Framework/Form/choice_widget_expanded.html.php", "/Users/zima/projekty/blogweb/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/choice_widget_expanded.html.php");
    }
}
