<?php

/* WebProfilerBundle:Collector:router.html.twig */
class __TwigTemplate_e6faadd3da724bbaf9c8cb7cd0b9870d0119c0d6cd93b6731b25dd8bc84441ea extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "WebProfilerBundle:Collector:router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0c16c67b5f5cf17cf96ba900e764b553091976bb251dd1e5ddc87e3c1348b54a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0c16c67b5f5cf17cf96ba900e764b553091976bb251dd1e5ddc87e3c1348b54a->enter($__internal_0c16c67b5f5cf17cf96ba900e764b553091976bb251dd1e5ddc87e3c1348b54a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:router.html.twig"));

        $__internal_fdf50f79d455cf471d853fb33abd45df5d033fa93965b6f1de21e933947ef163 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fdf50f79d455cf471d853fb33abd45df5d033fa93965b6f1de21e933947ef163->enter($__internal_fdf50f79d455cf471d853fb33abd45df5d033fa93965b6f1de21e933947ef163_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_0c16c67b5f5cf17cf96ba900e764b553091976bb251dd1e5ddc87e3c1348b54a->leave($__internal_0c16c67b5f5cf17cf96ba900e764b553091976bb251dd1e5ddc87e3c1348b54a_prof);

        
        $__internal_fdf50f79d455cf471d853fb33abd45df5d033fa93965b6f1de21e933947ef163->leave($__internal_fdf50f79d455cf471d853fb33abd45df5d033fa93965b6f1de21e933947ef163_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_27ed50d6fa5d11ad1c39993dc66f0ded57ff1bfcabd1025eb5c2d1397a20433b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_27ed50d6fa5d11ad1c39993dc66f0ded57ff1bfcabd1025eb5c2d1397a20433b->enter($__internal_27ed50d6fa5d11ad1c39993dc66f0ded57ff1bfcabd1025eb5c2d1397a20433b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        $__internal_8f79406cb66f6465feb179f89005d42b95a4b5606c0b04393cef834911b57336 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8f79406cb66f6465feb179f89005d42b95a4b5606c0b04393cef834911b57336->enter($__internal_8f79406cb66f6465feb179f89005d42b95a4b5606c0b04393cef834911b57336_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_8f79406cb66f6465feb179f89005d42b95a4b5606c0b04393cef834911b57336->leave($__internal_8f79406cb66f6465feb179f89005d42b95a4b5606c0b04393cef834911b57336_prof);

        
        $__internal_27ed50d6fa5d11ad1c39993dc66f0ded57ff1bfcabd1025eb5c2d1397a20433b->leave($__internal_27ed50d6fa5d11ad1c39993dc66f0ded57ff1bfcabd1025eb5c2d1397a20433b_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_4f342895c6fd462aa1a44af9dd6d63417abdb982e79e92fa9eff2a77b0da4ea8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4f342895c6fd462aa1a44af9dd6d63417abdb982e79e92fa9eff2a77b0da4ea8->enter($__internal_4f342895c6fd462aa1a44af9dd6d63417abdb982e79e92fa9eff2a77b0da4ea8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_264fa772a31f1569178ecb9d5f7e49c8490ae1688d291da0c1f8a3df209d4c2d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_264fa772a31f1569178ecb9d5f7e49c8490ae1688d291da0c1f8a3df209d4c2d->enter($__internal_264fa772a31f1569178ecb9d5f7e49c8490ae1688d291da0c1f8a3df209d4c2d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_264fa772a31f1569178ecb9d5f7e49c8490ae1688d291da0c1f8a3df209d4c2d->leave($__internal_264fa772a31f1569178ecb9d5f7e49c8490ae1688d291da0c1f8a3df209d4c2d_prof);

        
        $__internal_4f342895c6fd462aa1a44af9dd6d63417abdb982e79e92fa9eff2a77b0da4ea8->leave($__internal_4f342895c6fd462aa1a44af9dd6d63417abdb982e79e92fa9eff2a77b0da4ea8_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_f65da5fe49de31857b92fcc5388fe5e9bf82318a3cabad20a2b7cfc38a074da5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f65da5fe49de31857b92fcc5388fe5e9bf82318a3cabad20a2b7cfc38a074da5->enter($__internal_f65da5fe49de31857b92fcc5388fe5e9bf82318a3cabad20a2b7cfc38a074da5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_79bd68ec647ec40207870d2d7a5151567a35f9f0db7dc097f730359ed8b607ff = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_79bd68ec647ec40207870d2d7a5151567a35f9f0db7dc097f730359ed8b607ff->enter($__internal_79bd68ec647ec40207870d2d7a5151567a35f9f0db7dc097f730359ed8b607ff_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_router", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_79bd68ec647ec40207870d2d7a5151567a35f9f0db7dc097f730359ed8b607ff->leave($__internal_79bd68ec647ec40207870d2d7a5151567a35f9f0db7dc097f730359ed8b607ff_prof);

        
        $__internal_f65da5fe49de31857b92fcc5388fe5e9bf82318a3cabad20a2b7cfc38a074da5->leave($__internal_f65da5fe49de31857b92fcc5388fe5e9bf82318a3cabad20a2b7cfc38a074da5_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Collector:router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  94 => 13,  85 => 12,  71 => 7,  68 => 6,  59 => 5,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}{% endblock %}

{% block menu %}
<span class=\"label\">
    <span class=\"icon\">{{ include('@WebProfiler/Icon/router.svg') }}</span>
    <strong>Routing</strong>
</span>
{% endblock %}

{% block panel %}
    {{ render(path('_profiler_router', { token: token })) }}
{% endblock %}
", "WebProfilerBundle:Collector:router.html.twig", "/Users/zima/projekty/blogweb/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/router.html.twig");
    }
}
