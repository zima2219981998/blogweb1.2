<?php

/* WebProfilerBundle:Profiler:open.html.twig */
class __TwigTemplate_266b6ec330f39cba0cd500c64ae35e9c020ea3b98ebdac0f657c1f9b7d078b5a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/base.html.twig", "WebProfilerBundle:Profiler:open.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ff8a85c4dc4eb673759e4651a3da8da41b2d1b0ebc9c7f4b7e63f3e9712b1e97 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ff8a85c4dc4eb673759e4651a3da8da41b2d1b0ebc9c7f4b7e63f3e9712b1e97->enter($__internal_ff8a85c4dc4eb673759e4651a3da8da41b2d1b0ebc9c7f4b7e63f3e9712b1e97_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:open.html.twig"));

        $__internal_493c9430deb7f51be8b1183001d3c1d2c3a5d8ec79f8d3f435619c781d1a8141 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_493c9430deb7f51be8b1183001d3c1d2c3a5d8ec79f8d3f435619c781d1a8141->enter($__internal_493c9430deb7f51be8b1183001d3c1d2c3a5d8ec79f8d3f435619c781d1a8141_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:open.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_ff8a85c4dc4eb673759e4651a3da8da41b2d1b0ebc9c7f4b7e63f3e9712b1e97->leave($__internal_ff8a85c4dc4eb673759e4651a3da8da41b2d1b0ebc9c7f4b7e63f3e9712b1e97_prof);

        
        $__internal_493c9430deb7f51be8b1183001d3c1d2c3a5d8ec79f8d3f435619c781d1a8141->leave($__internal_493c9430deb7f51be8b1183001d3c1d2c3a5d8ec79f8d3f435619c781d1a8141_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_ba5a8a181ce66a899e3a27a50269d103099831abda95ad0ff4a9349ecb96f2af = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ba5a8a181ce66a899e3a27a50269d103099831abda95ad0ff4a9349ecb96f2af->enter($__internal_ba5a8a181ce66a899e3a27a50269d103099831abda95ad0ff4a9349ecb96f2af_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_ae6c7108b95b4d28c3faf125398da30b03cbbbc212b4bfabf525ce0e057d2c15 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ae6c7108b95b4d28c3faf125398da30b03cbbbc212b4bfabf525ce0e057d2c15->enter($__internal_ae6c7108b95b4d28c3faf125398da30b03cbbbc212b4bfabf525ce0e057d2c15_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <style>
        ";
        // line 5
        echo twig_include($this->env, $context, "@WebProfiler/Profiler/open.css.twig");
        echo "
    </style>
";
        
        $__internal_ae6c7108b95b4d28c3faf125398da30b03cbbbc212b4bfabf525ce0e057d2c15->leave($__internal_ae6c7108b95b4d28c3faf125398da30b03cbbbc212b4bfabf525ce0e057d2c15_prof);

        
        $__internal_ba5a8a181ce66a899e3a27a50269d103099831abda95ad0ff4a9349ecb96f2af->leave($__internal_ba5a8a181ce66a899e3a27a50269d103099831abda95ad0ff4a9349ecb96f2af_prof);

    }

    // line 9
    public function block_body($context, array $blocks = array())
    {
        $__internal_f05879cb27c5fe3209d457936a5477c186cb326383295a1e970fb8461015f34a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f05879cb27c5fe3209d457936a5477c186cb326383295a1e970fb8461015f34a->enter($__internal_f05879cb27c5fe3209d457936a5477c186cb326383295a1e970fb8461015f34a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_78a8850cc295a504edda32d024ce6e1ad17a3ae2b988e8499491e9c2791ce710 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_78a8850cc295a504edda32d024ce6e1ad17a3ae2b988e8499491e9c2791ce710->enter($__internal_78a8850cc295a504edda32d024ce6e1ad17a3ae2b988e8499491e9c2791ce710_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 10
        echo "<div class=\"header\">
    <h1>";
        // line 11
        echo twig_escape_filter($this->env, ($context["file"] ?? $this->getContext($context, "file")), "html", null, true);
        echo " <small>line ";
        echo twig_escape_filter($this->env, ($context["line"] ?? $this->getContext($context, "line")), "html", null, true);
        echo "</small></h1>
    <a class=\"doc\" href=\"https://symfony.com/doc/";
        // line 12
        echo twig_escape_filter($this->env, twig_constant("Symfony\\Component\\HttpKernel\\Kernel::VERSION"), "html", null, true);
        echo "/reference/configuration/framework.html#ide\" rel=\"help\">Open in your IDE?</a>
</div>
<div class=\"source\">
    ";
        // line 15
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\CodeExtension')->fileExcerpt(($context["filename"] ?? $this->getContext($context, "filename")), ($context["line"] ?? $this->getContext($context, "line")),  -1);
        echo "
</div>
";
        
        $__internal_78a8850cc295a504edda32d024ce6e1ad17a3ae2b988e8499491e9c2791ce710->leave($__internal_78a8850cc295a504edda32d024ce6e1ad17a3ae2b988e8499491e9c2791ce710_prof);

        
        $__internal_f05879cb27c5fe3209d457936a5477c186cb326383295a1e970fb8461015f34a->leave($__internal_f05879cb27c5fe3209d457936a5477c186cb326383295a1e970fb8461015f34a_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:open.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  90 => 15,  84 => 12,  78 => 11,  75 => 10,  66 => 9,  53 => 5,  50 => 4,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/base.html.twig' %}

{% block head %}
    <style>
        {{ include('@WebProfiler/Profiler/open.css.twig') }}
    </style>
{% endblock %}

{% block body %}
<div class=\"header\">
    <h1>{{ file }} <small>line {{ line }}</small></h1>
    <a class=\"doc\" href=\"https://symfony.com/doc/{{ constant('Symfony\\\\Component\\\\HttpKernel\\\\Kernel::VERSION') }}/reference/configuration/framework.html#ide\" rel=\"help\">Open in your IDE?</a>
</div>
<div class=\"source\">
    {{ filename|file_excerpt(line, -1) }}
</div>
{% endblock %}
", "WebProfilerBundle:Profiler:open.html.twig", "/Users/zima/projekty/blogweb/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Profiler/open.html.twig");
    }
}
