<?php

/* @Twig/images/chevron-right.svg */
class __TwigTemplate_b900b43d99a500b9fa08d6aecc0f341080d65c6cf03d3da34b25d792b84f5a2f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1785ea76a0abee2cb1e4c919543add3c8af36480b8250c77bc32feb74cddc336 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1785ea76a0abee2cb1e4c919543add3c8af36480b8250c77bc32feb74cddc336->enter($__internal_1785ea76a0abee2cb1e4c919543add3c8af36480b8250c77bc32feb74cddc336_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/images/chevron-right.svg"));

        $__internal_647457cb93b0b752dea835bd8dacf8e350b91ced9d318b382a257ff73abbb1a8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_647457cb93b0b752dea835bd8dacf8e350b91ced9d318b382a257ff73abbb1a8->enter($__internal_647457cb93b0b752dea835bd8dacf8e350b91ced9d318b382a257ff73abbb1a8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/images/chevron-right.svg"));

        // line 1
        echo "<svg width=\"1792\" height=\"1792\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\"><path fill=\"#FFF\" d=\"M1363 877l-742 742q-19 19-45 19t-45-19l-166-166q-19-19-19-45t19-45l531-531-531-531q-19-19-19-45t19-45l166-166q19-19 45-19t45 19l742 742q19 19 19 45t-19 45z\"/></svg>
";
        
        $__internal_1785ea76a0abee2cb1e4c919543add3c8af36480b8250c77bc32feb74cddc336->leave($__internal_1785ea76a0abee2cb1e4c919543add3c8af36480b8250c77bc32feb74cddc336_prof);

        
        $__internal_647457cb93b0b752dea835bd8dacf8e350b91ced9d318b382a257ff73abbb1a8->leave($__internal_647457cb93b0b752dea835bd8dacf8e350b91ced9d318b382a257ff73abbb1a8_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/images/chevron-right.svg";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<svg width=\"1792\" height=\"1792\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\"><path fill=\"#FFF\" d=\"M1363 877l-742 742q-19 19-45 19t-45-19l-166-166q-19-19-19-45t19-45l531-531-531-531q-19-19-19-45t19-45l166-166q19-19 45-19t45 19l742 742q19 19 19 45t-19 45z\"/></svg>
", "@Twig/images/chevron-right.svg", "/Users/zima/projekty/blogweb/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/images/chevron-right.svg");
    }
}
