<?php

/* WebProfilerBundle:Profiler:info.html.twig */
class __TwigTemplate_86568bd96120fd2ed840ccbd48a448ad4ddbe8c7b80d3da3cf6cc18d6e1af425 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "WebProfilerBundle:Profiler:info.html.twig", 1);
        $this->blocks = array(
            'summary' => array($this, 'block_summary'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3abb5bb9a4e69517e8e32ef3f65b9d6775f11be96e479679c6aaac97a9a8a812 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3abb5bb9a4e69517e8e32ef3f65b9d6775f11be96e479679c6aaac97a9a8a812->enter($__internal_3abb5bb9a4e69517e8e32ef3f65b9d6775f11be96e479679c6aaac97a9a8a812_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:info.html.twig"));

        $__internal_2f7be354be0574d1c2cec1d08eb8a9ae288feed523938662339210cad24e6bb6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2f7be354be0574d1c2cec1d08eb8a9ae288feed523938662339210cad24e6bb6->enter($__internal_2f7be354be0574d1c2cec1d08eb8a9ae288feed523938662339210cad24e6bb6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:info.html.twig"));

        // line 3
        $context["messages"] = array("no_token" => array("status" => "error", "title" => (((((        // line 6
array_key_exists("token", $context)) ? (_twig_default_filter(($context["token"] ?? $this->getContext($context, "token")), "")) : ("")) == "latest")) ? ("There are no profiles") : ("Token not found")), "message" => (((((        // line 7
array_key_exists("token", $context)) ? (_twig_default_filter(($context["token"] ?? $this->getContext($context, "token")), "")) : ("")) == "latest")) ? ("No profiles found in the database.") : ((("Token \"" . ((array_key_exists("token", $context)) ? (_twig_default_filter(($context["token"] ?? $this->getContext($context, "token")), "")) : (""))) . "\" was not found in the database.")))));
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_3abb5bb9a4e69517e8e32ef3f65b9d6775f11be96e479679c6aaac97a9a8a812->leave($__internal_3abb5bb9a4e69517e8e32ef3f65b9d6775f11be96e479679c6aaac97a9a8a812_prof);

        
        $__internal_2f7be354be0574d1c2cec1d08eb8a9ae288feed523938662339210cad24e6bb6->leave($__internal_2f7be354be0574d1c2cec1d08eb8a9ae288feed523938662339210cad24e6bb6_prof);

    }

    // line 11
    public function block_summary($context, array $blocks = array())
    {
        $__internal_909adda24b920ebec7720ddb1c0b8f2952bdc05615b83ab4e9b48cbfde631489 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_909adda24b920ebec7720ddb1c0b8f2952bdc05615b83ab4e9b48cbfde631489->enter($__internal_909adda24b920ebec7720ddb1c0b8f2952bdc05615b83ab4e9b48cbfde631489_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "summary"));

        $__internal_103333e5ddae2eb17897cd133671aad3c780e620ce003504ff169acfc4d0e336 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_103333e5ddae2eb17897cd133671aad3c780e620ce003504ff169acfc4d0e336->enter($__internal_103333e5ddae2eb17897cd133671aad3c780e620ce003504ff169acfc4d0e336_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "summary"));

        // line 12
        echo "    <div class=\"status status-";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["messages"] ?? $this->getContext($context, "messages")), ($context["about"] ?? $this->getContext($context, "about")), array(), "array"), "status", array()), "html", null, true);
        echo "\">
        <div class=\"container\">
            <h2>";
        // line 14
        echo twig_escape_filter($this->env, twig_title_string_filter($this->env, $this->getAttribute($this->getAttribute(($context["messages"] ?? $this->getContext($context, "messages")), ($context["about"] ?? $this->getContext($context, "about")), array(), "array"), "status", array())), "html", null, true);
        echo "</h2>
        </div>
    </div>
";
        
        $__internal_103333e5ddae2eb17897cd133671aad3c780e620ce003504ff169acfc4d0e336->leave($__internal_103333e5ddae2eb17897cd133671aad3c780e620ce003504ff169acfc4d0e336_prof);

        
        $__internal_909adda24b920ebec7720ddb1c0b8f2952bdc05615b83ab4e9b48cbfde631489->leave($__internal_909adda24b920ebec7720ddb1c0b8f2952bdc05615b83ab4e9b48cbfde631489_prof);

    }

    // line 19
    public function block_panel($context, array $blocks = array())
    {
        $__internal_8172b1d473aa2b2dbb15ebe9b40b45ff40af390ef520c7b2e1dd6906436f5b3b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8172b1d473aa2b2dbb15ebe9b40b45ff40af390ef520c7b2e1dd6906436f5b3b->enter($__internal_8172b1d473aa2b2dbb15ebe9b40b45ff40af390ef520c7b2e1dd6906436f5b3b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_aa50f96778485be40f3eb9bb09e17c09cddbb8037a6122b004dd49f18f52bd45 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_aa50f96778485be40f3eb9bb09e17c09cddbb8037a6122b004dd49f18f52bd45->enter($__internal_aa50f96778485be40f3eb9bb09e17c09cddbb8037a6122b004dd49f18f52bd45_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 20
        echo "    <h2>";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["messages"] ?? $this->getContext($context, "messages")), ($context["about"] ?? $this->getContext($context, "about")), array(), "array"), "title", array()), "html", null, true);
        echo "</h2>
    <p>";
        // line 21
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["messages"] ?? $this->getContext($context, "messages")), ($context["about"] ?? $this->getContext($context, "about")), array(), "array"), "message", array()), "html", null, true);
        echo "</p>
";
        
        $__internal_aa50f96778485be40f3eb9bb09e17c09cddbb8037a6122b004dd49f18f52bd45->leave($__internal_aa50f96778485be40f3eb9bb09e17c09cddbb8037a6122b004dd49f18f52bd45_prof);

        
        $__internal_8172b1d473aa2b2dbb15ebe9b40b45ff40af390ef520c7b2e1dd6906436f5b3b->leave($__internal_8172b1d473aa2b2dbb15ebe9b40b45ff40af390ef520c7b2e1dd6906436f5b3b_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:info.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  89 => 21,  84 => 20,  75 => 19,  61 => 14,  55 => 12,  46 => 11,  36 => 1,  34 => 7,  33 => 6,  32 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% set messages = {
    'no_token' : {
        status:  'error',
        title:   (token|default('') == 'latest') ? 'There are no profiles' : 'Token not found',
        message: (token|default('') == 'latest') ? 'No profiles found in the database.' : 'Token \"' ~ token|default('') ~ '\" was not found in the database.'
    }
} %}

{% block summary %}
    <div class=\"status status-{{ messages[about].status }}\">
        <div class=\"container\">
            <h2>{{ messages[about].status|title }}</h2>
        </div>
    </div>
{% endblock %}

{% block panel %}
    <h2>{{ messages[about].title }}</h2>
    <p>{{ messages[about].message }}</p>
{% endblock %}
", "WebProfilerBundle:Profiler:info.html.twig", "/Users/zima/projekty/blogweb/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Profiler/info.html.twig");
    }
}
