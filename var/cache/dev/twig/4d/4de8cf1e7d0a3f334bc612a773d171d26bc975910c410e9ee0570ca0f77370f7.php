<?php

/* TwigBundle:Exception:error.rdf.twig */
class __TwigTemplate_5b92fb1659f8831b633739cf3ef8e677d72b1bd6441d1d1ca2350a6c6688bfc8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_61f12fd050af71dc37a9b3dbc73f5214e3579052db1bfc2bcc41fa0cc19eb830 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_61f12fd050af71dc37a9b3dbc73f5214e3579052db1bfc2bcc41fa0cc19eb830->enter($__internal_61f12fd050af71dc37a9b3dbc73f5214e3579052db1bfc2bcc41fa0cc19eb830_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.rdf.twig"));

        $__internal_af0b35c5ba09ce035c0805691f8bc87f2e2ce646389b8b86c58cfc0bfe072e8a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_af0b35c5ba09ce035c0805691f8bc87f2e2ce646389b8b86c58cfc0bfe072e8a->enter($__internal_af0b35c5ba09ce035c0805691f8bc87f2e2ce646389b8b86c58cfc0bfe072e8a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.rdf.twig"));

        // line 1
        echo twig_include($this->env, $context, "@Twig/Exception/error.xml.twig");
        echo "
";
        
        $__internal_61f12fd050af71dc37a9b3dbc73f5214e3579052db1bfc2bcc41fa0cc19eb830->leave($__internal_61f12fd050af71dc37a9b3dbc73f5214e3579052db1bfc2bcc41fa0cc19eb830_prof);

        
        $__internal_af0b35c5ba09ce035c0805691f8bc87f2e2ce646389b8b86c58cfc0bfe072e8a->leave($__internal_af0b35c5ba09ce035c0805691f8bc87f2e2ce646389b8b86c58cfc0bfe072e8a_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.rdf.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ include('@Twig/Exception/error.xml.twig') }}
", "TwigBundle:Exception:error.rdf.twig", "/Users/zima/projekty/blogweb/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/error.rdf.twig");
    }
}
