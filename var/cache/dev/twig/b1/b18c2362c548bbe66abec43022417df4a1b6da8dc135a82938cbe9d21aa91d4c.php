<?php

/* @Framework/Form/search_widget.html.php */
class __TwigTemplate_71df56cd5170cb60fea0a461c71c652333deb15741666deb53233cf62b068473 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d01f226bdcfccb7ca03019c883e1046c65ee5795b3a876af751b04fb6f2c081e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d01f226bdcfccb7ca03019c883e1046c65ee5795b3a876af751b04fb6f2c081e->enter($__internal_d01f226bdcfccb7ca03019c883e1046c65ee5795b3a876af751b04fb6f2c081e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/search_widget.html.php"));

        $__internal_bcb35c20eed817165bcf5a2cd4166befe55f680b79ba47c9579e931c143c37df = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bcb35c20eed817165bcf5a2cd4166befe55f680b79ba47c9579e931c143c37df->enter($__internal_bcb35c20eed817165bcf5a2cd4166befe55f680b79ba47c9579e931c143c37df_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/search_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'search')) ?>
";
        
        $__internal_d01f226bdcfccb7ca03019c883e1046c65ee5795b3a876af751b04fb6f2c081e->leave($__internal_d01f226bdcfccb7ca03019c883e1046c65ee5795b3a876af751b04fb6f2c081e_prof);

        
        $__internal_bcb35c20eed817165bcf5a2cd4166befe55f680b79ba47c9579e931c143c37df->leave($__internal_bcb35c20eed817165bcf5a2cd4166befe55f680b79ba47c9579e931c143c37df_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/search_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'search')) ?>
", "@Framework/Form/search_widget.html.php", "/Users/zima/projekty/blogweb/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/search_widget.html.php");
    }
}
