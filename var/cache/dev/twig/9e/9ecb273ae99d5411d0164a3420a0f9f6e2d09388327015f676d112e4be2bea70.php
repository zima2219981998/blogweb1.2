<?php

/* ZimaBlogwebBundle:Blog:contents.html.twig */
class __TwigTemplate_32479f16b94f9321c2aced158aa595301ba6a07c1345975774dc444d8854cdfb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@ZimaBlogweb/base.html.twig", "ZimaBlogwebBundle:Blog:contents.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@ZimaBlogweb/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d1c03b605b3456ab9d7ef7af350c1557f26c8f76ab40383b6ab742e30d116eea = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d1c03b605b3456ab9d7ef7af350c1557f26c8f76ab40383b6ab742e30d116eea->enter($__internal_d1c03b605b3456ab9d7ef7af350c1557f26c8f76ab40383b6ab742e30d116eea_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "ZimaBlogwebBundle:Blog:contents.html.twig"));

        $__internal_20eca0efab7c2624ec34e1bf03bef21894fb43c73c836cc1de0b4969ebf20385 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_20eca0efab7c2624ec34e1bf03bef21894fb43c73c836cc1de0b4969ebf20385->enter($__internal_20eca0efab7c2624ec34e1bf03bef21894fb43c73c836cc1de0b4969ebf20385_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "ZimaBlogwebBundle:Blog:contents.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_d1c03b605b3456ab9d7ef7af350c1557f26c8f76ab40383b6ab742e30d116eea->leave($__internal_d1c03b605b3456ab9d7ef7af350c1557f26c8f76ab40383b6ab742e30d116eea_prof);

        
        $__internal_20eca0efab7c2624ec34e1bf03bef21894fb43c73c836cc1de0b4969ebf20385->leave($__internal_20eca0efab7c2624ec34e1bf03bef21894fb43c73c836cc1de0b4969ebf20385_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_c899455e5eb2bb4a0b6e51d2b3d3c23c59d75e52f249361a4b4d1d5f39e76f33 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c899455e5eb2bb4a0b6e51d2b3d3c23c59d75e52f249361a4b4d1d5f39e76f33->enter($__internal_c899455e5eb2bb4a0b6e51d2b3d3c23c59d75e52f249361a4b4d1d5f39e76f33_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_a1f4299015a3b38a53f86938cdffcd8f53615dc3a61ed667935a125b3cb4e5fa = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a1f4299015a3b38a53f86938cdffcd8f53615dc3a61ed667935a125b3cb4e5fa->enter($__internal_a1f4299015a3b38a53f86938cdffcd8f53615dc3a61ed667935a125b3cb4e5fa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "
    ";
        // line 6
        echo "        ";
        // line 7
        echo "            ";
        // line 8
        echo "                ";
        // line 9
        echo "                    ";
        // line 10
        echo "                        ";
        // line 11
        echo "                        ";
        // line 12
        echo "                    ";
        // line 13
        echo "                ";
        // line 14
        echo "            ";
        // line 15
        echo "        ";
        // line 16
        echo "    ";
        // line 17
        echo "
    <div class=\"well\">
        <p style=\"font-size: 25px;\">";
        // line 19
        echo twig_escape_filter($this->env, $this->getAttribute(($context["post"] ?? $this->getContext($context, "post")), "title", array()), "html", null, true);
        echo "</p>
        <p>wrote: <i>";
        // line 20
        echo twig_escape_filter($this->env, $this->getAttribute(($context["post"] ?? $this->getContext($context, "post")), "owner", array()), "html", null, true);
        echo "</i> ";
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute(($context["post"] ?? $this->getContext($context, "post")), "createdAt", array()), "Y-m-d H:i"), "html", null, true);
        echo " </p> <hr>
        ";
        // line 21
        echo $this->getAttribute(($context["post"] ?? $this->getContext($context, "post")), "contents", array());
        echo "
    </div>

    <hr>
    <h3>Comments:</h3>


    ";
        // line 28
        if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("ROLE_USER")) {
            // line 29
            echo "        ";
            echo             $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["commentForm"] ?? $this->getContext($context, "commentForm")), 'form_start', array("attr" => array("novalidate" => "novalidate")));
            echo "
        <div class=\"form-group\">
            ";
            // line 31
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["commentForm"] ?? $this->getContext($context, "commentForm")), "comment", array()), 'widget', array("attr" => array("placeholder" => "Write a comment")));
            echo "
        </div>
        ";
            // line 33
            echo             $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["commentForm"] ?? $this->getContext($context, "commentForm")), 'form_end');
            echo " <br>
    ";
        }
        // line 35
        echo "
    ";
        // line 37
        echo "    ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["selectcomments"] ?? $this->getContext($context, "selectcomments")));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["_key"] => $context["comment"]) {
            // line 38
            echo "        <div class=\"well well-sm\">
            <p><b>";
            // line 39
            echo twig_escape_filter($this->env, $this->getAttribute($context["comment"], "owner", array()), "html", null, true);
            echo "</b> (";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["comment"], "createdAt", array()), "Y-m-d H:i"), "html", null, true);
            echo ")</p>
            <p>";
            // line 40
            echo twig_escape_filter($this->env, $this->getAttribute($context["comment"], "comment", array()), "html", null, true);
            echo "</p>
            <a class=\"btn btn-danger\" href=\"";
            // line 41
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("blog_del_comment", array("id" => $this->getAttribute($context["comment"], "id", array()))), "html", null, true);
            echo "\">X</a>
        </div>
    ";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 44
            echo "        <div class=\"well\">
            <p>Brak komentarzy</p>
        </div>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['comment'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 48
        echo "

";
        
        $__internal_a1f4299015a3b38a53f86938cdffcd8f53615dc3a61ed667935a125b3cb4e5fa->leave($__internal_a1f4299015a3b38a53f86938cdffcd8f53615dc3a61ed667935a125b3cb4e5fa_prof);

        
        $__internal_c899455e5eb2bb4a0b6e51d2b3d3c23c59d75e52f249361a4b4d1d5f39e76f33->leave($__internal_c899455e5eb2bb4a0b6e51d2b3d3c23c59d75e52f249361a4b4d1d5f39e76f33_prof);

    }

    public function getTemplateName()
    {
        return "ZimaBlogwebBundle:Blog:contents.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  155 => 48,  146 => 44,  138 => 41,  134 => 40,  128 => 39,  125 => 38,  119 => 37,  116 => 35,  111 => 33,  106 => 31,  100 => 29,  98 => 28,  88 => 21,  82 => 20,  78 => 19,  74 => 17,  72 => 16,  70 => 15,  68 => 14,  66 => 13,  64 => 12,  62 => 11,  60 => 10,  58 => 9,  56 => 8,  54 => 7,  52 => 6,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@ZimaBlogweb/base.html.twig' %}

{% block body %}

    {#{% if is_granted(app.user.username) %}#}
        {#<nav class=\"navbar navbar-default \">#}
            {#<div class=\"container-fluid\">#}
                {#<div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-1\">#}
                    {#<ul class=\"nav navbar-nav\">#}
                        {#<li class=\"active\"><a href=\"{{ path('blog_edit', {\"id\": post.id}) }}\">Edit</a></li>#}
                        {#<li><a href=\"{{ path('blog_delete', {\"id\": post.id}) }}\">Delete</a></li>#}
                    {#</ul>#}
                {#</div>#}
            {#</div>#}
        {#</nav>#}
    {#{% endif %}#}

    <div class=\"well\">
        <p style=\"font-size: 25px;\">{{ post.title }}</p>
        <p>wrote: <i>{{ post.owner }}</i> {{ post.createdAt|date('Y-m-d H:i') }} </p> <hr>
        {{ post.contents|raw }}
    </div>

    <hr>
    <h3>Comments:</h3>


    {% if is_granted(\"ROLE_USER\") %}
        {{ form_start(commentForm, {\"attr\": {\"novalidate\": \"novalidate\"}}) }}
        <div class=\"form-group\">
            {{ form_widget(commentForm.comment, {'attr': {'placeholder': 'Write a comment'}}) }}
        </div>
        {{ form_end(commentForm) }} <br>
    {% endif %}

    {#wyswietlenie komentarza#}
    {% for comment in selectcomments %}
        <div class=\"well well-sm\">
            <p><b>{{ comment.owner }}</b> ({{ comment.createdAt|date('Y-m-d H:i') }})</p>
            <p>{{ comment.comment }}</p>
            <a class=\"btn btn-danger\" href=\"{{ path('blog_del_comment', {\"id\": comment.id}) }}\">X</a>
        </div>
    {% else %}
        <div class=\"well\">
            <p>Brak komentarzy</p>
        </div>
    {% endfor %}


{% endblock %}", "ZimaBlogwebBundle:Blog:contents.html.twig", "/Users/zima/projekty/blogweb/src/Zima/BlogwebBundle/Resources/views/Blog/contents.html.twig");
    }
}
