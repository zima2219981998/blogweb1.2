<?php

/* FOSUserBundle:Registration:check_email.html.twig */
class __TwigTemplate_9218689f82429f39eb46543b31084343caff5ee951631e8ffbe38f4210f0bfa5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Registration:check_email.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a9cff4115277a1ec51afe5feb79234b1e750ce60f760eed1ec32add680f86aff = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a9cff4115277a1ec51afe5feb79234b1e750ce60f760eed1ec32add680f86aff->enter($__internal_a9cff4115277a1ec51afe5feb79234b1e750ce60f760eed1ec32add680f86aff_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:check_email.html.twig"));

        $__internal_605840bc7ded2b2e3356d4410cb78aecdd5cc903adab1de3aead8d9eafd22ffb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_605840bc7ded2b2e3356d4410cb78aecdd5cc903adab1de3aead8d9eafd22ffb->enter($__internal_605840bc7ded2b2e3356d4410cb78aecdd5cc903adab1de3aead8d9eafd22ffb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:check_email.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_a9cff4115277a1ec51afe5feb79234b1e750ce60f760eed1ec32add680f86aff->leave($__internal_a9cff4115277a1ec51afe5feb79234b1e750ce60f760eed1ec32add680f86aff_prof);

        
        $__internal_605840bc7ded2b2e3356d4410cb78aecdd5cc903adab1de3aead8d9eafd22ffb->leave($__internal_605840bc7ded2b2e3356d4410cb78aecdd5cc903adab1de3aead8d9eafd22ffb_prof);

    }

    // line 5
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_fcb018eca51ee44b91d79f5921d6fde3c99c332939e2cf1499f7110642952027 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fcb018eca51ee44b91d79f5921d6fde3c99c332939e2cf1499f7110642952027->enter($__internal_fcb018eca51ee44b91d79f5921d6fde3c99c332939e2cf1499f7110642952027_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_982de56cdf626a0dcf0d1b0426d6e2cf076734a33166f639e43fe4fba828f4f1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_982de56cdf626a0dcf0d1b0426d6e2cf076734a33166f639e43fe4fba828f4f1->enter($__internal_982de56cdf626a0dcf0d1b0426d6e2cf076734a33166f639e43fe4fba828f4f1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 6
        echo "    <p>";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.check_email", array("%email%" => $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "email", array())), "FOSUserBundle"), "html", null, true);
        echo "</p>
";
        
        $__internal_982de56cdf626a0dcf0d1b0426d6e2cf076734a33166f639e43fe4fba828f4f1->leave($__internal_982de56cdf626a0dcf0d1b0426d6e2cf076734a33166f639e43fe4fba828f4f1_prof);

        
        $__internal_fcb018eca51ee44b91d79f5921d6fde3c99c332939e2cf1499f7110642952027->leave($__internal_fcb018eca51ee44b91d79f5921d6fde3c99c332939e2cf1499f7110642952027_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:check_email.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 6,  40 => 5,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% trans_default_domain 'FOSUserBundle' %}

{% block fos_user_content %}
    <p>{{ 'registration.check_email'|trans({'%email%': user.email}) }}</p>
{% endblock fos_user_content %}
", "FOSUserBundle:Registration:check_email.html.twig", "/Users/zima/projekty/blogweb/app/Resources/FOSUserBundle/views/Registration/check_email.html.twig");
    }
}
