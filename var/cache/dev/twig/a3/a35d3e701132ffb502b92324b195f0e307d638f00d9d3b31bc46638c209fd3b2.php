<?php

/* @Framework/Form/form_widget_compound.html.php */
class __TwigTemplate_a0ec00fc433752471aab829bbcdb5151610722fd7663842dc0e693540880731f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a5b94ac6449bf4cd9fca40c730cb0d9490044c68f60dc6452e8fec665efd7b11 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a5b94ac6449bf4cd9fca40c730cb0d9490044c68f60dc6452e8fec665efd7b11->enter($__internal_a5b94ac6449bf4cd9fca40c730cb0d9490044c68f60dc6452e8fec665efd7b11_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget_compound.html.php"));

        $__internal_e0ef1092f6111f72843fc109ac57043f071b1a72e08c98fc83532fe3a4eb1709 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e0ef1092f6111f72843fc109ac57043f071b1a72e08c98fc83532fe3a4eb1709->enter($__internal_e0ef1092f6111f72843fc109ac57043f071b1a72e08c98fc83532fe3a4eb1709_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget_compound.html.php"));

        // line 1
        echo "<div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
    <?php if (!\$form->parent && \$errors): ?>
    <?php echo \$view['form']->errors(\$form) ?>
    <?php endif ?>
    <?php echo \$view['form']->block(\$form, 'form_rows') ?>
    <?php echo \$view['form']->rest(\$form) ?>
</div>
";
        
        $__internal_a5b94ac6449bf4cd9fca40c730cb0d9490044c68f60dc6452e8fec665efd7b11->leave($__internal_a5b94ac6449bf4cd9fca40c730cb0d9490044c68f60dc6452e8fec665efd7b11_prof);

        
        $__internal_e0ef1092f6111f72843fc109ac57043f071b1a72e08c98fc83532fe3a4eb1709->leave($__internal_e0ef1092f6111f72843fc109ac57043f071b1a72e08c98fc83532fe3a4eb1709_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_widget_compound.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
    <?php if (!\$form->parent && \$errors): ?>
    <?php echo \$view['form']->errors(\$form) ?>
    <?php endif ?>
    <?php echo \$view['form']->block(\$form, 'form_rows') ?>
    <?php echo \$view['form']->rest(\$form) ?>
</div>
", "@Framework/Form/form_widget_compound.html.php", "/Users/zima/projekty/blogweb/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_widget_compound.html.php");
    }
}
