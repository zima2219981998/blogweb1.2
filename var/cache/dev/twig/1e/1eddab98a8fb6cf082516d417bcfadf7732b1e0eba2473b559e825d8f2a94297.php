<?php

/* FOSUserBundle:Registration:confirmed.html.twig */
class __TwigTemplate_b26100b875bb0ae0cd384b42641ef9e9ef71d9c56683ef3d9e354fb6ecf06695 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Registration:confirmed.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0d158b385dee617f987b66a4e1ac29354a33368032416be5b7c09160ccb73559 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0d158b385dee617f987b66a4e1ac29354a33368032416be5b7c09160ccb73559->enter($__internal_0d158b385dee617f987b66a4e1ac29354a33368032416be5b7c09160ccb73559_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:confirmed.html.twig"));

        $__internal_3f2436dde3d5e39687b557bb1279247b9abfe4abba4ed0bb8c27f2bdc621892c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3f2436dde3d5e39687b557bb1279247b9abfe4abba4ed0bb8c27f2bdc621892c->enter($__internal_3f2436dde3d5e39687b557bb1279247b9abfe4abba4ed0bb8c27f2bdc621892c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:confirmed.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_0d158b385dee617f987b66a4e1ac29354a33368032416be5b7c09160ccb73559->leave($__internal_0d158b385dee617f987b66a4e1ac29354a33368032416be5b7c09160ccb73559_prof);

        
        $__internal_3f2436dde3d5e39687b557bb1279247b9abfe4abba4ed0bb8c27f2bdc621892c->leave($__internal_3f2436dde3d5e39687b557bb1279247b9abfe4abba4ed0bb8c27f2bdc621892c_prof);

    }

    // line 5
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_15d582dc5f2f1056c12bc0c411be987c1a462ee9cbf042508721946086442126 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_15d582dc5f2f1056c12bc0c411be987c1a462ee9cbf042508721946086442126->enter($__internal_15d582dc5f2f1056c12bc0c411be987c1a462ee9cbf042508721946086442126_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_438f745408fca3361992967ac41219244f7be32a0dfb9877d65a31a68c32ade7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_438f745408fca3361992967ac41219244f7be32a0dfb9877d65a31a68c32ade7->enter($__internal_438f745408fca3361992967ac41219244f7be32a0dfb9877d65a31a68c32ade7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 6
        echo "    <h2 style=\"font-size: 30px;\">Congratulation</h2>
    <p style=\"font-size: 22px;\">Etap 1 - Create an account - Completed</p>
    <a href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("blog_settings", array("id" => $this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "user", array()), "id", array()))), "html", null, true);
        echo "\"><p style=\"font-size: 22px;\">Now add some informations about yourself and write a blog</p></a>


    ";
        // line 12
        echo "    ";
        // line 13
        echo "    ";
        
        $__internal_438f745408fca3361992967ac41219244f7be32a0dfb9877d65a31a68c32ade7->leave($__internal_438f745408fca3361992967ac41219244f7be32a0dfb9877d65a31a68c32ade7_prof);

        
        $__internal_15d582dc5f2f1056c12bc0c411be987c1a462ee9cbf042508721946086442126->leave($__internal_15d582dc5f2f1056c12bc0c411be987c1a462ee9cbf042508721946086442126_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:confirmed.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  61 => 13,  59 => 12,  53 => 8,  49 => 6,  40 => 5,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% trans_default_domain 'FOSUserBundle' %}

{% block fos_user_content %}
    <h2 style=\"font-size: 30px;\">Congratulation</h2>
    <p style=\"font-size: 22px;\">Etap 1 - Create an account - Completed</p>
    <a href=\"{{ path('blog_settings', {\"id\": app.user.id}) }}\"><p style=\"font-size: 22px;\">Now add some informations about yourself and write a blog</p></a>


    {#{% if targetUrl %}#}
    {#<p><a href=\"{{ targetUrl }}\">{{ 'registration.back'|trans }}</a></p>#}
    {#{% endif %}#}
{% endblock fos_user_content %}
", "FOSUserBundle:Registration:confirmed.html.twig", "/Users/zima/projekty/blogweb/app/Resources/FOSUserBundle/views/Registration/confirmed.html.twig");
    }
}
