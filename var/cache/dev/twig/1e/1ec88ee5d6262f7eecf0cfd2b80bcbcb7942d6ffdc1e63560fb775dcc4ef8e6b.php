<?php

/* TwigBundle:Exception:error.json.twig */
class __TwigTemplate_74645607508071a8ad718b198b7fd4136146e4282586a630f4a111e614519cc2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_69a3d35260f3d929780f6ac302336f621cfe80a5fe4ea4648620170f80009f35 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_69a3d35260f3d929780f6ac302336f621cfe80a5fe4ea4648620170f80009f35->enter($__internal_69a3d35260f3d929780f6ac302336f621cfe80a5fe4ea4648620170f80009f35_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.json.twig"));

        $__internal_4f765e6151ef2a38af712ec3570008efd5f7c730676b8ad0aa3ddca09b81cf9b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4f765e6151ef2a38af712ec3570008efd5f7c730676b8ad0aa3ddca09b81cf9b->enter($__internal_4f765e6151ef2a38af712ec3570008efd5f7c730676b8ad0aa3ddca09b81cf9b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.json.twig"));

        // line 1
        echo twig_jsonencode_filter(array("error" => array("code" => ($context["status_code"] ?? $this->getContext($context, "status_code")), "message" => ($context["status_text"] ?? $this->getContext($context, "status_text")))));
        echo "
";
        
        $__internal_69a3d35260f3d929780f6ac302336f621cfe80a5fe4ea4648620170f80009f35->leave($__internal_69a3d35260f3d929780f6ac302336f621cfe80a5fe4ea4648620170f80009f35_prof);

        
        $__internal_4f765e6151ef2a38af712ec3570008efd5f7c730676b8ad0aa3ddca09b81cf9b->leave($__internal_4f765e6151ef2a38af712ec3570008efd5f7c730676b8ad0aa3ddca09b81cf9b_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.json.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ { 'error': { 'code': status_code, 'message': status_text } }|json_encode|raw }}
", "TwigBundle:Exception:error.json.twig", "/Users/zima/projekty/blogweb/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/error.json.twig");
    }
}
