<?php

/* ZimaBlogwebBundle:Blog:userBlog.html.twig */
class __TwigTemplate_32a0c36c4356464532ef1a239f484d94604b379ba8d21061e38799a1a36569cf extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@ZimaBlogweb/base.html.twig", "ZimaBlogwebBundle:Blog:userBlog.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@ZimaBlogweb/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5ceeb6512babd8de70bb0a2fff7d8243cc1abf2855f41f9ec7998a38e5048b29 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5ceeb6512babd8de70bb0a2fff7d8243cc1abf2855f41f9ec7998a38e5048b29->enter($__internal_5ceeb6512babd8de70bb0a2fff7d8243cc1abf2855f41f9ec7998a38e5048b29_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "ZimaBlogwebBundle:Blog:userBlog.html.twig"));

        $__internal_dfd5530594d0e46561876313eed7e63f6e40983bf818c97ca12487d458867f7f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_dfd5530594d0e46561876313eed7e63f6e40983bf818c97ca12487d458867f7f->enter($__internal_dfd5530594d0e46561876313eed7e63f6e40983bf818c97ca12487d458867f7f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "ZimaBlogwebBundle:Blog:userBlog.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_5ceeb6512babd8de70bb0a2fff7d8243cc1abf2855f41f9ec7998a38e5048b29->leave($__internal_5ceeb6512babd8de70bb0a2fff7d8243cc1abf2855f41f9ec7998a38e5048b29_prof);

        
        $__internal_dfd5530594d0e46561876313eed7e63f6e40983bf818c97ca12487d458867f7f->leave($__internal_dfd5530594d0e46561876313eed7e63f6e40983bf818c97ca12487d458867f7f_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_5aa6e16c53d3a0c68e7509641f39c499c6e438cb243dab86538bd42520831c2d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5aa6e16c53d3a0c68e7509641f39c499c6e438cb243dab86538bd42520831c2d->enter($__internal_5aa6e16c53d3a0c68e7509641f39c499c6e438cb243dab86538bd42520831c2d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_48477783939399d15bc331825f35293e48fa399be981234d66cdf4baee565505 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_48477783939399d15bc331825f35293e48fa399be981234d66cdf4baee565505->enter($__internal_48477783939399d15bc331825f35293e48fa399be981234d66cdf4baee565505_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 4
        echo "    ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["infoAboutUser"] ?? $this->getContext($context, "infoAboutUser")));
        foreach ($context['_seq'] as $context["_key"] => $context["info"]) {
            // line 5
            echo "        BlogWEB|";
            echo twig_escape_filter($this->env, $this->getAttribute($context["info"], "username", array()), "html", null, true);
            echo "
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['info'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        
        $__internal_48477783939399d15bc331825f35293e48fa399be981234d66cdf4baee565505->leave($__internal_48477783939399d15bc331825f35293e48fa399be981234d66cdf4baee565505_prof);

        
        $__internal_5aa6e16c53d3a0c68e7509641f39c499c6e438cb243dab86538bd42520831c2d->leave($__internal_5aa6e16c53d3a0c68e7509641f39c499c6e438cb243dab86538bd42520831c2d_prof);

    }

    // line 9
    public function block_body($context, array $blocks = array())
    {
        $__internal_b4826f0956a0832d0def1e3ae4a24431ae4c44458f1d5c94266cbbd2665f7216 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b4826f0956a0832d0def1e3ae4a24431ae4c44458f1d5c94266cbbd2665f7216->enter($__internal_b4826f0956a0832d0def1e3ae4a24431ae4c44458f1d5c94266cbbd2665f7216_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_67d0aa77caff8f9a385244e503b84d9f40ebdd7a5c838d0a25a623930c60c3da = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_67d0aa77caff8f9a385244e503b84d9f40ebdd7a5c838d0a25a623930c60c3da->enter($__internal_67d0aa77caff8f9a385244e503b84d9f40ebdd7a5c838d0a25a623930c60c3da_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 10
        echo "
    <div class=\"col-lg-8 col-md-8 col-xs-12\">
        ";
        // line 12
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["findContents"] ?? $this->getContext($context, "findContents")));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["_key"] => $context["content"]) {
            // line 13
            echo "                <div class=\"well\">
                    <center><p style=\"font-size: 25px;\">";
            // line 14
            echo twig_escape_filter($this->env, $this->getAttribute($context["content"], "title", array()), "html", null, true);
            echo "</p></center>
                    <p>";
            // line 15
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["content"], "createdAt", array()), "Y-m-d H:i"), "html", null, true);
            echo "</p> <hr>
                    <p><b>";
            // line 16
            echo twig_escape_filter($this->env, $this->getAttribute($context["content"], "shortdescription", array()), "html", null, true);
            echo "</b></p>
                    <a href=\"";
            // line 17
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("blog_content", array("id" => $this->getAttribute($context["content"], "id", array()))), "html", null, true);
            echo "\">More…</a>
                </div>
        ";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 20
            echo "            <div class=\"col-xs-12 col-sm-7 col-md-9\">
                <div class=\"well\">
                    <center> <h3>You don't have any contents</h3> </center>
                </div>
            </div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['content'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 26
        echo "        <div class=\"navigation text-center\">
            ";
        // line 27
        echo $this->env->getExtension('Knp\Bundle\PaginatorBundle\Twig\Extension\PaginationExtension')->render($this->env, ($context["findContents"] ?? $this->getContext($context, "findContents")));
        echo "
        </div>
    </div>

    <div class=\"col-lg-4 col-md-4 col-xs-12\">
        <div class=\"panel panel-default\">
            <div class=\"panel-body\">
                ";
        // line 34
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["infoAboutUser"] ?? $this->getContext($context, "infoAboutUser")));
        foreach ($context['_seq'] as $context["_key"] => $context["info"]) {
            // line 35
            echo "                    <h3>";
            echo twig_escape_filter($this->env, $this->getAttribute($context["info"], "username", array()), "html", null, true);
            echo "</h3>
                    <p>";
            // line 36
            echo twig_escape_filter($this->env, $this->getAttribute($context["info"], "fullname", array()), "html", null, true);
            echo "</p>
                    <p>";
            // line 37
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["info"], "birthday", array()), "Y-m-d"), "html", null, true);
            echo "</p>
                    <p>";
            // line 38
            echo twig_escape_filter($this->env, $this->getAttribute($context["info"], "interests", array()), "html", null, true);
            echo "</p>
                    <p>";
            // line 39
            echo twig_escape_filter($this->env, $this->getAttribute($context["info"], "aboutme", array()), "html", null, true);
            echo "</p>
                    <a href=\"";
            // line 40
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("blog_add_friend", array("id" => $this->getAttribute($context["info"], "id", array()))), "html", null, true);
            echo "\">Follow</a>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['info'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 42
        echo "            </div>
        </div>
    </div>


";
        
        $__internal_67d0aa77caff8f9a385244e503b84d9f40ebdd7a5c838d0a25a623930c60c3da->leave($__internal_67d0aa77caff8f9a385244e503b84d9f40ebdd7a5c838d0a25a623930c60c3da_prof);

        
        $__internal_b4826f0956a0832d0def1e3ae4a24431ae4c44458f1d5c94266cbbd2665f7216->leave($__internal_b4826f0956a0832d0def1e3ae4a24431ae4c44458f1d5c94266cbbd2665f7216_prof);

    }

    public function getTemplateName()
    {
        return "ZimaBlogwebBundle:Blog:userBlog.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  170 => 42,  162 => 40,  158 => 39,  154 => 38,  150 => 37,  146 => 36,  141 => 35,  137 => 34,  127 => 27,  124 => 26,  113 => 20,  105 => 17,  101 => 16,  97 => 15,  93 => 14,  90 => 13,  85 => 12,  81 => 10,  72 => 9,  55 => 5,  50 => 4,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@ZimaBlogweb/base.html.twig' %}

{% block title %}
    {% for info in infoAboutUser %}
        BlogWEB|{{ info.username }}
    {% endfor %}
{% endblock %}

{% block body %}

    <div class=\"col-lg-8 col-md-8 col-xs-12\">
        {% for content in findContents %}
                <div class=\"well\">
                    <center><p style=\"font-size: 25px;\">{{ content.title }}</p></center>
                    <p>{{ content.createdAt|date('Y-m-d H:i') }}</p> <hr>
                    <p><b>{{ content.shortdescription }}</b></p>
                    <a href=\"{{ path('blog_content', {\"id\": content.id}) }}\">More…</a>
                </div>
        {% else %}
            <div class=\"col-xs-12 col-sm-7 col-md-9\">
                <div class=\"well\">
                    <center> <h3>You don't have any contents</h3> </center>
                </div>
            </div>
        {% endfor %}
        <div class=\"navigation text-center\">
            {{ knp_pagination_render(findContents) }}
        </div>
    </div>

    <div class=\"col-lg-4 col-md-4 col-xs-12\">
        <div class=\"panel panel-default\">
            <div class=\"panel-body\">
                {% for info in infoAboutUser %}
                    <h3>{{ info.username }}</h3>
                    <p>{{ info.fullname }}</p>
                    <p>{{ info.birthday|date(\"Y-m-d\") }}</p>
                    <p>{{ info.interests }}</p>
                    <p>{{ info.aboutme }}</p>
                    <a href=\"{{ path('blog_add_friend', {'id': info.id}) }}\">Follow</a>
                {% endfor %}
            </div>
        </div>
    </div>


{% endblock %}", "ZimaBlogwebBundle:Blog:userBlog.html.twig", "/Users/zima/projekty/blogweb/src/Zima/BlogwebBundle/Resources/views/Blog/userBlog.html.twig");
    }
}
