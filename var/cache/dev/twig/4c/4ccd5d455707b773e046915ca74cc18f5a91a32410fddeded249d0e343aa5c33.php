<?php

/* @Framework/Form/form_end.html.php */
class __TwigTemplate_b0bf302c1292e1f2829d9481ce4baf3ed961f7042152309977f6a96e503e4886 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_fb537d3641f23db2a1a2d7204967394a76564e4f49752cfa2bcdd3fd2e307034 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fb537d3641f23db2a1a2d7204967394a76564e4f49752cfa2bcdd3fd2e307034->enter($__internal_fb537d3641f23db2a1a2d7204967394a76564e4f49752cfa2bcdd3fd2e307034_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_end.html.php"));

        $__internal_36d6cddbfef722291a4b8193f88625681a9659018c1915ef9661ee0b07c36312 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_36d6cddbfef722291a4b8193f88625681a9659018c1915ef9661ee0b07c36312->enter($__internal_36d6cddbfef722291a4b8193f88625681a9659018c1915ef9661ee0b07c36312_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_end.html.php"));

        // line 1
        echo "<?php if (!isset(\$render_rest) || \$render_rest): ?>
<?php echo \$view['form']->rest(\$form) ?>
<?php endif ?>
</form>
";
        
        $__internal_fb537d3641f23db2a1a2d7204967394a76564e4f49752cfa2bcdd3fd2e307034->leave($__internal_fb537d3641f23db2a1a2d7204967394a76564e4f49752cfa2bcdd3fd2e307034_prof);

        
        $__internal_36d6cddbfef722291a4b8193f88625681a9659018c1915ef9661ee0b07c36312->leave($__internal_36d6cddbfef722291a4b8193f88625681a9659018c1915ef9661ee0b07c36312_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_end.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (!isset(\$render_rest) || \$render_rest): ?>
<?php echo \$view['form']->rest(\$form) ?>
<?php endif ?>
</form>
", "@Framework/Form/form_end.html.php", "/Users/zima/projekty/blogweb/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_end.html.php");
    }
}
