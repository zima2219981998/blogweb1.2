<?php

/* FOSUserBundle:Profile:show.html.twig */
class __TwigTemplate_d742d3540b9dd42cff7bbb0c4221cb8871e574444cc75d8dc0b08223b38209b9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Profile:show.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b3384d6eb0772e811efccc9c016433f33c0a1d7105640268cbbc70fd564b8d03 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b3384d6eb0772e811efccc9c016433f33c0a1d7105640268cbbc70fd564b8d03->enter($__internal_b3384d6eb0772e811efccc9c016433f33c0a1d7105640268cbbc70fd564b8d03_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Profile:show.html.twig"));

        $__internal_a3fa1c1bb585b4f4fa82626a526f22610b878a51c7928d8fc815a397ac892bef = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a3fa1c1bb585b4f4fa82626a526f22610b878a51c7928d8fc815a397ac892bef->enter($__internal_a3fa1c1bb585b4f4fa82626a526f22610b878a51c7928d8fc815a397ac892bef_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Profile:show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_b3384d6eb0772e811efccc9c016433f33c0a1d7105640268cbbc70fd564b8d03->leave($__internal_b3384d6eb0772e811efccc9c016433f33c0a1d7105640268cbbc70fd564b8d03_prof);

        
        $__internal_a3fa1c1bb585b4f4fa82626a526f22610b878a51c7928d8fc815a397ac892bef->leave($__internal_a3fa1c1bb585b4f4fa82626a526f22610b878a51c7928d8fc815a397ac892bef_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_f884464fba054dbee30152c9a2f7759157d0a0efd4714daaebf94c9bee829702 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f884464fba054dbee30152c9a2f7759157d0a0efd4714daaebf94c9bee829702->enter($__internal_f884464fba054dbee30152c9a2f7759157d0a0efd4714daaebf94c9bee829702_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_8c7997fb5eefd2d173168d1d0fb256c8103be61ec2a98af4b112e35cc80ba140 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8c7997fb5eefd2d173168d1d0fb256c8103be61ec2a98af4b112e35cc80ba140->enter($__internal_8c7997fb5eefd2d173168d1d0fb256c8103be61ec2a98af4b112e35cc80ba140_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Profile/show_content.html.twig", "FOSUserBundle:Profile:show.html.twig", 4)->display($context);
        
        $__internal_8c7997fb5eefd2d173168d1d0fb256c8103be61ec2a98af4b112e35cc80ba140->leave($__internal_8c7997fb5eefd2d173168d1d0fb256c8103be61ec2a98af4b112e35cc80ba140_prof);

        
        $__internal_f884464fba054dbee30152c9a2f7759157d0a0efd4714daaebf94c9bee829702->leave($__internal_f884464fba054dbee30152c9a2f7759157d0a0efd4714daaebf94c9bee829702_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Profile:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Profile/show_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Profile:show.html.twig", "/Users/zima/projekty/blogweb/app/Resources/FOSUserBundle/views/Profile/show.html.twig");
    }
}
