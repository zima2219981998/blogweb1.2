<?php

/* ZimaBlogwebBundle:Blog:add.html.twig */
class __TwigTemplate_0c0b5d4400db5cd2a96d4784c800a9ce4ad29ee6c05fa31715120bea1b0ddac6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@ZimaBlogweb/base.html.twig", "ZimaBlogwebBundle:Blog:add.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@ZimaBlogweb/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_42c33c83fa90a1412e7547f4c201e3fd4395d7ca8100a089ee2b72684589a640 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_42c33c83fa90a1412e7547f4c201e3fd4395d7ca8100a089ee2b72684589a640->enter($__internal_42c33c83fa90a1412e7547f4c201e3fd4395d7ca8100a089ee2b72684589a640_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "ZimaBlogwebBundle:Blog:add.html.twig"));

        $__internal_cd23b3d3ba3e7f913959b5808c54cf5f2e42a5334b120542f4d49317d8bbf987 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cd23b3d3ba3e7f913959b5808c54cf5f2e42a5334b120542f4d49317d8bbf987->enter($__internal_cd23b3d3ba3e7f913959b5808c54cf5f2e42a5334b120542f4d49317d8bbf987_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "ZimaBlogwebBundle:Blog:add.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_42c33c83fa90a1412e7547f4c201e3fd4395d7ca8100a089ee2b72684589a640->leave($__internal_42c33c83fa90a1412e7547f4c201e3fd4395d7ca8100a089ee2b72684589a640_prof);

        
        $__internal_cd23b3d3ba3e7f913959b5808c54cf5f2e42a5334b120542f4d49317d8bbf987->leave($__internal_cd23b3d3ba3e7f913959b5808c54cf5f2e42a5334b120542f4d49317d8bbf987_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_5a322732c88daa3097e9d0060f12527cbe88e4c5cf9be7e75b627e1059b53365 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5a322732c88daa3097e9d0060f12527cbe88e4c5cf9be7e75b627e1059b53365->enter($__internal_5a322732c88daa3097e9d0060f12527cbe88e4c5cf9be7e75b627e1059b53365_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_46384fe7feeae210e330e2bff883bcd8499112f50a23d947e555f1ac366ec288 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_46384fe7feeae210e330e2bff883bcd8499112f50a23d947e555f1ac366ec288->enter($__internal_46384fe7feeae210e330e2bff883bcd8499112f50a23d947e555f1ac366ec288_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "BlockWEB|Add";
        
        $__internal_46384fe7feeae210e330e2bff883bcd8499112f50a23d947e555f1ac366ec288->leave($__internal_46384fe7feeae210e330e2bff883bcd8499112f50a23d947e555f1ac366ec288_prof);

        
        $__internal_5a322732c88daa3097e9d0060f12527cbe88e4c5cf9be7e75b627e1059b53365->leave($__internal_5a322732c88daa3097e9d0060f12527cbe88e4c5cf9be7e75b627e1059b53365_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_90a4562a78eb8beff15ef6f662998292f77c254d0365cb085b12ee02911e50d8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_90a4562a78eb8beff15ef6f662998292f77c254d0365cb085b12ee02911e50d8->enter($__internal_90a4562a78eb8beff15ef6f662998292f77c254d0365cb085b12ee02911e50d8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_edbdfa800ae3e6ca101b405b61077b567cef1b6dbe4d0986f63f60bdf13830b5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_edbdfa800ae3e6ca101b405b61077b567cef1b6dbe4d0986f63f60bdf13830b5->enter($__internal_edbdfa800ae3e6ca101b405b61077b567cef1b6dbe4d0986f63f60bdf13830b5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <h1>Add Contents</h1>
    ";
        // line 7
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["formPost"] ?? $this->getContext($context, "formPost")), 'form_start', array("attr" => array("novalidate" => "novalidate")));
        echo "
    <div class=\"form-group\">
        ";
        // line 9
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["formPost"] ?? $this->getContext($context, "formPost")), "title", array()), 'widget', array("attr" => array("placeholder" => "Title")));
        echo "
    </div>
    <div class=\"form-group\">
    ";
        // line 12
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["formPost"] ?? $this->getContext($context, "formPost")), "tags", array()), 'widget', array("attr" => array("placeholder" => "tags")));
        echo "
    </div>
    <div class=\"form-group\">
        ";
        // line 15
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["formPost"] ?? $this->getContext($context, "formPost")), "short_description", array()), 'widget', array("attr" => array("placeholder" => "Short description")));
        echo "
    </div>
    <div class=\"form-group\">
    ";
        // line 18
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["formPost"] ?? $this->getContext($context, "formPost")), "contents", array()), 'widget', array("attr" => array("placeholder" => "Contents…")));
        echo "
    </div>
    ";
        // line 20
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["formPost"] ?? $this->getContext($context, "formPost")), 'form_end');
        echo "



";
        
        $__internal_edbdfa800ae3e6ca101b405b61077b567cef1b6dbe4d0986f63f60bdf13830b5->leave($__internal_edbdfa800ae3e6ca101b405b61077b567cef1b6dbe4d0986f63f60bdf13830b5_prof);

        
        $__internal_90a4562a78eb8beff15ef6f662998292f77c254d0365cb085b12ee02911e50d8->leave($__internal_90a4562a78eb8beff15ef6f662998292f77c254d0365cb085b12ee02911e50d8_prof);

    }

    public function getTemplateName()
    {
        return "ZimaBlogwebBundle:Blog:add.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  99 => 20,  94 => 18,  88 => 15,  82 => 12,  76 => 9,  71 => 7,  68 => 6,  59 => 5,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@ZimaBlogweb/base.html.twig' %}

{% block title %}BlockWEB|Add{% endblock %}

{% block body %}
    <h1>Add Contents</h1>
    {{ form_start(formPost, {\"attr\": {\"novalidate\": \"novalidate\"}}) }}
    <div class=\"form-group\">
        {{ form_widget(formPost.title, {'attr': {'placeholder': 'Title'}}) }}
    </div>
    <div class=\"form-group\">
    {{ form_widget(formPost.tags, {'attr': {'placeholder': 'tags'}}) }}
    </div>
    <div class=\"form-group\">
        {{ form_widget(formPost.short_description, {'attr': {'placeholder': 'Short description'}}) }}
    </div>
    <div class=\"form-group\">
    {{ form_widget(formPost.contents, {'attr': {'placeholder': 'Contents…'}}) }}
    </div>
    {{ form_end(formPost) }}



{% endblock %}", "ZimaBlogwebBundle:Blog:add.html.twig", "/Users/zima/projekty/blogweb/src/Zima/BlogwebBundle/Resources/views/Blog/add.html.twig");
    }
}
