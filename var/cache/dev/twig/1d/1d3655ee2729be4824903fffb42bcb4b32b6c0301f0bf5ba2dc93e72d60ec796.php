<?php

/* @Framework/FormTable/form_widget_compound.html.php */
class __TwigTemplate_67511aafb6618249c8ad26d5c048b035fe91b6ac4217e847cee505ccacac3354 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2bca280e1b6a5f5a53c8bb23e8c5880c7c56b17d4783d12ceee74a2064d02263 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2bca280e1b6a5f5a53c8bb23e8c5880c7c56b17d4783d12ceee74a2064d02263->enter($__internal_2bca280e1b6a5f5a53c8bb23e8c5880c7c56b17d4783d12ceee74a2064d02263_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/form_widget_compound.html.php"));

        $__internal_4475484ff0ebce89055d3bfb7b3a68e2db797c4b50187ff567495c305e8374d4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4475484ff0ebce89055d3bfb7b3a68e2db797c4b50187ff567495c305e8374d4->enter($__internal_4475484ff0ebce89055d3bfb7b3a68e2db797c4b50187ff567495c305e8374d4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/form_widget_compound.html.php"));

        // line 1
        echo "<table <?php echo \$view['form']->block(\$form, 'widget_container_attributes'); ?>>
    <?php if (!\$form->parent && \$errors): ?>
    <tr>
        <td colspan=\"2\">
            <?php echo \$view['form']->errors(\$form); ?>
        </td>
    </tr>
    <?php endif; ?>
    <?php echo \$view['form']->block(\$form, 'form_rows'); ?>
    <?php echo \$view['form']->rest(\$form); ?>
</table>
";
        
        $__internal_2bca280e1b6a5f5a53c8bb23e8c5880c7c56b17d4783d12ceee74a2064d02263->leave($__internal_2bca280e1b6a5f5a53c8bb23e8c5880c7c56b17d4783d12ceee74a2064d02263_prof);

        
        $__internal_4475484ff0ebce89055d3bfb7b3a68e2db797c4b50187ff567495c305e8374d4->leave($__internal_4475484ff0ebce89055d3bfb7b3a68e2db797c4b50187ff567495c305e8374d4_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/form_widget_compound.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<table <?php echo \$view['form']->block(\$form, 'widget_container_attributes'); ?>>
    <?php if (!\$form->parent && \$errors): ?>
    <tr>
        <td colspan=\"2\">
            <?php echo \$view['form']->errors(\$form); ?>
        </td>
    </tr>
    <?php endif; ?>
    <?php echo \$view['form']->block(\$form, 'form_rows'); ?>
    <?php echo \$view['form']->rest(\$form); ?>
</table>
", "@Framework/FormTable/form_widget_compound.html.php", "/Users/zima/projekty/blogweb/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/FormTable/form_widget_compound.html.php");
    }
}
