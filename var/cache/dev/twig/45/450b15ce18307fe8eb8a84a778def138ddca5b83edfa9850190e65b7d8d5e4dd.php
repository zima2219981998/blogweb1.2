<?php

/* FOSUserBundle:Resetting:reset.html.twig */
class __TwigTemplate_c8ab61f90726895349f2fbeede49924bc3786463f413b62f54bba78e8932e9ac extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Resetting:reset.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d1f9be5f881064f42e3a7b752cd55b166204772636deee4ec86e1a52a3598062 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d1f9be5f881064f42e3a7b752cd55b166204772636deee4ec86e1a52a3598062->enter($__internal_d1f9be5f881064f42e3a7b752cd55b166204772636deee4ec86e1a52a3598062_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:reset.html.twig"));

        $__internal_a0305cccdad9961629ffe8f82a4b3b45021b9dcbb806c255fc9027394062386d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a0305cccdad9961629ffe8f82a4b3b45021b9dcbb806c255fc9027394062386d->enter($__internal_a0305cccdad9961629ffe8f82a4b3b45021b9dcbb806c255fc9027394062386d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:reset.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_d1f9be5f881064f42e3a7b752cd55b166204772636deee4ec86e1a52a3598062->leave($__internal_d1f9be5f881064f42e3a7b752cd55b166204772636deee4ec86e1a52a3598062_prof);

        
        $__internal_a0305cccdad9961629ffe8f82a4b3b45021b9dcbb806c255fc9027394062386d->leave($__internal_a0305cccdad9961629ffe8f82a4b3b45021b9dcbb806c255fc9027394062386d_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_661bc0bc441f633bf4003323ca45996ee18bd5140fb4cb1a1d6a462f0a91ee30 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_661bc0bc441f633bf4003323ca45996ee18bd5140fb4cb1a1d6a462f0a91ee30->enter($__internal_661bc0bc441f633bf4003323ca45996ee18bd5140fb4cb1a1d6a462f0a91ee30_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_b9b534506dfd5e8eac9601bb360b1637c1610260ab877133e083bb401255e559 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b9b534506dfd5e8eac9601bb360b1637c1610260ab877133e083bb401255e559->enter($__internal_b9b534506dfd5e8eac9601bb360b1637c1610260ab877133e083bb401255e559_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Resetting/reset_content.html.twig", "FOSUserBundle:Resetting:reset.html.twig", 4)->display($context);
        
        $__internal_b9b534506dfd5e8eac9601bb360b1637c1610260ab877133e083bb401255e559->leave($__internal_b9b534506dfd5e8eac9601bb360b1637c1610260ab877133e083bb401255e559_prof);

        
        $__internal_661bc0bc441f633bf4003323ca45996ee18bd5140fb4cb1a1d6a462f0a91ee30->leave($__internal_661bc0bc441f633bf4003323ca45996ee18bd5140fb4cb1a1d6a462f0a91ee30_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:reset.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Resetting/reset_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Resetting:reset.html.twig", "/Users/zima/projekty/blogweb/app/Resources/FOSUserBundle/views/Resetting/reset.html.twig");
    }
}
