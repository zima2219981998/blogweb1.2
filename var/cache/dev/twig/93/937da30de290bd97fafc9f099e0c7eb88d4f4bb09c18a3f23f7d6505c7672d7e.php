<?php

/* @Framework/Form/button_attributes.html.php */
class __TwigTemplate_206642a6c4a2d62b8772b55e31773d99b05c5f65e166491512e2b9bd49f5e01c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5def95f2edd7de4cc0db4f0232948705b147b4aa6f76ced50e41d2175c09b2ea = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5def95f2edd7de4cc0db4f0232948705b147b4aa6f76ced50e41d2175c09b2ea->enter($__internal_5def95f2edd7de4cc0db4f0232948705b147b4aa6f76ced50e41d2175c09b2ea_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_attributes.html.php"));

        $__internal_9266bd23b5b2151a9ab72928557470ae8be1963b4718355845341a729d5d6056 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9266bd23b5b2151a9ab72928557470ae8be1963b4718355845341a729d5d6056->enter($__internal_9266bd23b5b2151a9ab72928557470ae8be1963b4718355845341a729d5d6056_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_attributes.html.php"));

        // line 1
        echo "id=\"<?php echo \$view->escape(\$id) ?>\" name=\"<?php echo \$view->escape(\$full_name) ?>\"<?php if (\$disabled): ?> disabled=\"disabled\"<?php endif ?>
<?php echo \$attr ? ' '.\$view['form']->block(\$form, 'attributes') : '' ?>
";
        
        $__internal_5def95f2edd7de4cc0db4f0232948705b147b4aa6f76ced50e41d2175c09b2ea->leave($__internal_5def95f2edd7de4cc0db4f0232948705b147b4aa6f76ced50e41d2175c09b2ea_prof);

        
        $__internal_9266bd23b5b2151a9ab72928557470ae8be1963b4718355845341a729d5d6056->leave($__internal_9266bd23b5b2151a9ab72928557470ae8be1963b4718355845341a729d5d6056_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/button_attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("id=\"<?php echo \$view->escape(\$id) ?>\" name=\"<?php echo \$view->escape(\$full_name) ?>\"<?php if (\$disabled): ?> disabled=\"disabled\"<?php endif ?>
<?php echo \$attr ? ' '.\$view['form']->block(\$form, 'attributes') : '' ?>
", "@Framework/Form/button_attributes.html.php", "/Users/zima/projekty/blogweb/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/button_attributes.html.php");
    }
}
