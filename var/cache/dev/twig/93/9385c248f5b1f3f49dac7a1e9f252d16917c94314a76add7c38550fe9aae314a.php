<?php

/* @Framework/Form/widget_container_attributes.html.php */
class __TwigTemplate_d39ac79eca1300fdb4d497315e6865027124b2751c480082dbd4514216f1c670 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1cf4659a9460d196218097cba440a21b131f7e9c8244e5b0e03a1f83cb275dd5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1cf4659a9460d196218097cba440a21b131f7e9c8244e5b0e03a1f83cb275dd5->enter($__internal_1cf4659a9460d196218097cba440a21b131f7e9c8244e5b0e03a1f83cb275dd5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/widget_container_attributes.html.php"));

        $__internal_48e1c9c3582010093482dc87663fcf1394cb7aa3923d7298bc574843d23d06dc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_48e1c9c3582010093482dc87663fcf1394cb7aa3923d7298bc574843d23d06dc->enter($__internal_48e1c9c3582010093482dc87663fcf1394cb7aa3923d7298bc574843d23d06dc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/widget_container_attributes.html.php"));

        // line 1
        echo "<?php if (!empty(\$id)): ?>id=\"<?php echo \$view->escape(\$id) ?>\"<?php endif ?>
<?php echo \$attr ? ' '.\$view['form']->block(\$form, 'attributes') : '' ?>
";
        
        $__internal_1cf4659a9460d196218097cba440a21b131f7e9c8244e5b0e03a1f83cb275dd5->leave($__internal_1cf4659a9460d196218097cba440a21b131f7e9c8244e5b0e03a1f83cb275dd5_prof);

        
        $__internal_48e1c9c3582010093482dc87663fcf1394cb7aa3923d7298bc574843d23d06dc->leave($__internal_48e1c9c3582010093482dc87663fcf1394cb7aa3923d7298bc574843d23d06dc_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/widget_container_attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (!empty(\$id)): ?>id=\"<?php echo \$view->escape(\$id) ?>\"<?php endif ?>
<?php echo \$attr ? ' '.\$view['form']->block(\$form, 'attributes') : '' ?>
", "@Framework/Form/widget_container_attributes.html.php", "/Users/zima/projekty/blogweb/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/widget_container_attributes.html.php");
    }
}
