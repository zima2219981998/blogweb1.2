<?php

/* FOSUserBundle:Group:list_content.html.twig */
class __TwigTemplate_6eb6f7ee64aed42d2ef3be4666c2324f15ff0777ce520979834efde82b892884 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8637253018c2fc3e547f5c3a48379f4424a73884cdf8b3f8d5817007c642a2d1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8637253018c2fc3e547f5c3a48379f4424a73884cdf8b3f8d5817007c642a2d1->enter($__internal_8637253018c2fc3e547f5c3a48379f4424a73884cdf8b3f8d5817007c642a2d1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:list_content.html.twig"));

        $__internal_fc85d87c20c08f6ac51ec58ca1edf5f3cb98fc16e0ee703dd11a8f5058f338e7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fc85d87c20c08f6ac51ec58ca1edf5f3cb98fc16e0ee703dd11a8f5058f338e7->enter($__internal_fc85d87c20c08f6ac51ec58ca1edf5f3cb98fc16e0ee703dd11a8f5058f338e7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:list_content.html.twig"));

        // line 1
        echo "<div class=\"fos_user_group_list\">
    <ul>
    ";
        // line 3
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["groups"] ?? $this->getContext($context, "groups")));
        foreach ($context['_seq'] as $context["_key"] => $context["group"]) {
            // line 4
            echo "        <li><a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_group_show", array("groupName" => $this->getAttribute($context["group"], "getName", array(), "method"))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["group"], "getName", array(), "method"), "html", null, true);
            echo "</a></li>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['group'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 6
        echo "    </ul>
</div>
";
        
        $__internal_8637253018c2fc3e547f5c3a48379f4424a73884cdf8b3f8d5817007c642a2d1->leave($__internal_8637253018c2fc3e547f5c3a48379f4424a73884cdf8b3f8d5817007c642a2d1_prof);

        
        $__internal_fc85d87c20c08f6ac51ec58ca1edf5f3cb98fc16e0ee703dd11a8f5058f338e7->leave($__internal_fc85d87c20c08f6ac51ec58ca1edf5f3cb98fc16e0ee703dd11a8f5058f338e7_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:list_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  44 => 6,  33 => 4,  29 => 3,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div class=\"fos_user_group_list\">
    <ul>
    {% for group in groups %}
        <li><a href=\"{{ path('fos_user_group_show', {'groupName': group.getName()} ) }}\">{{ group.getName() }}</a></li>
    {% endfor %}
    </ul>
</div>
", "FOSUserBundle:Group:list_content.html.twig", "/Users/zima/projekty/blogweb/vendor/friendsofsymfony/user-bundle/Resources/views/Group/list_content.html.twig");
    }
}
