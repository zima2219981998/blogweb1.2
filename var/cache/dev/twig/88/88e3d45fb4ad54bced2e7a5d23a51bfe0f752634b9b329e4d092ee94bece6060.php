<?php

/* form_table_layout.html.twig */
class __TwigTemplate_71d8c32ee5c254f87c13deda547ec388dc8d8b6b571f8894c9d3f4dc282dfd98 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $_trait_0 = $this->loadTemplate("form_div_layout.html.twig", "form_table_layout.html.twig", 1);
        // line 1
        if (!$_trait_0->isTraitable()) {
            throw new Twig_Error_Runtime('Template "'."form_div_layout.html.twig".'" cannot be used as a trait.');
        }
        $_trait_0_blocks = $_trait_0->getBlocks();

        $this->traits = $_trait_0_blocks;

        $this->blocks = array_merge(
            $this->traits,
            array(
                'form_row' => array($this, 'block_form_row'),
                'button_row' => array($this, 'block_button_row'),
                'hidden_row' => array($this, 'block_hidden_row'),
                'form_widget_compound' => array($this, 'block_form_widget_compound'),
            )
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_15509e1907a6f1888247e2a4cc58e779c5b29ff8a1a1e24c6a60ead1d6f53a35 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_15509e1907a6f1888247e2a4cc58e779c5b29ff8a1a1e24c6a60ead1d6f53a35->enter($__internal_15509e1907a6f1888247e2a4cc58e779c5b29ff8a1a1e24c6a60ead1d6f53a35_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "form_table_layout.html.twig"));

        $__internal_c8e842f136b8a446d28c8307e6cbfac63fefec1c6e3c7e149a3cc8569084301f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c8e842f136b8a446d28c8307e6cbfac63fefec1c6e3c7e149a3cc8569084301f->enter($__internal_c8e842f136b8a446d28c8307e6cbfac63fefec1c6e3c7e149a3cc8569084301f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "form_table_layout.html.twig"));

        // line 3
        $this->displayBlock('form_row', $context, $blocks);
        // line 15
        $this->displayBlock('button_row', $context, $blocks);
        // line 24
        $this->displayBlock('hidden_row', $context, $blocks);
        // line 32
        $this->displayBlock('form_widget_compound', $context, $blocks);
        
        $__internal_15509e1907a6f1888247e2a4cc58e779c5b29ff8a1a1e24c6a60ead1d6f53a35->leave($__internal_15509e1907a6f1888247e2a4cc58e779c5b29ff8a1a1e24c6a60ead1d6f53a35_prof);

        
        $__internal_c8e842f136b8a446d28c8307e6cbfac63fefec1c6e3c7e149a3cc8569084301f->leave($__internal_c8e842f136b8a446d28c8307e6cbfac63fefec1c6e3c7e149a3cc8569084301f_prof);

    }

    // line 3
    public function block_form_row($context, array $blocks = array())
    {
        $__internal_5b5230211ca48fa4d2741515daa464e025a0714ff4c182ba436bf3c40b111a39 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5b5230211ca48fa4d2741515daa464e025a0714ff4c182ba436bf3c40b111a39->enter($__internal_5b5230211ca48fa4d2741515daa464e025a0714ff4c182ba436bf3c40b111a39_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_row"));

        $__internal_7f8d147a6ea2b744e9e163af3ab0218ea8d57b83e053761e868ba6a256ac8a8c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7f8d147a6ea2b744e9e163af3ab0218ea8d57b83e053761e868ba6a256ac8a8c->enter($__internal_7f8d147a6ea2b744e9e163af3ab0218ea8d57b83e053761e868ba6a256ac8a8c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_row"));

        // line 4
        echo "<tr>
        <td>";
        // line 6
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'label');
        // line 7
        echo "</td>
        <td>";
        // line 9
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'errors');
        // line 10
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        // line 11
        echo "</td>
    </tr>";
        
        $__internal_7f8d147a6ea2b744e9e163af3ab0218ea8d57b83e053761e868ba6a256ac8a8c->leave($__internal_7f8d147a6ea2b744e9e163af3ab0218ea8d57b83e053761e868ba6a256ac8a8c_prof);

        
        $__internal_5b5230211ca48fa4d2741515daa464e025a0714ff4c182ba436bf3c40b111a39->leave($__internal_5b5230211ca48fa4d2741515daa464e025a0714ff4c182ba436bf3c40b111a39_prof);

    }

    // line 15
    public function block_button_row($context, array $blocks = array())
    {
        $__internal_3447d37aa79e6fe60662c7dfa96beb163ee23554a8cd3ca558554abc32fa3d44 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3447d37aa79e6fe60662c7dfa96beb163ee23554a8cd3ca558554abc32fa3d44->enter($__internal_3447d37aa79e6fe60662c7dfa96beb163ee23554a8cd3ca558554abc32fa3d44_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "button_row"));

        $__internal_25fa70ec9339b8b25b59cd1d3fa9fdcd7c0532c04aa50a67bed5ac875feaa1ac = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_25fa70ec9339b8b25b59cd1d3fa9fdcd7c0532c04aa50a67bed5ac875feaa1ac->enter($__internal_25fa70ec9339b8b25b59cd1d3fa9fdcd7c0532c04aa50a67bed5ac875feaa1ac_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "button_row"));

        // line 16
        echo "<tr>
        <td></td>
        <td>";
        // line 19
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        // line 20
        echo "</td>
    </tr>";
        
        $__internal_25fa70ec9339b8b25b59cd1d3fa9fdcd7c0532c04aa50a67bed5ac875feaa1ac->leave($__internal_25fa70ec9339b8b25b59cd1d3fa9fdcd7c0532c04aa50a67bed5ac875feaa1ac_prof);

        
        $__internal_3447d37aa79e6fe60662c7dfa96beb163ee23554a8cd3ca558554abc32fa3d44->leave($__internal_3447d37aa79e6fe60662c7dfa96beb163ee23554a8cd3ca558554abc32fa3d44_prof);

    }

    // line 24
    public function block_hidden_row($context, array $blocks = array())
    {
        $__internal_c76bed44abe88a43bd3d3e6b228a5cf874fe9e8ccf39ec91541b7f19674710a1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c76bed44abe88a43bd3d3e6b228a5cf874fe9e8ccf39ec91541b7f19674710a1->enter($__internal_c76bed44abe88a43bd3d3e6b228a5cf874fe9e8ccf39ec91541b7f19674710a1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "hidden_row"));

        $__internal_25f5d6ac6d2d3017ac0421bb32d2ccca6343554954f511f7748f06e1d8e8cfb8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_25f5d6ac6d2d3017ac0421bb32d2ccca6343554954f511f7748f06e1d8e8cfb8->enter($__internal_25f5d6ac6d2d3017ac0421bb32d2ccca6343554954f511f7748f06e1d8e8cfb8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "hidden_row"));

        // line 25
        echo "<tr style=\"display: none\">
        <td colspan=\"2\">";
        // line 27
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        // line 28
        echo "</td>
    </tr>";
        
        $__internal_25f5d6ac6d2d3017ac0421bb32d2ccca6343554954f511f7748f06e1d8e8cfb8->leave($__internal_25f5d6ac6d2d3017ac0421bb32d2ccca6343554954f511f7748f06e1d8e8cfb8_prof);

        
        $__internal_c76bed44abe88a43bd3d3e6b228a5cf874fe9e8ccf39ec91541b7f19674710a1->leave($__internal_c76bed44abe88a43bd3d3e6b228a5cf874fe9e8ccf39ec91541b7f19674710a1_prof);

    }

    // line 32
    public function block_form_widget_compound($context, array $blocks = array())
    {
        $__internal_aaf518c9040cfbefc95be2b9443c6b952f5f98d7fc94a668233b4c64c82b1cb5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_aaf518c9040cfbefc95be2b9443c6b952f5f98d7fc94a668233b4c64c82b1cb5->enter($__internal_aaf518c9040cfbefc95be2b9443c6b952f5f98d7fc94a668233b4c64c82b1cb5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_widget_compound"));

        $__internal_0480025c225240c1151440318945ae6745e9d521859452dc9e61953342f7cd18 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0480025c225240c1151440318945ae6745e9d521859452dc9e61953342f7cd18->enter($__internal_0480025c225240c1151440318945ae6745e9d521859452dc9e61953342f7cd18_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_widget_compound"));

        // line 33
        echo "<table ";
        $this->displayBlock("widget_container_attributes", $context, $blocks);
        echo ">";
        // line 34
        if ((twig_test_empty($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "parent", array())) && (twig_length_filter($this->env, ($context["errors"] ?? $this->getContext($context, "errors"))) > 0))) {
            // line 35
            echo "<tr>
            <td colspan=\"2\">";
            // line 37
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'errors');
            // line 38
            echo "</td>
        </tr>";
        }
        // line 41
        $this->displayBlock("form_rows", $context, $blocks);
        // line 42
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'rest');
        // line 43
        echo "</table>";
        
        $__internal_0480025c225240c1151440318945ae6745e9d521859452dc9e61953342f7cd18->leave($__internal_0480025c225240c1151440318945ae6745e9d521859452dc9e61953342f7cd18_prof);

        
        $__internal_aaf518c9040cfbefc95be2b9443c6b952f5f98d7fc94a668233b4c64c82b1cb5->leave($__internal_aaf518c9040cfbefc95be2b9443c6b952f5f98d7fc94a668233b4c64c82b1cb5_prof);

    }

    public function getTemplateName()
    {
        return "form_table_layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  168 => 43,  166 => 42,  164 => 41,  160 => 38,  158 => 37,  155 => 35,  153 => 34,  149 => 33,  140 => 32,  129 => 28,  127 => 27,  124 => 25,  115 => 24,  104 => 20,  102 => 19,  98 => 16,  89 => 15,  78 => 11,  76 => 10,  74 => 9,  71 => 7,  69 => 6,  66 => 4,  57 => 3,  47 => 32,  45 => 24,  43 => 15,  41 => 3,  14 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% use \"form_div_layout.html.twig\" %}

{%- block form_row -%}
    <tr>
        <td>
            {{- form_label(form) -}}
        </td>
        <td>
            {{- form_errors(form) -}}
            {{- form_widget(form) -}}
        </td>
    </tr>
{%- endblock form_row -%}

{%- block button_row -%}
    <tr>
        <td></td>
        <td>
            {{- form_widget(form) -}}
        </td>
    </tr>
{%- endblock button_row -%}

{%- block hidden_row -%}
    <tr style=\"display: none\">
        <td colspan=\"2\">
            {{- form_widget(form) -}}
        </td>
    </tr>
{%- endblock hidden_row -%}

{%- block form_widget_compound -%}
    <table {{ block('widget_container_attributes') }}>
        {%- if form.parent is empty and errors|length > 0 -%}
        <tr>
            <td colspan=\"2\">
                {{- form_errors(form) -}}
            </td>
        </tr>
        {%- endif -%}
        {{- block('form_rows') -}}
        {{- form_rest(form) -}}
    </table>
{%- endblock form_widget_compound -%}
", "form_table_layout.html.twig", "/Users/zima/projekty/blogweb/vendor/symfony/symfony/src/Symfony/Bridge/Twig/Resources/views/Form/form_table_layout.html.twig");
    }
}
