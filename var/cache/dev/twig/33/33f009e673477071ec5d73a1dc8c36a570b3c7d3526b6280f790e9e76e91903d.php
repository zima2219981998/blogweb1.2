<?php

/* @Framework/Form/form_widget.html.php */
class __TwigTemplate_590920db82efa3a891addf1432f551a653fcbed096c4fcdc8d2d1999ac1eae11 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9e5068cf196cfcfd228f0516778c32ac4b814102ac1bd2b1c29abfb93d247339 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9e5068cf196cfcfd228f0516778c32ac4b814102ac1bd2b1c29abfb93d247339->enter($__internal_9e5068cf196cfcfd228f0516778c32ac4b814102ac1bd2b1c29abfb93d247339_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget.html.php"));

        $__internal_783b0f571af5ab6e1543db6c1cef82f8290838098d64b6004db3dca903b28fce = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_783b0f571af5ab6e1543db6c1cef82f8290838098d64b6004db3dca903b28fce->enter($__internal_783b0f571af5ab6e1543db6c1cef82f8290838098d64b6004db3dca903b28fce_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget.html.php"));

        // line 1
        echo "<?php if (\$compound): ?>
<?php echo \$view['form']->block(\$form, 'form_widget_compound')?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'form_widget_simple')?>
<?php endif ?>
";
        
        $__internal_9e5068cf196cfcfd228f0516778c32ac4b814102ac1bd2b1c29abfb93d247339->leave($__internal_9e5068cf196cfcfd228f0516778c32ac4b814102ac1bd2b1c29abfb93d247339_prof);

        
        $__internal_783b0f571af5ab6e1543db6c1cef82f8290838098d64b6004db3dca903b28fce->leave($__internal_783b0f571af5ab6e1543db6c1cef82f8290838098d64b6004db3dca903b28fce_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (\$compound): ?>
<?php echo \$view['form']->block(\$form, 'form_widget_compound')?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'form_widget_simple')?>
<?php endif ?>
", "@Framework/Form/form_widget.html.php", "/Users/zima/projekty/blogweb/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_widget.html.php");
    }
}
