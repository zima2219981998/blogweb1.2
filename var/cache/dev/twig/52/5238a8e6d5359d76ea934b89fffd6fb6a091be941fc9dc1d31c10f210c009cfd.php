<?php

/* foundation_5_layout.html.twig */
class __TwigTemplate_f681c69a869c1fb043e02c797caabb6effd60a6f34553005785da28d1a04a5d7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("form_div_layout.html.twig", "foundation_5_layout.html.twig", 1);
        $this->blocks = array(
            'form_widget_simple' => array($this, 'block_form_widget_simple'),
            'textarea_widget' => array($this, 'block_textarea_widget'),
            'button_widget' => array($this, 'block_button_widget'),
            'money_widget' => array($this, 'block_money_widget'),
            'percent_widget' => array($this, 'block_percent_widget'),
            'datetime_widget' => array($this, 'block_datetime_widget'),
            'date_widget' => array($this, 'block_date_widget'),
            'time_widget' => array($this, 'block_time_widget'),
            'choice_widget_collapsed' => array($this, 'block_choice_widget_collapsed'),
            'choice_widget_expanded' => array($this, 'block_choice_widget_expanded'),
            'checkbox_widget' => array($this, 'block_checkbox_widget'),
            'radio_widget' => array($this, 'block_radio_widget'),
            'form_label' => array($this, 'block_form_label'),
            'choice_label' => array($this, 'block_choice_label'),
            'checkbox_label' => array($this, 'block_checkbox_label'),
            'radio_label' => array($this, 'block_radio_label'),
            'checkbox_radio_label' => array($this, 'block_checkbox_radio_label'),
            'form_row' => array($this, 'block_form_row'),
            'choice_row' => array($this, 'block_choice_row'),
            'date_row' => array($this, 'block_date_row'),
            'time_row' => array($this, 'block_time_row'),
            'datetime_row' => array($this, 'block_datetime_row'),
            'checkbox_row' => array($this, 'block_checkbox_row'),
            'radio_row' => array($this, 'block_radio_row'),
            'form_errors' => array($this, 'block_form_errors'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "form_div_layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6df6d9b39a72a06e7bb3064b076f8725d308d8d20171191d40d949623b57464a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6df6d9b39a72a06e7bb3064b076f8725d308d8d20171191d40d949623b57464a->enter($__internal_6df6d9b39a72a06e7bb3064b076f8725d308d8d20171191d40d949623b57464a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "foundation_5_layout.html.twig"));

        $__internal_52096f7f2379f8afd26acfe32291a173e87c718eee0fde9b8276972bb242cda5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_52096f7f2379f8afd26acfe32291a173e87c718eee0fde9b8276972bb242cda5->enter($__internal_52096f7f2379f8afd26acfe32291a173e87c718eee0fde9b8276972bb242cda5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "foundation_5_layout.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_6df6d9b39a72a06e7bb3064b076f8725d308d8d20171191d40d949623b57464a->leave($__internal_6df6d9b39a72a06e7bb3064b076f8725d308d8d20171191d40d949623b57464a_prof);

        
        $__internal_52096f7f2379f8afd26acfe32291a173e87c718eee0fde9b8276972bb242cda5->leave($__internal_52096f7f2379f8afd26acfe32291a173e87c718eee0fde9b8276972bb242cda5_prof);

    }

    // line 6
    public function block_form_widget_simple($context, array $blocks = array())
    {
        $__internal_4b3230e34a83fb00110cc927678b889159c101f31551f574ef8b053715f0aac0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4b3230e34a83fb00110cc927678b889159c101f31551f574ef8b053715f0aac0->enter($__internal_4b3230e34a83fb00110cc927678b889159c101f31551f574ef8b053715f0aac0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_widget_simple"));

        $__internal_343ba412280b3e940c699b78317b35084e8beeedde0db1fabec626018eaba3e3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_343ba412280b3e940c699b78317b35084e8beeedde0db1fabec626018eaba3e3->enter($__internal_343ba412280b3e940c699b78317b35084e8beeedde0db1fabec626018eaba3e3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_widget_simple"));

        // line 7
        if ((twig_length_filter($this->env, ($context["errors"] ?? $this->getContext($context, "errors"))) > 0)) {
            // line 8
            $context["attr"] = twig_array_merge(($context["attr"] ?? $this->getContext($context, "attr")), array("class" => twig_trim_filter(((($this->getAttribute(($context["attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["attr"] ?? null), "class", array()), "")) : ("")) . " error"))));
            // line 9
            echo "    ";
        }
        // line 10
        $this->displayParentBlock("form_widget_simple", $context, $blocks);
        
        $__internal_343ba412280b3e940c699b78317b35084e8beeedde0db1fabec626018eaba3e3->leave($__internal_343ba412280b3e940c699b78317b35084e8beeedde0db1fabec626018eaba3e3_prof);

        
        $__internal_4b3230e34a83fb00110cc927678b889159c101f31551f574ef8b053715f0aac0->leave($__internal_4b3230e34a83fb00110cc927678b889159c101f31551f574ef8b053715f0aac0_prof);

    }

    // line 13
    public function block_textarea_widget($context, array $blocks = array())
    {
        $__internal_33e794fad63be5c67e8019e02af22c908e9b1e8c0a7d54190c1627d44fbd4fac = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_33e794fad63be5c67e8019e02af22c908e9b1e8c0a7d54190c1627d44fbd4fac->enter($__internal_33e794fad63be5c67e8019e02af22c908e9b1e8c0a7d54190c1627d44fbd4fac_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "textarea_widget"));

        $__internal_53363126e9b5de1d2cd08f3e70ec358a1d880963ef71452671c052af6be7b54d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_53363126e9b5de1d2cd08f3e70ec358a1d880963ef71452671c052af6be7b54d->enter($__internal_53363126e9b5de1d2cd08f3e70ec358a1d880963ef71452671c052af6be7b54d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "textarea_widget"));

        // line 14
        if ((twig_length_filter($this->env, ($context["errors"] ?? $this->getContext($context, "errors"))) > 0)) {
            // line 15
            $context["attr"] = twig_array_merge(($context["attr"] ?? $this->getContext($context, "attr")), array("class" => twig_trim_filter(((($this->getAttribute(($context["attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["attr"] ?? null), "class", array()), "")) : ("")) . " error"))));
            // line 16
            echo "    ";
        }
        // line 17
        $this->displayParentBlock("textarea_widget", $context, $blocks);
        
        $__internal_53363126e9b5de1d2cd08f3e70ec358a1d880963ef71452671c052af6be7b54d->leave($__internal_53363126e9b5de1d2cd08f3e70ec358a1d880963ef71452671c052af6be7b54d_prof);

        
        $__internal_33e794fad63be5c67e8019e02af22c908e9b1e8c0a7d54190c1627d44fbd4fac->leave($__internal_33e794fad63be5c67e8019e02af22c908e9b1e8c0a7d54190c1627d44fbd4fac_prof);

    }

    // line 20
    public function block_button_widget($context, array $blocks = array())
    {
        $__internal_296f9c54afd681e5e2104cc67365ccb673ce49e4ccedab24736db38cb96c9166 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_296f9c54afd681e5e2104cc67365ccb673ce49e4ccedab24736db38cb96c9166->enter($__internal_296f9c54afd681e5e2104cc67365ccb673ce49e4ccedab24736db38cb96c9166_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "button_widget"));

        $__internal_692cf3f12be7f11f98efdaf09b5978f56bce182a0dffa83dcc2701eeac9906e3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_692cf3f12be7f11f98efdaf09b5978f56bce182a0dffa83dcc2701eeac9906e3->enter($__internal_692cf3f12be7f11f98efdaf09b5978f56bce182a0dffa83dcc2701eeac9906e3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "button_widget"));

        // line 21
        $context["attr"] = twig_array_merge(($context["attr"] ?? $this->getContext($context, "attr")), array("class" => twig_trim_filter(((($this->getAttribute(($context["attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["attr"] ?? null), "class", array()), "")) : ("")) . " button"))));
        // line 22
        $this->displayParentBlock("button_widget", $context, $blocks);
        
        $__internal_692cf3f12be7f11f98efdaf09b5978f56bce182a0dffa83dcc2701eeac9906e3->leave($__internal_692cf3f12be7f11f98efdaf09b5978f56bce182a0dffa83dcc2701eeac9906e3_prof);

        
        $__internal_296f9c54afd681e5e2104cc67365ccb673ce49e4ccedab24736db38cb96c9166->leave($__internal_296f9c54afd681e5e2104cc67365ccb673ce49e4ccedab24736db38cb96c9166_prof);

    }

    // line 25
    public function block_money_widget($context, array $blocks = array())
    {
        $__internal_c92576d449ef219a79b1d46f31c66d2d6732f8c79d420c39c7d2d11f173dd360 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c92576d449ef219a79b1d46f31c66d2d6732f8c79d420c39c7d2d11f173dd360->enter($__internal_c92576d449ef219a79b1d46f31c66d2d6732f8c79d420c39c7d2d11f173dd360_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "money_widget"));

        $__internal_359a0b8a99f0f1aa38fceb1746a61f3c74f78259afd8e41a8de9cb8251886fe8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_359a0b8a99f0f1aa38fceb1746a61f3c74f78259afd8e41a8de9cb8251886fe8->enter($__internal_359a0b8a99f0f1aa38fceb1746a61f3c74f78259afd8e41a8de9cb8251886fe8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "money_widget"));

        // line 26
        echo "<div class=\"row collapse\">
        ";
        // line 27
        $context["prepend"] = ("{{" == twig_slice($this->env, ($context["money_pattern"] ?? $this->getContext($context, "money_pattern")), 0, 2));
        // line 28
        echo "        ";
        if ( !($context["prepend"] ?? $this->getContext($context, "prepend"))) {
            // line 29
            echo "            <div class=\"small-3 large-2 columns\">
                <span class=\"prefix\">";
            // line 30
            echo twig_escape_filter($this->env, twig_replace_filter(($context["money_pattern"] ?? $this->getContext($context, "money_pattern")), array("{{ widget }}" => "")), "html", null, true);
            echo "</span>
            </div>
        ";
        }
        // line 33
        echo "        <div class=\"small-9 large-10 columns\">";
        // line 34
        $this->displayBlock("form_widget_simple", $context, $blocks);
        // line 35
        echo "</div>
        ";
        // line 36
        if (($context["prepend"] ?? $this->getContext($context, "prepend"))) {
            // line 37
            echo "            <div class=\"small-3 large-2 columns\">
                <span class=\"postfix\">";
            // line 38
            echo twig_escape_filter($this->env, twig_replace_filter(($context["money_pattern"] ?? $this->getContext($context, "money_pattern")), array("{{ widget }}" => "")), "html", null, true);
            echo "</span>
            </div>
        ";
        }
        // line 41
        echo "    </div>";
        
        $__internal_359a0b8a99f0f1aa38fceb1746a61f3c74f78259afd8e41a8de9cb8251886fe8->leave($__internal_359a0b8a99f0f1aa38fceb1746a61f3c74f78259afd8e41a8de9cb8251886fe8_prof);

        
        $__internal_c92576d449ef219a79b1d46f31c66d2d6732f8c79d420c39c7d2d11f173dd360->leave($__internal_c92576d449ef219a79b1d46f31c66d2d6732f8c79d420c39c7d2d11f173dd360_prof);

    }

    // line 44
    public function block_percent_widget($context, array $blocks = array())
    {
        $__internal_2a533787a0120ca498f75e5c1c37936ad4d815a9b704abb0f45a3c939622f9c6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2a533787a0120ca498f75e5c1c37936ad4d815a9b704abb0f45a3c939622f9c6->enter($__internal_2a533787a0120ca498f75e5c1c37936ad4d815a9b704abb0f45a3c939622f9c6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "percent_widget"));

        $__internal_63e7855b4a39a9083c3f214277d92d6f1a9390ec3a577733068eff2990cf9fd7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_63e7855b4a39a9083c3f214277d92d6f1a9390ec3a577733068eff2990cf9fd7->enter($__internal_63e7855b4a39a9083c3f214277d92d6f1a9390ec3a577733068eff2990cf9fd7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "percent_widget"));

        // line 45
        echo "<div class=\"row collapse\">
        <div class=\"small-9 large-10 columns\">";
        // line 47
        $this->displayBlock("form_widget_simple", $context, $blocks);
        // line 48
        echo "</div>
        <div class=\"small-3 large-2 columns\">
            <span class=\"postfix\">%</span>
        </div>
    </div>";
        
        $__internal_63e7855b4a39a9083c3f214277d92d6f1a9390ec3a577733068eff2990cf9fd7->leave($__internal_63e7855b4a39a9083c3f214277d92d6f1a9390ec3a577733068eff2990cf9fd7_prof);

        
        $__internal_2a533787a0120ca498f75e5c1c37936ad4d815a9b704abb0f45a3c939622f9c6->leave($__internal_2a533787a0120ca498f75e5c1c37936ad4d815a9b704abb0f45a3c939622f9c6_prof);

    }

    // line 55
    public function block_datetime_widget($context, array $blocks = array())
    {
        $__internal_2091e9fbbf0f31a438f4b4441b157c3d45877d07ac72a619f883a92c1ed0c0e7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2091e9fbbf0f31a438f4b4441b157c3d45877d07ac72a619f883a92c1ed0c0e7->enter($__internal_2091e9fbbf0f31a438f4b4441b157c3d45877d07ac72a619f883a92c1ed0c0e7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "datetime_widget"));

        $__internal_94bd091259f0bc7fff0788b54610aced707bb20b837848e4cfd3dbc8a84e5a92 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_94bd091259f0bc7fff0788b54610aced707bb20b837848e4cfd3dbc8a84e5a92->enter($__internal_94bd091259f0bc7fff0788b54610aced707bb20b837848e4cfd3dbc8a84e5a92_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "datetime_widget"));

        // line 56
        if ((($context["widget"] ?? $this->getContext($context, "widget")) == "single_text")) {
            // line 57
            $this->displayBlock("form_widget_simple", $context, $blocks);
        } else {
            // line 59
            echo "        ";
            $context["attr"] = twig_array_merge(($context["attr"] ?? $this->getContext($context, "attr")), array("class" => twig_trim_filter(((($this->getAttribute(($context["attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["attr"] ?? null), "class", array()), "")) : ("")) . " row"))));
            // line 60
            echo "        <div class=\"row\">
            <div class=\"large-7 columns\">";
            // line 61
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "date", array()), 'errors');
            echo "</div>
            <div class=\"large-5 columns\">";
            // line 62
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "time", array()), 'errors');
            echo "</div>
        </div>
        <div ";
            // line 64
            $this->displayBlock("widget_container_attributes", $context, $blocks);
            echo ">
            <div class=\"large-7 columns\">";
            // line 65
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "date", array()), 'widget', array("datetime" => true));
            echo "</div>
            <div class=\"large-5 columns\">";
            // line 66
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "time", array()), 'widget', array("datetime" => true));
            echo "</div>
        </div>
    ";
        }
        
        $__internal_94bd091259f0bc7fff0788b54610aced707bb20b837848e4cfd3dbc8a84e5a92->leave($__internal_94bd091259f0bc7fff0788b54610aced707bb20b837848e4cfd3dbc8a84e5a92_prof);

        
        $__internal_2091e9fbbf0f31a438f4b4441b157c3d45877d07ac72a619f883a92c1ed0c0e7->leave($__internal_2091e9fbbf0f31a438f4b4441b157c3d45877d07ac72a619f883a92c1ed0c0e7_prof);

    }

    // line 71
    public function block_date_widget($context, array $blocks = array())
    {
        $__internal_2ce6cfceea7a847151b147b37be1f64265ae162cc29ba9e5f96990fdeeb6eba9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2ce6cfceea7a847151b147b37be1f64265ae162cc29ba9e5f96990fdeeb6eba9->enter($__internal_2ce6cfceea7a847151b147b37be1f64265ae162cc29ba9e5f96990fdeeb6eba9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "date_widget"));

        $__internal_718336b2e4c4eb7e5d24f3cd820d1a084a1e9ee0e39112b8f41b863410b8cb99 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_718336b2e4c4eb7e5d24f3cd820d1a084a1e9ee0e39112b8f41b863410b8cb99->enter($__internal_718336b2e4c4eb7e5d24f3cd820d1a084a1e9ee0e39112b8f41b863410b8cb99_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "date_widget"));

        // line 72
        if ((($context["widget"] ?? $this->getContext($context, "widget")) == "single_text")) {
            // line 73
            $this->displayBlock("form_widget_simple", $context, $blocks);
        } else {
            // line 75
            echo "        ";
            $context["attr"] = twig_array_merge(($context["attr"] ?? $this->getContext($context, "attr")), array("class" => twig_trim_filter(((($this->getAttribute(($context["attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["attr"] ?? null), "class", array()), "")) : ("")) . " row"))));
            // line 76
            echo "        ";
            if (( !array_key_exists("datetime", $context) ||  !($context["datetime"] ?? $this->getContext($context, "datetime")))) {
                // line 77
                echo "            <div ";
                $this->displayBlock("widget_container_attributes", $context, $blocks);
                echo ">
        ";
            }
            // line 79
            echo twig_replace_filter(($context["date_pattern"] ?? $this->getContext($context, "date_pattern")), array("{{ year }}" => (("<div class=\"large-4 columns\">" .             // line 80
$this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "year", array()), 'widget')) . "</div>"), "{{ month }}" => (("<div class=\"large-4 columns\">" .             // line 81
$this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "month", array()), 'widget')) . "</div>"), "{{ day }}" => (("<div class=\"large-4 columns\">" .             // line 82
$this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "day", array()), 'widget')) . "</div>")));
            // line 84
            if (( !array_key_exists("datetime", $context) ||  !($context["datetime"] ?? $this->getContext($context, "datetime")))) {
                // line 85
                echo "            </div>
        ";
            }
            // line 87
            echo "    ";
        }
        
        $__internal_718336b2e4c4eb7e5d24f3cd820d1a084a1e9ee0e39112b8f41b863410b8cb99->leave($__internal_718336b2e4c4eb7e5d24f3cd820d1a084a1e9ee0e39112b8f41b863410b8cb99_prof);

        
        $__internal_2ce6cfceea7a847151b147b37be1f64265ae162cc29ba9e5f96990fdeeb6eba9->leave($__internal_2ce6cfceea7a847151b147b37be1f64265ae162cc29ba9e5f96990fdeeb6eba9_prof);

    }

    // line 90
    public function block_time_widget($context, array $blocks = array())
    {
        $__internal_0a4e14662614ee0d9c5228ac9baed31a9ee3998e16ab49778572ad8d25f44777 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0a4e14662614ee0d9c5228ac9baed31a9ee3998e16ab49778572ad8d25f44777->enter($__internal_0a4e14662614ee0d9c5228ac9baed31a9ee3998e16ab49778572ad8d25f44777_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "time_widget"));

        $__internal_ec10d63233cda648ba5863a31cd5a419a898dde6f437d68da463b851d1abe3e4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ec10d63233cda648ba5863a31cd5a419a898dde6f437d68da463b851d1abe3e4->enter($__internal_ec10d63233cda648ba5863a31cd5a419a898dde6f437d68da463b851d1abe3e4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "time_widget"));

        // line 91
        if ((($context["widget"] ?? $this->getContext($context, "widget")) == "single_text")) {
            // line 92
            $this->displayBlock("form_widget_simple", $context, $blocks);
        } else {
            // line 94
            echo "        ";
            $context["attr"] = twig_array_merge(($context["attr"] ?? $this->getContext($context, "attr")), array("class" => twig_trim_filter(((($this->getAttribute(($context["attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["attr"] ?? null), "class", array()), "")) : ("")) . " row"))));
            // line 95
            echo "        ";
            if (( !array_key_exists("datetime", $context) || (false == ($context["datetime"] ?? $this->getContext($context, "datetime"))))) {
                // line 96
                echo "            <div ";
                $this->displayBlock("widget_container_attributes", $context, $blocks);
                echo ">
        ";
            }
            // line 98
            echo "        ";
            if (($context["with_seconds"] ?? $this->getContext($context, "with_seconds"))) {
                // line 99
                echo "            <div class=\"large-4 columns\">";
                echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "hour", array()), 'widget');
                echo "</div>
            <div class=\"large-4 columns\">
                <div class=\"row collapse\">
                    <div class=\"small-3 large-2 columns\">
                        <span class=\"prefix\">:</span>
                    </div>
                    <div class=\"small-9 large-10 columns\">
                        ";
                // line 106
                echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "minute", array()), 'widget');
                echo "
                    </div>
                </div>
            </div>
            <div class=\"large-4 columns\">
                <div class=\"row collapse\">
                    <div class=\"small-3 large-2 columns\">
                        <span class=\"prefix\">:</span>
                    </div>
                    <div class=\"small-9 large-10 columns\">
                        ";
                // line 116
                echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "second", array()), 'widget');
                echo "
                    </div>
                </div>
            </div>
        ";
            } else {
                // line 121
                echo "            <div class=\"large-6 columns\">";
                echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "hour", array()), 'widget');
                echo "</div>
            <div class=\"large-6 columns\">
                <div class=\"row collapse\">
                    <div class=\"small-3 large-2 columns\">
                        <span class=\"prefix\">:</span>
                    </div>
                    <div class=\"small-9 large-10 columns\">
                        ";
                // line 128
                echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "minute", array()), 'widget');
                echo "
                    </div>
                </div>
            </div>
        ";
            }
            // line 133
            echo "        ";
            if (( !array_key_exists("datetime", $context) || (false == ($context["datetime"] ?? $this->getContext($context, "datetime"))))) {
                // line 134
                echo "            </div>
        ";
            }
            // line 136
            echo "    ";
        }
        
        $__internal_ec10d63233cda648ba5863a31cd5a419a898dde6f437d68da463b851d1abe3e4->leave($__internal_ec10d63233cda648ba5863a31cd5a419a898dde6f437d68da463b851d1abe3e4_prof);

        
        $__internal_0a4e14662614ee0d9c5228ac9baed31a9ee3998e16ab49778572ad8d25f44777->leave($__internal_0a4e14662614ee0d9c5228ac9baed31a9ee3998e16ab49778572ad8d25f44777_prof);

    }

    // line 139
    public function block_choice_widget_collapsed($context, array $blocks = array())
    {
        $__internal_795570251c872eed846620ecb5bb3ab9129e14782e29c72ad242be1b02f6b041 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_795570251c872eed846620ecb5bb3ab9129e14782e29c72ad242be1b02f6b041->enter($__internal_795570251c872eed846620ecb5bb3ab9129e14782e29c72ad242be1b02f6b041_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "choice_widget_collapsed"));

        $__internal_15f409bcb21ce2bdd494f23cfadabfc50f5e196d9ea01456346f43a0800f24bf = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_15f409bcb21ce2bdd494f23cfadabfc50f5e196d9ea01456346f43a0800f24bf->enter($__internal_15f409bcb21ce2bdd494f23cfadabfc50f5e196d9ea01456346f43a0800f24bf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "choice_widget_collapsed"));

        // line 140
        if ((twig_length_filter($this->env, ($context["errors"] ?? $this->getContext($context, "errors"))) > 0)) {
            // line 141
            $context["attr"] = twig_array_merge(($context["attr"] ?? $this->getContext($context, "attr")), array("class" => twig_trim_filter(((($this->getAttribute(($context["attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["attr"] ?? null), "class", array()), "")) : ("")) . " error"))));
            // line 142
            echo "    ";
        }
        // line 143
        echo "
    ";
        // line 144
        if (($context["multiple"] ?? $this->getContext($context, "multiple"))) {
            // line 145
            $context["attr"] = twig_array_merge(($context["attr"] ?? $this->getContext($context, "attr")), array("style" => twig_trim_filter(((($this->getAttribute(($context["attr"] ?? null), "style", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["attr"] ?? null), "style", array()), "")) : ("")) . " height: auto; background-image: none;"))));
            // line 146
            echo "    ";
        }
        // line 147
        echo "
    ";
        // line 148
        if ((((($context["required"] ?? $this->getContext($context, "required")) && (null === ($context["placeholder"] ?? $this->getContext($context, "placeholder")))) &&  !($context["placeholder_in_choices"] ?? $this->getContext($context, "placeholder_in_choices"))) &&  !($context["multiple"] ?? $this->getContext($context, "multiple")))) {
            // line 149
            $context["required"] = false;
        }
        // line 151
        echo "<select ";
        $this->displayBlock("widget_attributes", $context, $blocks);
        if (($context["multiple"] ?? $this->getContext($context, "multiple"))) {
            echo " multiple=\"multiple\" data-customforms=\"disabled\"";
        }
        echo ">
        ";
        // line 152
        if ( !(null === ($context["placeholder"] ?? $this->getContext($context, "placeholder")))) {
            // line 153
            echo "<option value=\"\"";
            if ((($context["required"] ?? $this->getContext($context, "required")) && twig_test_empty(($context["value"] ?? $this->getContext($context, "value"))))) {
                echo " selected=\"selected\"";
            }
            echo ">";
            echo twig_escape_filter($this->env, (((($context["translation_domain"] ?? $this->getContext($context, "translation_domain")) === false)) ? (($context["placeholder"] ?? $this->getContext($context, "placeholder"))) : ($this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans(($context["placeholder"] ?? $this->getContext($context, "placeholder")), array(), ($context["translation_domain"] ?? $this->getContext($context, "translation_domain"))))), "html", null, true);
            echo "</option>";
        }
        // line 155
        if ((twig_length_filter($this->env, ($context["preferred_choices"] ?? $this->getContext($context, "preferred_choices"))) > 0)) {
            // line 156
            $context["options"] = ($context["preferred_choices"] ?? $this->getContext($context, "preferred_choices"));
            // line 157
            $this->displayBlock("choice_widget_options", $context, $blocks);
            // line 158
            if (((twig_length_filter($this->env, ($context["choices"] ?? $this->getContext($context, "choices"))) > 0) &&  !(null === ($context["separator"] ?? $this->getContext($context, "separator"))))) {
                // line 159
                echo "<option disabled=\"disabled\">";
                echo twig_escape_filter($this->env, ($context["separator"] ?? $this->getContext($context, "separator")), "html", null, true);
                echo "</option>";
            }
        }
        // line 162
        $context["options"] = ($context["choices"] ?? $this->getContext($context, "choices"));
        // line 163
        $this->displayBlock("choice_widget_options", $context, $blocks);
        // line 164
        echo "</select>";
        
        $__internal_15f409bcb21ce2bdd494f23cfadabfc50f5e196d9ea01456346f43a0800f24bf->leave($__internal_15f409bcb21ce2bdd494f23cfadabfc50f5e196d9ea01456346f43a0800f24bf_prof);

        
        $__internal_795570251c872eed846620ecb5bb3ab9129e14782e29c72ad242be1b02f6b041->leave($__internal_795570251c872eed846620ecb5bb3ab9129e14782e29c72ad242be1b02f6b041_prof);

    }

    // line 167
    public function block_choice_widget_expanded($context, array $blocks = array())
    {
        $__internal_96ce41b68068bb40ba02538f93d9600a7c35d8801bda754637965e1c82086f0d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_96ce41b68068bb40ba02538f93d9600a7c35d8801bda754637965e1c82086f0d->enter($__internal_96ce41b68068bb40ba02538f93d9600a7c35d8801bda754637965e1c82086f0d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "choice_widget_expanded"));

        $__internal_4c41aefd52272804e822faff386e9bc4731a810f0a4ce869248cdc889c6b6f5a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4c41aefd52272804e822faff386e9bc4731a810f0a4ce869248cdc889c6b6f5a->enter($__internal_4c41aefd52272804e822faff386e9bc4731a810f0a4ce869248cdc889c6b6f5a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "choice_widget_expanded"));

        // line 168
        if (twig_in_filter("-inline", (($this->getAttribute(($context["label_attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["label_attr"] ?? null), "class", array()), "")) : ("")))) {
            // line 169
            echo "        <ul class=\"inline-list\">
            ";
            // line 170
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["form"] ?? $this->getContext($context, "form")));
            foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                // line 171
                echo "                <li>";
                echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($context["child"], 'widget', array("parent_label_class" => (($this->getAttribute(                // line 172
($context["label_attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["label_attr"] ?? null), "class", array()), "")) : (""))));
                // line 173
                echo "</li>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 175
            echo "        </ul>
    ";
        } else {
            // line 177
            echo "        <div ";
            $this->displayBlock("widget_container_attributes", $context, $blocks);
            echo ">
            ";
            // line 178
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["form"] ?? $this->getContext($context, "form")));
            foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                // line 179
                echo "                ";
                echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($context["child"], 'widget', array("parent_label_class" => (($this->getAttribute(                // line 180
($context["label_attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["label_attr"] ?? null), "class", array()), "")) : (""))));
                // line 181
                echo "
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 183
            echo "        </div>
    ";
        }
        
        $__internal_4c41aefd52272804e822faff386e9bc4731a810f0a4ce869248cdc889c6b6f5a->leave($__internal_4c41aefd52272804e822faff386e9bc4731a810f0a4ce869248cdc889c6b6f5a_prof);

        
        $__internal_96ce41b68068bb40ba02538f93d9600a7c35d8801bda754637965e1c82086f0d->leave($__internal_96ce41b68068bb40ba02538f93d9600a7c35d8801bda754637965e1c82086f0d_prof);

    }

    // line 187
    public function block_checkbox_widget($context, array $blocks = array())
    {
        $__internal_1b71ee104867b03ae3b838676020fca245e971ac23f85a28f67071d2b43df4fc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1b71ee104867b03ae3b838676020fca245e971ac23f85a28f67071d2b43df4fc->enter($__internal_1b71ee104867b03ae3b838676020fca245e971ac23f85a28f67071d2b43df4fc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_widget"));

        $__internal_c3fb1adf50e4e4fa2ca8ae6849138a7c256dec7ded4a050d39c7e3b3049fcb36 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c3fb1adf50e4e4fa2ca8ae6849138a7c256dec7ded4a050d39c7e3b3049fcb36->enter($__internal_c3fb1adf50e4e4fa2ca8ae6849138a7c256dec7ded4a050d39c7e3b3049fcb36_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_widget"));

        // line 188
        $context["parent_label_class"] = ((array_key_exists("parent_label_class", $context)) ? (_twig_default_filter(($context["parent_label_class"] ?? $this->getContext($context, "parent_label_class")), "")) : (""));
        // line 189
        echo "    ";
        if ((twig_length_filter($this->env, ($context["errors"] ?? $this->getContext($context, "errors"))) > 0)) {
            // line 190
            $context["attr"] = twig_array_merge(($context["attr"] ?? $this->getContext($context, "attr")), array("class" => twig_trim_filter(((($this->getAttribute(($context["attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["attr"] ?? null), "class", array()), "")) : ("")) . " error"))));
            // line 191
            echo "    ";
        }
        // line 192
        echo "    ";
        if (twig_in_filter("checkbox-inline", ($context["parent_label_class"] ?? $this->getContext($context, "parent_label_class")))) {
            // line 193
            echo "        ";
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'label', array("widget" => $this->renderParentBlock("checkbox_widget", $context, $blocks)));
            echo "
    ";
        } else {
            // line 195
            echo "        <div class=\"checkbox\">
            ";
            // line 196
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'label', array("widget" => $this->renderParentBlock("checkbox_widget", $context, $blocks)));
            echo "
        </div>
    ";
        }
        
        $__internal_c3fb1adf50e4e4fa2ca8ae6849138a7c256dec7ded4a050d39c7e3b3049fcb36->leave($__internal_c3fb1adf50e4e4fa2ca8ae6849138a7c256dec7ded4a050d39c7e3b3049fcb36_prof);

        
        $__internal_1b71ee104867b03ae3b838676020fca245e971ac23f85a28f67071d2b43df4fc->leave($__internal_1b71ee104867b03ae3b838676020fca245e971ac23f85a28f67071d2b43df4fc_prof);

    }

    // line 201
    public function block_radio_widget($context, array $blocks = array())
    {
        $__internal_b99c4bb0e588e494474dddbde300bb746712aa336083555aefff73a87a52cdf3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b99c4bb0e588e494474dddbde300bb746712aa336083555aefff73a87a52cdf3->enter($__internal_b99c4bb0e588e494474dddbde300bb746712aa336083555aefff73a87a52cdf3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "radio_widget"));

        $__internal_a1eff8ba156dccd693a4c66605c4189ef4ceecd0729cfb7a87d0eeca1221735f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a1eff8ba156dccd693a4c66605c4189ef4ceecd0729cfb7a87d0eeca1221735f->enter($__internal_a1eff8ba156dccd693a4c66605c4189ef4ceecd0729cfb7a87d0eeca1221735f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "radio_widget"));

        // line 202
        $context["parent_label_class"] = ((array_key_exists("parent_label_class", $context)) ? (_twig_default_filter(($context["parent_label_class"] ?? $this->getContext($context, "parent_label_class")), "")) : (""));
        // line 203
        echo "    ";
        if (twig_in_filter("radio-inline", ($context["parent_label_class"] ?? $this->getContext($context, "parent_label_class")))) {
            // line 204
            echo "        ";
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'label', array("widget" => $this->renderParentBlock("radio_widget", $context, $blocks)));
            echo "
    ";
        } else {
            // line 206
            echo "        ";
            if ((twig_length_filter($this->env, ($context["errors"] ?? $this->getContext($context, "errors"))) > 0)) {
                // line 207
                $context["attr"] = twig_array_merge(($context["attr"] ?? $this->getContext($context, "attr")), array("class" => twig_trim_filter(((($this->getAttribute(($context["attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["attr"] ?? null), "class", array()), "")) : ("")) . " error"))));
                // line 208
                echo "        ";
            }
            // line 209
            echo "        <div class=\"radio\">
            ";
            // line 210
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'label', array("widget" => $this->renderParentBlock("radio_widget", $context, $blocks)));
            echo "
        </div>
    ";
        }
        
        $__internal_a1eff8ba156dccd693a4c66605c4189ef4ceecd0729cfb7a87d0eeca1221735f->leave($__internal_a1eff8ba156dccd693a4c66605c4189ef4ceecd0729cfb7a87d0eeca1221735f_prof);

        
        $__internal_b99c4bb0e588e494474dddbde300bb746712aa336083555aefff73a87a52cdf3->leave($__internal_b99c4bb0e588e494474dddbde300bb746712aa336083555aefff73a87a52cdf3_prof);

    }

    // line 217
    public function block_form_label($context, array $blocks = array())
    {
        $__internal_0bc3b580bca724e39bcda565e077fd4f5a3ec3bd3d8cc4de47d804762cea1774 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0bc3b580bca724e39bcda565e077fd4f5a3ec3bd3d8cc4de47d804762cea1774->enter($__internal_0bc3b580bca724e39bcda565e077fd4f5a3ec3bd3d8cc4de47d804762cea1774_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_label"));

        $__internal_fe287f2ee8b60ef63dd126588c2f21a06509444065e24e5da765b78882323cbc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fe287f2ee8b60ef63dd126588c2f21a06509444065e24e5da765b78882323cbc->enter($__internal_fe287f2ee8b60ef63dd126588c2f21a06509444065e24e5da765b78882323cbc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_label"));

        // line 218
        if ((twig_length_filter($this->env, ($context["errors"] ?? $this->getContext($context, "errors"))) > 0)) {
            // line 219
            $context["label_attr"] = twig_array_merge(($context["label_attr"] ?? $this->getContext($context, "label_attr")), array("class" => twig_trim_filter(((($this->getAttribute(($context["label_attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["label_attr"] ?? null), "class", array()), "")) : ("")) . " error"))));
            // line 220
            echo "    ";
        }
        // line 221
        $this->displayParentBlock("form_label", $context, $blocks);
        
        $__internal_fe287f2ee8b60ef63dd126588c2f21a06509444065e24e5da765b78882323cbc->leave($__internal_fe287f2ee8b60ef63dd126588c2f21a06509444065e24e5da765b78882323cbc_prof);

        
        $__internal_0bc3b580bca724e39bcda565e077fd4f5a3ec3bd3d8cc4de47d804762cea1774->leave($__internal_0bc3b580bca724e39bcda565e077fd4f5a3ec3bd3d8cc4de47d804762cea1774_prof);

    }

    // line 224
    public function block_choice_label($context, array $blocks = array())
    {
        $__internal_7d640d93a18041d221b232995bb1dbace414a92c6cf1468e1b1e20a152a3edf9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7d640d93a18041d221b232995bb1dbace414a92c6cf1468e1b1e20a152a3edf9->enter($__internal_7d640d93a18041d221b232995bb1dbace414a92c6cf1468e1b1e20a152a3edf9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "choice_label"));

        $__internal_21c08b15e4e5e85f6fda8b7645fe76cde5eeff12f83e6f03b4a5c4c482e5e79c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_21c08b15e4e5e85f6fda8b7645fe76cde5eeff12f83e6f03b4a5c4c482e5e79c->enter($__internal_21c08b15e4e5e85f6fda8b7645fe76cde5eeff12f83e6f03b4a5c4c482e5e79c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "choice_label"));

        // line 225
        if ((twig_length_filter($this->env, ($context["errors"] ?? $this->getContext($context, "errors"))) > 0)) {
            // line 226
            $context["label_attr"] = twig_array_merge(($context["label_attr"] ?? $this->getContext($context, "label_attr")), array("class" => twig_trim_filter(((($this->getAttribute(($context["label_attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["label_attr"] ?? null), "class", array()), "")) : ("")) . " error"))));
            // line 227
            echo "    ";
        }
        // line 228
        echo "    ";
        // line 229
        echo "    ";
        $context["label_attr"] = twig_array_merge(($context["label_attr"] ?? $this->getContext($context, "label_attr")), array("class" => twig_trim_filter(twig_replace_filter((($this->getAttribute(($context["label_attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["label_attr"] ?? null), "class", array()), "")) : ("")), array("checkbox-inline" => "", "radio-inline" => "")))));
        // line 230
        $this->displayBlock("form_label", $context, $blocks);
        
        $__internal_21c08b15e4e5e85f6fda8b7645fe76cde5eeff12f83e6f03b4a5c4c482e5e79c->leave($__internal_21c08b15e4e5e85f6fda8b7645fe76cde5eeff12f83e6f03b4a5c4c482e5e79c_prof);

        
        $__internal_7d640d93a18041d221b232995bb1dbace414a92c6cf1468e1b1e20a152a3edf9->leave($__internal_7d640d93a18041d221b232995bb1dbace414a92c6cf1468e1b1e20a152a3edf9_prof);

    }

    // line 233
    public function block_checkbox_label($context, array $blocks = array())
    {
        $__internal_5edfde89b359ef91d393374f1f2daf7ede0c8e6ea138ed23996ed225e8dfae77 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5edfde89b359ef91d393374f1f2daf7ede0c8e6ea138ed23996ed225e8dfae77->enter($__internal_5edfde89b359ef91d393374f1f2daf7ede0c8e6ea138ed23996ed225e8dfae77_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_label"));

        $__internal_ba7faad09ac73cd354e9563cab44e777ceee1fa3d00d3abb958b581e0e841b1c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ba7faad09ac73cd354e9563cab44e777ceee1fa3d00d3abb958b581e0e841b1c->enter($__internal_ba7faad09ac73cd354e9563cab44e777ceee1fa3d00d3abb958b581e0e841b1c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_label"));

        // line 234
        $this->displayBlock("checkbox_radio_label", $context, $blocks);
        
        $__internal_ba7faad09ac73cd354e9563cab44e777ceee1fa3d00d3abb958b581e0e841b1c->leave($__internal_ba7faad09ac73cd354e9563cab44e777ceee1fa3d00d3abb958b581e0e841b1c_prof);

        
        $__internal_5edfde89b359ef91d393374f1f2daf7ede0c8e6ea138ed23996ed225e8dfae77->leave($__internal_5edfde89b359ef91d393374f1f2daf7ede0c8e6ea138ed23996ed225e8dfae77_prof);

    }

    // line 237
    public function block_radio_label($context, array $blocks = array())
    {
        $__internal_c8fa2bce900f5bc6b87edcaf4fe8a2133927a9abbb9c6b5e8fa795ed2654902d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c8fa2bce900f5bc6b87edcaf4fe8a2133927a9abbb9c6b5e8fa795ed2654902d->enter($__internal_c8fa2bce900f5bc6b87edcaf4fe8a2133927a9abbb9c6b5e8fa795ed2654902d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "radio_label"));

        $__internal_bd0feda9c74825ce039e7e5246cab04bbba8ebf454333dd5a1dc9999c1785824 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bd0feda9c74825ce039e7e5246cab04bbba8ebf454333dd5a1dc9999c1785824->enter($__internal_bd0feda9c74825ce039e7e5246cab04bbba8ebf454333dd5a1dc9999c1785824_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "radio_label"));

        // line 238
        $this->displayBlock("checkbox_radio_label", $context, $blocks);
        
        $__internal_bd0feda9c74825ce039e7e5246cab04bbba8ebf454333dd5a1dc9999c1785824->leave($__internal_bd0feda9c74825ce039e7e5246cab04bbba8ebf454333dd5a1dc9999c1785824_prof);

        
        $__internal_c8fa2bce900f5bc6b87edcaf4fe8a2133927a9abbb9c6b5e8fa795ed2654902d->leave($__internal_c8fa2bce900f5bc6b87edcaf4fe8a2133927a9abbb9c6b5e8fa795ed2654902d_prof);

    }

    // line 241
    public function block_checkbox_radio_label($context, array $blocks = array())
    {
        $__internal_c8408c45c4457e242fe35529c477556a2f41f01105060fdc3a70ccbc5e5a36b3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c8408c45c4457e242fe35529c477556a2f41f01105060fdc3a70ccbc5e5a36b3->enter($__internal_c8408c45c4457e242fe35529c477556a2f41f01105060fdc3a70ccbc5e5a36b3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_radio_label"));

        $__internal_6c35b9b66f8b505d2a7731fcb7b05d21aa171b546890282855c0f9c2108746c7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6c35b9b66f8b505d2a7731fcb7b05d21aa171b546890282855c0f9c2108746c7->enter($__internal_6c35b9b66f8b505d2a7731fcb7b05d21aa171b546890282855c0f9c2108746c7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_radio_label"));

        // line 242
        if (($context["required"] ?? $this->getContext($context, "required"))) {
            // line 243
            echo "        ";
            $context["label_attr"] = twig_array_merge(($context["label_attr"] ?? $this->getContext($context, "label_attr")), array("class" => twig_trim_filter(((($this->getAttribute(($context["label_attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["label_attr"] ?? null), "class", array()), "")) : ("")) . " required"))));
            // line 244
            echo "    ";
        }
        // line 245
        echo "    ";
        if ((twig_length_filter($this->env, ($context["errors"] ?? $this->getContext($context, "errors"))) > 0)) {
            // line 246
            $context["label_attr"] = twig_array_merge(($context["label_attr"] ?? $this->getContext($context, "label_attr")), array("class" => twig_trim_filter(((($this->getAttribute(($context["label_attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["label_attr"] ?? null), "class", array()), "")) : ("")) . " error"))));
            // line 247
            echo "    ";
        }
        // line 248
        echo "    ";
        if (array_key_exists("parent_label_class", $context)) {
            // line 249
            echo "        ";
            $context["label_attr"] = twig_array_merge(($context["label_attr"] ?? $this->getContext($context, "label_attr")), array("class" => twig_trim_filter(((($this->getAttribute(($context["label_attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["label_attr"] ?? null), "class", array()), "")) : ("")) . ($context["parent_label_class"] ?? $this->getContext($context, "parent_label_class"))))));
            // line 250
            echo "    ";
        }
        // line 251
        echo "    ";
        if (twig_test_empty(($context["label"] ?? $this->getContext($context, "label")))) {
            // line 252
            if ( !twig_test_empty(($context["label_format"] ?? $this->getContext($context, "label_format")))) {
                // line 253
                $context["label"] = twig_replace_filter(($context["label_format"] ?? $this->getContext($context, "label_format")), array("%name%" =>                 // line 254
($context["name"] ?? $this->getContext($context, "name")), "%id%" =>                 // line 255
($context["id"] ?? $this->getContext($context, "id"))));
            } else {
                // line 258
                $context["label"] = $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->humanize(($context["name"] ?? $this->getContext($context, "name")));
            }
        }
        // line 261
        echo "    <label";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["label_attr"] ?? $this->getContext($context, "label_attr")));
        foreach ($context['_seq'] as $context["attrname"] => $context["attrvalue"]) {
            echo " ";
            echo twig_escape_filter($this->env, $context["attrname"], "html", null, true);
            echo "=\"";
            echo twig_escape_filter($this->env, $context["attrvalue"], "html", null, true);
            echo "\"";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['attrname'], $context['attrvalue'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        echo ">
        ";
        // line 262
        echo ($context["widget"] ?? $this->getContext($context, "widget"));
        echo "
        ";
        // line 263
        echo twig_escape_filter($this->env, (((($context["translation_domain"] ?? $this->getContext($context, "translation_domain")) === false)) ? (($context["label"] ?? $this->getContext($context, "label"))) : ($this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans(($context["label"] ?? $this->getContext($context, "label")), array(), ($context["translation_domain"] ?? $this->getContext($context, "translation_domain"))))), "html", null, true);
        echo "
    </label>";
        
        $__internal_6c35b9b66f8b505d2a7731fcb7b05d21aa171b546890282855c0f9c2108746c7->leave($__internal_6c35b9b66f8b505d2a7731fcb7b05d21aa171b546890282855c0f9c2108746c7_prof);

        
        $__internal_c8408c45c4457e242fe35529c477556a2f41f01105060fdc3a70ccbc5e5a36b3->leave($__internal_c8408c45c4457e242fe35529c477556a2f41f01105060fdc3a70ccbc5e5a36b3_prof);

    }

    // line 269
    public function block_form_row($context, array $blocks = array())
    {
        $__internal_91238fa58d5259fb868bd00653a86b3876901aa9532e790c2709d8c2b5064061 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_91238fa58d5259fb868bd00653a86b3876901aa9532e790c2709d8c2b5064061->enter($__internal_91238fa58d5259fb868bd00653a86b3876901aa9532e790c2709d8c2b5064061_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_row"));

        $__internal_f2d4b9aec3e03e062aef95456710013aa85c3e0e12a2d631f560fb8cf5ad0cec = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f2d4b9aec3e03e062aef95456710013aa85c3e0e12a2d631f560fb8cf5ad0cec->enter($__internal_f2d4b9aec3e03e062aef95456710013aa85c3e0e12a2d631f560fb8cf5ad0cec_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_row"));

        // line 270
        echo "<div class=\"row\">
        <div class=\"large-12 columns";
        // line 271
        if ((( !($context["compound"] ?? $this->getContext($context, "compound")) || ((array_key_exists("force_error", $context)) ? (_twig_default_filter(($context["force_error"] ?? $this->getContext($context, "force_error")), false)) : (false))) &&  !($context["valid"] ?? $this->getContext($context, "valid")))) {
            echo " error";
        }
        echo "\">
            ";
        // line 272
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'label');
        echo "
            ";
        // line 273
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        echo "
            ";
        // line 274
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'errors');
        echo "
        </div>
    </div>";
        
        $__internal_f2d4b9aec3e03e062aef95456710013aa85c3e0e12a2d631f560fb8cf5ad0cec->leave($__internal_f2d4b9aec3e03e062aef95456710013aa85c3e0e12a2d631f560fb8cf5ad0cec_prof);

        
        $__internal_91238fa58d5259fb868bd00653a86b3876901aa9532e790c2709d8c2b5064061->leave($__internal_91238fa58d5259fb868bd00653a86b3876901aa9532e790c2709d8c2b5064061_prof);

    }

    // line 279
    public function block_choice_row($context, array $blocks = array())
    {
        $__internal_4c27d10c4af70fdd1d8363bbd0e073dfa3948f683d4ed21f7ffc4807d6549eb2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4c27d10c4af70fdd1d8363bbd0e073dfa3948f683d4ed21f7ffc4807d6549eb2->enter($__internal_4c27d10c4af70fdd1d8363bbd0e073dfa3948f683d4ed21f7ffc4807d6549eb2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "choice_row"));

        $__internal_c3cec70d3fadf3292a06e228d6e9bc58d3577f669bfd5d147b430807c848fb20 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c3cec70d3fadf3292a06e228d6e9bc58d3577f669bfd5d147b430807c848fb20->enter($__internal_c3cec70d3fadf3292a06e228d6e9bc58d3577f669bfd5d147b430807c848fb20_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "choice_row"));

        // line 280
        $context["force_error"] = true;
        // line 281
        echo "    ";
        $this->displayBlock("form_row", $context, $blocks);
        
        $__internal_c3cec70d3fadf3292a06e228d6e9bc58d3577f669bfd5d147b430807c848fb20->leave($__internal_c3cec70d3fadf3292a06e228d6e9bc58d3577f669bfd5d147b430807c848fb20_prof);

        
        $__internal_4c27d10c4af70fdd1d8363bbd0e073dfa3948f683d4ed21f7ffc4807d6549eb2->leave($__internal_4c27d10c4af70fdd1d8363bbd0e073dfa3948f683d4ed21f7ffc4807d6549eb2_prof);

    }

    // line 284
    public function block_date_row($context, array $blocks = array())
    {
        $__internal_ce4af6bb06875b6e13c06638b92c9b796b6932ee3ecb2c1820b22183dcd1a689 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ce4af6bb06875b6e13c06638b92c9b796b6932ee3ecb2c1820b22183dcd1a689->enter($__internal_ce4af6bb06875b6e13c06638b92c9b796b6932ee3ecb2c1820b22183dcd1a689_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "date_row"));

        $__internal_5cb07021f9b1b2f68cbd961ee49f8b1cb3e1213807d351f30f013c0fa0490d93 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5cb07021f9b1b2f68cbd961ee49f8b1cb3e1213807d351f30f013c0fa0490d93->enter($__internal_5cb07021f9b1b2f68cbd961ee49f8b1cb3e1213807d351f30f013c0fa0490d93_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "date_row"));

        // line 285
        $context["force_error"] = true;
        // line 286
        echo "    ";
        $this->displayBlock("form_row", $context, $blocks);
        
        $__internal_5cb07021f9b1b2f68cbd961ee49f8b1cb3e1213807d351f30f013c0fa0490d93->leave($__internal_5cb07021f9b1b2f68cbd961ee49f8b1cb3e1213807d351f30f013c0fa0490d93_prof);

        
        $__internal_ce4af6bb06875b6e13c06638b92c9b796b6932ee3ecb2c1820b22183dcd1a689->leave($__internal_ce4af6bb06875b6e13c06638b92c9b796b6932ee3ecb2c1820b22183dcd1a689_prof);

    }

    // line 289
    public function block_time_row($context, array $blocks = array())
    {
        $__internal_b187ef9f172fb8dd23add02394dcd06d3f41340b58853619ad4b4efb6a297c06 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b187ef9f172fb8dd23add02394dcd06d3f41340b58853619ad4b4efb6a297c06->enter($__internal_b187ef9f172fb8dd23add02394dcd06d3f41340b58853619ad4b4efb6a297c06_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "time_row"));

        $__internal_90780ec099c82231699da8db6eff73598e8a8b4814d3343ba9e60e23d4bb838a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_90780ec099c82231699da8db6eff73598e8a8b4814d3343ba9e60e23d4bb838a->enter($__internal_90780ec099c82231699da8db6eff73598e8a8b4814d3343ba9e60e23d4bb838a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "time_row"));

        // line 290
        $context["force_error"] = true;
        // line 291
        echo "    ";
        $this->displayBlock("form_row", $context, $blocks);
        
        $__internal_90780ec099c82231699da8db6eff73598e8a8b4814d3343ba9e60e23d4bb838a->leave($__internal_90780ec099c82231699da8db6eff73598e8a8b4814d3343ba9e60e23d4bb838a_prof);

        
        $__internal_b187ef9f172fb8dd23add02394dcd06d3f41340b58853619ad4b4efb6a297c06->leave($__internal_b187ef9f172fb8dd23add02394dcd06d3f41340b58853619ad4b4efb6a297c06_prof);

    }

    // line 294
    public function block_datetime_row($context, array $blocks = array())
    {
        $__internal_e24741c4d842ffc7a90313a446faf9a079cb6d03e4b62a3c4445112edaeae0da = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e24741c4d842ffc7a90313a446faf9a079cb6d03e4b62a3c4445112edaeae0da->enter($__internal_e24741c4d842ffc7a90313a446faf9a079cb6d03e4b62a3c4445112edaeae0da_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "datetime_row"));

        $__internal_8beea524de75e58381e3ac50212f6ac4a55d642ecc57c2beb647460d06332bb5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8beea524de75e58381e3ac50212f6ac4a55d642ecc57c2beb647460d06332bb5->enter($__internal_8beea524de75e58381e3ac50212f6ac4a55d642ecc57c2beb647460d06332bb5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "datetime_row"));

        // line 295
        $context["force_error"] = true;
        // line 296
        echo "    ";
        $this->displayBlock("form_row", $context, $blocks);
        
        $__internal_8beea524de75e58381e3ac50212f6ac4a55d642ecc57c2beb647460d06332bb5->leave($__internal_8beea524de75e58381e3ac50212f6ac4a55d642ecc57c2beb647460d06332bb5_prof);

        
        $__internal_e24741c4d842ffc7a90313a446faf9a079cb6d03e4b62a3c4445112edaeae0da->leave($__internal_e24741c4d842ffc7a90313a446faf9a079cb6d03e4b62a3c4445112edaeae0da_prof);

    }

    // line 299
    public function block_checkbox_row($context, array $blocks = array())
    {
        $__internal_f41882567b273e180ad300719fa8704ef39584ac8a7c007e63dc3c137f5a07cb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f41882567b273e180ad300719fa8704ef39584ac8a7c007e63dc3c137f5a07cb->enter($__internal_f41882567b273e180ad300719fa8704ef39584ac8a7c007e63dc3c137f5a07cb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_row"));

        $__internal_9348be97fe1ca1161a0e5c746b4e9069a3bac543c7bb00c7c1b6cb1a06187851 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9348be97fe1ca1161a0e5c746b4e9069a3bac543c7bb00c7c1b6cb1a06187851->enter($__internal_9348be97fe1ca1161a0e5c746b4e9069a3bac543c7bb00c7c1b6cb1a06187851_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_row"));

        // line 300
        echo "<div class=\"row\">
        <div class=\"large-12 columns";
        // line 301
        if ( !($context["valid"] ?? $this->getContext($context, "valid"))) {
            echo " error";
        }
        echo "\">
            ";
        // line 302
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        echo "
            ";
        // line 303
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'errors');
        echo "
        </div>
    </div>";
        
        $__internal_9348be97fe1ca1161a0e5c746b4e9069a3bac543c7bb00c7c1b6cb1a06187851->leave($__internal_9348be97fe1ca1161a0e5c746b4e9069a3bac543c7bb00c7c1b6cb1a06187851_prof);

        
        $__internal_f41882567b273e180ad300719fa8704ef39584ac8a7c007e63dc3c137f5a07cb->leave($__internal_f41882567b273e180ad300719fa8704ef39584ac8a7c007e63dc3c137f5a07cb_prof);

    }

    // line 308
    public function block_radio_row($context, array $blocks = array())
    {
        $__internal_24e6b2f3bcdb1dc79f0b270b34623b2fc732b37a92c52d2fb55ececdf5ce3886 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_24e6b2f3bcdb1dc79f0b270b34623b2fc732b37a92c52d2fb55ececdf5ce3886->enter($__internal_24e6b2f3bcdb1dc79f0b270b34623b2fc732b37a92c52d2fb55ececdf5ce3886_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "radio_row"));

        $__internal_3cd0cb9e3cdec986d1e88d18a6175152a89fa23de91ef6d0a7f02c6cfb5613cc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3cd0cb9e3cdec986d1e88d18a6175152a89fa23de91ef6d0a7f02c6cfb5613cc->enter($__internal_3cd0cb9e3cdec986d1e88d18a6175152a89fa23de91ef6d0a7f02c6cfb5613cc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "radio_row"));

        // line 309
        echo "<div class=\"row\">
        <div class=\"large-12 columns";
        // line 310
        if ( !($context["valid"] ?? $this->getContext($context, "valid"))) {
            echo " error";
        }
        echo "\">
            ";
        // line 311
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        echo "
            ";
        // line 312
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'errors');
        echo "
        </div>
    </div>";
        
        $__internal_3cd0cb9e3cdec986d1e88d18a6175152a89fa23de91ef6d0a7f02c6cfb5613cc->leave($__internal_3cd0cb9e3cdec986d1e88d18a6175152a89fa23de91ef6d0a7f02c6cfb5613cc_prof);

        
        $__internal_24e6b2f3bcdb1dc79f0b270b34623b2fc732b37a92c52d2fb55ececdf5ce3886->leave($__internal_24e6b2f3bcdb1dc79f0b270b34623b2fc732b37a92c52d2fb55ececdf5ce3886_prof);

    }

    // line 319
    public function block_form_errors($context, array $blocks = array())
    {
        $__internal_9da981371fb2d7294b45ccba8ee17936d1e33aa4991a14db2e61411b4012747e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9da981371fb2d7294b45ccba8ee17936d1e33aa4991a14db2e61411b4012747e->enter($__internal_9da981371fb2d7294b45ccba8ee17936d1e33aa4991a14db2e61411b4012747e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_errors"));

        $__internal_5e0d8082e18454b98f8a427d2d071246925a7cc2ff11301a1f0ba0d4c7878f54 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5e0d8082e18454b98f8a427d2d071246925a7cc2ff11301a1f0ba0d4c7878f54->enter($__internal_5e0d8082e18454b98f8a427d2d071246925a7cc2ff11301a1f0ba0d4c7878f54_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_errors"));

        // line 320
        if ((twig_length_filter($this->env, ($context["errors"] ?? $this->getContext($context, "errors"))) > 0)) {
            // line 321
            if ($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "parent", array())) {
                echo "<small class=\"error\">";
            } else {
                echo "<div data-alert class=\"alert-box alert\">";
            }
            // line 322
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["errors"] ?? $this->getContext($context, "errors")));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 323
                echo twig_escape_filter($this->env, $this->getAttribute($context["error"], "message", array()), "html", null, true);
                echo "
            ";
                // line 324
                if ( !$this->getAttribute($context["loop"], "last", array())) {
                    echo ", ";
                }
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 326
            if ($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "parent", array())) {
                echo "</small>";
            } else {
                echo "</div>";
            }
        }
        
        $__internal_5e0d8082e18454b98f8a427d2d071246925a7cc2ff11301a1f0ba0d4c7878f54->leave($__internal_5e0d8082e18454b98f8a427d2d071246925a7cc2ff11301a1f0ba0d4c7878f54_prof);

        
        $__internal_9da981371fb2d7294b45ccba8ee17936d1e33aa4991a14db2e61411b4012747e->leave($__internal_9da981371fb2d7294b45ccba8ee17936d1e33aa4991a14db2e61411b4012747e_prof);

    }

    public function getTemplateName()
    {
        return "foundation_5_layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1078 => 326,  1062 => 324,  1058 => 323,  1041 => 322,  1035 => 321,  1033 => 320,  1024 => 319,  1011 => 312,  1007 => 311,  1001 => 310,  998 => 309,  989 => 308,  976 => 303,  972 => 302,  966 => 301,  963 => 300,  954 => 299,  943 => 296,  941 => 295,  932 => 294,  921 => 291,  919 => 290,  910 => 289,  899 => 286,  897 => 285,  888 => 284,  877 => 281,  875 => 280,  866 => 279,  853 => 274,  849 => 273,  845 => 272,  839 => 271,  836 => 270,  827 => 269,  815 => 263,  811 => 262,  795 => 261,  791 => 258,  788 => 255,  787 => 254,  786 => 253,  784 => 252,  781 => 251,  778 => 250,  775 => 249,  772 => 248,  769 => 247,  767 => 246,  764 => 245,  761 => 244,  758 => 243,  756 => 242,  747 => 241,  737 => 238,  728 => 237,  718 => 234,  709 => 233,  699 => 230,  696 => 229,  694 => 228,  691 => 227,  689 => 226,  687 => 225,  678 => 224,  668 => 221,  665 => 220,  663 => 219,  661 => 218,  652 => 217,  638 => 210,  635 => 209,  632 => 208,  630 => 207,  627 => 206,  621 => 204,  618 => 203,  616 => 202,  607 => 201,  593 => 196,  590 => 195,  584 => 193,  581 => 192,  578 => 191,  576 => 190,  573 => 189,  571 => 188,  562 => 187,  550 => 183,  543 => 181,  541 => 180,  539 => 179,  535 => 178,  530 => 177,  526 => 175,  519 => 173,  517 => 172,  515 => 171,  511 => 170,  508 => 169,  506 => 168,  497 => 167,  487 => 164,  485 => 163,  483 => 162,  477 => 159,  475 => 158,  473 => 157,  471 => 156,  469 => 155,  460 => 153,  458 => 152,  450 => 151,  447 => 149,  445 => 148,  442 => 147,  439 => 146,  437 => 145,  435 => 144,  432 => 143,  429 => 142,  427 => 141,  425 => 140,  416 => 139,  405 => 136,  401 => 134,  398 => 133,  390 => 128,  379 => 121,  371 => 116,  358 => 106,  347 => 99,  344 => 98,  338 => 96,  335 => 95,  332 => 94,  329 => 92,  327 => 91,  318 => 90,  307 => 87,  303 => 85,  301 => 84,  299 => 82,  298 => 81,  297 => 80,  296 => 79,  290 => 77,  287 => 76,  284 => 75,  281 => 73,  279 => 72,  270 => 71,  256 => 66,  252 => 65,  248 => 64,  243 => 62,  239 => 61,  236 => 60,  233 => 59,  230 => 57,  228 => 56,  219 => 55,  205 => 48,  203 => 47,  200 => 45,  191 => 44,  181 => 41,  175 => 38,  172 => 37,  170 => 36,  167 => 35,  165 => 34,  163 => 33,  157 => 30,  154 => 29,  151 => 28,  149 => 27,  146 => 26,  137 => 25,  127 => 22,  125 => 21,  116 => 20,  106 => 17,  103 => 16,  101 => 15,  99 => 14,  90 => 13,  80 => 10,  77 => 9,  75 => 8,  73 => 7,  64 => 6,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"form_div_layout.html.twig\" %}

{# Based on Foundation 5 Doc #}
{# Widgets #}

{% block form_widget_simple -%}
    {% if errors|length > 0 -%}
        {% set attr = attr|merge({class: (attr.class|default('') ~ ' error')|trim}) %}
    {% endif %}
    {{- parent() -}}
{%- endblock form_widget_simple %}

{% block textarea_widget -%}
    {% if errors|length > 0 -%}
        {% set attr = attr|merge({class: (attr.class|default('') ~ ' error')|trim}) %}
    {% endif %}
    {{- parent() -}}
{%- endblock textarea_widget %}

{% block button_widget -%}
    {% set attr = attr|merge({class: (attr.class|default('') ~ ' button')|trim}) %}
    {{- parent() -}}
{%- endblock %}

{% block money_widget -%}
    <div class=\"row collapse\">
        {% set prepend = '{{' == money_pattern[0:2] %}
        {% if not prepend %}
            <div class=\"small-3 large-2 columns\">
                <span class=\"prefix\">{{ money_pattern|replace({ '{{ widget }}':''}) }}</span>
            </div>
        {% endif %}
        <div class=\"small-9 large-10 columns\">
            {{- block('form_widget_simple') -}}
        </div>
        {% if prepend %}
            <div class=\"small-3 large-2 columns\">
                <span class=\"postfix\">{{ money_pattern|replace({ '{{ widget }}':''}) }}</span>
            </div>
        {% endif %}
    </div>
{%- endblock money_widget %}

{% block percent_widget -%}
    <div class=\"row collapse\">
        <div class=\"small-9 large-10 columns\">
            {{- block('form_widget_simple') -}}
        </div>
        <div class=\"small-3 large-2 columns\">
            <span class=\"postfix\">%</span>
        </div>
    </div>
{%- endblock percent_widget %}

{% block datetime_widget -%}
    {% if widget == 'single_text' %}
        {{- block('form_widget_simple') -}}
    {% else %}
        {% set attr = attr|merge({class: (attr.class|default('') ~ ' row')|trim}) %}
        <div class=\"row\">
            <div class=\"large-7 columns\">{{ form_errors(form.date) }}</div>
            <div class=\"large-5 columns\">{{ form_errors(form.time) }}</div>
        </div>
        <div {{ block('widget_container_attributes') }}>
            <div class=\"large-7 columns\">{{ form_widget(form.date, { datetime: true } ) }}</div>
            <div class=\"large-5 columns\">{{ form_widget(form.time, { datetime: true } ) }}</div>
        </div>
    {% endif %}
{%- endblock datetime_widget %}

{% block date_widget -%}
    {% if widget == 'single_text' %}
        {{- block('form_widget_simple') -}}
    {% else %}
        {% set attr = attr|merge({class: (attr.class|default('') ~ ' row')|trim}) %}
        {% if datetime is not defined or not datetime %}
            <div {{ block('widget_container_attributes') }}>
        {% endif %}
        {{- date_pattern|replace({
            '{{ year }}': '<div class=\"large-4 columns\">' ~ form_widget(form.year) ~ '</div>',
            '{{ month }}': '<div class=\"large-4 columns\">' ~ form_widget(form.month) ~ '</div>',
            '{{ day }}': '<div class=\"large-4 columns\">' ~ form_widget(form.day) ~ '</div>',
        })|raw -}}
        {% if datetime is not defined or not datetime %}
            </div>
        {% endif %}
    {% endif %}
{%- endblock date_widget %}

{% block time_widget -%}
    {% if widget == 'single_text' %}
        {{- block('form_widget_simple') -}}
    {% else %}
        {% set attr = attr|merge({class: (attr.class|default('') ~ ' row')|trim}) %}
        {% if datetime is not defined or false == datetime %}
            <div {{ block('widget_container_attributes') -}}>
        {% endif %}
        {% if with_seconds %}
            <div class=\"large-4 columns\">{{ form_widget(form.hour) }}</div>
            <div class=\"large-4 columns\">
                <div class=\"row collapse\">
                    <div class=\"small-3 large-2 columns\">
                        <span class=\"prefix\">:</span>
                    </div>
                    <div class=\"small-9 large-10 columns\">
                        {{ form_widget(form.minute) }}
                    </div>
                </div>
            </div>
            <div class=\"large-4 columns\">
                <div class=\"row collapse\">
                    <div class=\"small-3 large-2 columns\">
                        <span class=\"prefix\">:</span>
                    </div>
                    <div class=\"small-9 large-10 columns\">
                        {{ form_widget(form.second) }}
                    </div>
                </div>
            </div>
        {% else %}
            <div class=\"large-6 columns\">{{ form_widget(form.hour) }}</div>
            <div class=\"large-6 columns\">
                <div class=\"row collapse\">
                    <div class=\"small-3 large-2 columns\">
                        <span class=\"prefix\">:</span>
                    </div>
                    <div class=\"small-9 large-10 columns\">
                        {{ form_widget(form.minute) }}
                    </div>
                </div>
            </div>
        {% endif %}
        {% if datetime is not defined or false == datetime %}
            </div>
        {% endif %}
    {% endif %}
{%- endblock time_widget %}

{% block choice_widget_collapsed -%}
    {% if errors|length > 0 -%}
        {% set attr = attr|merge({class: (attr.class|default('') ~ ' error')|trim}) %}
    {% endif %}

    {% if multiple -%}
        {% set attr = attr|merge({style: (attr.style|default('') ~ ' height: auto; background-image: none;')|trim}) %}
    {% endif %}

    {% if required and placeholder is none and not placeholder_in_choices and not multiple -%}
        {% set required = false %}
    {%- endif -%}
    <select {{ block('widget_attributes') }}{% if multiple %} multiple=\"multiple\" data-customforms=\"disabled\"{% endif %}>
        {% if placeholder is not none -%}
            <option value=\"\"{% if required and value is empty %} selected=\"selected\"{% endif %}>{{ translation_domain is same as(false) ? placeholder : placeholder|trans({}, translation_domain) }}</option>
        {%- endif %}
        {%- if preferred_choices|length > 0 -%}
            {% set options = preferred_choices %}
            {{- block('choice_widget_options') -}}
            {% if choices|length > 0 and separator is not none -%}
                <option disabled=\"disabled\">{{ separator }}</option>
            {%- endif %}
        {%- endif -%}
        {% set options = choices -%}
        {{- block('choice_widget_options') -}}
    </select>
{%- endblock choice_widget_collapsed %}

{% block choice_widget_expanded -%}
    {% if '-inline' in label_attr.class|default('') %}
        <ul class=\"inline-list\">
            {% for child in form %}
                <li>{{ form_widget(child, {
                        parent_label_class: label_attr.class|default(''),
                    }) }}</li>
            {% endfor %}
        </ul>
    {% else %}
        <div {{ block('widget_container_attributes') }}>
            {% for child in form %}
                {{ form_widget(child, {
                    parent_label_class: label_attr.class|default(''),
                }) }}
            {% endfor %}
        </div>
    {% endif %}
{%- endblock choice_widget_expanded %}

{% block checkbox_widget -%}
    {% set parent_label_class = parent_label_class|default('') %}
    {% if errors|length > 0 -%}
        {% set attr = attr|merge({class: (attr.class|default('') ~ ' error')|trim}) %}
    {% endif %}
    {% if 'checkbox-inline' in parent_label_class %}
        {{ form_label(form, null, { widget: parent() }) }}
    {% else %}
        <div class=\"checkbox\">
            {{ form_label(form, null, { widget: parent() }) }}
        </div>
    {% endif %}
{%- endblock checkbox_widget %}

{% block radio_widget -%}
    {% set parent_label_class = parent_label_class|default('') %}
    {% if 'radio-inline' in parent_label_class %}
        {{ form_label(form, null, { widget: parent() }) }}
    {% else %}
        {% if errors|length > 0 -%}
            {% set attr = attr|merge({class: (attr.class|default('') ~ ' error')|trim}) %}
        {% endif %}
        <div class=\"radio\">
            {{ form_label(form, null, { widget: parent() }) }}
        </div>
    {% endif %}
{%- endblock radio_widget %}

{# Labels #}

{% block form_label -%}
    {% if errors|length > 0 -%}
        {% set label_attr = label_attr|merge({class: (label_attr.class|default('') ~ ' error')|trim}) %}
    {% endif %}
    {{- parent() -}}
{%- endblock form_label %}

{% block choice_label -%}
    {% if errors|length > 0 -%}
        {% set label_attr = label_attr|merge({class: (label_attr.class|default('') ~ ' error')|trim}) %}
    {% endif %}
    {# remove the checkbox-inline and radio-inline class, it's only useful for embed labels #}
    {% set label_attr = label_attr|merge({class: label_attr.class|default('')|replace({'checkbox-inline': '', 'radio-inline': ''})|trim}) %}
    {{- block('form_label') -}}
{%- endblock %}

{% block checkbox_label -%}
    {{- block('checkbox_radio_label') -}}
{%- endblock checkbox_label %}

{% block radio_label -%}
    {{- block('checkbox_radio_label') -}}
{%- endblock radio_label %}

{% block checkbox_radio_label -%}
    {% if required %}
        {% set label_attr = label_attr|merge({class: (label_attr.class|default('') ~ ' required')|trim}) %}
    {% endif %}
    {% if errors|length > 0 -%}
        {% set label_attr = label_attr|merge({class: (label_attr.class|default('') ~ ' error')|trim}) %}
    {% endif %}
    {% if parent_label_class is defined %}
        {% set label_attr = label_attr|merge({class: (label_attr.class|default('') ~ parent_label_class)|trim}) %}
    {% endif %}
    {% if label is empty %}
        {%- if label_format is not empty -%}
            {% set label = label_format|replace({
                '%name%': name,
                '%id%': id,
            }) %}
        {%- else -%}
            {% set label = name|humanize %}
        {%- endif -%}
    {% endif %}
    <label{% for attrname, attrvalue in label_attr %} {{ attrname }}=\"{{ attrvalue }}\"{% endfor %}>
        {{ widget|raw }}
        {{ translation_domain is same as(false) ? label : label|trans({}, translation_domain) }}
    </label>
{%- endblock checkbox_radio_label %}

{# Rows #}

{% block form_row -%}
    <div class=\"row\">
        <div class=\"large-12 columns{% if (not compound or force_error|default(false)) and not valid %} error{% endif %}\">
            {{ form_label(form) }}
            {{ form_widget(form) }}
            {{ form_errors(form) }}
        </div>
    </div>
{%- endblock form_row %}

{% block choice_row -%}
    {% set force_error = true %}
    {{ block('form_row') }}
{%- endblock choice_row %}

{% block date_row -%}
    {% set force_error = true %}
    {{ block('form_row') }}
{%- endblock date_row %}

{% block time_row -%}
    {% set force_error = true %}
    {{ block('form_row') }}
{%- endblock time_row %}

{% block datetime_row -%}
    {% set force_error = true %}
    {{ block('form_row') }}
{%- endblock datetime_row %}

{% block checkbox_row -%}
    <div class=\"row\">
        <div class=\"large-12 columns{% if not valid %} error{% endif %}\">
            {{ form_widget(form) }}
            {{ form_errors(form) }}
        </div>
    </div>
{%- endblock checkbox_row %}

{% block radio_row -%}
    <div class=\"row\">
        <div class=\"large-12 columns{% if not valid %} error{% endif %}\">
            {{ form_widget(form) }}
            {{ form_errors(form) }}
        </div>
    </div>
{%- endblock radio_row %}

{# Errors #}

{% block form_errors -%}
    {% if errors|length > 0 -%}
        {% if form.parent %}<small class=\"error\">{% else %}<div data-alert class=\"alert-box alert\">{% endif %}
        {%- for error in errors -%}
            {{ error.message }}
            {% if not loop.last %}, {% endif %}
        {%- endfor -%}
        {% if form.parent %}</small>{% else %}</div>{% endif %}
    {%- endif %}
{%- endblock form_errors %}
", "foundation_5_layout.html.twig", "/Users/zima/projekty/blogweb/vendor/symfony/symfony/src/Symfony/Bridge/Twig/Resources/views/Form/foundation_5_layout.html.twig");
    }
}
