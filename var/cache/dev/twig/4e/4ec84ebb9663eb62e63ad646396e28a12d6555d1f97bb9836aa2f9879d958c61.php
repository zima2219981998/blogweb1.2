<?php

/* FOSUserBundle:Group:show_content.html.twig */
class __TwigTemplate_7ba63add48f919b20a8e4f780cc1bd6c5aaf5d00185e164ef0b29489ab9705a3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7bf01262fd5e12bdca873f5abb6ca9e72490cbbcc06beb30eb70a02a422f6b3f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7bf01262fd5e12bdca873f5abb6ca9e72490cbbcc06beb30eb70a02a422f6b3f->enter($__internal_7bf01262fd5e12bdca873f5abb6ca9e72490cbbcc06beb30eb70a02a422f6b3f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:show_content.html.twig"));

        $__internal_9dba3ab6d814217c8f5212404ea712d3768e59415346ef263fe7faed9a6c2a06 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9dba3ab6d814217c8f5212404ea712d3768e59415346ef263fe7faed9a6c2a06->enter($__internal_9dba3ab6d814217c8f5212404ea712d3768e59415346ef263fe7faed9a6c2a06_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:show_content.html.twig"));

        // line 2
        echo "
<div class=\"fos_user_group_show\">
    <p>";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("group.show.name", array(), "FOSUserBundle"), "html", null, true);
        echo ": ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["group"] ?? $this->getContext($context, "group")), "getName", array(), "method"), "html", null, true);
        echo "</p>
</div>
";
        
        $__internal_7bf01262fd5e12bdca873f5abb6ca9e72490cbbcc06beb30eb70a02a422f6b3f->leave($__internal_7bf01262fd5e12bdca873f5abb6ca9e72490cbbcc06beb30eb70a02a422f6b3f_prof);

        
        $__internal_9dba3ab6d814217c8f5212404ea712d3768e59415346ef263fe7faed9a6c2a06->leave($__internal_9dba3ab6d814217c8f5212404ea712d3768e59415346ef263fe7faed9a6c2a06_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:show_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  29 => 4,  25 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% trans_default_domain 'FOSUserBundle' %}

<div class=\"fos_user_group_show\">
    <p>{{ 'group.show.name'|trans }}: {{ group.getName() }}</p>
</div>
", "FOSUserBundle:Group:show_content.html.twig", "/Users/zima/projekty/blogweb/vendor/friendsofsymfony/user-bundle/Resources/views/Group/show_content.html.twig");
    }
}
