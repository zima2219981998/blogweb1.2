<?php

/* @Framework/FormTable/button_row.html.php */
class __TwigTemplate_be6709a7802f641194633738ac4f72cbc28f0d7c6ce4d77fe42f2b49ec5d5701 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_78258cc4f7071f890de77bec4b46a9b2f6303723efa113bf8713986575b08d18 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_78258cc4f7071f890de77bec4b46a9b2f6303723efa113bf8713986575b08d18->enter($__internal_78258cc4f7071f890de77bec4b46a9b2f6303723efa113bf8713986575b08d18_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/button_row.html.php"));

        $__internal_8eaee6792bbd99c7b0d254c962ec347cc3a8bb768e23fb93c68af291f50fb66a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8eaee6792bbd99c7b0d254c962ec347cc3a8bb768e23fb93c68af291f50fb66a->enter($__internal_8eaee6792bbd99c7b0d254c962ec347cc3a8bb768e23fb93c68af291f50fb66a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/button_row.html.php"));

        // line 1
        echo "<tr>
    <td></td>
    <td>
        <?php echo \$view['form']->widget(\$form); ?>
    </td>
</tr>
";
        
        $__internal_78258cc4f7071f890de77bec4b46a9b2f6303723efa113bf8713986575b08d18->leave($__internal_78258cc4f7071f890de77bec4b46a9b2f6303723efa113bf8713986575b08d18_prof);

        
        $__internal_8eaee6792bbd99c7b0d254c962ec347cc3a8bb768e23fb93c68af291f50fb66a->leave($__internal_8eaee6792bbd99c7b0d254c962ec347cc3a8bb768e23fb93c68af291f50fb66a_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/button_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<tr>
    <td></td>
    <td>
        <?php echo \$view['form']->widget(\$form); ?>
    </td>
</tr>
", "@Framework/FormTable/button_row.html.php", "/Users/zima/projekty/blogweb/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/FormTable/button_row.html.php");
    }
}
