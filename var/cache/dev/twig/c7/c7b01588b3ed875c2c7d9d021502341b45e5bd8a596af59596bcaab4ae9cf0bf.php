<?php

/* @Framework/Form/form_row.html.php */
class __TwigTemplate_f28098ada07f42179e54da30bd5f45c30910c7d86be6cdabeaf5ef6c038e5aed extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5ab568421a749356b7c9d747d3d0736b988bb09bab99c86d3dde4a65d3dbb602 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5ab568421a749356b7c9d747d3d0736b988bb09bab99c86d3dde4a65d3dbb602->enter($__internal_5ab568421a749356b7c9d747d3d0736b988bb09bab99c86d3dde4a65d3dbb602_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_row.html.php"));

        $__internal_cb1e46a79cf6d136df7c8a89bf8f48b98b8f6920ade34e0c269eda5ab15408b6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cb1e46a79cf6d136df7c8a89bf8f48b98b8f6920ade34e0c269eda5ab15408b6->enter($__internal_cb1e46a79cf6d136df7c8a89bf8f48b98b8f6920ade34e0c269eda5ab15408b6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_row.html.php"));

        // line 1
        echo "<div>
    <?php echo \$view['form']->label(\$form) ?>
    <?php echo \$view['form']->errors(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
";
        
        $__internal_5ab568421a749356b7c9d747d3d0736b988bb09bab99c86d3dde4a65d3dbb602->leave($__internal_5ab568421a749356b7c9d747d3d0736b988bb09bab99c86d3dde4a65d3dbb602_prof);

        
        $__internal_cb1e46a79cf6d136df7c8a89bf8f48b98b8f6920ade34e0c269eda5ab15408b6->leave($__internal_cb1e46a79cf6d136df7c8a89bf8f48b98b8f6920ade34e0c269eda5ab15408b6_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div>
    <?php echo \$view['form']->label(\$form) ?>
    <?php echo \$view['form']->errors(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
", "@Framework/Form/form_row.html.php", "/Users/zima/projekty/blogweb/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_row.html.php");
    }
}
