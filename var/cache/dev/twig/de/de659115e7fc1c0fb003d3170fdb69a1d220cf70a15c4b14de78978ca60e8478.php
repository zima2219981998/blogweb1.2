<?php

/* @FOSUser/layout.html.twig */
class __TwigTemplate_f0fc0e9c3d90ac64731f63883e54b38e45823f59141f8612ce778206eec882ec extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@ZimaBlogweb/base.html.twig", "@FOSUser/layout.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@ZimaBlogweb/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_af4b39bfaa7e4825993eb4c9de411da8348f6c5793ce48bc5f08c1f3ebf78c06 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_af4b39bfaa7e4825993eb4c9de411da8348f6c5793ce48bc5f08c1f3ebf78c06->enter($__internal_af4b39bfaa7e4825993eb4c9de411da8348f6c5793ce48bc5f08c1f3ebf78c06_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/layout.html.twig"));

        $__internal_14bafee448c6e2e8706ba5064ba4c02c2c04110a80d11ee1a38d694588d6988e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_14bafee448c6e2e8706ba5064ba4c02c2c04110a80d11ee1a38d694588d6988e->enter($__internal_14bafee448c6e2e8706ba5064ba4c02c2c04110a80d11ee1a38d694588d6988e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/layout.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_af4b39bfaa7e4825993eb4c9de411da8348f6c5793ce48bc5f08c1f3ebf78c06->leave($__internal_af4b39bfaa7e4825993eb4c9de411da8348f6c5793ce48bc5f08c1f3ebf78c06_prof);

        
        $__internal_14bafee448c6e2e8706ba5064ba4c02c2c04110a80d11ee1a38d694588d6988e->leave($__internal_14bafee448c6e2e8706ba5064ba4c02c2c04110a80d11ee1a38d694588d6988e_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_3cfb129674b3578f7a315857a505f1a916f41051847535ca387e193ee810307d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3cfb129674b3578f7a315857a505f1a916f41051847535ca387e193ee810307d->enter($__internal_3cfb129674b3578f7a315857a505f1a916f41051847535ca387e193ee810307d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_d6b699e966a2f9a23617801e25caccc1736827795e5762ae7a40255680badfb4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d6b699e966a2f9a23617801e25caccc1736827795e5762ae7a40255680badfb4->enter($__internal_d6b699e966a2f9a23617801e25caccc1736827795e5762ae7a40255680badfb4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    ";
        $this->displayBlock('fos_user_content', $context, $blocks);
        
        $__internal_d6b699e966a2f9a23617801e25caccc1736827795e5762ae7a40255680badfb4->leave($__internal_d6b699e966a2f9a23617801e25caccc1736827795e5762ae7a40255680badfb4_prof);

        
        $__internal_3cfb129674b3578f7a315857a505f1a916f41051847535ca387e193ee810307d->leave($__internal_3cfb129674b3578f7a315857a505f1a916f41051847535ca387e193ee810307d_prof);

    }

    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_85e729655f2f6afc9b6c863920802a0d61df97cd024b376347b96867f5f98b28 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_85e729655f2f6afc9b6c863920802a0d61df97cd024b376347b96867f5f98b28->enter($__internal_85e729655f2f6afc9b6c863920802a0d61df97cd024b376347b96867f5f98b28_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_05f893dd1966cf8797891cf58cf58ff77b292276824471f3bed6d57c92dffb25 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_05f893dd1966cf8797891cf58cf58ff77b292276824471f3bed6d57c92dffb25->enter($__internal_05f893dd1966cf8797891cf58cf58ff77b292276824471f3bed6d57c92dffb25_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 5
        echo "    ";
        
        $__internal_05f893dd1966cf8797891cf58cf58ff77b292276824471f3bed6d57c92dffb25->leave($__internal_05f893dd1966cf8797891cf58cf58ff77b292276824471f3bed6d57c92dffb25_prof);

        
        $__internal_85e729655f2f6afc9b6c863920802a0d61df97cd024b376347b96867f5f98b28->leave($__internal_85e729655f2f6afc9b6c863920802a0d61df97cd024b376347b96867f5f98b28_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  69 => 5,  50 => 4,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@ZimaBlogweb/base.html.twig\" %}

{% block body %}
    {% block fos_user_content %}
    {% endblock fos_user_content %}
{% endblock %}
", "@FOSUser/layout.html.twig", "/Users/zima/projekty/blogweb/app/Resources/FOSUserBundle/views/layout.html.twig");
    }
}
