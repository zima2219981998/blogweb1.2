<?php

/* @Framework/Form/container_attributes.html.php */
class __TwigTemplate_3dd6dec5d72cd7965f61febfa2c721627d792b8dc294389354b121394b77342b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_51a65ac99f67e7f533d70dd6852ae16ced59e11001441e35e3f1f1d682ce8e11 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_51a65ac99f67e7f533d70dd6852ae16ced59e11001441e35e3f1f1d682ce8e11->enter($__internal_51a65ac99f67e7f533d70dd6852ae16ced59e11001441e35e3f1f1d682ce8e11_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/container_attributes.html.php"));

        $__internal_194e10671a682683efc68db1881beb7892bbb0b45098100439e8afbc6d8b0139 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_194e10671a682683efc68db1881beb7892bbb0b45098100439e8afbc6d8b0139->enter($__internal_194e10671a682683efc68db1881beb7892bbb0b45098100439e8afbc6d8b0139_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/container_attributes.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>
";
        
        $__internal_51a65ac99f67e7f533d70dd6852ae16ced59e11001441e35e3f1f1d682ce8e11->leave($__internal_51a65ac99f67e7f533d70dd6852ae16ced59e11001441e35e3f1f1d682ce8e11_prof);

        
        $__internal_194e10671a682683efc68db1881beb7892bbb0b45098100439e8afbc6d8b0139->leave($__internal_194e10671a682683efc68db1881beb7892bbb0b45098100439e8afbc6d8b0139_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/container_attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>
", "@Framework/Form/container_attributes.html.php", "/Users/zima/projekty/blogweb/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/container_attributes.html.php");
    }
}
