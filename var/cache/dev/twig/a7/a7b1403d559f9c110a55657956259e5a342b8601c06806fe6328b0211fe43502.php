<?php

/* TwigBundle:Exception:exception.js.twig */
class __TwigTemplate_d5f9d1f1e405202c662779828c816e1db3b69b6aec1d75f952a86b17774a550a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6f976be94bbe53c52efce88f6d4b470ad33e2d13ad47d14ef25c085f110276a5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6f976be94bbe53c52efce88f6d4b470ad33e2d13ad47d14ef25c085f110276a5->enter($__internal_6f976be94bbe53c52efce88f6d4b470ad33e2d13ad47d14ef25c085f110276a5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.js.twig"));

        $__internal_1c5c9e80017e1d6770c3a3eca534aa5225d95681bffcad67838b55d1ca3c9b75 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1c5c9e80017e1d6770c3a3eca534aa5225d95681bffcad67838b55d1ca3c9b75->enter($__internal_1c5c9e80017e1d6770c3a3eca534aa5225d95681bffcad67838b55d1ca3c9b75_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.js.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twig_include($this->env, $context, "@Twig/Exception/exception.txt.twig", array("exception" => ($context["exception"] ?? $this->getContext($context, "exception"))));
        echo "
*/
";
        
        $__internal_6f976be94bbe53c52efce88f6d4b470ad33e2d13ad47d14ef25c085f110276a5->leave($__internal_6f976be94bbe53c52efce88f6d4b470ad33e2d13ad47d14ef25c085f110276a5_prof);

        
        $__internal_1c5c9e80017e1d6770c3a3eca534aa5225d95681bffcad67838b55d1ca3c9b75->leave($__internal_1c5c9e80017e1d6770c3a3eca534aa5225d95681bffcad67838b55d1ca3c9b75_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.js.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 2,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("/*
{{ include('@Twig/Exception/exception.txt.twig', { exception: exception }) }}
*/
", "TwigBundle:Exception:exception.js.twig", "/Users/zima/projekty/blogweb/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception.js.twig");
    }
}
