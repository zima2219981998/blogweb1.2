<?php

/* @IvoryCKEditor/Form/ckeditor_widget.html.php */
class __TwigTemplate_37ab0dec82e63a990f96f3c6104e21675c1262b4591c6ec3270a3a09d41c52d9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c898d485aad15a448a765df8cef48f43f90ede96007aec55cf40ceb6817a7ef2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c898d485aad15a448a765df8cef48f43f90ede96007aec55cf40ceb6817a7ef2->enter($__internal_c898d485aad15a448a765df8cef48f43f90ede96007aec55cf40ceb6817a7ef2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@IvoryCKEditor/Form/ckeditor_widget.html.php"));

        $__internal_9f580982e4c77c8d1db590398d741f75d7ac14f4950d08fc4c238db6d321f016 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9f580982e4c77c8d1db590398d741f75d7ac14f4950d08fc4c238db6d321f016->enter($__internal_9f580982e4c77c8d1db590398d741f75d7ac14f4950d08fc4c238db6d321f016_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@IvoryCKEditor/Form/ckeditor_widget.html.php"));

        // line 1
        echo "<textarea <?php echo \$view['form']->block(\$form, 'attributes') ?>><?php echo htmlspecialchars(\$value) ?></textarea>

<?php if (\$enable && !\$async) : ?>
    <?php include __DIR__.'/_ckeditor_javascript.html.php' ?>
<?php endif; ?>
";
        
        $__internal_c898d485aad15a448a765df8cef48f43f90ede96007aec55cf40ceb6817a7ef2->leave($__internal_c898d485aad15a448a765df8cef48f43f90ede96007aec55cf40ceb6817a7ef2_prof);

        
        $__internal_9f580982e4c77c8d1db590398d741f75d7ac14f4950d08fc4c238db6d321f016->leave($__internal_9f580982e4c77c8d1db590398d741f75d7ac14f4950d08fc4c238db6d321f016_prof);

    }

    public function getTemplateName()
    {
        return "@IvoryCKEditor/Form/ckeditor_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<textarea <?php echo \$view['form']->block(\$form, 'attributes') ?>><?php echo htmlspecialchars(\$value) ?></textarea>

<?php if (\$enable && !\$async) : ?>
    <?php include __DIR__.'/_ckeditor_javascript.html.php' ?>
<?php endif; ?>
", "@IvoryCKEditor/Form/ckeditor_widget.html.php", "/Users/zima/projekty/blogweb/vendor/egeloen/ckeditor-bundle/Resources/views/Form/ckeditor_widget.html.php");
    }
}
