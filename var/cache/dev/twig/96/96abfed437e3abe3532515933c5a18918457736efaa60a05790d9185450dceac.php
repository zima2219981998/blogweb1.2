<?php

/* @Framework/Form/hidden_widget.html.php */
class __TwigTemplate_8d47524d9a21f7a63155784dc0e061ffcab4da813ee1f818c1ebc0f51981b376 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5197bb4b353ec97ec5ebb31c146a9f90233ba83039f316a5b7a4253e72b34f49 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5197bb4b353ec97ec5ebb31c146a9f90233ba83039f316a5b7a4253e72b34f49->enter($__internal_5197bb4b353ec97ec5ebb31c146a9f90233ba83039f316a5b7a4253e72b34f49_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_widget.html.php"));

        $__internal_580ddbda5914a549947ce49f213d2c377d880890cf670034db9aec1ceec95b1e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_580ddbda5914a549947ce49f213d2c377d880890cf670034db9aec1ceec95b1e->enter($__internal_580ddbda5914a549947ce49f213d2c377d880890cf670034db9aec1ceec95b1e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'hidden')) ?>
";
        
        $__internal_5197bb4b353ec97ec5ebb31c146a9f90233ba83039f316a5b7a4253e72b34f49->leave($__internal_5197bb4b353ec97ec5ebb31c146a9f90233ba83039f316a5b7a4253e72b34f49_prof);

        
        $__internal_580ddbda5914a549947ce49f213d2c377d880890cf670034db9aec1ceec95b1e->leave($__internal_580ddbda5914a549947ce49f213d2c377d880890cf670034db9aec1ceec95b1e_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/hidden_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'hidden')) ?>
", "@Framework/Form/hidden_widget.html.php", "/Users/zima/projekty/blogweb/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/hidden_widget.html.php");
    }
}
