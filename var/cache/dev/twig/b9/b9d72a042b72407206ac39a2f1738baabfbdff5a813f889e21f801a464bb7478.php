<?php

/* @FOSUser/ChangePassword/change_password_content.html.twig */
class __TwigTemplate_ace3d7d37e12f8c91620f5c476490fc08782fa1647a4f56401c80ec123a69180 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_012c4fb5825c694efbd340dfb6e525289a0d3f7433676d697a7c3fd21e7dfe8a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_012c4fb5825c694efbd340dfb6e525289a0d3f7433676d697a7c3fd21e7dfe8a->enter($__internal_012c4fb5825c694efbd340dfb6e525289a0d3f7433676d697a7c3fd21e7dfe8a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/ChangePassword/change_password_content.html.twig"));

        $__internal_8041807f11ef6195d29665e3eb8207829e1ee103cff03b02aff39e2acc42040c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8041807f11ef6195d29665e3eb8207829e1ee103cff03b02aff39e2acc42040c->enter($__internal_8041807f11ef6195d29665e3eb8207829e1ee103cff03b02aff39e2acc42040c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/ChangePassword/change_password_content.html.twig"));

        // line 2
        echo "
";
        // line 3
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_start', array("action" => $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_change_password"), "attr" => array("class" => "fos_user_change_password")));
        echo "
    ";
        // line 4
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        echo "
    <div>
        <input type=\"submit\" value=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("change_password.submit", array(), "FOSUserBundle"), "html", null, true);
        echo "\" class=\"btn btn-default\" />
    </div>
";
        // line 8
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_end');
        echo "
";
        
        $__internal_012c4fb5825c694efbd340dfb6e525289a0d3f7433676d697a7c3fd21e7dfe8a->leave($__internal_012c4fb5825c694efbd340dfb6e525289a0d3f7433676d697a7c3fd21e7dfe8a_prof);

        
        $__internal_8041807f11ef6195d29665e3eb8207829e1ee103cff03b02aff39e2acc42040c->leave($__internal_8041807f11ef6195d29665e3eb8207829e1ee103cff03b02aff39e2acc42040c_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/ChangePassword/change_password_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  42 => 8,  37 => 6,  32 => 4,  28 => 3,  25 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% trans_default_domain 'FOSUserBundle' %}

{{ form_start(form, { 'action': path('fos_user_change_password'), 'attr': { 'class': 'fos_user_change_password' } }) }}
    {{ form_widget(form) }}
    <div>
        <input type=\"submit\" value=\"{{ 'change_password.submit'|trans }}\" class=\"btn btn-default\" />
    </div>
{{ form_end(form) }}
", "@FOSUser/ChangePassword/change_password_content.html.twig", "/Users/zima/projekty/blogweb/app/Resources/FOSUserBundle/views/ChangePassword/change_password_content.html.twig");
    }
}
