<?php

/* FOSUserBundle:Security:login.html.twig */
class __TwigTemplate_6925b000ee6efdec547df392ce324e9fac8865e2a3c1a3cb578a79f3b80798f3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Security:login.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f0cdef37176e2f6c5cb7b13999eff5515f25b46f7c053a214b8953654b17eaa9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f0cdef37176e2f6c5cb7b13999eff5515f25b46f7c053a214b8953654b17eaa9->enter($__internal_f0cdef37176e2f6c5cb7b13999eff5515f25b46f7c053a214b8953654b17eaa9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Security:login.html.twig"));

        $__internal_ae14835747c297d48773b74df4bb78f2c6f369c428ac4625bcd9f80ed2f0460e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ae14835747c297d48773b74df4bb78f2c6f369c428ac4625bcd9f80ed2f0460e->enter($__internal_ae14835747c297d48773b74df4bb78f2c6f369c428ac4625bcd9f80ed2f0460e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Security:login.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_f0cdef37176e2f6c5cb7b13999eff5515f25b46f7c053a214b8953654b17eaa9->leave($__internal_f0cdef37176e2f6c5cb7b13999eff5515f25b46f7c053a214b8953654b17eaa9_prof);

        
        $__internal_ae14835747c297d48773b74df4bb78f2c6f369c428ac4625bcd9f80ed2f0460e->leave($__internal_ae14835747c297d48773b74df4bb78f2c6f369c428ac4625bcd9f80ed2f0460e_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_5da7a23d4055717bac7a40e480c9ff0bb67ae4307de01b26e30450233c8536f4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5da7a23d4055717bac7a40e480c9ff0bb67ae4307de01b26e30450233c8536f4->enter($__internal_5da7a23d4055717bac7a40e480c9ff0bb67ae4307de01b26e30450233c8536f4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_a34a1ee41d24b1aa0838f77b5972f0c75e2a06fc0633ba60a0f3f348d49335bf = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a34a1ee41d24b1aa0838f77b5972f0c75e2a06fc0633ba60a0f3f348d49335bf->enter($__internal_a34a1ee41d24b1aa0838f77b5972f0c75e2a06fc0633ba60a0f3f348d49335bf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        echo "    ";
        echo twig_include($this->env, $context, "@FOSUser/Security/login_content.html.twig");
        echo "
";
        
        $__internal_a34a1ee41d24b1aa0838f77b5972f0c75e2a06fc0633ba60a0f3f348d49335bf->leave($__internal_a34a1ee41d24b1aa0838f77b5972f0c75e2a06fc0633ba60a0f3f348d49335bf_prof);

        
        $__internal_5da7a23d4055717bac7a40e480c9ff0bb67ae4307de01b26e30450233c8536f4->leave($__internal_5da7a23d4055717bac7a40e480c9ff0bb67ae4307de01b26e30450233c8536f4_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Security:login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
    {{ include('@FOSUser/Security/login_content.html.twig') }}
{% endblock fos_user_content %}", "FOSUserBundle:Security:login.html.twig", "/Users/zima/projekty/blogweb/app/Resources/FOSUserBundle/views/Security/login.html.twig");
    }
}
