<?php

/* @FOSUser/Security/login_content.html.twig */
class __TwigTemplate_189216b3068a62de239a94cd42e04c36245b784f4be33dba2887833c0899d628 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f1d1ffbe2eea11314d839899d1d4229c9ccc9dca8fa06265fbf46d72c07048df = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f1d1ffbe2eea11314d839899d1d4229c9ccc9dca8fa06265fbf46d72c07048df->enter($__internal_f1d1ffbe2eea11314d839899d1d4229c9ccc9dca8fa06265fbf46d72c07048df_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Security/login_content.html.twig"));

        $__internal_1ce0eef0ef232bd9100f6ed09b54a85cae5f93409d95cad35b0c939cbe8684b9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1ce0eef0ef232bd9100f6ed09b54a85cae5f93409d95cad35b0c939cbe8684b9->enter($__internal_1ce0eef0ef232bd9100f6ed09b54a85cae5f93409d95cad35b0c939cbe8684b9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Security/login_content.html.twig"));

        // line 2
        echo "
<div class=\"row\" style=\"text-decoration: none; text-align: center;  margin-top: 120px;\">

    <div class=\"col-md-8 col-sm-6 col-xs-6\">
        <center>
            <div class=\"jumbotron\">
                <img src=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("logo.png"), "html", null, true);
        echo "\" class=\"img img-responsive\">
                <h2>Create your own blog</h2>
                <a class=\"btn btn-primary btn-lg\" href=\"";
        // line 10
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_registration_register");
        echo "\" role=\"button\">Sign up</a>
            </div>
        </center>
    </div>

    <div class=\"col-md-4 col-sm-6 col-xs-6\">
        <div class=\"jumbotron\">
            <h3>Sign In</h3>
            <form class=\"form-group\" action=\"";
        // line 18
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_security_check");
        echo "\" method=\"post\">
                ";
        // line 19
        if (($context["csrf_token"] ?? $this->getContext($context, "csrf_token"))) {
            // line 20
            echo "                    <input type=\"hidden\" name=\"_csrf_token\" value=\"";
            echo twig_escape_filter($this->env, ($context["csrf_token"] ?? $this->getContext($context, "csrf_token")), "html", null, true);
            echo "\" />
                ";
        }
        // line 22
        echo "
                <input class=\"form-control\" type=\"text\" id=\"username\" name=\"_username\" placeholder=\"Email\" value=\"";
        // line 23
        echo twig_escape_filter($this->env, ($context["last_username"] ?? $this->getContext($context, "last_username")), "html", null, true);
        echo "\" required=\"required\" />

                <input class=\"form-control\" type=\"password\" id=\"password\" name=\"_password\" placeholder=\"Password\" required=\"required\" />

                <input type=\"checkbox\" id=\"remember_me\" name=\"_remember_me\" value=\"on\" />
                <label class=\"control-label\" for=\"remember_me\">Remember me</label>

                <input class=\"btn btn-default\" type=\"submit\" id=\"_submit\" name=\"_submit\" value=\"Login\" />
            </form>
        </div>
    </div>
</div>

";
        // line 36
        if (($context["error"] ?? $this->getContext($context, "error"))) {
            // line 37
            echo "    <div class=\"alert alert-warning\" role=\"alert\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans($this->getAttribute(($context["error"] ?? $this->getContext($context, "error")), "messageKey", array()), $this->getAttribute(($context["error"] ?? $this->getContext($context, "error")), "messageData", array()), "security"), "html", null, true);
            echo "</div>
";
        }
        // line 39
        echo "
<div class=\"container\" style=\"position:fixed; bottom:0;\">
    <hr>
    <p class=\"text-center\">BlogWEB © 2018</p>
</div>";
        
        $__internal_f1d1ffbe2eea11314d839899d1d4229c9ccc9dca8fa06265fbf46d72c07048df->leave($__internal_f1d1ffbe2eea11314d839899d1d4229c9ccc9dca8fa06265fbf46d72c07048df_prof);

        
        $__internal_1ce0eef0ef232bd9100f6ed09b54a85cae5f93409d95cad35b0c939cbe8684b9->leave($__internal_1ce0eef0ef232bd9100f6ed09b54a85cae5f93409d95cad35b0c939cbe8684b9_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Security/login_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  88 => 39,  82 => 37,  80 => 36,  64 => 23,  61 => 22,  55 => 20,  53 => 19,  49 => 18,  38 => 10,  33 => 8,  25 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% trans_default_domain 'FOSUserBundle' %}

<div class=\"row\" style=\"text-decoration: none; text-align: center;  margin-top: 120px;\">

    <div class=\"col-md-8 col-sm-6 col-xs-6\">
        <center>
            <div class=\"jumbotron\">
                <img src=\"{{ asset('logo.png') }}\" class=\"img img-responsive\">
                <h2>Create your own blog</h2>
                <a class=\"btn btn-primary btn-lg\" href=\"{{ path('fos_user_registration_register') }}\" role=\"button\">Sign up</a>
            </div>
        </center>
    </div>

    <div class=\"col-md-4 col-sm-6 col-xs-6\">
        <div class=\"jumbotron\">
            <h3>Sign In</h3>
            <form class=\"form-group\" action=\"{{ path(\"fos_user_security_check\") }}\" method=\"post\">
                {% if csrf_token %}
                    <input type=\"hidden\" name=\"_csrf_token\" value=\"{{ csrf_token }}\" />
                {% endif %}

                <input class=\"form-control\" type=\"text\" id=\"username\" name=\"_username\" placeholder=\"Email\" value=\"{{ last_username }}\" required=\"required\" />

                <input class=\"form-control\" type=\"password\" id=\"password\" name=\"_password\" placeholder=\"Password\" required=\"required\" />

                <input type=\"checkbox\" id=\"remember_me\" name=\"_remember_me\" value=\"on\" />
                <label class=\"control-label\" for=\"remember_me\">Remember me</label>

                <input class=\"btn btn-default\" type=\"submit\" id=\"_submit\" name=\"_submit\" value=\"Login\" />
            </form>
        </div>
    </div>
</div>

{% if error %}
    <div class=\"alert alert-warning\" role=\"alert\">{{ error.messageKey|trans(error.messageData, 'security') }}</div>
{% endif %}

<div class=\"container\" style=\"position:fixed; bottom:0;\">
    <hr>
    <p class=\"text-center\">BlogWEB © 2018</p>
</div>", "@FOSUser/Security/login_content.html.twig", "/Users/zima/projekty/blogweb/app/Resources/FOSUserBundle/views/Security/login_content.html.twig");
    }
}
