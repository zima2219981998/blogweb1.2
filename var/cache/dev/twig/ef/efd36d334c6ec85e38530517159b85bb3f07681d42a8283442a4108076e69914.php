<?php

/* ::base.html.twig */
class __TwigTemplate_c57b755ea1356e66f7bc0c60aae1b0143b261a7396a62496921d295069727d91 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_253297175dd447f66baa7f48dc1bbac6fd4e5e1b9a440811e4c360b815723962 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_253297175dd447f66baa7f48dc1bbac6fd4e5e1b9a440811e4c360b815723962->enter($__internal_253297175dd447f66baa7f48dc1bbac6fd4e5e1b9a440811e4c360b815723962_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::base.html.twig"));

        $__internal_02709fc6af58a6cb49f14c9f5c1fdddb59a2664721eccd938559a575263eed29 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_02709fc6af58a6cb49f14c9f5c1fdddb59a2664721eccd938559a575263eed29->enter($__internal_02709fc6af58a6cb49f14c9f5c1fdddb59a2664721eccd938559a575263eed29_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 7
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body>
        ";
        // line 10
        $this->displayBlock('body', $context, $blocks);
        // line 11
        echo "        ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 12
        echo "    </body>
</html>
";
        
        $__internal_253297175dd447f66baa7f48dc1bbac6fd4e5e1b9a440811e4c360b815723962->leave($__internal_253297175dd447f66baa7f48dc1bbac6fd4e5e1b9a440811e4c360b815723962_prof);

        
        $__internal_02709fc6af58a6cb49f14c9f5c1fdddb59a2664721eccd938559a575263eed29->leave($__internal_02709fc6af58a6cb49f14c9f5c1fdddb59a2664721eccd938559a575263eed29_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_1095798cf81f8e71a4447baa21451b2bb8ae5b791a26402533cd50c139b6a217 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1095798cf81f8e71a4447baa21451b2bb8ae5b791a26402533cd50c139b6a217->enter($__internal_1095798cf81f8e71a4447baa21451b2bb8ae5b791a26402533cd50c139b6a217_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_59d09434051eecea3e984c8d56233bc6ba33798d995a6b4498e8d916f7165b59 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_59d09434051eecea3e984c8d56233bc6ba33798d995a6b4498e8d916f7165b59->enter($__internal_59d09434051eecea3e984c8d56233bc6ba33798d995a6b4498e8d916f7165b59_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $__internal_59d09434051eecea3e984c8d56233bc6ba33798d995a6b4498e8d916f7165b59->leave($__internal_59d09434051eecea3e984c8d56233bc6ba33798d995a6b4498e8d916f7165b59_prof);

        
        $__internal_1095798cf81f8e71a4447baa21451b2bb8ae5b791a26402533cd50c139b6a217->leave($__internal_1095798cf81f8e71a4447baa21451b2bb8ae5b791a26402533cd50c139b6a217_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_bd80dc55551eacc7b94dd812bccbf0dc41f790eef1c2823457d1922bdf54b671 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bd80dc55551eacc7b94dd812bccbf0dc41f790eef1c2823457d1922bdf54b671->enter($__internal_bd80dc55551eacc7b94dd812bccbf0dc41f790eef1c2823457d1922bdf54b671_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_b45ce103db3374d2b7dd9eaeb26861d35a5679910d3e1a8585a3f2d66fc98445 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b45ce103db3374d2b7dd9eaeb26861d35a5679910d3e1a8585a3f2d66fc98445->enter($__internal_b45ce103db3374d2b7dd9eaeb26861d35a5679910d3e1a8585a3f2d66fc98445_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_b45ce103db3374d2b7dd9eaeb26861d35a5679910d3e1a8585a3f2d66fc98445->leave($__internal_b45ce103db3374d2b7dd9eaeb26861d35a5679910d3e1a8585a3f2d66fc98445_prof);

        
        $__internal_bd80dc55551eacc7b94dd812bccbf0dc41f790eef1c2823457d1922bdf54b671->leave($__internal_bd80dc55551eacc7b94dd812bccbf0dc41f790eef1c2823457d1922bdf54b671_prof);

    }

    // line 10
    public function block_body($context, array $blocks = array())
    {
        $__internal_83e3dc7e9f7348fe68cab51af9b562779e85d01635b0dd23295fa1ade8a7fa72 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_83e3dc7e9f7348fe68cab51af9b562779e85d01635b0dd23295fa1ade8a7fa72->enter($__internal_83e3dc7e9f7348fe68cab51af9b562779e85d01635b0dd23295fa1ade8a7fa72_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_b1c034efe5bdca01c07fa19ca68d7cba8260529d2b0843f938fea12e35cbea26 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b1c034efe5bdca01c07fa19ca68d7cba8260529d2b0843f938fea12e35cbea26->enter($__internal_b1c034efe5bdca01c07fa19ca68d7cba8260529d2b0843f938fea12e35cbea26_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_b1c034efe5bdca01c07fa19ca68d7cba8260529d2b0843f938fea12e35cbea26->leave($__internal_b1c034efe5bdca01c07fa19ca68d7cba8260529d2b0843f938fea12e35cbea26_prof);

        
        $__internal_83e3dc7e9f7348fe68cab51af9b562779e85d01635b0dd23295fa1ade8a7fa72->leave($__internal_83e3dc7e9f7348fe68cab51af9b562779e85d01635b0dd23295fa1ade8a7fa72_prof);

    }

    // line 11
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_6a9086ed30f93d93f9b3b14ad5dfe49be8a17a6de6ae6d7a99a13bc8d5177840 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6a9086ed30f93d93f9b3b14ad5dfe49be8a17a6de6ae6d7a99a13bc8d5177840->enter($__internal_6a9086ed30f93d93f9b3b14ad5dfe49be8a17a6de6ae6d7a99a13bc8d5177840_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_5b22764517dafb890f2586f400a9bf478e461444bbdcaedc3af361413d3e474e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5b22764517dafb890f2586f400a9bf478e461444bbdcaedc3af361413d3e474e->enter($__internal_5b22764517dafb890f2586f400a9bf478e461444bbdcaedc3af361413d3e474e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_5b22764517dafb890f2586f400a9bf478e461444bbdcaedc3af361413d3e474e->leave($__internal_5b22764517dafb890f2586f400a9bf478e461444bbdcaedc3af361413d3e474e_prof);

        
        $__internal_6a9086ed30f93d93f9b3b14ad5dfe49be8a17a6de6ae6d7a99a13bc8d5177840->leave($__internal_6a9086ed30f93d93f9b3b14ad5dfe49be8a17a6de6ae6d7a99a13bc8d5177840_prof);

    }

    public function getTemplateName()
    {
        return "::base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  117 => 11,  100 => 10,  83 => 6,  65 => 5,  53 => 12,  50 => 11,  48 => 10,  41 => 7,  39 => 6,  35 => 5,  29 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>{% block title %}Welcome!{% endblock %}</title>
        {% block stylesheets %}{% endblock %}
        <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('favicon.ico') }}\" />
    </head>
    <body>
        {% block body %}{% endblock %}
        {% block javascripts %}{% endblock %}
    </body>
</html>
", "::base.html.twig", "/Users/zima/projekty/blogweb/app/Resources/views/base.html.twig");
    }
}
