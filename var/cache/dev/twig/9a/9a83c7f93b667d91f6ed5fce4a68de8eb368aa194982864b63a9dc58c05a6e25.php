<?php

/* WebProfilerBundle:Collector:exception.html.twig */
class __TwigTemplate_fadb121f0677d6ec3ab8446be7c70260d3ab10321f044550f65f02ae8d87d236 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "WebProfilerBundle:Collector:exception.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_90434baf927282db42889686f75450aec03b5694348ecedbd8ac416753f0c8e2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_90434baf927282db42889686f75450aec03b5694348ecedbd8ac416753f0c8e2->enter($__internal_90434baf927282db42889686f75450aec03b5694348ecedbd8ac416753f0c8e2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:exception.html.twig"));

        $__internal_1dcaff037d76190df5ecd892e595a2868e01e5616865f0a28b5295b2dd99e1bd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1dcaff037d76190df5ecd892e595a2868e01e5616865f0a28b5295b2dd99e1bd->enter($__internal_1dcaff037d76190df5ecd892e595a2868e01e5616865f0a28b5295b2dd99e1bd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:exception.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_90434baf927282db42889686f75450aec03b5694348ecedbd8ac416753f0c8e2->leave($__internal_90434baf927282db42889686f75450aec03b5694348ecedbd8ac416753f0c8e2_prof);

        
        $__internal_1dcaff037d76190df5ecd892e595a2868e01e5616865f0a28b5295b2dd99e1bd->leave($__internal_1dcaff037d76190df5ecd892e595a2868e01e5616865f0a28b5295b2dd99e1bd_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_2b00e9f317cf6f4f1a44017e0bb9894f0c11679d715baa5b1a18ae41502ef914 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2b00e9f317cf6f4f1a44017e0bb9894f0c11679d715baa5b1a18ae41502ef914->enter($__internal_2b00e9f317cf6f4f1a44017e0bb9894f0c11679d715baa5b1a18ae41502ef914_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_6b4581c235eef8b393d790110f94dcad9c4b57a2c185c0beaf369526b034d438 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6b4581c235eef8b393d790110f94dcad9c4b57a2c185c0beaf369526b034d438->enter($__internal_6b4581c235eef8b393d790110f94dcad9c4b57a2c185c0beaf369526b034d438_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    ";
        if ($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 5
            echo "        <style>
            ";
            // line 6
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception_css", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
            echo "
        </style>
    ";
        }
        // line 9
        echo "    ";
        $this->displayParentBlock("head", $context, $blocks);
        echo "
";
        
        $__internal_6b4581c235eef8b393d790110f94dcad9c4b57a2c185c0beaf369526b034d438->leave($__internal_6b4581c235eef8b393d790110f94dcad9c4b57a2c185c0beaf369526b034d438_prof);

        
        $__internal_2b00e9f317cf6f4f1a44017e0bb9894f0c11679d715baa5b1a18ae41502ef914->leave($__internal_2b00e9f317cf6f4f1a44017e0bb9894f0c11679d715baa5b1a18ae41502ef914_prof);

    }

    // line 12
    public function block_menu($context, array $blocks = array())
    {
        $__internal_a54ae24cc57106954ce981b384be136a3ca406ae919586865d60e834cb682911 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a54ae24cc57106954ce981b384be136a3ca406ae919586865d60e834cb682911->enter($__internal_a54ae24cc57106954ce981b384be136a3ca406ae919586865d60e834cb682911_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_3bfd31a60f8f758df784aa62b7a3db97db642a8fc7df524e130a9ef6b124efa7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3bfd31a60f8f758df784aa62b7a3db97db642a8fc7df524e130a9ef6b124efa7->enter($__internal_3bfd31a60f8f758df784aa62b7a3db97db642a8fc7df524e130a9ef6b124efa7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 13
        echo "    <span class=\"label ";
        echo (($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) ? ("label-status-error") : ("disabled"));
        echo "\">
        <span class=\"icon\">";
        // line 14
        echo twig_include($this->env, $context, "@WebProfiler/Icon/exception.svg");
        echo "</span>
        <strong>Exception</strong>
        ";
        // line 16
        if ($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 17
            echo "            <span class=\"count\">
                <span>1</span>
            </span>
        ";
        }
        // line 21
        echo "    </span>
";
        
        $__internal_3bfd31a60f8f758df784aa62b7a3db97db642a8fc7df524e130a9ef6b124efa7->leave($__internal_3bfd31a60f8f758df784aa62b7a3db97db642a8fc7df524e130a9ef6b124efa7_prof);

        
        $__internal_a54ae24cc57106954ce981b384be136a3ca406ae919586865d60e834cb682911->leave($__internal_a54ae24cc57106954ce981b384be136a3ca406ae919586865d60e834cb682911_prof);

    }

    // line 24
    public function block_panel($context, array $blocks = array())
    {
        $__internal_0c823bd5c086a36847964ad7015bbf174c2ba6390db070e193c7733c9f2b0046 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0c823bd5c086a36847964ad7015bbf174c2ba6390db070e193c7733c9f2b0046->enter($__internal_0c823bd5c086a36847964ad7015bbf174c2ba6390db070e193c7733c9f2b0046_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_09eebbfea36462fcc3c00993a37e0533d91adb5fd5d2fb999142dda149033518 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_09eebbfea36462fcc3c00993a37e0533d91adb5fd5d2fb999142dda149033518->enter($__internal_09eebbfea36462fcc3c00993a37e0533d91adb5fd5d2fb999142dda149033518_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 25
        echo "    <h2>Exceptions</h2>

    ";
        // line 27
        if ( !$this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 28
            echo "        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    ";
        } else {
            // line 32
            echo "        <div class=\"sf-reset\">
            ";
            // line 33
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
            echo "
        </div>
    ";
        }
        
        $__internal_09eebbfea36462fcc3c00993a37e0533d91adb5fd5d2fb999142dda149033518->leave($__internal_09eebbfea36462fcc3c00993a37e0533d91adb5fd5d2fb999142dda149033518_prof);

        
        $__internal_0c823bd5c086a36847964ad7015bbf174c2ba6390db070e193c7733c9f2b0046->leave($__internal_0c823bd5c086a36847964ad7015bbf174c2ba6390db070e193c7733c9f2b0046_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Collector:exception.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  138 => 33,  135 => 32,  129 => 28,  127 => 27,  123 => 25,  114 => 24,  103 => 21,  97 => 17,  95 => 16,  90 => 14,  85 => 13,  76 => 12,  63 => 9,  57 => 6,  54 => 5,  51 => 4,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block head %}
    {% if collector.hasexception %}
        <style>
            {{ render(path('_profiler_exception_css', { token: token })) }}
        </style>
    {% endif %}
    {{ parent() }}
{% endblock %}

{% block menu %}
    <span class=\"label {{ collector.hasexception ? 'label-status-error' : 'disabled' }}\">
        <span class=\"icon\">{{ include('@WebProfiler/Icon/exception.svg') }}</span>
        <strong>Exception</strong>
        {% if collector.hasexception %}
            <span class=\"count\">
                <span>1</span>
            </span>
        {% endif %}
    </span>
{% endblock %}

{% block panel %}
    <h2>Exceptions</h2>

    {% if not collector.hasexception %}
        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    {% else %}
        <div class=\"sf-reset\">
            {{ render(path('_profiler_exception', { token: token })) }}
        </div>
    {% endif %}
{% endblock %}
", "WebProfilerBundle:Collector:exception.html.twig", "/Users/zima/projekty/blogweb/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/exception.html.twig");
    }
}
