<?php

/* WebProfilerBundle:Profiler:toolbar_redirect.html.twig */
class __TwigTemplate_8dfc0973a7a0d4adbb79faa0948c816f56c6a817e7a014f9b7f4e6cd9195225a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_851db020a0cf44ec486f1e1e6dcab20994927d0c82ec13c31a3e9ad84cb7afa1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_851db020a0cf44ec486f1e1e6dcab20994927d0c82ec13c31a3e9ad84cb7afa1->enter($__internal_851db020a0cf44ec486f1e1e6dcab20994927d0c82ec13c31a3e9ad84cb7afa1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig"));

        $__internal_fd9348a09b08a05f8ef4d70cb6b1f489e958fd42ba0fd8bbdd68270785779db6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fd9348a09b08a05f8ef4d70cb6b1f489e958fd42ba0fd8bbdd68270785779db6->enter($__internal_fd9348a09b08a05f8ef4d70cb6b1f489e958fd42ba0fd8bbdd68270785779db6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_851db020a0cf44ec486f1e1e6dcab20994927d0c82ec13c31a3e9ad84cb7afa1->leave($__internal_851db020a0cf44ec486f1e1e6dcab20994927d0c82ec13c31a3e9ad84cb7afa1_prof);

        
        $__internal_fd9348a09b08a05f8ef4d70cb6b1f489e958fd42ba0fd8bbdd68270785779db6->leave($__internal_fd9348a09b08a05f8ef4d70cb6b1f489e958fd42ba0fd8bbdd68270785779db6_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_04393debe159417a36ba30f5e5345a8b91ef8eeb945ead294680ebddf290dc89 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_04393debe159417a36ba30f5e5345a8b91ef8eeb945ead294680ebddf290dc89->enter($__internal_04393debe159417a36ba30f5e5345a8b91ef8eeb945ead294680ebddf290dc89_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_0a78cc7633cdd2b49c947e513c580ae911a5459403a8fa1c0e0413feb75e0dc5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0a78cc7633cdd2b49c947e513c580ae911a5459403a8fa1c0e0413feb75e0dc5->enter($__internal_0a78cc7633cdd2b49c947e513c580ae911a5459403a8fa1c0e0413feb75e0dc5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Redirection Intercepted";
        
        $__internal_0a78cc7633cdd2b49c947e513c580ae911a5459403a8fa1c0e0413feb75e0dc5->leave($__internal_0a78cc7633cdd2b49c947e513c580ae911a5459403a8fa1c0e0413feb75e0dc5_prof);

        
        $__internal_04393debe159417a36ba30f5e5345a8b91ef8eeb945ead294680ebddf290dc89->leave($__internal_04393debe159417a36ba30f5e5345a8b91ef8eeb945ead294680ebddf290dc89_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_57ae1d50f859b6af610afdf07763ef13c487a46af2cf628db9fd87a6fefdcbe3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_57ae1d50f859b6af610afdf07763ef13c487a46af2cf628db9fd87a6fefdcbe3->enter($__internal_57ae1d50f859b6af610afdf07763ef13c487a46af2cf628db9fd87a6fefdcbe3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_9022c14d0e6132dac5b413b565aa12717ff8170034cda4cddc8b476f3b6f7280 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9022c14d0e6132dac5b413b565aa12717ff8170034cda4cddc8b476f3b6f7280->enter($__internal_9022c14d0e6132dac5b413b565aa12717ff8170034cda4cddc8b476f3b6f7280_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <div class=\"sf-reset\">
        <div class=\"block-exception\">
            <h1>This request redirects to <a href=\"";
        // line 8
        echo twig_escape_filter($this->env, ($context["location"] ?? $this->getContext($context, "location")), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, ($context["location"] ?? $this->getContext($context, "location")), "html", null, true);
        echo "</a>.</h1>

            <p>
                <small>
                    The redirect was intercepted by the web debug toolbar to help debugging.
                    For more information, see the \"intercept-redirects\" option of the Profiler.
                </small>
            </p>
        </div>
    </div>
";
        
        $__internal_9022c14d0e6132dac5b413b565aa12717ff8170034cda4cddc8b476f3b6f7280->leave($__internal_9022c14d0e6132dac5b413b565aa12717ff8170034cda4cddc8b476f3b6f7280_prof);

        
        $__internal_57ae1d50f859b6af610afdf07763ef13c487a46af2cf628db9fd87a6fefdcbe3->leave($__internal_57ae1d50f859b6af610afdf07763ef13c487a46af2cf628db9fd87a6fefdcbe3_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:toolbar_redirect.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  72 => 8,  68 => 6,  59 => 5,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@Twig/layout.html.twig' %}

{% block title 'Redirection Intercepted' %}

{% block body %}
    <div class=\"sf-reset\">
        <div class=\"block-exception\">
            <h1>This request redirects to <a href=\"{{ location }}\">{{ location }}</a>.</h1>

            <p>
                <small>
                    The redirect was intercepted by the web debug toolbar to help debugging.
                    For more information, see the \"intercept-redirects\" option of the Profiler.
                </small>
            </p>
        </div>
    </div>
{% endblock %}
", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig", "/Users/zima/projekty/blogweb/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Profiler/toolbar_redirect.html.twig");
    }
}
