<?php

/* @Framework/Form/form_enctype.html.php */
class __TwigTemplate_3aa971a49210407fb4bab47e147f31b3eb62f706d7a6ea13d5f2915e6085e314 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d2cab9c7c7f56bdcbdd430993dc1df1d63044a826f22ab43967a82ac666c673c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d2cab9c7c7f56bdcbdd430993dc1df1d63044a826f22ab43967a82ac666c673c->enter($__internal_d2cab9c7c7f56bdcbdd430993dc1df1d63044a826f22ab43967a82ac666c673c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_enctype.html.php"));

        $__internal_0c4920b6df2be6c2afd113ae0253f0a785d2c5b98f64a94adb09b6be857cda9f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0c4920b6df2be6c2afd113ae0253f0a785d2c5b98f64a94adb09b6be857cda9f->enter($__internal_0c4920b6df2be6c2afd113ae0253f0a785d2c5b98f64a94adb09b6be857cda9f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_enctype.html.php"));

        // line 1
        echo "<?php if (\$form->vars['multipart']): ?>enctype=\"multipart/form-data\"<?php endif ?>
";
        
        $__internal_d2cab9c7c7f56bdcbdd430993dc1df1d63044a826f22ab43967a82ac666c673c->leave($__internal_d2cab9c7c7f56bdcbdd430993dc1df1d63044a826f22ab43967a82ac666c673c_prof);

        
        $__internal_0c4920b6df2be6c2afd113ae0253f0a785d2c5b98f64a94adb09b6be857cda9f->leave($__internal_0c4920b6df2be6c2afd113ae0253f0a785d2c5b98f64a94adb09b6be857cda9f_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_enctype.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (\$form->vars['multipart']): ?>enctype=\"multipart/form-data\"<?php endif ?>
", "@Framework/Form/form_enctype.html.php", "/Users/zima/projekty/blogweb/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_enctype.html.php");
    }
}
