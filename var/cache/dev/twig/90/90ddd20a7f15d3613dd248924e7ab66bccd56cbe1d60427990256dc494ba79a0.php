<?php

/* @Framework/FormTable/hidden_row.html.php */
class __TwigTemplate_23679e34170cba557010ea2c8f10ce203b261073b0704b53627af0ad24618322 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_cfda10dfa92ed97a4c7c5722589939ccfed1b4dd8bbc364d0783d6b833941ad9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_cfda10dfa92ed97a4c7c5722589939ccfed1b4dd8bbc364d0783d6b833941ad9->enter($__internal_cfda10dfa92ed97a4c7c5722589939ccfed1b4dd8bbc364d0783d6b833941ad9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/hidden_row.html.php"));

        $__internal_00c719b94bf2fb1dcda093b5f6f46febcedfdddb1fca0a1dbfaf53b7905b24c0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_00c719b94bf2fb1dcda093b5f6f46febcedfdddb1fca0a1dbfaf53b7905b24c0->enter($__internal_00c719b94bf2fb1dcda093b5f6f46febcedfdddb1fca0a1dbfaf53b7905b24c0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/hidden_row.html.php"));

        // line 1
        echo "<tr style=\"display: none\">
    <td colspan=\"2\">
        <?php echo \$view['form']->widget(\$form); ?>
    </td>
</tr>
";
        
        $__internal_cfda10dfa92ed97a4c7c5722589939ccfed1b4dd8bbc364d0783d6b833941ad9->leave($__internal_cfda10dfa92ed97a4c7c5722589939ccfed1b4dd8bbc364d0783d6b833941ad9_prof);

        
        $__internal_00c719b94bf2fb1dcda093b5f6f46febcedfdddb1fca0a1dbfaf53b7905b24c0->leave($__internal_00c719b94bf2fb1dcda093b5f6f46febcedfdddb1fca0a1dbfaf53b7905b24c0_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/hidden_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<tr style=\"display: none\">
    <td colspan=\"2\">
        <?php echo \$view['form']->widget(\$form); ?>
    </td>
</tr>
", "@Framework/FormTable/hidden_row.html.php", "/Users/zima/projekty/blogweb/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/FormTable/hidden_row.html.php");
    }
}
