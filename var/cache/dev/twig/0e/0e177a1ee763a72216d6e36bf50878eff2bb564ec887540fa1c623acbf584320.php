<?php

/* @Framework/Form/percent_widget.html.php */
class __TwigTemplate_b42763150a7b756a8c5146e2cbc413acad12231d9f1dd9325d8b3d53d13bf4d7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0bc7c4c931a4a75f8b5de4e34062d132a25d3996dd53ba295a06a1c2c8de93f1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0bc7c4c931a4a75f8b5de4e34062d132a25d3996dd53ba295a06a1c2c8de93f1->enter($__internal_0bc7c4c931a4a75f8b5de4e34062d132a25d3996dd53ba295a06a1c2c8de93f1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/percent_widget.html.php"));

        $__internal_ee840c6a7fcdd8306442f296460359633a59fad9dfad68d5ea020ddcd30bf70b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ee840c6a7fcdd8306442f296460359633a59fad9dfad68d5ea020ddcd30bf70b->enter($__internal_ee840c6a7fcdd8306442f296460359633a59fad9dfad68d5ea020ddcd30bf70b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/percent_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'text')) ?> %
";
        
        $__internal_0bc7c4c931a4a75f8b5de4e34062d132a25d3996dd53ba295a06a1c2c8de93f1->leave($__internal_0bc7c4c931a4a75f8b5de4e34062d132a25d3996dd53ba295a06a1c2c8de93f1_prof);

        
        $__internal_ee840c6a7fcdd8306442f296460359633a59fad9dfad68d5ea020ddcd30bf70b->leave($__internal_ee840c6a7fcdd8306442f296460359633a59fad9dfad68d5ea020ddcd30bf70b_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/percent_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'text')) ?> %
", "@Framework/Form/percent_widget.html.php", "/Users/zima/projekty/blogweb/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/percent_widget.html.php");
    }
}
