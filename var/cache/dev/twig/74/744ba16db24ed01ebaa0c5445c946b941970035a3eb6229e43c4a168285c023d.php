<?php

/* bootstrap_3_horizontal_layout.html.twig */
class __TwigTemplate_4d65df34c52df2a82d77d871d7410500d6503590f02fd11a1b193efa82662ca4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $_trait_0 = $this->loadTemplate("bootstrap_3_layout.html.twig", "bootstrap_3_horizontal_layout.html.twig", 1);
        // line 1
        if (!$_trait_0->isTraitable()) {
            throw new Twig_Error_Runtime('Template "'."bootstrap_3_layout.html.twig".'" cannot be used as a trait.');
        }
        $_trait_0_blocks = $_trait_0->getBlocks();

        $this->traits = $_trait_0_blocks;

        $this->blocks = array_merge(
            $this->traits,
            array(
                'form_start' => array($this, 'block_form_start'),
                'form_label' => array($this, 'block_form_label'),
                'form_label_class' => array($this, 'block_form_label_class'),
                'form_row' => array($this, 'block_form_row'),
                'checkbox_row' => array($this, 'block_checkbox_row'),
                'radio_row' => array($this, 'block_radio_row'),
                'checkbox_radio_row' => array($this, 'block_checkbox_radio_row'),
                'submit_row' => array($this, 'block_submit_row'),
                'reset_row' => array($this, 'block_reset_row'),
                'form_group_class' => array($this, 'block_form_group_class'),
            )
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8a7a8b06847251ec73e53f6ddbcff94934f4bf1f55425fab2036f7209c1347e7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8a7a8b06847251ec73e53f6ddbcff94934f4bf1f55425fab2036f7209c1347e7->enter($__internal_8a7a8b06847251ec73e53f6ddbcff94934f4bf1f55425fab2036f7209c1347e7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "bootstrap_3_horizontal_layout.html.twig"));

        $__internal_677c048f857fd8a4f9eb9df92d7c94f72390673ed3d628cfd759951223721463 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_677c048f857fd8a4f9eb9df92d7c94f72390673ed3d628cfd759951223721463->enter($__internal_677c048f857fd8a4f9eb9df92d7c94f72390673ed3d628cfd759951223721463_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "bootstrap_3_horizontal_layout.html.twig"));

        // line 2
        echo "
";
        // line 3
        $this->displayBlock('form_start', $context, $blocks);
        // line 7
        echo "
";
        // line 9
        echo "
";
        // line 10
        $this->displayBlock('form_label', $context, $blocks);
        // line 20
        echo "
";
        // line 21
        $this->displayBlock('form_label_class', $context, $blocks);
        // line 24
        echo "
";
        // line 26
        echo "
";
        // line 27
        $this->displayBlock('form_row', $context, $blocks);
        // line 36
        echo "
";
        // line 37
        $this->displayBlock('checkbox_row', $context, $blocks);
        // line 40
        echo "
";
        // line 41
        $this->displayBlock('radio_row', $context, $blocks);
        // line 44
        echo "
";
        // line 45
        $this->displayBlock('checkbox_radio_row', $context, $blocks);
        // line 56
        echo "
";
        // line 57
        $this->displayBlock('submit_row', $context, $blocks);
        // line 67
        echo "
";
        // line 68
        $this->displayBlock('reset_row', $context, $blocks);
        // line 78
        echo "
";
        // line 79
        $this->displayBlock('form_group_class', $context, $blocks);
        
        $__internal_8a7a8b06847251ec73e53f6ddbcff94934f4bf1f55425fab2036f7209c1347e7->leave($__internal_8a7a8b06847251ec73e53f6ddbcff94934f4bf1f55425fab2036f7209c1347e7_prof);

        
        $__internal_677c048f857fd8a4f9eb9df92d7c94f72390673ed3d628cfd759951223721463->leave($__internal_677c048f857fd8a4f9eb9df92d7c94f72390673ed3d628cfd759951223721463_prof);

    }

    // line 3
    public function block_form_start($context, array $blocks = array())
    {
        $__internal_fee051956cabb42fa3fcd5fcf62b21c9e1a43ac4fdb9c9406cb43fb2cf3eb05e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fee051956cabb42fa3fcd5fcf62b21c9e1a43ac4fdb9c9406cb43fb2cf3eb05e->enter($__internal_fee051956cabb42fa3fcd5fcf62b21c9e1a43ac4fdb9c9406cb43fb2cf3eb05e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_start"));

        $__internal_528d2dd1027f49a4bfe48195ec55c3791dfd8d59774a6bbcd5cb7e22cfc7c6ad = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_528d2dd1027f49a4bfe48195ec55c3791dfd8d59774a6bbcd5cb7e22cfc7c6ad->enter($__internal_528d2dd1027f49a4bfe48195ec55c3791dfd8d59774a6bbcd5cb7e22cfc7c6ad_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_start"));

        // line 4
        $context["attr"] = twig_array_merge(($context["attr"] ?? $this->getContext($context, "attr")), array("class" => twig_trim_filter(((($this->getAttribute(($context["attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["attr"] ?? null), "class", array()), "")) : ("")) . " form-horizontal"))));
        // line 5
        $this->displayParentBlock("form_start", $context, $blocks);
        
        $__internal_528d2dd1027f49a4bfe48195ec55c3791dfd8d59774a6bbcd5cb7e22cfc7c6ad->leave($__internal_528d2dd1027f49a4bfe48195ec55c3791dfd8d59774a6bbcd5cb7e22cfc7c6ad_prof);

        
        $__internal_fee051956cabb42fa3fcd5fcf62b21c9e1a43ac4fdb9c9406cb43fb2cf3eb05e->leave($__internal_fee051956cabb42fa3fcd5fcf62b21c9e1a43ac4fdb9c9406cb43fb2cf3eb05e_prof);

    }

    // line 10
    public function block_form_label($context, array $blocks = array())
    {
        $__internal_91bd914c4985166367b3a7192578cf071fd6525b2f4224ae8fa827979ff57272 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_91bd914c4985166367b3a7192578cf071fd6525b2f4224ae8fa827979ff57272->enter($__internal_91bd914c4985166367b3a7192578cf071fd6525b2f4224ae8fa827979ff57272_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_label"));

        $__internal_abb5bde9329d2c1ee0e4fef3e80bee1c93abff7f7043ab474d475b4e0a94a9ab = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_abb5bde9329d2c1ee0e4fef3e80bee1c93abff7f7043ab474d475b4e0a94a9ab->enter($__internal_abb5bde9329d2c1ee0e4fef3e80bee1c93abff7f7043ab474d475b4e0a94a9ab_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_label"));

        // line 11
        ob_start();
        // line 12
        echo "    ";
        if ((($context["label"] ?? $this->getContext($context, "label")) === false)) {
            // line 13
            echo "        <div class=\"";
            $this->displayBlock("form_label_class", $context, $blocks);
            echo "\"></div>
    ";
        } else {
            // line 15
            echo "        ";
            $context["label_attr"] = twig_array_merge(($context["label_attr"] ?? $this->getContext($context, "label_attr")), array("class" => twig_trim_filter((((($this->getAttribute(($context["label_attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["label_attr"] ?? null), "class", array()), "")) : ("")) . " ") .             $this->renderBlock("form_label_class", $context, $blocks)))));
            // line 16
            $this->displayParentBlock("form_label", $context, $blocks);
        }
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        
        $__internal_abb5bde9329d2c1ee0e4fef3e80bee1c93abff7f7043ab474d475b4e0a94a9ab->leave($__internal_abb5bde9329d2c1ee0e4fef3e80bee1c93abff7f7043ab474d475b4e0a94a9ab_prof);

        
        $__internal_91bd914c4985166367b3a7192578cf071fd6525b2f4224ae8fa827979ff57272->leave($__internal_91bd914c4985166367b3a7192578cf071fd6525b2f4224ae8fa827979ff57272_prof);

    }

    // line 21
    public function block_form_label_class($context, array $blocks = array())
    {
        $__internal_24941196ff6eade013a21d056acf86c741894bd8d1acded5c2bece7e3e061dac = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_24941196ff6eade013a21d056acf86c741894bd8d1acded5c2bece7e3e061dac->enter($__internal_24941196ff6eade013a21d056acf86c741894bd8d1acded5c2bece7e3e061dac_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_label_class"));

        $__internal_1e3559a69b3dcefe8d7a6f34baea62cc3a584cc26a5dc72bd3a000e1ee460944 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1e3559a69b3dcefe8d7a6f34baea62cc3a584cc26a5dc72bd3a000e1ee460944->enter($__internal_1e3559a69b3dcefe8d7a6f34baea62cc3a584cc26a5dc72bd3a000e1ee460944_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_label_class"));

        // line 22
        echo "col-sm-2";
        
        $__internal_1e3559a69b3dcefe8d7a6f34baea62cc3a584cc26a5dc72bd3a000e1ee460944->leave($__internal_1e3559a69b3dcefe8d7a6f34baea62cc3a584cc26a5dc72bd3a000e1ee460944_prof);

        
        $__internal_24941196ff6eade013a21d056acf86c741894bd8d1acded5c2bece7e3e061dac->leave($__internal_24941196ff6eade013a21d056acf86c741894bd8d1acded5c2bece7e3e061dac_prof);

    }

    // line 27
    public function block_form_row($context, array $blocks = array())
    {
        $__internal_95f2f08933fc440f19492c3f125d607759bfe476e5564900a83f2b37fb21f3ae = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_95f2f08933fc440f19492c3f125d607759bfe476e5564900a83f2b37fb21f3ae->enter($__internal_95f2f08933fc440f19492c3f125d607759bfe476e5564900a83f2b37fb21f3ae_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_row"));

        $__internal_f04af0261667bf9c99acff2c0193e49c6d1a071324af404c99827c51e28b2b87 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f04af0261667bf9c99acff2c0193e49c6d1a071324af404c99827c51e28b2b87->enter($__internal_f04af0261667bf9c99acff2c0193e49c6d1a071324af404c99827c51e28b2b87_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_row"));

        // line 28
        echo "<div class=\"form-group";
        if ((( !($context["compound"] ?? $this->getContext($context, "compound")) || ((array_key_exists("force_error", $context)) ? (_twig_default_filter(($context["force_error"] ?? $this->getContext($context, "force_error")), false)) : (false))) &&  !($context["valid"] ?? $this->getContext($context, "valid")))) {
            echo " has-error";
        }
        echo "\">";
        // line 29
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'label');
        // line 30
        echo "<div class=\"";
        $this->displayBlock("form_group_class", $context, $blocks);
        echo "\">";
        // line 31
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        // line 32
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'errors');
        // line 33
        echo "</div>
";
        // line 34
        echo "</div>";
        
        $__internal_f04af0261667bf9c99acff2c0193e49c6d1a071324af404c99827c51e28b2b87->leave($__internal_f04af0261667bf9c99acff2c0193e49c6d1a071324af404c99827c51e28b2b87_prof);

        
        $__internal_95f2f08933fc440f19492c3f125d607759bfe476e5564900a83f2b37fb21f3ae->leave($__internal_95f2f08933fc440f19492c3f125d607759bfe476e5564900a83f2b37fb21f3ae_prof);

    }

    // line 37
    public function block_checkbox_row($context, array $blocks = array())
    {
        $__internal_2bf5e516b65ea7fc2c570c9ff1f63fdbd35812dd4fbbb448da32ae07144e29a1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2bf5e516b65ea7fc2c570c9ff1f63fdbd35812dd4fbbb448da32ae07144e29a1->enter($__internal_2bf5e516b65ea7fc2c570c9ff1f63fdbd35812dd4fbbb448da32ae07144e29a1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_row"));

        $__internal_2875a1cc9f163770ab7ea6e48b5b1425a744203a7c7b620c70bf51bd4633813b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2875a1cc9f163770ab7ea6e48b5b1425a744203a7c7b620c70bf51bd4633813b->enter($__internal_2875a1cc9f163770ab7ea6e48b5b1425a744203a7c7b620c70bf51bd4633813b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_row"));

        // line 38
        $this->displayBlock("checkbox_radio_row", $context, $blocks);
        
        $__internal_2875a1cc9f163770ab7ea6e48b5b1425a744203a7c7b620c70bf51bd4633813b->leave($__internal_2875a1cc9f163770ab7ea6e48b5b1425a744203a7c7b620c70bf51bd4633813b_prof);

        
        $__internal_2bf5e516b65ea7fc2c570c9ff1f63fdbd35812dd4fbbb448da32ae07144e29a1->leave($__internal_2bf5e516b65ea7fc2c570c9ff1f63fdbd35812dd4fbbb448da32ae07144e29a1_prof);

    }

    // line 41
    public function block_radio_row($context, array $blocks = array())
    {
        $__internal_ba974c729073881ccf91160ca7a63a27aac1ff2244dc33493553632842e8ce4d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ba974c729073881ccf91160ca7a63a27aac1ff2244dc33493553632842e8ce4d->enter($__internal_ba974c729073881ccf91160ca7a63a27aac1ff2244dc33493553632842e8ce4d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "radio_row"));

        $__internal_4878b940b6975b1de10848edd8a156f3a6610497b9f0fb909eaa024ba743fbe7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4878b940b6975b1de10848edd8a156f3a6610497b9f0fb909eaa024ba743fbe7->enter($__internal_4878b940b6975b1de10848edd8a156f3a6610497b9f0fb909eaa024ba743fbe7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "radio_row"));

        // line 42
        $this->displayBlock("checkbox_radio_row", $context, $blocks);
        
        $__internal_4878b940b6975b1de10848edd8a156f3a6610497b9f0fb909eaa024ba743fbe7->leave($__internal_4878b940b6975b1de10848edd8a156f3a6610497b9f0fb909eaa024ba743fbe7_prof);

        
        $__internal_ba974c729073881ccf91160ca7a63a27aac1ff2244dc33493553632842e8ce4d->leave($__internal_ba974c729073881ccf91160ca7a63a27aac1ff2244dc33493553632842e8ce4d_prof);

    }

    // line 45
    public function block_checkbox_radio_row($context, array $blocks = array())
    {
        $__internal_73eb0c54947af454a2bee98a912d4386643f031019c2707cf248bca313a2cce7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_73eb0c54947af454a2bee98a912d4386643f031019c2707cf248bca313a2cce7->enter($__internal_73eb0c54947af454a2bee98a912d4386643f031019c2707cf248bca313a2cce7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_radio_row"));

        $__internal_5ef8a808c294064dc9f4028baa59b478fb1c36eb81efe19ff1c26838ce0317ff = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5ef8a808c294064dc9f4028baa59b478fb1c36eb81efe19ff1c26838ce0317ff->enter($__internal_5ef8a808c294064dc9f4028baa59b478fb1c36eb81efe19ff1c26838ce0317ff_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_radio_row"));

        // line 46
        ob_start();
        // line 47
        echo "    <div class=\"form-group";
        if ( !($context["valid"] ?? $this->getContext($context, "valid"))) {
            echo " has-error";
        }
        echo "\">
        <div class=\"";
        // line 48
        $this->displayBlock("form_label_class", $context, $blocks);
        echo "\"></div>
        <div class=\"";
        // line 49
        $this->displayBlock("form_group_class", $context, $blocks);
        echo "\">
            ";
        // line 50
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        echo "
            ";
        // line 51
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'errors');
        echo "
        </div>
    </div>
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        
        $__internal_5ef8a808c294064dc9f4028baa59b478fb1c36eb81efe19ff1c26838ce0317ff->leave($__internal_5ef8a808c294064dc9f4028baa59b478fb1c36eb81efe19ff1c26838ce0317ff_prof);

        
        $__internal_73eb0c54947af454a2bee98a912d4386643f031019c2707cf248bca313a2cce7->leave($__internal_73eb0c54947af454a2bee98a912d4386643f031019c2707cf248bca313a2cce7_prof);

    }

    // line 57
    public function block_submit_row($context, array $blocks = array())
    {
        $__internal_2f312b537df053bc623257474dd9d70d18f5b502a4413548cf5d9dbe23ac076e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2f312b537df053bc623257474dd9d70d18f5b502a4413548cf5d9dbe23ac076e->enter($__internal_2f312b537df053bc623257474dd9d70d18f5b502a4413548cf5d9dbe23ac076e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "submit_row"));

        $__internal_b19489cce64616b4de239e8b333aaaa2b42cda1b893b40a22b5094628db5ba3c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b19489cce64616b4de239e8b333aaaa2b42cda1b893b40a22b5094628db5ba3c->enter($__internal_b19489cce64616b4de239e8b333aaaa2b42cda1b893b40a22b5094628db5ba3c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "submit_row"));

        // line 58
        ob_start();
        // line 59
        echo "    <div class=\"form-group\">
        <div class=\"";
        // line 60
        $this->displayBlock("form_label_class", $context, $blocks);
        echo "\"></div>
        <div class=\"";
        // line 61
        $this->displayBlock("form_group_class", $context, $blocks);
        echo "\">
            ";
        // line 62
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        echo "
        </div>
    </div>
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        
        $__internal_b19489cce64616b4de239e8b333aaaa2b42cda1b893b40a22b5094628db5ba3c->leave($__internal_b19489cce64616b4de239e8b333aaaa2b42cda1b893b40a22b5094628db5ba3c_prof);

        
        $__internal_2f312b537df053bc623257474dd9d70d18f5b502a4413548cf5d9dbe23ac076e->leave($__internal_2f312b537df053bc623257474dd9d70d18f5b502a4413548cf5d9dbe23ac076e_prof);

    }

    // line 68
    public function block_reset_row($context, array $blocks = array())
    {
        $__internal_22ab3edcc47383dc6de6271d972f7498ffd769dab5df373c49f262c1ef018e39 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_22ab3edcc47383dc6de6271d972f7498ffd769dab5df373c49f262c1ef018e39->enter($__internal_22ab3edcc47383dc6de6271d972f7498ffd769dab5df373c49f262c1ef018e39_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "reset_row"));

        $__internal_0fb01ceebc6b8f065aa26a62d82546856b56d181893021e5831123adb25ad587 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0fb01ceebc6b8f065aa26a62d82546856b56d181893021e5831123adb25ad587->enter($__internal_0fb01ceebc6b8f065aa26a62d82546856b56d181893021e5831123adb25ad587_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "reset_row"));

        // line 69
        ob_start();
        // line 70
        echo "    <div class=\"form-group\">
        <div class=\"";
        // line 71
        $this->displayBlock("form_label_class", $context, $blocks);
        echo "\"></div>
        <div class=\"";
        // line 72
        $this->displayBlock("form_group_class", $context, $blocks);
        echo "\">
            ";
        // line 73
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        echo "
        </div>
    </div>
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        
        $__internal_0fb01ceebc6b8f065aa26a62d82546856b56d181893021e5831123adb25ad587->leave($__internal_0fb01ceebc6b8f065aa26a62d82546856b56d181893021e5831123adb25ad587_prof);

        
        $__internal_22ab3edcc47383dc6de6271d972f7498ffd769dab5df373c49f262c1ef018e39->leave($__internal_22ab3edcc47383dc6de6271d972f7498ffd769dab5df373c49f262c1ef018e39_prof);

    }

    // line 79
    public function block_form_group_class($context, array $blocks = array())
    {
        $__internal_d277d94dc3a513f11af1867eccffb69a42c8343902acef09b89888c4843ea95a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d277d94dc3a513f11af1867eccffb69a42c8343902acef09b89888c4843ea95a->enter($__internal_d277d94dc3a513f11af1867eccffb69a42c8343902acef09b89888c4843ea95a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_group_class"));

        $__internal_4fb9a40a5e4dcb632b60085e52044567705065a123568676ac7ab7548b1ff8de = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4fb9a40a5e4dcb632b60085e52044567705065a123568676ac7ab7548b1ff8de->enter($__internal_4fb9a40a5e4dcb632b60085e52044567705065a123568676ac7ab7548b1ff8de_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_group_class"));

        // line 80
        echo "col-sm-10";
        
        $__internal_4fb9a40a5e4dcb632b60085e52044567705065a123568676ac7ab7548b1ff8de->leave($__internal_4fb9a40a5e4dcb632b60085e52044567705065a123568676ac7ab7548b1ff8de_prof);

        
        $__internal_d277d94dc3a513f11af1867eccffb69a42c8343902acef09b89888c4843ea95a->leave($__internal_d277d94dc3a513f11af1867eccffb69a42c8343902acef09b89888c4843ea95a_prof);

    }

    public function getTemplateName()
    {
        return "bootstrap_3_horizontal_layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  390 => 80,  381 => 79,  366 => 73,  362 => 72,  358 => 71,  355 => 70,  353 => 69,  344 => 68,  329 => 62,  325 => 61,  321 => 60,  318 => 59,  316 => 58,  307 => 57,  292 => 51,  288 => 50,  284 => 49,  280 => 48,  273 => 47,  271 => 46,  262 => 45,  252 => 42,  243 => 41,  233 => 38,  224 => 37,  214 => 34,  211 => 33,  209 => 32,  207 => 31,  203 => 30,  201 => 29,  195 => 28,  186 => 27,  176 => 22,  167 => 21,  155 => 16,  152 => 15,  146 => 13,  143 => 12,  141 => 11,  132 => 10,  122 => 5,  120 => 4,  111 => 3,  101 => 79,  98 => 78,  96 => 68,  93 => 67,  91 => 57,  88 => 56,  86 => 45,  83 => 44,  81 => 41,  78 => 40,  76 => 37,  73 => 36,  71 => 27,  68 => 26,  65 => 24,  63 => 21,  60 => 20,  58 => 10,  55 => 9,  52 => 7,  50 => 3,  47 => 2,  14 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% use \"bootstrap_3_layout.html.twig\" %}

{% block form_start -%}
    {% set attr = attr|merge({class: (attr.class|default('') ~ ' form-horizontal')|trim}) %}
    {{- parent() -}}
{%- endblock form_start %}

{# Labels #}

{% block form_label -%}
{% spaceless %}
    {% if label is same as(false) %}
        <div class=\"{{ block('form_label_class') }}\"></div>
    {% else %}
        {% set label_attr = label_attr|merge({class: (label_attr.class|default('') ~ ' ' ~ block('form_label_class'))|trim}) %}
        {{- parent() -}}
    {% endif %}
{% endspaceless %}
{%- endblock form_label %}

{% block form_label_class -%}
col-sm-2
{%- endblock form_label_class %}

{# Rows #}

{% block form_row -%}
    <div class=\"form-group{% if (not compound or force_error|default(false)) and not valid %} has-error{% endif %}\">
        {{- form_label(form) -}}
        <div class=\"{{ block('form_group_class') }}\">
            {{- form_widget(form) -}}
            {{- form_errors(form) -}}
        </div>
{##}</div>
{%- endblock form_row %}

{% block checkbox_row -%}
    {{- block('checkbox_radio_row') -}}
{%- endblock checkbox_row %}

{% block radio_row -%}
    {{- block('checkbox_radio_row') -}}
{%- endblock radio_row %}

{% block checkbox_radio_row -%}
{% spaceless %}
    <div class=\"form-group{% if not valid %} has-error{% endif %}\">
        <div class=\"{{ block('form_label_class') }}\"></div>
        <div class=\"{{ block('form_group_class') }}\">
            {{ form_widget(form) }}
            {{ form_errors(form) }}
        </div>
    </div>
{% endspaceless %}
{%- endblock checkbox_radio_row %}

{% block submit_row -%}
{% spaceless %}
    <div class=\"form-group\">
        <div class=\"{{ block('form_label_class') }}\"></div>
        <div class=\"{{ block('form_group_class') }}\">
            {{ form_widget(form) }}
        </div>
    </div>
{% endspaceless %}
{% endblock submit_row %}

{% block reset_row -%}
{% spaceless %}
    <div class=\"form-group\">
        <div class=\"{{ block('form_label_class') }}\"></div>
        <div class=\"{{ block('form_group_class') }}\">
            {{ form_widget(form) }}
        </div>
    </div>
{% endspaceless %}
{% endblock reset_row %}

{% block form_group_class -%}
col-sm-10
{%- endblock form_group_class %}
", "bootstrap_3_horizontal_layout.html.twig", "/Users/zima/projekty/blogweb/vendor/symfony/symfony/src/Symfony/Bridge/Twig/Resources/views/Form/bootstrap_3_horizontal_layout.html.twig");
    }
}
