<?php

/* TwigBundle:Exception:exception.json.twig */
class __TwigTemplate_523ffe6492aa67b572c015f3c9c682d10677ea7f7e848304f87977e7b13d2c3d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4d7f39eacd466cd4ea51096f07cc7a88626fc40b589b6e6949441c4caaf735c8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4d7f39eacd466cd4ea51096f07cc7a88626fc40b589b6e6949441c4caaf735c8->enter($__internal_4d7f39eacd466cd4ea51096f07cc7a88626fc40b589b6e6949441c4caaf735c8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.json.twig"));

        $__internal_4a7a9b9cbbe8fe2d3185b9f19d96c5dc39385f9ab0a9841447de8d72b887d5a8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4a7a9b9cbbe8fe2d3185b9f19d96c5dc39385f9ab0a9841447de8d72b887d5a8->enter($__internal_4a7a9b9cbbe8fe2d3185b9f19d96c5dc39385f9ab0a9841447de8d72b887d5a8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.json.twig"));

        // line 1
        echo twig_jsonencode_filter(array("error" => array("code" => ($context["status_code"] ?? $this->getContext($context, "status_code")), "message" => ($context["status_text"] ?? $this->getContext($context, "status_text")), "exception" => $this->getAttribute(($context["exception"] ?? $this->getContext($context, "exception")), "toarray", array()))));
        echo "
";
        
        $__internal_4d7f39eacd466cd4ea51096f07cc7a88626fc40b589b6e6949441c4caaf735c8->leave($__internal_4d7f39eacd466cd4ea51096f07cc7a88626fc40b589b6e6949441c4caaf735c8_prof);

        
        $__internal_4a7a9b9cbbe8fe2d3185b9f19d96c5dc39385f9ab0a9841447de8d72b887d5a8->leave($__internal_4a7a9b9cbbe8fe2d3185b9f19d96c5dc39385f9ab0a9841447de8d72b887d5a8_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.json.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ { 'error': { 'code': status_code, 'message': status_text, 'exception': exception.toarray } }|json_encode|raw }}
", "TwigBundle:Exception:exception.json.twig", "/Users/zima/projekty/blogweb/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception.json.twig");
    }
}
