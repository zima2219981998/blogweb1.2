<?php

/* TwigBundle::layout.html.twig */
class __TwigTemplate_0b6fb2f229f1fe50a7199036eac5de108bac969410d123b2701f32f29a987086 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'head' => array($this, 'block_head'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_58fe1ab3a7335b9bdda8bc8c34e28bb208a750e5f14c3f3e5b1671c9e2294bda = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_58fe1ab3a7335b9bdda8bc8c34e28bb208a750e5f14c3f3e5b1671c9e2294bda->enter($__internal_58fe1ab3a7335b9bdda8bc8c34e28bb208a750e5f14c3f3e5b1671c9e2294bda_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle::layout.html.twig"));

        $__internal_c27f20310c95d9390dbbb2f15b4f7318f542462dbc292fac2ebb5aa631fa511c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c27f20310c95d9390dbbb2f15b4f7318f542462dbc292fac2ebb5aa631fa511c->enter($__internal_c27f20310c95d9390dbbb2f15b4f7318f542462dbc292fac2ebb5aa631fa511c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle::layout.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getCharset(), "html", null, true);
        echo "\" />
        <meta name=\"robots\" content=\"noindex,nofollow\" />
        <meta name=\"viewport\" content=\"width=device-width,initial-scale=1\" />
        <title>";
        // line 7
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        <link rel=\"icon\" type=\"image/png\" href=\"";
        // line 8
        echo twig_include($this->env, $context, "@Twig/images/favicon.png.base64");
        echo "\">
        <style>";
        // line 9
        echo twig_include($this->env, $context, "@Twig/exception.css.twig");
        echo "</style>
        ";
        // line 10
        $this->displayBlock('head', $context, $blocks);
        // line 11
        echo "    </head>
    <body>
        <header>
            <div class=\"container\">
                <h1 class=\"logo\">";
        // line 15
        echo twig_include($this->env, $context, "@Twig/images/symfony-logo.svg");
        echo " Symfony Exception</h1>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/doc\">
                        <span class=\"icon\">";
        // line 19
        echo twig_include($this->env, $context, "@Twig/images/icon-book.svg");
        echo "</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Docs
                    </a>
                </div>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/support\">
                        <span class=\"icon\">";
        // line 26
        echo twig_include($this->env, $context, "@Twig/images/icon-support.svg");
        echo "</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Support
                    </a>
                </div>
            </div>
        </header>

        ";
        // line 33
        $this->displayBlock('body', $context, $blocks);
        // line 34
        echo "        ";
        echo twig_include($this->env, $context, "@Twig/base_js.html.twig");
        echo "
    </body>
</html>
";
        
        $__internal_58fe1ab3a7335b9bdda8bc8c34e28bb208a750e5f14c3f3e5b1671c9e2294bda->leave($__internal_58fe1ab3a7335b9bdda8bc8c34e28bb208a750e5f14c3f3e5b1671c9e2294bda_prof);

        
        $__internal_c27f20310c95d9390dbbb2f15b4f7318f542462dbc292fac2ebb5aa631fa511c->leave($__internal_c27f20310c95d9390dbbb2f15b4f7318f542462dbc292fac2ebb5aa631fa511c_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_890bd3360fa274be54581c415b5bf4192283f47019ab6c1345dca21b52893763 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_890bd3360fa274be54581c415b5bf4192283f47019ab6c1345dca21b52893763->enter($__internal_890bd3360fa274be54581c415b5bf4192283f47019ab6c1345dca21b52893763_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_115f23588113a9d6407c28dd3b2c035e39970806c69d11cc0ff539bc0500b44b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_115f23588113a9d6407c28dd3b2c035e39970806c69d11cc0ff539bc0500b44b->enter($__internal_115f23588113a9d6407c28dd3b2c035e39970806c69d11cc0ff539bc0500b44b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        
        $__internal_115f23588113a9d6407c28dd3b2c035e39970806c69d11cc0ff539bc0500b44b->leave($__internal_115f23588113a9d6407c28dd3b2c035e39970806c69d11cc0ff539bc0500b44b_prof);

        
        $__internal_890bd3360fa274be54581c415b5bf4192283f47019ab6c1345dca21b52893763->leave($__internal_890bd3360fa274be54581c415b5bf4192283f47019ab6c1345dca21b52893763_prof);

    }

    // line 10
    public function block_head($context, array $blocks = array())
    {
        $__internal_efbc3446f31f04e72df13660804840679101396863e7e1dd4626555bba438106 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_efbc3446f31f04e72df13660804840679101396863e7e1dd4626555bba438106->enter($__internal_efbc3446f31f04e72df13660804840679101396863e7e1dd4626555bba438106_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_cf3545e05ab5eb13894653d68fa360d22ffedcd44a366e6539a45a79a792c7d4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cf3545e05ab5eb13894653d68fa360d22ffedcd44a366e6539a45a79a792c7d4->enter($__internal_cf3545e05ab5eb13894653d68fa360d22ffedcd44a366e6539a45a79a792c7d4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        
        $__internal_cf3545e05ab5eb13894653d68fa360d22ffedcd44a366e6539a45a79a792c7d4->leave($__internal_cf3545e05ab5eb13894653d68fa360d22ffedcd44a366e6539a45a79a792c7d4_prof);

        
        $__internal_efbc3446f31f04e72df13660804840679101396863e7e1dd4626555bba438106->leave($__internal_efbc3446f31f04e72df13660804840679101396863e7e1dd4626555bba438106_prof);

    }

    // line 33
    public function block_body($context, array $blocks = array())
    {
        $__internal_b83ce8099199d72c38198cbfe8b954c6c6852d298633ea02aa77a25589aa833a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b83ce8099199d72c38198cbfe8b954c6c6852d298633ea02aa77a25589aa833a->enter($__internal_b83ce8099199d72c38198cbfe8b954c6c6852d298633ea02aa77a25589aa833a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_7685c06c8933e111629fb3ef6d4893920f5b1f7a82366f195be7f31fe3eaa83f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7685c06c8933e111629fb3ef6d4893920f5b1f7a82366f195be7f31fe3eaa83f->enter($__internal_7685c06c8933e111629fb3ef6d4893920f5b1f7a82366f195be7f31fe3eaa83f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_7685c06c8933e111629fb3ef6d4893920f5b1f7a82366f195be7f31fe3eaa83f->leave($__internal_7685c06c8933e111629fb3ef6d4893920f5b1f7a82366f195be7f31fe3eaa83f_prof);

        
        $__internal_b83ce8099199d72c38198cbfe8b954c6c6852d298633ea02aa77a25589aa833a->leave($__internal_b83ce8099199d72c38198cbfe8b954c6c6852d298633ea02aa77a25589aa833a_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle::layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  137 => 33,  120 => 10,  103 => 7,  88 => 34,  86 => 33,  76 => 26,  66 => 19,  59 => 15,  53 => 11,  51 => 10,  47 => 9,  43 => 8,  39 => 7,  33 => 4,  28 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"{{ _charset }}\" />
        <meta name=\"robots\" content=\"noindex,nofollow\" />
        <meta name=\"viewport\" content=\"width=device-width,initial-scale=1\" />
        <title>{% block title %}{% endblock %}</title>
        <link rel=\"icon\" type=\"image/png\" href=\"{{ include('@Twig/images/favicon.png.base64') }}\">
        <style>{{ include('@Twig/exception.css.twig') }}</style>
        {% block head %}{% endblock %}
    </head>
    <body>
        <header>
            <div class=\"container\">
                <h1 class=\"logo\">{{ include('@Twig/images/symfony-logo.svg') }} Symfony Exception</h1>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/doc\">
                        <span class=\"icon\">{{ include('@Twig/images/icon-book.svg') }}</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Docs
                    </a>
                </div>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/support\">
                        <span class=\"icon\">{{ include('@Twig/images/icon-support.svg') }}</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Support
                    </a>
                </div>
            </div>
        </header>

        {% block body %}{% endblock %}
        {{ include('@Twig/base_js.html.twig') }}
    </body>
</html>
", "TwigBundle::layout.html.twig", "/Users/zima/projekty/blogweb/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/layout.html.twig");
    }
}
