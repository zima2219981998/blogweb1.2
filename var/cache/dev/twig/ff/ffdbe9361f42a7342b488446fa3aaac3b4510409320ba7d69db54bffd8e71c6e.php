<?php

/* ZimaBlogwebBundle:Blog:friendsContents.html.twig */
class __TwigTemplate_678478fba1e7a78509a920d2261b28f1713c94653a355b39492ff4de484bb629 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@ZimaBlogweb/base.html.twig", "ZimaBlogwebBundle:Blog:friendsContents.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@ZimaBlogweb/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_25eea6a576b81306cca174a30c7cf43d37d3556d462e8a8265f209b339f43596 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_25eea6a576b81306cca174a30c7cf43d37d3556d462e8a8265f209b339f43596->enter($__internal_25eea6a576b81306cca174a30c7cf43d37d3556d462e8a8265f209b339f43596_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "ZimaBlogwebBundle:Blog:friendsContents.html.twig"));

        $__internal_bd9626de06172a1363a9431a53091128f11cadcce9401219f1cbfaf5f5998d5c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bd9626de06172a1363a9431a53091128f11cadcce9401219f1cbfaf5f5998d5c->enter($__internal_bd9626de06172a1363a9431a53091128f11cadcce9401219f1cbfaf5f5998d5c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "ZimaBlogwebBundle:Blog:friendsContents.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_25eea6a576b81306cca174a30c7cf43d37d3556d462e8a8265f209b339f43596->leave($__internal_25eea6a576b81306cca174a30c7cf43d37d3556d462e8a8265f209b339f43596_prof);

        
        $__internal_bd9626de06172a1363a9431a53091128f11cadcce9401219f1cbfaf5f5998d5c->leave($__internal_bd9626de06172a1363a9431a53091128f11cadcce9401219f1cbfaf5f5998d5c_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_150edcef431b5fbc916a642ff9125d7efb9be1c2adf4d0b707eaf7f6e663e18e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_150edcef431b5fbc916a642ff9125d7efb9be1c2adf4d0b707eaf7f6e663e18e->enter($__internal_150edcef431b5fbc916a642ff9125d7efb9be1c2adf4d0b707eaf7f6e663e18e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_9c6d0b3d58bb88ed4fa23b039c675396c531316e903a49f42445c94b4a5abd6b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9c6d0b3d58bb88ed4fa23b039c675396c531316e903a49f42445c94b4a5abd6b->enter($__internal_9c6d0b3d58bb88ed4fa23b039c675396c531316e903a49f42445c94b4a5abd6b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "
    <div class=\"col-xs-12 col-sm-7 col-md-9\">

        <h2>coś zjebałem w PostRepository i nie działa</h2>
        <h2>{nie żeby wcześniej działało…}</h2>

        ";
        // line 10
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["selectFriendsPost"] ?? $this->getContext($context, "selectFriendsPost")));
        foreach ($context['_seq'] as $context["_key"] => $context["content"]) {
            // line 11
            echo "            <div class=\"well\">
                <center><p style=\"font-size: 25px;\">";
            // line 12
            echo twig_escape_filter($this->env, $this->getAttribute($context["content"], "title", array()), "html", null, true);
            echo "</p></center>
                <p>";
            // line 13
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["content"], "createdAt", array()), "Y-m-d H:i"), "html", null, true);
            echo "</p> <hr>
                <p><b>";
            // line 14
            echo twig_escape_filter($this->env, $this->getAttribute($context["content"], "shortdescription", array()), "html", null, true);
            echo "</b></p>
                <a href=\"";
            // line 15
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("blog_content", array("id" => $this->getAttribute($context["content"], "id", array()))), "html", null, true);
            echo "\">More…</a>
            </div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['content'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 18
        echo "        <div class=\"navigation text-center\">
            ";
        // line 19
        echo $this->env->getExtension('Knp\Bundle\PaginatorBundle\Twig\Extension\PaginationExtension')->render($this->env, ($context["rows"] ?? $this->getContext($context, "rows")));
        echo "
        </div>
    </div>

";
        
        $__internal_9c6d0b3d58bb88ed4fa23b039c675396c531316e903a49f42445c94b4a5abd6b->leave($__internal_9c6d0b3d58bb88ed4fa23b039c675396c531316e903a49f42445c94b4a5abd6b_prof);

        
        $__internal_150edcef431b5fbc916a642ff9125d7efb9be1c2adf4d0b707eaf7f6e663e18e->leave($__internal_150edcef431b5fbc916a642ff9125d7efb9be1c2adf4d0b707eaf7f6e663e18e_prof);

    }

    public function getTemplateName()
    {
        return "ZimaBlogwebBundle:Blog:friendsContents.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  88 => 19,  85 => 18,  76 => 15,  72 => 14,  68 => 13,  64 => 12,  61 => 11,  57 => 10,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@ZimaBlogweb/base.html.twig' %}

{% block body %}

    <div class=\"col-xs-12 col-sm-7 col-md-9\">

        <h2>coś zjebałem w PostRepository i nie działa</h2>
        <h2>{nie żeby wcześniej działało…}</h2>

        {% for content in selectFriendsPost %}
            <div class=\"well\">
                <center><p style=\"font-size: 25px;\">{{ content.title }}</p></center>
                <p>{{ content.createdAt|date('Y-m-d H:i') }}</p> <hr>
                <p><b>{{ content.shortdescription }}</b></p>
                <a href=\"{{ path('blog_content', {\"id\": content.id}) }}\">More…</a>
            </div>
        {% endfor %}
        <div class=\"navigation text-center\">
            {{ knp_pagination_render(rows) }}
        </div>
    </div>

{% endblock %}", "ZimaBlogwebBundle:Blog:friendsContents.html.twig", "/Users/zima/projekty/blogweb/src/Zima/BlogwebBundle/Resources/views/Blog/friendsContents.html.twig");
    }
}
