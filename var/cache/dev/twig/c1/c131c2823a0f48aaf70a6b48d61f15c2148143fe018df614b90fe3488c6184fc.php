<?php

/* FOSUserBundle:Profile:show_content.html.twig */
class __TwigTemplate_8742fa969b09a19ce240c2687de9c421595add4b9c3eb9087e7c90d0702edade extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_64d689b68b939a05089463c2ad84deb27a27c4c7deac9efd5754618bfee49059 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_64d689b68b939a05089463c2ad84deb27a27c4c7deac9efd5754618bfee49059->enter($__internal_64d689b68b939a05089463c2ad84deb27a27c4c7deac9efd5754618bfee49059_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Profile:show_content.html.twig"));

        $__internal_b0b1d619de8435f01cd4403d7549c538e3bd341cc63c5fa12f6936ff54baf609 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b0b1d619de8435f01cd4403d7549c538e3bd341cc63c5fa12f6936ff54baf609->enter($__internal_b0b1d619de8435f01cd4403d7549c538e3bd341cc63c5fa12f6936ff54baf609_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Profile:show_content.html.twig"));

        // line 2
        echo "
<div class=\"fos_user_user_show\">
    <p>";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("profile.show.username", array(), "FOSUserBundle"), "html", null, true);
        echo ": ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "username", array()), "html", null, true);
        echo "</p>
    <p>";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("profile.show.email", array(), "FOSUserBundle"), "html", null, true);
        echo ": ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "email", array()), "html", null, true);
        echo "</p>
</div>
";
        
        $__internal_64d689b68b939a05089463c2ad84deb27a27c4c7deac9efd5754618bfee49059->leave($__internal_64d689b68b939a05089463c2ad84deb27a27c4c7deac9efd5754618bfee49059_prof);

        
        $__internal_b0b1d619de8435f01cd4403d7549c538e3bd341cc63c5fa12f6936ff54baf609->leave($__internal_b0b1d619de8435f01cd4403d7549c538e3bd341cc63c5fa12f6936ff54baf609_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Profile:show_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  35 => 5,  29 => 4,  25 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% trans_default_domain 'FOSUserBundle' %}

<div class=\"fos_user_user_show\">
    <p>{{ 'profile.show.username'|trans }}: {{ user.username }}</p>
    <p>{{ 'profile.show.email'|trans }}: {{ user.email }}</p>
</div>
", "FOSUserBundle:Profile:show_content.html.twig", "/Users/zima/projekty/blogweb/app/Resources/FOSUserBundle/views/Profile/show_content.html.twig");
    }
}
