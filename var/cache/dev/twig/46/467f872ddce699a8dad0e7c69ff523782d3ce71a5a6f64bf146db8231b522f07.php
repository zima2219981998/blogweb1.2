<?php

/* FOSUserBundle:ChangePassword:change_password.html.twig */
class __TwigTemplate_bf7423f72979b7e980c8b23203bdc18e0874dfca89431e71827694cc25ddae7c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:ChangePassword:change_password.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_315dddbe6b936cdc6f7709937e96785407f0a9aaa7c8f79f3c9b39c6a1882e75 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_315dddbe6b936cdc6f7709937e96785407f0a9aaa7c8f79f3c9b39c6a1882e75->enter($__internal_315dddbe6b936cdc6f7709937e96785407f0a9aaa7c8f79f3c9b39c6a1882e75_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:ChangePassword:change_password.html.twig"));

        $__internal_28b8ed108c31107cab987481d36ae43dac88921a60d3d570a899f49ddcceaf78 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_28b8ed108c31107cab987481d36ae43dac88921a60d3d570a899f49ddcceaf78->enter($__internal_28b8ed108c31107cab987481d36ae43dac88921a60d3d570a899f49ddcceaf78_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:ChangePassword:change_password.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_315dddbe6b936cdc6f7709937e96785407f0a9aaa7c8f79f3c9b39c6a1882e75->leave($__internal_315dddbe6b936cdc6f7709937e96785407f0a9aaa7c8f79f3c9b39c6a1882e75_prof);

        
        $__internal_28b8ed108c31107cab987481d36ae43dac88921a60d3d570a899f49ddcceaf78->leave($__internal_28b8ed108c31107cab987481d36ae43dac88921a60d3d570a899f49ddcceaf78_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_6872646dfcaf04b96a1e755693630e763ea9ed0b4872c2f40bfcbbfae93a04d7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6872646dfcaf04b96a1e755693630e763ea9ed0b4872c2f40bfcbbfae93a04d7->enter($__internal_6872646dfcaf04b96a1e755693630e763ea9ed0b4872c2f40bfcbbfae93a04d7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_c8283682a45791e2a9b2cd332788deec0d217a28890ac75523fdc79037c2cdfb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c8283682a45791e2a9b2cd332788deec0d217a28890ac75523fdc79037c2cdfb->enter($__internal_c8283682a45791e2a9b2cd332788deec0d217a28890ac75523fdc79037c2cdfb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/ChangePassword/change_password_content.html.twig", "FOSUserBundle:ChangePassword:change_password.html.twig", 4)->display($context);
        
        $__internal_c8283682a45791e2a9b2cd332788deec0d217a28890ac75523fdc79037c2cdfb->leave($__internal_c8283682a45791e2a9b2cd332788deec0d217a28890ac75523fdc79037c2cdfb_prof);

        
        $__internal_6872646dfcaf04b96a1e755693630e763ea9ed0b4872c2f40bfcbbfae93a04d7->leave($__internal_6872646dfcaf04b96a1e755693630e763ea9ed0b4872c2f40bfcbbfae93a04d7_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:ChangePassword:change_password.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/ChangePassword/change_password_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:ChangePassword:change_password.html.twig", "/Users/zima/projekty/blogweb/app/Resources/FOSUserBundle/views/ChangePassword/change_password.html.twig");
    }
}
