<?php

/* @Framework/Form/choice_widget.html.php */
class __TwigTemplate_8723d9211aecd95685fc3777a2a5a8113cd9da2e70e87ad33a90f33de42f3172 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0e54f853e9b44b7a1ffad9b97d47a0c3c0ae877b2821facfddb8011d41f02605 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0e54f853e9b44b7a1ffad9b97d47a0c3c0ae877b2821facfddb8011d41f02605->enter($__internal_0e54f853e9b44b7a1ffad9b97d47a0c3c0ae877b2821facfddb8011d41f02605_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_widget.html.php"));

        $__internal_f074083dfb3682a1d5e0d072d7ab0ffe9a300b7618b65d18b5d4878ba4f19c55 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f074083dfb3682a1d5e0d072d7ab0ffe9a300b7618b65d18b5d4878ba4f19c55->enter($__internal_f074083dfb3682a1d5e0d072d7ab0ffe9a300b7618b65d18b5d4878ba4f19c55_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_widget.html.php"));

        // line 1
        echo "<?php if (\$expanded): ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_expanded') ?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_collapsed') ?>
<?php endif ?>
";
        
        $__internal_0e54f853e9b44b7a1ffad9b97d47a0c3c0ae877b2821facfddb8011d41f02605->leave($__internal_0e54f853e9b44b7a1ffad9b97d47a0c3c0ae877b2821facfddb8011d41f02605_prof);

        
        $__internal_f074083dfb3682a1d5e0d072d7ab0ffe9a300b7618b65d18b5d4878ba4f19c55->leave($__internal_f074083dfb3682a1d5e0d072d7ab0ffe9a300b7618b65d18b5d4878ba4f19c55_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choice_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (\$expanded): ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_expanded') ?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_collapsed') ?>
<?php endif ?>
", "@Framework/Form/choice_widget.html.php", "/Users/zima/projekty/blogweb/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/choice_widget.html.php");
    }
}
