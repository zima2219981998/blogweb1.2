<?php

/* WebProfilerBundle:Collector:ajax.html.twig */
class __TwigTemplate_cf671c196295822c33ca187c6a289fadc92db3d10cf977c284f660c16314c62d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "WebProfilerBundle:Collector:ajax.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_95e080a77dfe251ca4e4eb15072a0b4636d8f084f8b09008c2b0d0664081b72c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_95e080a77dfe251ca4e4eb15072a0b4636d8f084f8b09008c2b0d0664081b72c->enter($__internal_95e080a77dfe251ca4e4eb15072a0b4636d8f084f8b09008c2b0d0664081b72c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:ajax.html.twig"));

        $__internal_e50c57e2c4bc232044ce7c6941a1c3060f8fb81aa7dc460e2612023b45a0d380 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e50c57e2c4bc232044ce7c6941a1c3060f8fb81aa7dc460e2612023b45a0d380->enter($__internal_e50c57e2c4bc232044ce7c6941a1c3060f8fb81aa7dc460e2612023b45a0d380_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:ajax.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_95e080a77dfe251ca4e4eb15072a0b4636d8f084f8b09008c2b0d0664081b72c->leave($__internal_95e080a77dfe251ca4e4eb15072a0b4636d8f084f8b09008c2b0d0664081b72c_prof);

        
        $__internal_e50c57e2c4bc232044ce7c6941a1c3060f8fb81aa7dc460e2612023b45a0d380->leave($__internal_e50c57e2c4bc232044ce7c6941a1c3060f8fb81aa7dc460e2612023b45a0d380_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_31d7c2649c2360df209506f1a6ff055a0e665a0c1264430a1f788f2571eee07c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_31d7c2649c2360df209506f1a6ff055a0e665a0c1264430a1f788f2571eee07c->enter($__internal_31d7c2649c2360df209506f1a6ff055a0e665a0c1264430a1f788f2571eee07c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        $__internal_fcb3029eaaf85e02799105dbea93a15198d686799dbe6e6f83e78ce39ca8575b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fcb3029eaaf85e02799105dbea93a15198d686799dbe6e6f83e78ce39ca8575b->enter($__internal_fcb3029eaaf85e02799105dbea93a15198d686799dbe6e6f83e78ce39ca8575b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        // line 4
        echo "    ";
        ob_start();
        // line 5
        echo "        ";
        echo twig_include($this->env, $context, "@WebProfiler/Icon/ajax.svg");
        echo "
        <span class=\"sf-toolbar-value sf-toolbar-ajax-request-counter\">0</span>
    ";
        $context["icon"] = ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
        // line 8
        echo "
    ";
        // line 9
        $context["text"] = ('' === $tmp = "        <div class=\"sf-toolbar-info-piece\">
            <b class=\"sf-toolbar-ajax-info\"></b>
        </div>
        <div class=\"sf-toolbar-info-piece\">
            <table class=\"sf-toolbar-ajax-requests\">
                <thead>
                    <tr>
                        <th>Method</th>
                        <th>Type</th>
                        <th>Status</th>
                        <th>URL</th>
                        <th>Time</th>
                        <th>Profile</th>
                    </tr>
                </thead>
                <tbody class=\"sf-toolbar-ajax-request-list\"></tbody>
            </table>
        </div>
    ") ? '' : new Twig_Markup($tmp, $this->env->getCharset());
        // line 29
        echo "
    ";
        // line 30
        echo twig_include($this->env, $context, "@WebProfiler/Profiler/toolbar_item.html.twig", array("link" => false));
        echo "
";
        
        $__internal_fcb3029eaaf85e02799105dbea93a15198d686799dbe6e6f83e78ce39ca8575b->leave($__internal_fcb3029eaaf85e02799105dbea93a15198d686799dbe6e6f83e78ce39ca8575b_prof);

        
        $__internal_31d7c2649c2360df209506f1a6ff055a0e665a0c1264430a1f788f2571eee07c->leave($__internal_31d7c2649c2360df209506f1a6ff055a0e665a0c1264430a1f788f2571eee07c_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Collector:ajax.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  85 => 30,  82 => 29,  62 => 9,  59 => 8,  52 => 5,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}
    {% set icon %}
        {{ include('@WebProfiler/Icon/ajax.svg') }}
        <span class=\"sf-toolbar-value sf-toolbar-ajax-request-counter\">0</span>
    {% endset %}

    {% set text %}
        <div class=\"sf-toolbar-info-piece\">
            <b class=\"sf-toolbar-ajax-info\"></b>
        </div>
        <div class=\"sf-toolbar-info-piece\">
            <table class=\"sf-toolbar-ajax-requests\">
                <thead>
                    <tr>
                        <th>Method</th>
                        <th>Type</th>
                        <th>Status</th>
                        <th>URL</th>
                        <th>Time</th>
                        <th>Profile</th>
                    </tr>
                </thead>
                <tbody class=\"sf-toolbar-ajax-request-list\"></tbody>
            </table>
        </div>
    {% endset %}

    {{ include('@WebProfiler/Profiler/toolbar_item.html.twig', { link: false }) }}
{% endblock %}
", "WebProfilerBundle:Collector:ajax.html.twig", "/Users/zima/projekty/blogweb/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/ajax.html.twig");
    }
}
