<?php

/* FOSUserBundle:Group:list.html.twig */
class __TwigTemplate_3e11f80d9a79bcc9e396010dce618991b51dae3e1682fb116ff63759e1da3c43 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Group:list.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0a988553f04d1de2396fab21651789ac17bd1b728ba71644ab7bcfbe297cba99 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0a988553f04d1de2396fab21651789ac17bd1b728ba71644ab7bcfbe297cba99->enter($__internal_0a988553f04d1de2396fab21651789ac17bd1b728ba71644ab7bcfbe297cba99_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:list.html.twig"));

        $__internal_38552a98edce80ad1850caf2d2dcaf7c211056931e2e6d7f55341359585ff578 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_38552a98edce80ad1850caf2d2dcaf7c211056931e2e6d7f55341359585ff578->enter($__internal_38552a98edce80ad1850caf2d2dcaf7c211056931e2e6d7f55341359585ff578_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:list.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_0a988553f04d1de2396fab21651789ac17bd1b728ba71644ab7bcfbe297cba99->leave($__internal_0a988553f04d1de2396fab21651789ac17bd1b728ba71644ab7bcfbe297cba99_prof);

        
        $__internal_38552a98edce80ad1850caf2d2dcaf7c211056931e2e6d7f55341359585ff578->leave($__internal_38552a98edce80ad1850caf2d2dcaf7c211056931e2e6d7f55341359585ff578_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_4f572e03a642a118906d8b74c8cd202aab52c36be596c26fd022a925bc7c009a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4f572e03a642a118906d8b74c8cd202aab52c36be596c26fd022a925bc7c009a->enter($__internal_4f572e03a642a118906d8b74c8cd202aab52c36be596c26fd022a925bc7c009a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_9f480f19d2d37ff31fc6e3e99cc16f93ccf94dabe678126c72116c69b79ad912 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9f480f19d2d37ff31fc6e3e99cc16f93ccf94dabe678126c72116c69b79ad912->enter($__internal_9f480f19d2d37ff31fc6e3e99cc16f93ccf94dabe678126c72116c69b79ad912_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Group/list_content.html.twig", "FOSUserBundle:Group:list.html.twig", 4)->display($context);
        
        $__internal_9f480f19d2d37ff31fc6e3e99cc16f93ccf94dabe678126c72116c69b79ad912->leave($__internal_9f480f19d2d37ff31fc6e3e99cc16f93ccf94dabe678126c72116c69b79ad912_prof);

        
        $__internal_4f572e03a642a118906d8b74c8cd202aab52c36be596c26fd022a925bc7c009a->leave($__internal_4f572e03a642a118906d8b74c8cd202aab52c36be596c26fd022a925bc7c009a_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Group/list_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Group:list.html.twig", "/Users/zima/projekty/blogweb/vendor/friendsofsymfony/user-bundle/Resources/views/Group/list.html.twig");
    }
}
