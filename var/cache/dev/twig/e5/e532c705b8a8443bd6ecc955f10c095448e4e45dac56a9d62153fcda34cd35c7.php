<?php

/* WebProfilerBundle:Profiler:ajax_layout.html.twig */
class __TwigTemplate_9c31f532d106a65c3c3373ab5a1b36ce91ee4bfc67afe71756b6251dffaa5687 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d7d4419443aaecf746c99df38b0f732ad7f6e292230384d3e6c65db19bb0bc6b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d7d4419443aaecf746c99df38b0f732ad7f6e292230384d3e6c65db19bb0bc6b->enter($__internal_d7d4419443aaecf746c99df38b0f732ad7f6e292230384d3e6c65db19bb0bc6b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:ajax_layout.html.twig"));

        $__internal_d7f78b99570acf76d3468f1e38a88843514e393416810acf97933d6fe6271048 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d7f78b99570acf76d3468f1e38a88843514e393416810acf97933d6fe6271048->enter($__internal_d7f78b99570acf76d3468f1e38a88843514e393416810acf97933d6fe6271048_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:ajax_layout.html.twig"));

        // line 1
        $this->displayBlock('panel', $context, $blocks);
        
        $__internal_d7d4419443aaecf746c99df38b0f732ad7f6e292230384d3e6c65db19bb0bc6b->leave($__internal_d7d4419443aaecf746c99df38b0f732ad7f6e292230384d3e6c65db19bb0bc6b_prof);

        
        $__internal_d7f78b99570acf76d3468f1e38a88843514e393416810acf97933d6fe6271048->leave($__internal_d7f78b99570acf76d3468f1e38a88843514e393416810acf97933d6fe6271048_prof);

    }

    public function block_panel($context, array $blocks = array())
    {
        $__internal_0e41af7d1f4da45e3a3d927414eac900fc3ea51bf36dea9b68ae55dc8bdfd406 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0e41af7d1f4da45e3a3d927414eac900fc3ea51bf36dea9b68ae55dc8bdfd406->enter($__internal_0e41af7d1f4da45e3a3d927414eac900fc3ea51bf36dea9b68ae55dc8bdfd406_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_921b561e9670e689cc07212c8d0659dce7895b4a9220a481c55cfcfaa7c74d41 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_921b561e9670e689cc07212c8d0659dce7895b4a9220a481c55cfcfaa7c74d41->enter($__internal_921b561e9670e689cc07212c8d0659dce7895b4a9220a481c55cfcfaa7c74d41_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        echo "";
        
        $__internal_921b561e9670e689cc07212c8d0659dce7895b4a9220a481c55cfcfaa7c74d41->leave($__internal_921b561e9670e689cc07212c8d0659dce7895b4a9220a481c55cfcfaa7c74d41_prof);

        
        $__internal_0e41af7d1f4da45e3a3d927414eac900fc3ea51bf36dea9b68ae55dc8bdfd406->leave($__internal_0e41af7d1f4da45e3a3d927414eac900fc3ea51bf36dea9b68ae55dc8bdfd406_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:ajax_layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  26 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% block panel '' %}
", "WebProfilerBundle:Profiler:ajax_layout.html.twig", "/Users/zima/projekty/blogweb/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Profiler/ajax_layout.html.twig");
    }
}
