<?php

/* @Framework/Form/textarea_widget.html.php */
class __TwigTemplate_59af6ff31c705bdf4c6a37cf21aa3e24c3a6273ed87678317dfbc1434148ebe8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_051233a02dbd4236ed2abacffbb2797acf1da6d50ff9fbe042532333746b7707 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_051233a02dbd4236ed2abacffbb2797acf1da6d50ff9fbe042532333746b7707->enter($__internal_051233a02dbd4236ed2abacffbb2797acf1da6d50ff9fbe042532333746b7707_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/textarea_widget.html.php"));

        $__internal_1cee9b3313045c940937dfd7fd7672dfe11bcf61d0be7b262503f97f6c930d97 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1cee9b3313045c940937dfd7fd7672dfe11bcf61d0be7b262503f97f6c930d97->enter($__internal_1cee9b3313045c940937dfd7fd7672dfe11bcf61d0be7b262503f97f6c930d97_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/textarea_widget.html.php"));

        // line 1
        echo "<textarea <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>><?php echo \$view->escape(\$value) ?></textarea>
";
        
        $__internal_051233a02dbd4236ed2abacffbb2797acf1da6d50ff9fbe042532333746b7707->leave($__internal_051233a02dbd4236ed2abacffbb2797acf1da6d50ff9fbe042532333746b7707_prof);

        
        $__internal_1cee9b3313045c940937dfd7fd7672dfe11bcf61d0be7b262503f97f6c930d97->leave($__internal_1cee9b3313045c940937dfd7fd7672dfe11bcf61d0be7b262503f97f6c930d97_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/textarea_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<textarea <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>><?php echo \$view->escape(\$value) ?></textarea>
", "@Framework/Form/textarea_widget.html.php", "/Users/zima/projekty/blogweb/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/textarea_widget.html.php");
    }
}
