<?php

/* @Framework/Form/number_widget.html.php */
class __TwigTemplate_a44dc57e48bdf86151baed18d7c28562d1927725230e50d99eed05a6deef5ebf extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_164ed963ffad324b52f790303934f0e4961cb6fc92014ee38dd6b2b7990c2d34 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_164ed963ffad324b52f790303934f0e4961cb6fc92014ee38dd6b2b7990c2d34->enter($__internal_164ed963ffad324b52f790303934f0e4961cb6fc92014ee38dd6b2b7990c2d34_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/number_widget.html.php"));

        $__internal_f97fe80decb2945de229a2c6a28a6d9d67c695961e4a98875f48de985f8edd3e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f97fe80decb2945de229a2c6a28a6d9d67c695961e4a98875f48de985f8edd3e->enter($__internal_f97fe80decb2945de229a2c6a28a6d9d67c695961e4a98875f48de985f8edd3e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/number_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'text')) ?>
";
        
        $__internal_164ed963ffad324b52f790303934f0e4961cb6fc92014ee38dd6b2b7990c2d34->leave($__internal_164ed963ffad324b52f790303934f0e4961cb6fc92014ee38dd6b2b7990c2d34_prof);

        
        $__internal_f97fe80decb2945de229a2c6a28a6d9d67c695961e4a98875f48de985f8edd3e->leave($__internal_f97fe80decb2945de229a2c6a28a6d9d67c695961e4a98875f48de985f8edd3e_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/number_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'text')) ?>
", "@Framework/Form/number_widget.html.php", "/Users/zima/projekty/blogweb/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/number_widget.html.php");
    }
}
