<?php

/* @IvoryCKEditor/Form/ckeditor_javascript.html.php */
class __TwigTemplate_7480d08363ebb161caa852def2090c207d871b4e344f0ee15eb2c6a81c1262ca extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3a7c867eeebd46200940ca29c16a6903e7e51b2bdd7275fa654ace2b00099660 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3a7c867eeebd46200940ca29c16a6903e7e51b2bdd7275fa654ace2b00099660->enter($__internal_3a7c867eeebd46200940ca29c16a6903e7e51b2bdd7275fa654ace2b00099660_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@IvoryCKEditor/Form/ckeditor_javascript.html.php"));

        $__internal_251edab9e6ca98eb42f8a76032798ffafad351065977d0da3e6421413a282100 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_251edab9e6ca98eb42f8a76032798ffafad351065977d0da3e6421413a282100->enter($__internal_251edab9e6ca98eb42f8a76032798ffafad351065977d0da3e6421413a282100_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@IvoryCKEditor/Form/ckeditor_javascript.html.php"));

        // line 1
        echo "<?php if (\$enable && \$async) : ?>
    <?php include __DIR__.'/_ckeditor_javascript.html.php' ?>
<?php endif; ?>
";
        
        $__internal_3a7c867eeebd46200940ca29c16a6903e7e51b2bdd7275fa654ace2b00099660->leave($__internal_3a7c867eeebd46200940ca29c16a6903e7e51b2bdd7275fa654ace2b00099660_prof);

        
        $__internal_251edab9e6ca98eb42f8a76032798ffafad351065977d0da3e6421413a282100->leave($__internal_251edab9e6ca98eb42f8a76032798ffafad351065977d0da3e6421413a282100_prof);

    }

    public function getTemplateName()
    {
        return "@IvoryCKEditor/Form/ckeditor_javascript.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (\$enable && \$async) : ?>
    <?php include __DIR__.'/_ckeditor_javascript.html.php' ?>
<?php endif; ?>
", "@IvoryCKEditor/Form/ckeditor_javascript.html.php", "/Users/zima/projekty/blogweb/vendor/egeloen/ckeditor-bundle/Resources/views/Form/ckeditor_javascript.html.php");
    }
}
