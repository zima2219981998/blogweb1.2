<?php

/* @Twig/images/icon-plus-square.svg */
class __TwigTemplate_cbff4d6f6960f31adc054ff77370d021a8b2858561965b39eb2da71a20f52f0e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6bd3ea3ed65e08f55fc1fd5a5a0405137270fb250b0ffd40e43bb73777efd421 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6bd3ea3ed65e08f55fc1fd5a5a0405137270fb250b0ffd40e43bb73777efd421->enter($__internal_6bd3ea3ed65e08f55fc1fd5a5a0405137270fb250b0ffd40e43bb73777efd421_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/images/icon-plus-square.svg"));

        $__internal_a19f9c4b8aa3ce7d48c9b53e39d54ea5639fbb8c0e331d2ce7756f50ba7edcd5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a19f9c4b8aa3ce7d48c9b53e39d54ea5639fbb8c0e331d2ce7756f50ba7edcd5->enter($__internal_a19f9c4b8aa3ce7d48c9b53e39d54ea5639fbb8c0e331d2ce7756f50ba7edcd5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/images/icon-plus-square.svg"));

        // line 1
        echo "<svg width=\"1792\" height=\"1792\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\"><path d=\"M1408 960v-128q0-26-19-45t-45-19h-320v-320q0-26-19-45t-45-19h-128q-26 0-45 19t-19 45v320h-320q-26 0-45 19t-19 45v128q0 26 19 45t45 19h320v320q0 26 19 45t45 19h128q26 0 45-19t19-45v-320h320q26 0 45-19t19-45zm256-544v960q0 119-84.5 203.5t-203.5 84.5h-960q-119 0-203.5-84.5t-84.5-203.5v-960q0-119 84.5-203.5t203.5-84.5h960q119 0 203.5 84.5t84.5 203.5z\"/></svg>
";
        
        $__internal_6bd3ea3ed65e08f55fc1fd5a5a0405137270fb250b0ffd40e43bb73777efd421->leave($__internal_6bd3ea3ed65e08f55fc1fd5a5a0405137270fb250b0ffd40e43bb73777efd421_prof);

        
        $__internal_a19f9c4b8aa3ce7d48c9b53e39d54ea5639fbb8c0e331d2ce7756f50ba7edcd5->leave($__internal_a19f9c4b8aa3ce7d48c9b53e39d54ea5639fbb8c0e331d2ce7756f50ba7edcd5_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/images/icon-plus-square.svg";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<svg width=\"1792\" height=\"1792\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\"><path d=\"M1408 960v-128q0-26-19-45t-45-19h-320v-320q0-26-19-45t-45-19h-128q-26 0-45 19t-19 45v320h-320q-26 0-45 19t-19 45v128q0 26 19 45t45 19h320v320q0 26 19 45t45 19h128q26 0 45-19t19-45v-320h320q26 0 45-19t19-45zm256-544v960q0 119-84.5 203.5t-203.5 84.5h-960q-119 0-203.5-84.5t-84.5-203.5v-960q0-119 84.5-203.5t203.5-84.5h960q119 0 203.5 84.5t84.5 203.5z\"/></svg>
", "@Twig/images/icon-plus-square.svg", "/Users/zima/projekty/blogweb/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/images/icon-plus-square.svg");
    }
}
