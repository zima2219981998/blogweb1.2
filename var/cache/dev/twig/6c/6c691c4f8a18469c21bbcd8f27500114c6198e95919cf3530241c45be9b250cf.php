<?php

/* @Framework/Form/hidden_row.html.php */
class __TwigTemplate_cd1d64e3bb417ee293f8624d2e64121cb4843252b0e5aa439d5ca6124f78de0b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1840a02a2a42976760430d8635829fe71d0bcf8d8323081aa3b0fb722a4ac7ce = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1840a02a2a42976760430d8635829fe71d0bcf8d8323081aa3b0fb722a4ac7ce->enter($__internal_1840a02a2a42976760430d8635829fe71d0bcf8d8323081aa3b0fb722a4ac7ce_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_row.html.php"));

        $__internal_f344965f0c7c82aed6b7998f36082883eaed5084c5d4ad458d9830864a799eb7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f344965f0c7c82aed6b7998f36082883eaed5084c5d4ad458d9830864a799eb7->enter($__internal_f344965f0c7c82aed6b7998f36082883eaed5084c5d4ad458d9830864a799eb7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_row.html.php"));

        // line 1
        echo "<?php echo \$view['form']->widget(\$form) ?>
";
        
        $__internal_1840a02a2a42976760430d8635829fe71d0bcf8d8323081aa3b0fb722a4ac7ce->leave($__internal_1840a02a2a42976760430d8635829fe71d0bcf8d8323081aa3b0fb722a4ac7ce_prof);

        
        $__internal_f344965f0c7c82aed6b7998f36082883eaed5084c5d4ad458d9830864a799eb7->leave($__internal_f344965f0c7c82aed6b7998f36082883eaed5084c5d4ad458d9830864a799eb7_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/hidden_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->widget(\$form) ?>
", "@Framework/Form/hidden_row.html.php", "/Users/zima/projekty/blogweb/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/hidden_row.html.php");
    }
}
