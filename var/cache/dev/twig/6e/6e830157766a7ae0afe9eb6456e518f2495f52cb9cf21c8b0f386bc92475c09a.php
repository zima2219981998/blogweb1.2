<?php

/* @Framework/Form/form_rows.html.php */
class __TwigTemplate_6aa7a1993b66f2de0f81a87147626a32b0dca21e0db790d9630cd0fb2a98e4f3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d33f1f3a1f92058a1e4501a40b065fa88b24d3ca4cacff2eeaacbca3d813e9ce = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d33f1f3a1f92058a1e4501a40b065fa88b24d3ca4cacff2eeaacbca3d813e9ce->enter($__internal_d33f1f3a1f92058a1e4501a40b065fa88b24d3ca4cacff2eeaacbca3d813e9ce_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rows.html.php"));

        $__internal_51875b619836c40428eaac3dcbe1dfce9532e62b4a4342662baad25c9ffd3af5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_51875b619836c40428eaac3dcbe1dfce9532e62b4a4342662baad25c9ffd3af5->enter($__internal_51875b619836c40428eaac3dcbe1dfce9532e62b4a4342662baad25c9ffd3af5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rows.html.php"));

        // line 1
        echo "<?php foreach (\$form as \$child) : ?>
    <?php echo \$view['form']->row(\$child) ?>
<?php endforeach; ?>
";
        
        $__internal_d33f1f3a1f92058a1e4501a40b065fa88b24d3ca4cacff2eeaacbca3d813e9ce->leave($__internal_d33f1f3a1f92058a1e4501a40b065fa88b24d3ca4cacff2eeaacbca3d813e9ce_prof);

        
        $__internal_51875b619836c40428eaac3dcbe1dfce9532e62b4a4342662baad25c9ffd3af5->leave($__internal_51875b619836c40428eaac3dcbe1dfce9532e62b4a4342662baad25c9ffd3af5_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_rows.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php foreach (\$form as \$child) : ?>
    <?php echo \$view['form']->row(\$child) ?>
<?php endforeach; ?>
", "@Framework/Form/form_rows.html.php", "/Users/zima/projekty/blogweb/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_rows.html.php");
    }
}
