<?php

/* ZimaBlogwebBundle:Blog:settings.html.twig */
class __TwigTemplate_559735ae4d816e3a6bbb4e68da391f7941f60ba4bb5888ccbe1bb794aeea1832 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@ZimaBlogweb/base.html.twig", "ZimaBlogwebBundle:Blog:settings.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@ZimaBlogweb/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f5833fe74357b2e5333c621d3b111545e4cd268d5813bfa47db00c25e4c70b7b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f5833fe74357b2e5333c621d3b111545e4cd268d5813bfa47db00c25e4c70b7b->enter($__internal_f5833fe74357b2e5333c621d3b111545e4cd268d5813bfa47db00c25e4c70b7b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "ZimaBlogwebBundle:Blog:settings.html.twig"));

        $__internal_a82da4eda015a79997fb49f554972a0881d0c21c79bdac88a5c2effb1a6babd1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a82da4eda015a79997fb49f554972a0881d0c21c79bdac88a5c2effb1a6babd1->enter($__internal_a82da4eda015a79997fb49f554972a0881d0c21c79bdac88a5c2effb1a6babd1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "ZimaBlogwebBundle:Blog:settings.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_f5833fe74357b2e5333c621d3b111545e4cd268d5813bfa47db00c25e4c70b7b->leave($__internal_f5833fe74357b2e5333c621d3b111545e4cd268d5813bfa47db00c25e4c70b7b_prof);

        
        $__internal_a82da4eda015a79997fb49f554972a0881d0c21c79bdac88a5c2effb1a6babd1->leave($__internal_a82da4eda015a79997fb49f554972a0881d0c21c79bdac88a5c2effb1a6babd1_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_8a788523769d08701893cca503be96d672b38681396fa8f0eb8640636121859f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8a788523769d08701893cca503be96d672b38681396fa8f0eb8640636121859f->enter($__internal_8a788523769d08701893cca503be96d672b38681396fa8f0eb8640636121859f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_346a225e209b1d1bf5f852d593285a65f249621a91a66f84bb4d0e838928984e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_346a225e209b1d1bf5f852d593285a65f249621a91a66f84bb4d0e838928984e->enter($__internal_346a225e209b1d1bf5f852d593285a65f249621a91a66f84bb4d0e838928984e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "BlockWEB|Settings";
        
        $__internal_346a225e209b1d1bf5f852d593285a65f249621a91a66f84bb4d0e838928984e->leave($__internal_346a225e209b1d1bf5f852d593285a65f249621a91a66f84bb4d0e838928984e_prof);

        
        $__internal_8a788523769d08701893cca503be96d672b38681396fa8f0eb8640636121859f->leave($__internal_8a788523769d08701893cca503be96d672b38681396fa8f0eb8640636121859f_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_c857025233dd66cd7c08e9208ec59fadfb8764b8e1c1bcc268810d3832f88207 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c857025233dd66cd7c08e9208ec59fadfb8764b8e1c1bcc268810d3832f88207->enter($__internal_c857025233dd66cd7c08e9208ec59fadfb8764b8e1c1bcc268810d3832f88207_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_455e8c8f0b1d16955d2bbf4c324bb6144d96598910f1b925f5159b737c209255 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_455e8c8f0b1d16955d2bbf4c324bb6144d96598910f1b925f5159b737c209255->enter($__internal_455e8c8f0b1d16955d2bbf4c324bb6144d96598910f1b925f5159b737c209255_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    ";
        // line 7
        echo "    <div class=\"col-lg-6 col-md-6 col-xs-12\">
        <h2>Profile</h2> <hr>
        ";
        // line 9
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_start', array("attr" => array("novalidate" => "novalidate")));
        echo "
        <div class=\"form-group\">
            <label>Username</label> ";
        // line 11
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "username", array()), 'widget');
        echo "
        </div>
        <div class=\"form-group\">
            <label>Fullname</label> ";
        // line 14
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "fullname", array()), 'widget');
        echo "
        </div>
        <div class=\"form-group\">
            <label>Interests</label> ";
        // line 17
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "interests", array()), 'widget');
        echo "
        </div>
        <div class=\"form-group\">
            <label>About Me</label> ";
        // line 20
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "aboutme", array()), 'widget');
        echo "
        </div>
        <div class=\"form-group\">
            <label>Birthday</label> ";
        // line 23
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "birthday", array()), 'widget');
        echo "
        </div>
        <div class=\"form-group\">
            ";
        // line 26
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "submit", array()), 'widget', array("attr" => array("class" => "btn btn-primary")));
        echo "
        </div>
    </div>
    <div class=\"col-lg-6 col-md-6 col-xs-12\">
        <h2>Account</h2> <hr>
        <div class=\"form-group\">
            <label>Password</label> <a href=\"";
        // line 32
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_change_password", array("id" => $this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "user", array()), "id", array()))), "html", null, true);
        echo "\"><i>Change password</i></a>
        </div>
        <div class=\"form-group\">
            <a href=\"";
        // line 35
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("blog_delete_account", array("id" => $this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "user", array()), "id", array()))), "html", null, true);
        echo "\"><i>Delete an account</i></a>
        </div>
    </div>
    ";
        // line 38
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_end');
        echo " <br>




";
        
        $__internal_455e8c8f0b1d16955d2bbf4c324bb6144d96598910f1b925f5159b737c209255->leave($__internal_455e8c8f0b1d16955d2bbf4c324bb6144d96598910f1b925f5159b737c209255_prof);

        
        $__internal_c857025233dd66cd7c08e9208ec59fadfb8764b8e1c1bcc268810d3832f88207->leave($__internal_c857025233dd66cd7c08e9208ec59fadfb8764b8e1c1bcc268810d3832f88207_prof);

    }

    public function getTemplateName()
    {
        return "ZimaBlogwebBundle:Blog:settings.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  130 => 38,  124 => 35,  118 => 32,  109 => 26,  103 => 23,  97 => 20,  91 => 17,  85 => 14,  79 => 11,  74 => 9,  70 => 7,  68 => 6,  59 => 5,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@ZimaBlogweb/base.html.twig' %}

{% block title %}BlockWEB|Settings{% endblock %}

{% block body %}
    {#<center><h1>Settings</h1></center>#}
    <div class=\"col-lg-6 col-md-6 col-xs-12\">
        <h2>Profile</h2> <hr>
        {{ form_start(form, {\"attr\": {\"novalidate\": \"novalidate\"}}) }}
        <div class=\"form-group\">
            <label>Username</label> {{ form_widget(form.username) }}
        </div>
        <div class=\"form-group\">
            <label>Fullname</label> {{ form_widget(form.fullname) }}
        </div>
        <div class=\"form-group\">
            <label>Interests</label> {{ form_widget(form.interests) }}
        </div>
        <div class=\"form-group\">
            <label>About Me</label> {{ form_widget(form.aboutme) }}
        </div>
        <div class=\"form-group\">
            <label>Birthday</label> {{ form_widget(form.birthday) }}
        </div>
        <div class=\"form-group\">
            {{ form_widget(form.submit, {'attr': {'class': 'btn btn-primary'}}) }}
        </div>
    </div>
    <div class=\"col-lg-6 col-md-6 col-xs-12\">
        <h2>Account</h2> <hr>
        <div class=\"form-group\">
            <label>Password</label> <a href=\"{{ path('fos_user_change_password', {'id': app.user.id}) }}\"><i>Change password</i></a>
        </div>
        <div class=\"form-group\">
            <a href=\"{{ path('blog_delete_account', {'id': app.user.id}) }}\"><i>Delete an account</i></a>
        </div>
    </div>
    {{ form_end(form) }} <br>




{% endblock %}", "ZimaBlogwebBundle:Blog:settings.html.twig", "/Users/zima/projekty/blogweb/src/Zima/BlogwebBundle/Resources/views/Blog/settings.html.twig");
    }
}
