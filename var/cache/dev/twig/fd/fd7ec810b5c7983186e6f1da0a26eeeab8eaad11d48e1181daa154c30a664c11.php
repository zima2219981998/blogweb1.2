<?php

/* TwigBundle:Exception:exception.css.twig */
class __TwigTemplate_06ceda35cd8fb1bdf38f9e18746bbd40376132b1ddbdf4a430e2210b6e0cb12b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_83a486c38ff4f893b074e00d55390380dd310b4a0a8beb3e703fc150cdfc58fb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_83a486c38ff4f893b074e00d55390380dd310b4a0a8beb3e703fc150cdfc58fb->enter($__internal_83a486c38ff4f893b074e00d55390380dd310b4a0a8beb3e703fc150cdfc58fb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.css.twig"));

        $__internal_fe4dd7edb72a615603f7e6a88f3a25059f7b0a61fc3114a76a7f5456b93dfb87 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fe4dd7edb72a615603f7e6a88f3a25059f7b0a61fc3114a76a7f5456b93dfb87->enter($__internal_fe4dd7edb72a615603f7e6a88f3a25059f7b0a61fc3114a76a7f5456b93dfb87_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.css.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twig_include($this->env, $context, "@Twig/Exception/exception.txt.twig", array("exception" => ($context["exception"] ?? $this->getContext($context, "exception"))));
        echo "
*/
";
        
        $__internal_83a486c38ff4f893b074e00d55390380dd310b4a0a8beb3e703fc150cdfc58fb->leave($__internal_83a486c38ff4f893b074e00d55390380dd310b4a0a8beb3e703fc150cdfc58fb_prof);

        
        $__internal_fe4dd7edb72a615603f7e6a88f3a25059f7b0a61fc3114a76a7f5456b93dfb87->leave($__internal_fe4dd7edb72a615603f7e6a88f3a25059f7b0a61fc3114a76a7f5456b93dfb87_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.css.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 2,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("/*
{{ include('@Twig/Exception/exception.txt.twig', { exception: exception }) }}
*/
", "TwigBundle:Exception:exception.css.twig", "/Users/zima/projekty/blogweb/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception.css.twig");
    }
}
