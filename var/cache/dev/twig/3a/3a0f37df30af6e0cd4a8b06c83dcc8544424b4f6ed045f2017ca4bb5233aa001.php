<?php

/* ZimaBlogwebBundle:Blog:contentsUser.html.twig */
class __TwigTemplate_8a12a616b56639fe108bbd251eb5d9b6228a3910040431a6d99fce3ddd8f38a2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@ZimaBlogweb/base.html.twig", "ZimaBlogwebBundle:Blog:contentsUser.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@ZimaBlogweb/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_96142440f9ca4432a987bd4b998b3266d048617f118f0ccf7fc0566a04522800 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_96142440f9ca4432a987bd4b998b3266d048617f118f0ccf7fc0566a04522800->enter($__internal_96142440f9ca4432a987bd4b998b3266d048617f118f0ccf7fc0566a04522800_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "ZimaBlogwebBundle:Blog:contentsUser.html.twig"));

        $__internal_e60e9a2ac0f87e2a441b371a98dd758cca847ae445f33ba20a59c2b199810fba = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e60e9a2ac0f87e2a441b371a98dd758cca847ae445f33ba20a59c2b199810fba->enter($__internal_e60e9a2ac0f87e2a441b371a98dd758cca847ae445f33ba20a59c2b199810fba_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "ZimaBlogwebBundle:Blog:contentsUser.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_96142440f9ca4432a987bd4b998b3266d048617f118f0ccf7fc0566a04522800->leave($__internal_96142440f9ca4432a987bd4b998b3266d048617f118f0ccf7fc0566a04522800_prof);

        
        $__internal_e60e9a2ac0f87e2a441b371a98dd758cca847ae445f33ba20a59c2b199810fba->leave($__internal_e60e9a2ac0f87e2a441b371a98dd758cca847ae445f33ba20a59c2b199810fba_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_1b2b117f0e85276ae0100c29fd2ad80d760ebeeab8474982f33964bce509a3b7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1b2b117f0e85276ae0100c29fd2ad80d760ebeeab8474982f33964bce509a3b7->enter($__internal_1b2b117f0e85276ae0100c29fd2ad80d760ebeeab8474982f33964bce509a3b7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_29d0848cbd5f1520972e4a29f8d245f6f8239cec00929761fdbf143d668b8e6d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_29d0848cbd5f1520972e4a29f8d245f6f8239cec00929761fdbf143d668b8e6d->enter($__internal_29d0848cbd5f1520972e4a29f8d245f6f8239cec00929761fdbf143d668b8e6d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "
    <nav class=\"navbar navbar-default \">
        <div class=\"container-fluid\">
            <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-1\">
                <ul class=\"nav navbar-nav\">
                    <li class=\"active\"><a href=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("blog_edit", array("id" => $this->getAttribute(($context["post"] ?? $this->getContext($context, "post")), "id", array()))), "html", null, true);
        echo "\">Edit</a></li>
                    <li><a href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("blog_delete", array("id" => $this->getAttribute(($context["post"] ?? $this->getContext($context, "post")), "id", array()))), "html", null, true);
        echo "\">Delete</a></li>
                </ul>
            </div>
        </div>
    </nav>

    <div class=\"well\">
        <p style=\"font-size: 40px;\">";
        // line 17
        echo twig_escape_filter($this->env, $this->getAttribute(($context["post"] ?? $this->getContext($context, "post")), "title", array()), "html", null, true);
        echo "</p>
        <p>wrote: <i>";
        // line 18
        echo twig_escape_filter($this->env, $this->getAttribute(($context["post"] ?? $this->getContext($context, "post")), "owner", array()), "html", null, true);
        echo "</i> ";
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute(($context["post"] ?? $this->getContext($context, "post")), "createdAt", array()), "Y-m-d H:i"), "html", null, true);
        echo " </p> <hr>
        ";
        // line 19
        echo $this->getAttribute(($context["post"] ?? $this->getContext($context, "post")), "contents", array());
        echo "
    </div>

    <hr>
    <h3>Comments:</h3>

    ";
        // line 25
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["commentForm"] ?? $this->getContext($context, "commentForm")), 'form_start', array("attr" => array("novalidate" => "novalidate")));
        echo "
    <div class=\"form-group\">
        ";
        // line 27
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["commentForm"] ?? $this->getContext($context, "commentForm")), "comment", array()), 'widget', array("attr" => array("placeholder" => "Write a comment")));
        echo "
    </div>
    ";
        // line 29
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["commentForm"] ?? $this->getContext($context, "commentForm")), 'form_end');
        echo " <br>



    ";
        // line 34
        echo "    ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["selectcomments"] ?? $this->getContext($context, "selectcomments")));
        foreach ($context['_seq'] as $context["_key"] => $context["comment"]) {
            // line 35
            echo "        <div class=\"well well-sm\">
            <p><b>";
            // line 36
            echo twig_escape_filter($this->env, $this->getAttribute($context["comment"], "owner", array()), "html", null, true);
            echo "</b> (";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["comment"], "createdAt", array()), "Y-m-d H:i"), "html", null, true);
            echo ")</p>
            <p>";
            // line 37
            echo twig_escape_filter($this->env, $this->getAttribute($context["comment"], "comment", array()), "html", null, true);
            echo "</p>
            ";
            // line 39
            echo "        </div>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['comment'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 41
        echo "

";
        
        $__internal_29d0848cbd5f1520972e4a29f8d245f6f8239cec00929761fdbf143d668b8e6d->leave($__internal_29d0848cbd5f1520972e4a29f8d245f6f8239cec00929761fdbf143d668b8e6d_prof);

        
        $__internal_1b2b117f0e85276ae0100c29fd2ad80d760ebeeab8474982f33964bce509a3b7->leave($__internal_1b2b117f0e85276ae0100c29fd2ad80d760ebeeab8474982f33964bce509a3b7_prof);

    }

    public function getTemplateName()
    {
        return "ZimaBlogwebBundle:Blog:contentsUser.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  131 => 41,  124 => 39,  120 => 37,  114 => 36,  111 => 35,  106 => 34,  99 => 29,  94 => 27,  89 => 25,  80 => 19,  74 => 18,  70 => 17,  60 => 10,  56 => 9,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@ZimaBlogweb/base.html.twig' %}

{% block body %}

    <nav class=\"navbar navbar-default \">
        <div class=\"container-fluid\">
            <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-1\">
                <ul class=\"nav navbar-nav\">
                    <li class=\"active\"><a href=\"{{ path('blog_edit', {\"id\": post.id}) }}\">Edit</a></li>
                    <li><a href=\"{{ path('blog_delete', {\"id\": post.id}) }}\">Delete</a></li>
                </ul>
            </div>
        </div>
    </nav>

    <div class=\"well\">
        <p style=\"font-size: 40px;\">{{ post.title }}</p>
        <p>wrote: <i>{{ post.owner }}</i> {{ post.createdAt|date('Y-m-d H:i') }} </p> <hr>
        {{ post.contents|raw }}
    </div>

    <hr>
    <h3>Comments:</h3>

    {{ form_start(commentForm, {\"attr\": {\"novalidate\": \"novalidate\"}}) }}
    <div class=\"form-group\">
        {{ form_widget(commentForm.comment, {'attr': {'placeholder': 'Write a comment'}}) }}
    </div>
    {{ form_end(commentForm) }} <br>



    {#wyswietlenie komentarza#}
    {% for comment in selectcomments %}
        <div class=\"well well-sm\">
            <p><b>{{ comment.owner }}</b> ({{ comment.createdAt|date('Y-m-d H:i') }})</p>
            <p>{{ comment.comment }}</p>
            {#<a class=\"btn btn-danger\" href=\"{{ path('blog_del_comment', {\"id\": comment.id}) }}\">X</a>#}
        </div>
    {% endfor %}


{% endblock %}", "ZimaBlogwebBundle:Blog:contentsUser.html.twig", "/Users/zima/projekty/blogweb/src/Zima/BlogwebBundle/Resources/views/Blog/contentsUser.html.twig");
    }
}
