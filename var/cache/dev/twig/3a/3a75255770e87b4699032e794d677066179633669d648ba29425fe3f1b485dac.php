<?php

/* FOSUserBundle:Registration:register.html.twig */
class __TwigTemplate_596e3c1cb4d6d491588f8ccee1f21608412b4b447a65b3b7ef6aa8f0773db812 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Registration:register.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_66534c0aed3cf624ab4261c768b875615efb7e18df9c0436169beaa720f1c5d1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_66534c0aed3cf624ab4261c768b875615efb7e18df9c0436169beaa720f1c5d1->enter($__internal_66534c0aed3cf624ab4261c768b875615efb7e18df9c0436169beaa720f1c5d1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:register.html.twig"));

        $__internal_aefd9ad684945368039960dd99223791fc45cc0a34dfb04b3507cf22668be1ce = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_aefd9ad684945368039960dd99223791fc45cc0a34dfb04b3507cf22668be1ce->enter($__internal_aefd9ad684945368039960dd99223791fc45cc0a34dfb04b3507cf22668be1ce_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:register.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_66534c0aed3cf624ab4261c768b875615efb7e18df9c0436169beaa720f1c5d1->leave($__internal_66534c0aed3cf624ab4261c768b875615efb7e18df9c0436169beaa720f1c5d1_prof);

        
        $__internal_aefd9ad684945368039960dd99223791fc45cc0a34dfb04b3507cf22668be1ce->leave($__internal_aefd9ad684945368039960dd99223791fc45cc0a34dfb04b3507cf22668be1ce_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_d1a11f5393a9d0eee431f496f6c56b751cbad0f20dfa5a3fd45ef6896d4138f6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d1a11f5393a9d0eee431f496f6c56b751cbad0f20dfa5a3fd45ef6896d4138f6->enter($__internal_d1a11f5393a9d0eee431f496f6c56b751cbad0f20dfa5a3fd45ef6896d4138f6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_8344124952bce5823f50a54cc08a540ec37770fd60b62b5f5c2b5fe244fac273 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8344124952bce5823f50a54cc08a540ec37770fd60b62b5f5c2b5fe244fac273->enter($__internal_8344124952bce5823f50a54cc08a540ec37770fd60b62b5f5c2b5fe244fac273_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Registration/register_content.html.twig", "FOSUserBundle:Registration:register.html.twig", 4)->display($context);
        
        $__internal_8344124952bce5823f50a54cc08a540ec37770fd60b62b5f5c2b5fe244fac273->leave($__internal_8344124952bce5823f50a54cc08a540ec37770fd60b62b5f5c2b5fe244fac273_prof);

        
        $__internal_d1a11f5393a9d0eee431f496f6c56b751cbad0f20dfa5a3fd45ef6896d4138f6->leave($__internal_d1a11f5393a9d0eee431f496f6c56b751cbad0f20dfa5a3fd45ef6896d4138f6_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:register.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Registration/register_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Registration:register.html.twig", "/Users/zima/projekty/blogweb/app/Resources/FOSUserBundle/views/Registration/register.html.twig");
    }
}
